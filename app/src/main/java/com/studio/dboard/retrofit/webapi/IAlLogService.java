package com.studio.dboard.retrofit.webapi;


import com.studio.dboard.retrofit.bean.TokenRes;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by peak on 2017/11/23.
 */

public interface IAlLogService {

    /**
     * 版本更新升级
     * @return
     */
    @GET("/")
    Call<TokenRes> allogs();
}
