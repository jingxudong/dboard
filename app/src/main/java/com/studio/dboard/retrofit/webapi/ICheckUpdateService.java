package com.studio.dboard.retrofit.webapi;

import com.studio.dboard.retrofit.bean.Update;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by peak on 2017/11/23.
 */

public interface ICheckUpdateService {

    /**
     * 版本更新升级
     * @param code
     * @return
     */
    @FormUrlEncoded
    @POST("xlauncher/app/chkupgrade")
    Observable<Update> update(@Field("ver") String code, @Field("vn") String versionName);
}
