package com.studio.dboard.retrofit.webapi;


import com.studio.dboard.retrofit.bean.TwoCodeInfo;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by peak on 2017/11/23.
 */

public interface ITwoCodeService {

    /**
     * 获取用户激活二维码
     * @param uu
     * @return
     */
    @FormUrlEncoded
    @POST("wx/scurl")
    Observable<TwoCodeInfo> getTwoCode(@Field("uu") String uu, @Field("ver") String code, @Field("vn") String vername);
}
