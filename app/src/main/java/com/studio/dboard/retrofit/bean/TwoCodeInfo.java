package com.studio.dboard.retrofit.bean;

/**
 * Created by peak on 2017/11/28.
 */

public class TwoCodeInfo {

    private int result;
    private String err;
    private String scurl;

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public String getErr() {
        return err;
    }

    public void setErr(String err) {
        this.err = err;
    }

    public String getScurl() {
        return scurl;
    }

    public void setScurl(String scurl) {
        this.scurl = scurl;
    }
}
