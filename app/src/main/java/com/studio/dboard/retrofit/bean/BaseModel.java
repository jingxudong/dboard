package com.studio.dboard.retrofit.bean;

/**
 * Created by mateng on 2017/6/5.
 */

public class BaseModel {
    private int result;
    private ErrBean err;

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public ErrBean getErr() {
        return err;
    }

    public void setErr(ErrBean err) {
        this.err = err;
    }

    @Override
    public String toString() {
        return "BaseModel{" +
                "result=" + result +
                ", err=" + err +
                '}';
    }
}
