package com.studio.dboard.retrofit;



import com.studio.dboard.Constant;
import com.studio.dboard.retrofit.log.HttpLoggingInterceptor;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.internal.cache.CacheInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

public class ServiceGenerator {

    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
            .connectTimeout(5, TimeUnit.SECONDS)
            //设置请求日志拦截器
            .addInterceptor(new HttpLoggingInterceptor()
                    .setLevel(HttpLoggingInterceptor.Level.BODY))
            //设置重试拦截器
            .addInterceptor(new RetryIntercepter(3));

    private static Retrofit.Builder builder1 =
            new Retrofit.Builder()
                    .baseUrl(Constant.SERVER_URL)
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create());

    private static Retrofit.Builder builder2 =
            new Retrofit.Builder()
                    .baseUrl(Constant.SERVER_URL)
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .addConverterFactory(SimpleXmlConverterFactory.create());

    private static Retrofit.Builder builder3 =
            new Retrofit.Builder()
                    .baseUrl(Constant.LOG_SERVER)
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create());

    public static <T> T createService(Class<T> serviceClass) {
        Retrofit retrofit = builder1.client(httpClient.build()).build();
        return retrofit.create(serviceClass);
    }

    public static <T> T createXmlService(Class<T> serviceClass) {
        Retrofit retrofit = builder2.client(httpClient.build()).build();
        return retrofit.create(serviceClass);
    }

    public static <T> T createLogService(Class<T> serviceClass) {
        Retrofit retrofit = builder3.client(httpClient.build()).build();
        return retrofit.create(serviceClass);
    }
}