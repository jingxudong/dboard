package com.studio.dboard.retrofit;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class RetryIntercepter implements Interceptor {

    public int maxRetry;//最大重试次数
    private int retryNum = 0;//假如设置为3次重试的话，则最大可能请求4次（默认1次+3次重试）

    public RetryIntercepter(int maxRetry) {
        this.maxRetry = maxRetry;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Response response = chain.proceed(request);
        while (!response.isSuccessful() && retryNum < maxRetry) {
            retryNum++;
            response = chain.proceed(request);
        }
//        if (response.isSuccessful()){ //设置缓存有效期
//            CacheControl.Builder builder = new CacheControl.Builder()
//                    .maxAge(7, TimeUnit.DAYS);
//            return response.newBuilder().header("Cache-Control", builder.build().toString())
//                    .build();
//        }
        return response;
    }
}
