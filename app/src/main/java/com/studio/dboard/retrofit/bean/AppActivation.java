package com.studio.dboard.retrofit.bean;

/**
 * Created by mateng on 2017/3/29.
 */

public class AppActivation extends BaseModel {


    /**
     * result : 0
     * err : {"code":-2,"info":"1.data参数为空；2.data不能转为object对象，参数有误"}
     * key : A8WCYdsvOQ35SckUzK09FTRGuQeZw+cWPTsFVhJ1nlFeSpfLAGulAaUuf5efkWgea7pzjl4x0EKifbWUqyZdYsoNNZPCveaQHyXgBAtmA3RJvixkU0G0LHEO0BVbqKOHAINg8qSr4xsGdtmj7Cjee1mtrrEC5wnQErpyLbheEDU=
     * publickey : MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCjGAh11cHJh/WaPVynuG0SwJraf8XRHyM00qr2fQ41hmJEUZ4V4qTP8zQ5Fx9z8jO7Xst2KxLTXzE2sQC2MGiYWte/s/iWKrpPEqbazM+atT2aUmz5tYBdFLknFhGt+Eoml+t8viM8MKwqrnc1y8yxWMXUqsXJOxdAucQM+3jaRwIDAQAB
     */

    private String key;
    private String publickey;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getPublickey() {
        return publickey;
    }

    public void setPublickey(String publickey) {
        this.publickey = publickey;
    }

    @Override
    public String toString() {
        return "AppActivation{" +
                "key='" + key + '\'' +
                ", publickey='" + publickey + '\'' +
                ", " + super.toString() + '}';
    }
}
