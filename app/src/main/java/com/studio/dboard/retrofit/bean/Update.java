package com.studio.dboard.retrofit.bean;

/**
 * Created by peak on 2017/11/23.
 */

public class Update extends BaseModel {

    private int nvc; //最新版本code
    private String nv; //最新版本号
    private String desc; //最新版本说明
    private String pn; //APK包名
    private String fname; //APP文件名
    private int fsize; //app文件大小
    private String furl; //APP下载地址
    private int updtype; //更新类型 0：无更新  1：有更新  2：强制更新

    public int getNvc() {
        return nvc;
    }

    public void setNvc(int nvc) {
        this.nvc = nvc;
    }

    public String getNv() {
        return nv;
    }

    public void setNv(String nv) {
        this.nv = nv;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getPn() {
        return pn;
    }

    public void setPn(String pn) {
        this.pn = pn;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public int getFsize() {
        return fsize;
    }

    public void setFsize(int fsize) {
        this.fsize = fsize;
    }

    public String getFurl() {
        return furl;
    }

    public void setFurl(String furl) {
        this.furl = furl;
    }

    public int getUpdtype() {
        return updtype;
    }

    public void setUpdtype(int updtype) {
        this.updtype = updtype;
    }

    @Override
    public String toString() {
        return "Update{" +
                "nvc=" + nvc +
                ", nv='" + nv + '\'' +
                ", desc='" + desc + '\'' +
                ", pn='" + pn + '\'' +
                ", fname='" + fname + '\'' +
                ", fsize=" + fsize +
                ", furl='" + furl + '\'' +
                ", updtype=" + updtype +
                '}';
    }
}
