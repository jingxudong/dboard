package com.studio.dboard.retrofit.webapi;


import com.studio.dboard.retrofit.bean.AppActivation;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import rx.Observable;


/**
 * Created by mateng on 2017/4/25.
 */

public interface IActivationService {

    /**
     * 激活
     *
     * @param data
     * @return
     */
    @FormUrlEncoded
    @POST("launcherActivation")
    Observable<AppActivation> activation(@Field("data") String data, @Field("ver") String code,
                                         @Field("vn") String vername);

}
