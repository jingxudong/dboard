package com.studio.dboard.entity;

import com.studio.dboard.R;

public class Board {


    /**
     * 多彩仪表
     */
    public static final int ID_BOARD_1 = 1;
    /**
     * 幻影动力
     */
    public static final int ID_BOARD_2 = 2;
    /**
     * 蓝色引擎
     */
    public static final int ID_BOARD_3 = 3;
    /**
     * 时空隧道
     */
    public static final int ID_BOARD_4 = 4;
    /**
     * 黑曜石钟
     */
    public static final int ID_BOARD_5 = 5;
    /**
     * 速度风暴
     */
    public static final int ID_BOARD_6 = 6;
    /**
     * 彩虹时刻
     */
    public static final int ID_BOARD_7 = 7;
    /**
     * 奢华名表
     */
    public static final int ID_BOARD_8 = 8;
    /**
     * 动态天气
     */
    public static final int ID_BOARD_9 = 9;
    /**
     * 自定义
     */
    public static final int ID_BOARD_10 = 10;


    public static final String[] titles = new String[]{
            "多彩仪表",
            "幻影动力",
            "蓝色引擎",
            "时空隧道",
            "黑曜时钟",
            "速度风暴",
            "彩虹时刻",
            "奢华名表",
            "动态天气",
            "自定义"
    };

    private static int[] ids = new int[]{
            ID_BOARD_1,
            ID_BOARD_2,
            ID_BOARD_3,
            ID_BOARD_4,
            ID_BOARD_5,
            ID_BOARD_6,
            ID_BOARD_7,
            ID_BOARD_8,
            ID_BOARD_9,
            ID_BOARD_10
    };

    public static final int[] resIds = new int[]{
            R.drawable.dashbroad_thumb_01,
            R.drawable.dashbroad_thumb_02,
            R.drawable.dashbroad_thumb_03,
            R.drawable.dashbroad_thumb_04,
            R.drawable.dashbroad_thumb_05,
            R.drawable.dashbroad_thumb_06,
            R.drawable.dashbroad_thumb_07,
            R.drawable.dashbroad_thumb_08,
            R.drawable.dashbroad_thumb_09,
            R.drawable.dashbroad_thumb_10
    };

    public static int getId(int position){
        return ids[position];
    }

    public static int getPosition(int dreamId){
        for (int i = 0; i < ids.length; i++) {
            int id = ids[i];
            if (id == dreamId){
                return i;
            }
        }
        return 0;
    }
}
