package com.studio.dboard.entity;

import java.util.ArrayList;

/**
 * Created by peak on 2017/11/29.
 */

public class TimeImage {

    private int begin;
    private int end;
    private ArrayList<String> images;

    public int getBegin() {
        return begin;
    }

    public void setBegin(int begin) {
        this.begin = begin;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }
}
