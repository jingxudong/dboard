package com.studio.dboard.entity;

import android.content.Context;

import com.studio.dboard.allog.LogUtils;
import com.studio.dboard.utils.SystemUtils;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Transient;

@Entity
public class Config {

    @Transient
    public static final int DEFAULT_SPEED_OFFSET = 70;
    @Transient
    public static final int DEFAULT_SPEED_MAX_DELT = 190; //-70 ～ 120

    @Id(autoincrement = false)
    private Long id = 0L;
    private String uu;//用户身份标示
    private String productId = "";//产品标示
    private String token = ""; //阿里log token
    private String activeKey = "";//激活信息
    private String sign = "";//车机唯一标示（IMEI/MAC/ANDROID_ID）
    private Integer speedOffset = DEFAULT_SPEED_OFFSET;//速度偏移量调整(记录的是位置)
    private Integer boardId = Board.ID_BOARD_1; // 被设置的仪表盘ID
    private Integer previewBoardId = Board.ID_BOARD_1;//正在预览的仪表盘ID
    private Integer skipVerCode = 0; //版本升级要跳过的版本
    private Boolean enableFlowBubble = true; //是否启动小圆球
    private Boolean enableGpsThrough = true; //是否允许导航信息透传
    private String qrCode = "";//二维码字符串
    private String applySign = "";//申请恢复激活的授权标示
    private String screenSaveImgPath = "";//自定义屏保图片地址
    private String screenSaveOriginImgPath = "";//自定义屏保原始图片地址
    private Boolean isSpeedMock = false;//速度模拟
    private Integer dashStartTime = -1; //仪表弹出时间（默认关闭）
    private Boolean isFirstStart = true; //是否第一次启动
    private Boolean isProcessStartBefore = false; //自己的进程是否已经启动
    private Boolean isDisTips = false; //是否显示过tip教程提示
    private Boolean isFullScreen = true; //是否全屏显示


    @Generated(hash = 588649278)
    public Config(Long id, String uu, String productId, String token,
            String activeKey, String sign, Integer speedOffset, Integer boardId,
            Integer previewBoardId, Integer skipVerCode, Boolean enableFlowBubble,
            Boolean enableGpsThrough, String qrCode, String applySign,
            String screenSaveImgPath, String screenSaveOriginImgPath,
            Boolean isSpeedMock, Integer dashStartTime, Boolean isFirstStart,
            Boolean isProcessStartBefore, Boolean isDisTips, Boolean isFullScreen) {
        this.id = id;
        this.uu = uu;
        this.productId = productId;
        this.token = token;
        this.activeKey = activeKey;
        this.sign = sign;
        this.speedOffset = speedOffset;
        this.boardId = boardId;
        this.previewBoardId = previewBoardId;
        this.skipVerCode = skipVerCode;
        this.enableFlowBubble = enableFlowBubble;
        this.enableGpsThrough = enableGpsThrough;
        this.qrCode = qrCode;
        this.applySign = applySign;
        this.screenSaveImgPath = screenSaveImgPath;
        this.screenSaveOriginImgPath = screenSaveOriginImgPath;
        this.isSpeedMock = isSpeedMock;
        this.dashStartTime = dashStartTime;
        this.isFirstStart = isFirstStart;
        this.isProcessStartBefore = isProcessStartBefore;
        this.isDisTips = isDisTips;
        this.isFullScreen = isFullScreen;
    }


    @Generated(hash = 589037648)
    public Config() {
    }


    public void initValue(Context context){
        if (id == null){
            id = 0L;
        }
        if (uu == null){
            uu = SystemUtils.getUu();
        }
        if (productId == null){
            productId = SystemUtils.getProductId(context);
        }
        if (token == null){
            token = "";
        }
        if (activeKey == null){
            activeKey = "";
        }
        if (sign == null){
            sign = "";
        }
        if (speedOffset == null){
            speedOffset = DEFAULT_SPEED_OFFSET;
        }
        if (boardId == null){
            boardId = Board.ID_BOARD_1;
        }
        if (previewBoardId == null){
            previewBoardId = Board.ID_BOARD_1;
        }
        if (skipVerCode == null){
            skipVerCode = 0;
        }
        if (enableFlowBubble == null){
            enableFlowBubble = true;
        }
        if (enableGpsThrough == null){
            enableGpsThrough = true;
        }
        if (qrCode == null){
            qrCode = "";
        }
        if (applySign == null){
            applySign = "";
        }
        if (screenSaveImgPath == null){
            screenSaveImgPath = "";
        }
        if (screenSaveOriginImgPath == null){
            screenSaveOriginImgPath = "";
        }
        if (isSpeedMock == null){
            isSpeedMock = false;
        }
        if (dashStartTime == null){
            dashStartTime = -1;
        }
        if (isFirstStart == null){
            isFirstStart = true;
        }
        if (isProcessStartBefore == null){
            isProcessStartBefore = false;
        }
        if (isDisTips == null){
            isDisTips = false;
        }
        if (isFullScreen == null){
            isFullScreen = true;
        }
    }
    

    public String getToken() {
        return this.token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getActiveKey() {
        return this.activeKey;
    }

    public void setActiveKey(String activeKey) {
        this.activeKey = activeKey;
    }

    public String getSign() {
        return this.sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getSpeedOffset() {
        return this.speedOffset;
    }

    public void setSpeedOffset(Integer speedOffset) {
        this.speedOffset = speedOffset;
    }

    public Integer getBoardId() {
        return this.boardId;
    }

    public void setBoardId(Integer boardId) {
        this.boardId = boardId;
    }

    public Integer getSkipVerCode() {
        return this.skipVerCode;
    }

    public void setSkipVerCode(Integer skipVerCode) {
        this.skipVerCode = skipVerCode;
    }

    public Boolean getEnableFlowBubble() {
        return this.enableFlowBubble;
    }

    public void setEnableFlowBubble(Boolean enableFlowBubble) {
        this.enableFlowBubble = enableFlowBubble;
    }

    public Boolean getEnableGpsThrough() {
        return this.enableGpsThrough;
    }

    public void setEnableGpsThrough(Boolean enableGpsThrough) {
        this.enableGpsThrough = enableGpsThrough;
    }

    public String getQrCode() {
        return this.qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public String getUu() {
        return this.uu;
    }

    public void setUu(String uu) {
        this.uu = uu;
    }

    public String getApplySign() {
        return this.applySign;
    }

    public void setApplySign(String applySign) {
        this.applySign = applySign;
    }

    public String getScreenSaveImgPath() {
        return this.screenSaveImgPath;
    }

    public void setScreenSaveImgPath(String screenSaveImgPath) {
        this.screenSaveImgPath = screenSaveImgPath;
    }

    public String getScreenSaveOriginImgPath() {
        return this.screenSaveOriginImgPath;
    }

    public void setScreenSaveOriginImgPath(String screenSaveOriginImgPath) {
        this.screenSaveOriginImgPath = screenSaveOriginImgPath;
    }

    public Boolean getIsSpeedMock() {
        return this.isSpeedMock;
    }

    public void setIsSpeedMock(Boolean isSpeedMock) {
        this.isSpeedMock = isSpeedMock;
    }

    public Integer getPreviewBoardId() {
        return this.previewBoardId;
    }

    public void setPreviewBoardId(Integer previewBoardId) {
        this.previewBoardId = previewBoardId;
    }

    public Integer getDashStartTime() {
        return this.dashStartTime;
    }

    public void setDashStartTime(Integer dashStartTime) {
        this.dashStartTime = dashStartTime;
    }

    public Boolean getIsFirstStart() {
        return this.isFirstStart;
    }

    public void setIsFirstStart(Boolean isFirstStart) {
        this.isFirstStart = isFirstStart;
    }

    public Boolean getIsProcessStartBefore() {
        return this.isProcessStartBefore;
    }

    public void setIsProcessStartBefore(Boolean isProcessStartBefore) {
        this.isProcessStartBefore = isProcessStartBefore;
    }

    public String getProductId() {
        return this.productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Boolean getIsDisTips() {
        return this.isDisTips;
    }

    public void setIsDisTips(Boolean isDisTips) {
        this.isDisTips = isDisTips;
    }

    public Boolean getIsFullScreen() {
        return this.isFullScreen;
    }

    public void setIsFullScreen(Boolean isFullScreen) {
        this.isFullScreen = isFullScreen;
    }

}
