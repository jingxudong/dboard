package com.studio.dboard.entity;

import java.util.ArrayList;

/**
 * Created by peak on 2017/11/29.
 */

public class Weather {

    private int id;
    private String name;
    private ArrayList<TimeImage> timeImages;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<TimeImage> getTimeImages() {
        return timeImages;
    }

    public void setTimeImages(ArrayList<TimeImage> timeImages) {
        this.timeImages = timeImages;
    }
}
