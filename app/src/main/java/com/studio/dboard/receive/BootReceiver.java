package com.studio.dboard.receive;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.studio.dboard.BoardService;
import com.studio.dboard.allog.LogUtils;


public class BootReceiver extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action.equals(Intent.ACTION_BOOT_COMPLETED)){
            Intent serviceIntent = new Intent(context, BoardService.class);
            context.startService(serviceIntent);
        }
    }


}
