package com.studio.dboard.allog;

import android.content.Context;
import android.os.Environment;
import android.text.TextUtils;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.aliyun.sls.android.sdk.ClientConfiguration;
import com.aliyun.sls.android.sdk.LOGClient;
import com.aliyun.sls.android.sdk.LogEntity;
import com.aliyun.sls.android.sdk.LogException;
import com.aliyun.sls.android.sdk.SLSDatabaseManager;
import com.aliyun.sls.android.sdk.core.auth.StsTokenCredentialProvider;
import com.aliyun.sls.android.sdk.core.callback.CompletedCallback;
import com.aliyun.sls.android.sdk.model.Log;
import com.aliyun.sls.android.sdk.model.LogGroup;
import com.aliyun.sls.android.sdk.request.PostLogRequest;
import com.aliyun.sls.android.sdk.result.PostLogResult;
import com.studio.dboard.Constant;
import com.studio.dboard.dao.GreenDaoManager;
import com.studio.dboard.entity.Config;
import com.studio.dboard.retrofit.ServiceGenerator;
import com.studio.dboard.retrofit.bean.TokenRes;
import com.studio.dboard.retrofit.webapi.IAlLogService;
import com.studio.dboard.utils.DateUtils;
import com.studio.dboard.utils.NetworkUtils;
import com.studio.dboard.utils.SystemUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AlLogHelper {

    private static final String tag = "AlLogHelper";

    public static class Type {
        public static final String TOPIC_ALL = "全部日志";
        public static final String TOPIC_ERROR = "崩溃日志";
        public static final String TOPIC_NORMAL = "普通日志";
        public static final String TOPIC_NORMAL_SPEED = "速度日志";
        public static final String DEVICE_PROPERTY = "设备参数";
        public static final String ERROR_MSG = "异常详细信息";
        public static final String LOG_MSG = "普通日志详细信息";
        public static final String TAG = "TAG";
        public static final String TAG_CLASS = "class";
        public static final String TAG_METHOD = "method";
        public static final String TAG_VERSION = "版本";
    }

    public static class LogStore{
        public static final int LOGSTORE_DEBUG = 0; //用于问题调试
        public static final int LOGSTORE_EXCEPTION = 1; //用于存储崩溃日志
        public static final int LOGSTORE_3 = 2;
        public static final int LOGSTORE_4 = 3;
        public static final int LOGSTORE_5 = 4;
    }

    private static final String[] logStores = new String[]{
            "internal-consumergroup_log",
            "internal-logtail_alarm",
            "internal-logtail_profile",
            "internal-metering",
            "internal-operation_log"
    };

    private static final String endpoint = "http://cn-beijing.log.aliyuncs.com";
    private static final String project = "dboard";



    private Context mContext;
    private Config config;
    private LogGroup logGroup;
    private LOGClient logClient;
    private String logStore = logStores[0];

    private static AlLogHelper instance = null;

    private CompletedCallback<PostLogRequest, PostLogResult> callback;

    public void setCallback(CompletedCallback<PostLogRequest, PostLogResult> callback) {
        this.callback = callback;
    }

    private AlLogHelper(Context context) {
        this.mContext = context;
        config = GreenDaoManager.getInstance(mContext).getSession()
                .getConfigDao().load(0L);
    }

    public static AlLogHelper getInstance(Context context){
        if (instance == null){
            instance = new AlLogHelper(context);
        }
        return instance;
    }

    /**
     * check token
     */
    private void checkToken() {
        String token = config.getToken();
        if (TextUtils.isEmpty(token)){
            requestToken();
        }else {
            TokenRes tokenRes = JSONObject.parseObject(token, TokenRes.class);
            String expiration = tokenRes.getExpiration();
            long timeLong = DateUtils.stampToLocal8Long(expiration);
            if (System.currentTimeMillis() < timeLong){
                if (logClient == null){
                    setupSLSClient(tokenRes);
                }
                uploadNow();
            }else {
                requestToken();
            }
        }
    }

    /**
     * 请求日志token
     */
    private void requestToken(){
        ServiceGenerator.createLogService(IAlLogService.class)
                .allogs()
                .enqueue(new Callback<TokenRes>() {
                    @Override
                    public void onResponse(Call<TokenRes> call, Response<TokenRes> response) {
                        TokenRes tokenRes = response.body();
                        String jsonToken = JSONObject.toJSONString(tokenRes);
                        config.setToken(jsonToken);
                        GreenDaoManager.getInstance(mContext).getSession().getConfigDao().update(config);
                        setupSLSClient(tokenRes);
                        uploadNow();
                    }

                    @Override
                    public void onFailure(Call<TokenRes> call, Throwable t) {
                        if (callback != null){
                            callback.onFailure(null, null);
                        }
                    }
                });
    }

    /**
     * 初始化 logClient
     * @param tokenRes
     */
    private void setupSLSClient(TokenRes tokenRes){
        StsTokenCredentialProvider credentialProvider =
                new StsTokenCredentialProvider(tokenRes.getAccessKeyId(),
                        tokenRes.getAccessKeySecret(), tokenRes.getSecurityToken());


        ClientConfiguration conf = new ClientConfiguration();
        conf.setConnectionTimeout(15 * 1000); // 连接超时，默认15秒
        conf.setSocketTimeout(15 * 1000); // socket超时，默认15秒
        conf.setMaxConcurrentRequest(5); // 最大并发请求书，默认5个
        conf.setMaxErrorRetry(2); // 失败后最大重试次数，默认2次
        conf.setCachable(true);
        conf.setConnectType(ClientConfiguration.NetworkPolicy.WWAN_OR_WIFI);
//        SLSLog.enableLog(); // log打印在控制台
        logClient = new LOGClient(mContext, endpoint, credentialProvider, conf);
    }

    /**
     * 立即上传
     */
    private void uploadNow(){
        try {
            PostLogRequest request = new PostLogRequest(project, logStore, logGroup);
            if (logClient != null){
                logClient.asyncPostLog(request, callback);
            }
        } catch (LogException e) {
            e.printStackTrace();
        }
    }

    public void uploadLogs(LogGroup logGroup, int logStoreIndex){
        this.logGroup = logGroup;
        this.logStore = logStores[logStoreIndex];
        checkToken();
    }

    public void uploadLogs(LogGroup logGroup, String logStore){
        this.logGroup = logGroup;
        this.logStore = logStore;
        checkToken();
    }

    public void insertLogToDb(LogGroup logGroup, int logStoreIndex){
        LogEntity entity = new LogEntity();
        entity.setEndPoint(endpoint);
        entity.setStore(logStores[logStoreIndex]);
        entity.setProject(project);
        entity.setJsonString(logGroup.LogGroupToJsonString());
        entity.setTimestamp(System.currentTimeMillis());
        SLSDatabaseManager.getInstance().insertRecordIntoDB(entity);
    }

    public void uploadLogsOfDb(){
        new Thread(){
            @Override
            public void run() {
                List<LogEntity> list = SLSDatabaseManager.getInstance().queryRecordFromDB();
                for (LogEntity logEntity : list){
                    String jsonStr = logEntity.getJsonString();
                    LogGroup logGroup = jsonToLogGroup(jsonStr);
                    uploadLogs(logGroup, logEntity.getStore());
                    SLSDatabaseManager.getInstance().deleteRecordFromDB(logEntity);
                }

            }
        }.start();
    }

    public void uploadDiskCachedLog(CompletedCallback<PostLogRequest, PostLogResult> callback){
        this.callback = callback;
        final File file = new File(Environment.getExternalStorageDirectory()
                .getAbsolutePath() + File.separator + Constant.ROOT_DIR + "log/normal/cached.log");
        if (!file.exists()){
            Toast.makeText(mContext, "没找到日志文件！", Toast.LENGTH_LONG).show();
            return;
        }
        new Thread(){
            static final int EVERAGE_LENGTH = 500 * 1024;//500K
            @Override
            public void run() {
                BufferedReader br = null;
                FileInputStream fis = null;
                try {
                    fis = new FileInputStream(file);
                    br = new BufferedReader(new InputStreamReader(fis));
                    String line = null;
                    StringBuilder builder = new StringBuilder();

                    while ((line = br.readLine()) != null){
                        builder.append(line + "\n");
                        if (builder.length() >= EVERAGE_LENGTH){ //每读取500K左右上传一次
                            LogGroup logGroup = new LogGroup(Type.TOPIC_NORMAL, NetworkUtils.getIpAddress(mContext));
                            Log log = new Log();
                            log.PutContent(Type.TAG_VERSION, SystemUtils.getVersion(mContext));
                            log.PutContent(Type.LOG_MSG, builder.toString());
                            logGroup.PutLog(log);
                            uploadLogs(logGroup, LogStore.LOGSTORE_DEBUG);
                            builder.delete(0, builder.length() - 1);
                            builder.setLength(0);
                        }
                    }

                    if (builder.capacity() > 0) {
                        LogGroup logGroup = new LogGroup(Type.TOPIC_NORMAL, NetworkUtils.getIpAddress(mContext));
                        Log log = new Log();
                        log.PutContent(Type.TAG_VERSION, SystemUtils.getVersion(mContext));
                        log.PutContent(Type.LOG_MSG, builder.toString());
                        logGroup.PutLog(log);
                        uploadLogs(logGroup, LogStore.LOGSTORE_DEBUG);
                        builder.delete(0, builder.length() - 1);
                        builder.setLength(0);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }finally {
                    try {
                        if (br != null){
                            br.close();
                        }
                        if (fis != null){
                            fis.close();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        }.start();
    }

    public LogGroup jsonToLogGroup(String jsonStr){
        LogGroup logGroup = new LogGroup();
        JSONObject jsonObject = JSON.parseObject(jsonStr);
        String topic = (String) jsonObject.get("__topic__");
        if (topic != null){
            logGroup.PutTopic(topic);
        }
        String source = (String) jsonObject.get("__source__");
        if (source != null){
            logGroup.PutSource(source);
        }

        JSONArray logJsonArray = jsonObject.getJSONArray("__logs__");
        if (logJsonArray != null){
            for (int i = 0; i < logJsonArray.size(); i++){
                JSONObject logJSONObject = logJsonArray.getJSONObject(i);
                Set keySet = logJSONObject.keySet();
                Iterator it = keySet.iterator();
                Log log = new Log();
                while (it.hasNext()){
                    String key = (String) it.next();
                    if (key.equals("__time__")){
                        int time = (int) logJSONObject.get(key);
                        log.PutTime(time);
                    }else {
                        String value = (String) logJSONObject.get(key);
                        log.PutContent(key, value);
                    }
                }
                logGroup.PutLog(log);
            }
        }
        return logGroup;
    }
}
