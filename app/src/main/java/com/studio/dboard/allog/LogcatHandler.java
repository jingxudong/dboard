package com.studio.dboard.allog;

import android.content.Context;
import android.os.Environment;

import com.studio.dboard.Constant;
import com.studio.dboard.utils.DateUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class LogcatHandler {

    private static final String tag = "LogcatHandler";
    private static LogcatHandler INSTANCE = null;
    private static String PATH_LOGCAT;
    private LogDumper mLogDumper = null;  
    private int mPId;
    private String[] tags = new String[]{
            "BoardService",
            "Render",
            "BoardApp"
    };
    private Context mContext;
  
    /**  
     *  
     * 初始化目录  
     *  
     * */  
    public void init(Context context) {
        mContext = context;
        if (Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {// 优先保存到SD卡中
            PATH_LOGCAT = Environment.getExternalStorageDirectory()
                    .getAbsolutePath() + File.separator + Constant.ROOT_DIR + "log/normal";
        } else {// 如果SD卡不存在，就保存到本应用的目录下  
            PATH_LOGCAT = context.getFilesDir().getAbsolutePath()  
                    + File.separator + Constant.ROOT_DIR + "log/normal";
        }  
        File file = new File(PATH_LOGCAT);
        if (!file.exists()) {  
            file.mkdirs();  
        }  
    }  
  
    public static LogcatHandler getInstance(Context context) {
        if (INSTANCE == null) {  
            INSTANCE = new LogcatHandler(context);
        }  
        return INSTANCE;  
    }  
  
    private LogcatHandler(Context context) {
        init(context);  
        mPId = android.os.Process.myPid();  
    }

    public void start() {
        if (mLogDumper != null){
            mLogDumper.stopLogs();
            mLogDumper = null;
        }
        mLogDumper = new LogDumper(String.valueOf(mPId), PATH_LOGCAT);
        mLogDumper.start();
    }


  
    public void stop() {  
        if (mLogDumper != null) {  
            mLogDumper.stopLogs();  
            mLogDumper = null;  
        }  
    }  
  
    private class LogDumper extends Thread {

        private Process logcatProc;
        private BufferedReader mReader = null;
        private boolean mRunning = true;  
        String cmds = null;
        private String mPID;
        private FileOutputStream out = null;
  
        public LogDumper(String pid, String dir) {
            mPID = pid;  
            try {
                File path = new File(dir);
                if (!path.exists()){
                    path.mkdirs();
                }
                File file = new File(path, "cached.log");
                if (file.exists()){
                    file.delete();
                }
                file.createNewFile();
                out = new FileOutputStream(file);
            } catch (Exception e) {
                e.printStackTrace();
            }
  
            /**  
             *  
             * 日志等级：*:v , *:d , *:w , *:e , *:f , *:s  
             *  
             * 显示当前mPID程序的 E和W等级的日志.  
             *  
             * */  
  
//             cmds = "logcat *:e *:w | grep \"(" + mPID + ")\""; //打印警告和异常日志
//             cmds = "logcat  | grep \"(" + mPID + ")\"";//打印所有日志信息
             //cmds = "logcat";//打印所有日志信息  
//             cmds = "logcat -s Launcher";//打印标签过滤信息
//            cmds = "logcat -v process Launcher:I *:S";//打印标签过滤信息
            //cmds = "logcat *:e *:i | grep \"(" + mPID + ")\"";  

            if (tags != null && tags.length > 0){
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < tags.length; i++){
                    builder.append(tags[i]);
                    builder.append(":I");
                    builder.append(" ");
                }
                cmds = "logcat -v tag " + builder.toString() + "*:S";//打印标签过滤信息
            }
        }  
  
        public void stopLogs() {  
            mRunning = false;  
        }  
  
        @Override
        public void run() {  
            try {
                logcatProc = Runtime.getRuntime().exec(cmds);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            BufferedReader br = null;
                            br = new BufferedReader(new InputStreamReader(logcatProc.getErrorStream()));
                            String line = null;
                            while (mRunning && (line = br.readLine()) != null) {
                                //异步消费掉错误类型数据流，防止缓冲区堵塞而崩溃
                            }
                            logcatProc.waitFor();
                            if (logcatProc != null) {
                                logcatProc.destroy();
                            }
                            br.close();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }).start();

                mReader = new BufferedReader(new InputStreamReader(
                        logcatProc.getInputStream()), 1024);
                String line = "";
                while (mRunning && (line = mReader.readLine()) != null) {
                    if (!mRunning) {
                        break;
                    }
                    if (out != null) {
                        out.write((DateUtils.getFormatYMDHMS() + "  " + line + "\n").getBytes());
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (logcatProc != null) {
                    logcatProc.destroy();
                    logcatProc = null;
                }
                if (mReader != null) {
                    try {
                        mReader.close();
                        mReader = null;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (out != null) {
                    try {
                        out.close();
                        out = null;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }
  
        }  
  
    }  
}  