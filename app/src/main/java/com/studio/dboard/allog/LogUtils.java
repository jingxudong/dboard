package com.studio.dboard.allog;

import android.util.Log;

import com.studio.dboard.BuildConfig;
import com.studio.dboard.utils.FileUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fanlei on 2016/11/21.
 * 用于开发调试
 */
public class LogUtils {

    public final static boolean OPEN = BuildConfig.DEBUG;
//    public final static boolean OPEN = true;
    public static List<String> ENABLE_TAGS = new ArrayList<>();

    static {
//        ENABLE_TAGS.add("Launcher");
//        ENABLE_TAGS.add("LauncherService");
//        ENABLE_TAGS.add("Application");
//        ENABLE_TAGS.add("AssetsCopyer");
//        ENABLE_TAGS.add("ActivationUtils");
//        ENABLE_TAGS.add("FileUtils");
//        ENABLE_TAGS.add("peak");
//        ENABLE_TAGS.add("AdvancedSetting");
//        ENABLE_TAGS.add("Fragment");
    }



    public static void i(String tag, String msg) {
        if (OPEN) {
            Log.i(tag, msg);
            FileUtils.saveLog2File(msg);
        }
    }
}
