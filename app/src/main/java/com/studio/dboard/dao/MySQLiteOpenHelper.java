package com.studio.dboard.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import org.greenrobot.greendao.database.Database;

/**
 * Created by fanlei on 2017/5/10.
 * 数据库升级所需工具类
 */

public class MySQLiteOpenHelper extends DaoMaster.OpenHelper {

    private static final String tag = "OpenHelper";

    private Context mContext;

    public MySQLiteOpenHelper(Context context, String name) {
        super(context, name);
        this.mContext = context;
    }

    public MySQLiteOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory) {
        super(context, name, factory);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        MigrationHelper.migrate(db, new MigrationHelper.ReCreateAllTableListener() {
            @Override
            public void onCreateAllTables(Database db, boolean ifNotExists) {
                DaoMaster.createAllTables(db, ifNotExists);
            }

            @Override
            public void onDropAllTables(Database db, boolean ifExists) {
                DaoMaster.dropAllTables(db, ifExists);
            }
        }, ConfigDao.class);

//        try {
//
//        }catch (Exception e){
//            e.printStackTrace();
//        }
    }

}
