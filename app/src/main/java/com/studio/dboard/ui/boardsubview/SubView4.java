package com.studio.dboard.ui.boardsubview;

import android.content.Context;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.studio.dboard.R;

public class SubView4 extends BaseSubView {

    private static final String fontPath = "fonts/Microgme.ttf";
    private static final String unitFontPath = "fonts/Fairview_Regular.otf";

    private static final int SPEED_ROOT_PADDING = 50;
    private static final int SPEED_SIZE = 75;
    private static final int SPEED_UNIT_SIZE = 18;

    private View speedRootV;
    private TextView speedV;
    private TextView speedUnitV;

    public SubView4(Context mContext) {
        super(mContext);
    }

    @Override
    public View getSubView() {
        return subView;
    }

    @Override
    public void inflateSubView() {
        subView = LayoutInflater.from(mContext).inflate(R.layout.board_sublayout_4, null);
    }

    @Override
    public void findViews() {
        super.findViews();
        speedRootV = subView.findViewById(R.id.speed_root);
        speedV = subView.findViewById(R.id.speed);
        speedUnitV = subView.findViewById(R.id.speed_unit);
    }

    @Override
    public void updateSpeedStyle(float scale) {
        speedRootV.setPadding(0, 0, 0, (int) (SPEED_ROOT_PADDING * scale));

        Typeface typeface = Typeface.createFromAsset(mContext.getAssets(), fontPath);
        speedV.setTypeface(typeface);
        speedV.setTextSize(TypedValue.COMPLEX_UNIT_PX, SPEED_SIZE * scale);

        speedUnitV.setTextSize(TypedValue.COMPLEX_UNIT_PX, SPEED_UNIT_SIZE * scale);
    }

    @Override
    public void updateNaviStyle(float scale) {

    }

    @Override
    public void updateSpeed(int speed) {
        super.updateSpeed(speed);
        if (speed >= 0) {
            speedV.setText(speed + "");
        }else{
            speedV.setText("--");
        }
    }
}
