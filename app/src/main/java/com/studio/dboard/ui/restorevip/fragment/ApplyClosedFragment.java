package com.studio.dboard.ui.restorevip.fragment;

import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.studio.dboard.Constant;
import com.studio.dboard.R;

public class ApplyClosedFragment extends BaseFragment {

    private static final String qrUrl = Constant.BASE_SERVER_URL + "img/contact_qrcode.png";

    private ImageView qrV;

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_apply_closed;
    }

    @Override
    public void initView(View view) {
        qrV = view.findViewById(R.id.qr);
    }

    @Override
    public void onResume() {
        super.onResume();
        Glide.with(this)
                .load(qrUrl)
                .apply(new RequestOptions()
                            .skipMemoryCache(true)
                            .diskCacheStrategy(DiskCacheStrategy.NONE))
                .into(qrV);
    }
}
