package com.studio.dboard.ui.restorevip.fragment;

import android.content.Intent;
import android.view.View;
import com.studio.dboard.R;
import com.studio.dboard.ui.setting.SettingActivity;


public class WaitApplyFragment extends BaseFragment implements View.OnClickListener {




    @Override
    public int getLayoutRes() {
        return R.layout.fragment_wait_apply;
    }

    @Override
    public void initView(View view) {
        View overV = view.findViewById(R.id.onOver);
        overV.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.onOver){
            Intent intent = new Intent(getActivity(), SettingActivity.class);
            startActivity(intent);
            getActivity().finish();
            getActivity().overridePendingTransition(R.anim.anim_bottom_in, R.anim.anim_top_out);
        }
    }
}
