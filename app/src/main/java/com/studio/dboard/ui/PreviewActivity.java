package com.studio.dboard.ui;


import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.studio.dboard.BoardService;
import com.studio.dboard.Constant;
import com.studio.dboard.INaviListener;
import com.studio.dboard.ISpeedListener;
import com.studio.dboard.ITimeListener;
import com.studio.dboard.IWeatherListener;
import com.studio.dboard.R;
import com.studio.dboard.allog.LogUtils;
import com.studio.dboard.dao.GreenDaoManager;
import com.studio.dboard.entity.Config;
import com.studio.dboard.ui.boardsubview.BaseSubView;
import com.studio.dboard.ui.boardsubview.SubViewFactory;
import com.studio.dboard.utils.WeatherUtils;
import com.studio.dboard.view.dashboard.BaseBoardView;
import com.studio.dboard.view.dashboard.PreBoardView;


public class PreviewActivity extends BaseFSActivity {

    private static final String tag = "BoardUI";


    private static final String tmpFontPath = "fonts/Helvetica_LT_35_Thin.ttf";

    private ViewGroup boardParent;
    private BaseBoardView boardView;
    private TextView tempuratureV;
    private ImageView weatherImgV;
    private TextView weatherStateV;
    private TextView cityV;
    private ViewGroup contentV;
    private BaseSubView subView;

    private ServiceConnection connection;
    private BoardService.BoardBinder mBinder;


    ISpeedListener speedListener = new ISpeedListener.Stub() {
        @Override
        public void onSpeed(final int speed) throws RemoteException {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    boardView.setSpeed(speed);
                    subView.startSpeedAnim(speed);
                }
            });
        }
    };

    IWeatherListener weatherListener = new IWeatherListener.Stub() {
        @Override
        public void onWeather(final String local, final String weatherState, final String temperature) throws RemoteException {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    setWeather(local, weatherState, temperature);
                }
            });

        }
    };

    INaviListener naviListener = new INaviListener.Stub() {
        @Override
        public void onNaviInfo(final int iconIndex, final int distance, final String roadName) throws RemoteException {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    setNaviInfo(iconIndex, distance, roadName);
                }
            });
        }

        @Override
        public void onNaviStateChanged(final int state) throws RemoteException {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    controlNaviVisible(state);
                }
            });
        }
    };

    ITimeListener timeListener = new ITimeListener.Stub() {
        @Override
        public void onTime(final int hour, final int min, final int second) throws RemoteException {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    setTime(hour, min, second);
                }
            });
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview);

        initView();
    }

    private void initView() {
        boardParent = findViewById(R.id.board_parent);
        boardView = new PreBoardView(getApplicationContext());
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        boardView.setLayoutParams(params);
        boardParent.removeAllViews();
        boardParent.addView(boardView);
        contentV = findViewById(R.id.content);
        subView = SubViewFactory.buildPreSubView(getApplicationContext(), contentV);
        weatherImgV = findViewById(R.id.weather_img);
        weatherStateV = findViewById(R.id.weather_state);
        cityV = findViewById(R.id.weather_location);
        tempuratureV = findViewById(R.id.temperature);
        Typeface typeface = Typeface.createFromAsset(getAssets(), tmpFontPath);
        tempuratureV.setTypeface(typeface);

        boardView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                config.setIsSpeedMock(false);
                GreenDaoManager.getInstance(getApplicationContext()).getSession()
                        .getConfigDao().update(config);

                Intent intent = new Intent(PreviewActivity.this, BoardlistActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.anim_top_in, R.anim.anim_bottom_out);
                finish();
                return true;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        boardView.onResume();
        bindBoardService();

        config.setIsSpeedMock(true);
        GreenDaoManager.getInstance(getApplicationContext()).getSession()
                .getConfigDao().update(config);
    }

    private void bindBoardService(){
        connection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                LogUtils.i(tag, "onServiceConnected");
                mBinder = (BoardService.BoardBinder) service;
                if (mBinder != null) {
                    mBinder.registerSpeedListener(speedListener);
                    mBinder.registerWeatherListener(weatherListener);
                    mBinder.registerNaviListener(naviListener);
                    mBinder.registerTimeListener(timeListener);
                    mBinder.queryWeather();
                }
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {

            }
        };
        Intent intent = new Intent(this, BoardService.class);
        bindService(intent, connection, BIND_AUTO_CREATE);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus){
            updateSubViewParams();
        }
    }

    private void updateSubViewParams(){
        int width = contentV.getWidth();
        int height = contentV.getHeight();
        float scale = Math.min(width * 1.0f / Constant.STANDARD_WIDTH,
                height * 1.0f / Constant.STANDARD_HEIGHT);
        subView.updateSpeedStyle(scale);
    }

    private void setWeather(String local, String weatherState, String temperature){
        if (!TextUtils.isEmpty(local) && !TextUtils.isEmpty(weatherState)) {
            Bitmap bitmap = WeatherUtils.getWeatherImage(getApplicationContext(), weatherState);
            weatherImgV.setImageBitmap(bitmap);
            tempuratureV.setText(temperature);
            weatherStateV.setText(weatherState);
            cityV.setText(local);
            boardView.setWeather(weatherState);
        }
    }

    private void setNaviInfo(int iconIndex, int distance, String roadName){
        subView.updateNaviState(iconIndex, roadName, distance);
    }

    private void controlNaviVisible(int state){
        subView.updateExtraState(state);
    }

    private void setTime(int hour, int min, int second){
        boardView.setTime(hour, min, second);
    }


    @Override
    protected void onPause() {
        super.onPause();
        boardView.onPause();
        if (mBinder != null){
            mBinder.unregisterSpeedListener(speedListener);
            mBinder.unregisterWeatherListener(weatherListener);
            mBinder.unregisterNaviListener(naviListener);
            mBinder.unregisterTimeListener(timeListener);
        }
        unbindService(connection);

        //速度模拟状态设置成false
        config.setIsSpeedMock(false);
        GreenDaoManager.getInstance(getApplicationContext()).getSession()
                .getConfigDao().update(config);
        //防止点击HOME，下次重新预览其他待机，导致无法重新创建新的界面，显示的还是上次的待机
        finish();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, BoardlistActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.anim_top_in, R.anim.anim_bottom_out);
        finish();
    }

}
