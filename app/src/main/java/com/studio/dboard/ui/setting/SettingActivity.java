package com.studio.dboard.ui.setting;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.studio.dboard.R;
import com.studio.dboard.ui.BaseActivity;
import com.studio.dboard.ui.BoardlistActivity;
import com.studio.dboard.ui.setting.adapter.SettingAdapter;
import com.studio.dboard.ui.setting.fragment.AboutFragment;
import com.studio.dboard.ui.setting.fragment.NeedHelpFragment;
import com.studio.dboard.ui.setting.fragment.NormalSettingFragment;
import com.studio.dboard.update.UpdateManager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SettingActivity extends BaseActivity implements AdapterView.OnItemSelectedListener,
        SettingAdapter.OnItemClickListener {


    private View checkUpdateBtn;
    private ListView listView;
    private List<String> titles = new ArrayList<>();
    private SettingAdapter adapter;
    private NormalSettingFragment normalSettingFragment;
    private NeedHelpFragment needHelpFragment;
    private AboutFragment aboutFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        initData();
        initView();
    }

    private void initData() {
        titles.clear();
        titles.addAll(Arrays.asList(getResources().getStringArray(R.array.theme_setting)));
    }

    private void initView() {
        normalSettingFragment = new NormalSettingFragment();
        needHelpFragment = new NeedHelpFragment();
        aboutFragment = new AboutFragment();

        listView = (ListView) findViewById(R.id.help_list);
        listView.setOnItemSelectedListener(this);
        adapter = new SettingAdapter(titles);
        listView.setAdapter(adapter);
        adapter.setListener(this);
        checkUpdateBtn = findViewById(R.id.check_update);
        checkUpdateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new UpdateManager(getApplicationContext()).checkHandleUpdate();
            }
        });

        listView.post(new Runnable() {
            @Override
            public void run() {
                View view = listView.getChildAt(0);
                if (view != null) {
                    view.findViewById(R.id.frame_item).performClick();
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_content,
                            normalSettingFragment).commitAllowingStateLoss();
                }
            }
        });
    }

    public void onReturn(View view){
        onBackPressed();
    }

    private void refreshPage(int position){
        if (position == 0) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_content,
                    normalSettingFragment).commitAllowingStateLoss();
        }
        else if (position == 1){
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_content,
                    needHelpFragment).commitAllowingStateLoss();
        }
        else if (position == 2){
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_content,
                    aboutFragment).commitAllowingStateLoss();
        }

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        refreshPage(position);
        view.setSelected(true);
        adapter.setCheckPosition(position);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onItemClick(View view, int position) {
        refreshPage(position);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, BoardlistActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.anim_left_in, R.anim.anim_right_out);
        finish();
    }
}
