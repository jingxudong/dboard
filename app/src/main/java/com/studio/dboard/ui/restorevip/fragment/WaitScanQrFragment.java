package com.studio.dboard.ui.restorevip.fragment;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.studio.dboard.Constant;
import com.studio.dboard.R;
import com.studio.dboard.allog.LogUtils;
import com.studio.dboard.dao.GreenDaoManager;
import com.studio.dboard.entity.Config;
import com.studio.dboard.utils.ImageUtils;

public class WaitScanQrFragment extends BaseFragment {

    private static final String DESC_URL = Constant.BASE_SERVER_URL + "img/reapply_wait_scancode.png";

    private ImageView descV;
    private ImageView twoCodeV;


    @Override
    public int getLayoutRes() {
        return R.layout.fragment_wait_scan;
    }

    @Override
    public void initView(View view) {
        descV = view.findViewById(R.id.desc);
        twoCodeV = view.findViewById(R.id.two_code);
    }

    @Override
    public void onResume() {
        super.onResume();
        Glide.with(this)
                .load(DESC_URL)
                .apply(new RequestOptions()
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true))
                .into(descV);


        Config config = GreenDaoManager.getInstance(getContext()).getSession()
                .getConfigDao()
                .load(0L);
        String qrCodeStr = config.getQrCode();
        LogUtils.i("peak", "qrCodeStr="+qrCodeStr);
        int size = getResources().getDimensionPixelSize(R.dimen.dp_230);
        if (!TextUtils.isEmpty(qrCodeStr)) {
            twoCodeV.setImageBitmap(ImageUtils.encodeAsBitmap(qrCodeStr, size));
        }
    }
}
