package com.studio.dboard.ui.updatelog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.studio.dboard.R;
import java.util.List;

/**
 * Created by peak on 2017/8/16.
 */

public class LoglistAdapter extends BaseAdapter {

    private Context mContext;
    private List<String> datas;

    public LoglistAdapter(Context context, List<String> datas) {
        this.mContext = context;
        this.datas = datas;
    }

    @Override
    public int getCount() {
        if (datas != null){
            return datas.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        if (datas != null){
            datas.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder = null;
        if (convertView == null){
            holder = new Holder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_log, null);
            holder.log = (TextView) convertView.findViewById(R.id.log);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        holder.log.setText(datas.get(position));
        return convertView;
    }

    class Holder {
        TextView log;
    }
}
