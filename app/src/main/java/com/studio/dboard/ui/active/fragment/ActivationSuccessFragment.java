package com.studio.dboard.ui.active.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;

import com.studio.dboard.R;
import com.studio.dboard.ui.mainui.BoardActivity;
import com.studio.dboard.ui.restorevip.fragment.BaseFragment;


/**
 * Created by mateng on 2017/3/29.
 */

public class ActivationSuccessFragment extends BaseFragment {

    private ImageView img_success;

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_activation_success;
    }

    @Override
    public void initView(View view) {

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    Intent intent = new Intent(getActivity(), BoardActivity.class);
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.anim_left_in, R.anim.anim_right_out);
                    getActivity().finish();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }, 1500);
    }

}
