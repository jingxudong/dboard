package com.studio.dboard.ui;

import android.app.Activity;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.studio.dboard.BoardService;
import com.studio.dboard.Constant;
import com.studio.dboard.R;
import com.studio.dboard.allog.LogUtils;
import com.studio.dboard.dao.GreenDaoManager;
import com.studio.dboard.entity.Board;
import com.studio.dboard.entity.Config;
import com.studio.dboard.imgpicker.ImagePicker;
import com.studio.dboard.imgpicker.data.ImageBean;
import com.studio.dboard.imgpicker.data.ImagePickType;
import com.studio.dboard.imgpicker.data.ImagePickerCropParams;
import com.studio.dboard.imgpicker.utils.GlideImagePickerDisplayer;
import com.studio.dboard.ui.active.UserActivationActivity;
import com.studio.dboard.ui.mainui.BoardActivity;
import com.studio.dboard.ui.setting.SettingActivity;
import com.studio.dboard.utils.ActivationUtils;
import com.studio.dboard.utils.SPUtils;
import com.studio.dboard.view.headerandfooterrecyclerview.HeaderAndFooterWrapper;


import java.io.File;

/**
 * Created by peak on 2017/12/4.
 */

public class BoardlistActivity extends BaseActivity implements View.OnClickListener {

    private static final String tag = "BoardlistActivity";

    private View rootView;
    private RecyclerView recyclerView;
    private RecyclerViewAdapter adapter;
    private HeaderAndFooterWrapper wrapper;
    private View backV;
    private View activeV;
    private View settingV;

    private Config config;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rootView = LayoutInflater.from(getApplicationContext())
                .inflate(R.layout.activity_board_list, null);
        setContentView(rootView);
        config = GreenDaoManager.getInstance(getApplicationContext()).getSession()
                .getConfigDao()
                .load(0L);
        if (!config.getIsProcessStartBefore()) {
            startSplash();
        }else{
            initView();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void startSplash(){
        Intent intent = new Intent(this, SplashActivity.class);
        intent.putExtra("activity", SplashActivity.ACTIVITY_BOARD_LIST);
        startActivity(intent);
        finish();
    }

    private void initView() {
        backV = findViewById(R.id.back);
        backV.setOnClickListener(this);
        activeV = findViewById(R.id.active);
        activeV.setOnClickListener(this);
        boolean isActive = ActivationUtils.checkActive(getApplicationContext(), config.getActiveKey(),
                Constant.ACTIVE_FILE_PATH);
        if (isActive){
            activeV.setVisibility(View.GONE);
        }else{
            activeV.setVisibility(View.VISIBLE);
        }
        settingV = findViewById(R.id.setting);
        settingV.setOnClickListener(this);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.post(new Runnable() {
            @Override
            public void run() {
                int columWidth = getResources().getDimensionPixelSize(R.dimen.dp_330);
                int colum = (recyclerView.getWidth() - recyclerView.getPaddingLeft() * 2) / columWidth;
                recyclerView.setLayoutManager(new StaggeredGridLayoutManager(colum,
                        StaggeredGridLayoutManager.VERTICAL));
                recyclerView.requestLayout();
            }
        });

        adapter = new RecyclerViewAdapter(this);
        int boardId = config.getBoardId();
        adapter.setCurPosition(Board.getPosition(boardId));
        wrapper = new HeaderAndFooterWrapper(adapter);
        View footer = LayoutInflater.from(getApplicationContext()).inflate(R.layout.screen_save_footer, null);
        wrapper.addFootView(footer);
        recyclerView.setAdapter(wrapper);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, BoardActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.anim_left_in, R.anim.anim_right_out);
        finish();
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        if (v.getId() == R.id.back) {
            onBackPressed();
            return;
        }else if (v.getId() == R.id.active){
            intent = new Intent(this, UserActivationActivity.class);
        }else if(v.getId() == R.id.setting){
            intent = new Intent(this, SettingActivity.class);
        }
        startActivity(intent);
        overridePendingTransition(R.anim.anim_right_in, R.anim.anim_left_out);
        finish();
    }

    class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ItemViewHolder>{

        private Activity mContext;
        private int curPosition;
        private int selectedPos = -1;

        public RecyclerViewAdapter(Activity mContext) {
            this.mContext = mContext;
        }

        public void setCurPosition(int curPosition) {
            this.curPosition = curPosition;
        }

        @Override
        public RecyclerViewAdapter.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.item_board, null);
            ItemViewHolder holder = new ItemViewHolder(view);
            return holder;
        }

        @Override
        public void onBindViewHolder(final RecyclerViewAdapter.ItemViewHolder holder, final int position) {
            String cropPath = config.getScreenSaveImgPath();
            RequestManager requestManager = Glide.with(getApplicationContext());
            RequestBuilder drawableTypeRequest = null;
            File file = new File(cropPath);
            final int dreamId = Board.getId(position);
            if (dreamId == Board.ID_BOARD_10 && file.exists()){
                drawableTypeRequest = requestManager.load(file);
            }else {
                drawableTypeRequest = requestManager.load(Board.resIds[position]);
            }
            drawableTypeRequest
                    .apply(new RequestOptions()
                            .optionalTransform(new RoundedCorners(15)))
                    .into(holder.screenImg);


            holder.screenTitle.setText(Board.titles[position]);

            if (curPosition == position){
                holder.selected.setImageResource(R.drawable.pic_selected_01);
                holder.customBtns.setVisibility(View.GONE);
                if (dreamId == Board.ID_BOARD_10){
                    holder.edit.setVisibility(View.VISIBLE);
                }else {
                    holder.edit.setVisibility(View.GONE);
                }
            }else if (selectedPos == position){
                holder.selected.setImageResource(R.drawable.pic_selected_02);
                holder.customBtns.setVisibility(View.VISIBLE);
                holder.edit.setVisibility(View.GONE);
                if (dreamId == Board.ID_BOARD_10){
                    holder.editOrPreview.setImageResource(R.drawable.custom_thumb_btn_edit_default);
                }else {
                    holder.editOrPreview.setImageResource(R.drawable.btn_review_default);
                }
            }else {
                holder.selected.setImageBitmap(null);
                holder.customBtns.setVisibility(View.GONE);
                holder.edit.setVisibility(View.GONE);
            }

            holder.screenImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (curPosition != position) {
                        selectedPos = position;
                        wrapper.notifyDataSetChanged();
                    }
                }
            });

            holder.edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    editPic();
                }
            });

            holder.editOrPreview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (dreamId == Board.ID_BOARD_10) {
                        editPic();
                    }else {
                        config.setPreviewBoardId(dreamId);
                        GreenDaoManager.getInstance(mContext).getSession().getConfigDao().update(config);
                        Intent intent = new Intent(mContext, PreviewActivity.class);
                        startActivity(intent);
                        mContext.overridePendingTransition(R.anim.anim_bottom_in, R.anim.anim_top_out);
                    }
                }
            });

            holder.applyV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    curPosition = position;
                    config.setBoardId(Board.getId(position));
                    GreenDaoManager.getInstance(mContext).getSession().getConfigDao().update(config);
                    wrapper.notifyDataSetChanged();

                    showSelectDialog();
                }
            });
        }

        @Override
        public int getItemCount() {
            return Board.resIds.length;
        }

        private void editPic(){
            String activeKey = config.getActiveKey();

            boolean isActive = ActivationUtils.checkActive(mContext, activeKey,
                    Constant.ACTIVE_FILE_PATH);
            String originPath = config.getScreenSaveOriginImgPath();
            int width = rootView.getWidth();
            int height = rootView.getHeight();
            ImagePicker.getInstance()
                    .isActive(isActive)
                    .needGuideLine(true)
                    .pickType(ImagePickType.SINGLE)//设置选取类型(拍照、单选、多选)
                    .maxNum(1)//设置最大选择数量(拍照和单选都是1，修改后也无效)
                    .needCamera(false)//是否需要在界面中显示相机入口(类似微信)
                    .cachePath(Constant.IMG_DIR)//自定义缓存路径
                    .doCrop(new ImagePickerCropParams(width, height,
                            width, height))//裁剪功能需要调用这个方法，多选模式下无效
                    .displayer(new GlideImagePickerDisplayer())//自定义图片加载器，默认是Glide实现的,可自定义图片加载器
                    .setToActivelistener(new ImagePicker.ToActiveListener() {
                        @Override
                        public void toActive() {
                            Intent intent = new Intent(mContext, UserActivationActivity.class);
                            startActivity(intent);
                            mContext.overridePendingTransition(R.anim.anim_bottom_in, R.anim.anim_top_out);
                            finish();
                        }
                    })
                    .setOnResultListener(new ImagePicker.OnResultListener() {
                        @Override
                        public void onResult(ImageBean imageBean) {
                            onCropResult(imageBean);
                        }
                    })
                    .startCrop(BoardlistActivity.this, originPath);
        }

        private void onCropResult(ImageBean imageBean){
            if (imageBean != null) {
                Config config = GreenDaoManager.getInstance(mContext).getSession()
                        .getConfigDao()
                        .load(0L);
                String cropPath = config.getScreenSaveImgPath();
                if (!TextUtils.isEmpty(cropPath)) {
                    File file = new File(cropPath);
                    if (file.exists()) {
                        file.delete();
                    }
                }
                config.setScreenSaveImgPath(imageBean.getImagePath());
                config.setScreenSaveOriginImgPath(imageBean.getOriginPath());
                GreenDaoManager.getInstance(mContext).getSession()
                        .getConfigDao()
                        .update(config);
                wrapper.notifyDataSetChanged();
            }
        }

        private void showSelectDialog(){
            final Dialog dialog = new Dialog(mContext);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCanceledOnTouchOutside(false);
            dialog.getWindow().setDimAmount(0.9f);
            dialog.setContentView(R.layout.dialog_board_select);
            View confirm = dialog.findViewById(R.id.confirm);
            confirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    Intent intent = new Intent(mContext, BoardActivity.class);
                    startActivity(intent);
                    mContext.overridePendingTransition(R.anim.anim_left_in, R.anim.anim_right_out);
                    finish();
                }
            });
            dialog.show();
        }

        class ItemViewHolder extends RecyclerView.ViewHolder {
            ImageView screenImg;
            ImageView selected;
            View customBtns;
            ImageView edit;
            ImageView editOrPreview;
            TextView screenTitle;
            View applyV;

            public ItemViewHolder(View itemView) {
                super(itemView);
                screenImg = (ImageView) itemView.findViewById(R.id.screen_img);
                selected = (ImageView) itemView.findViewById(R.id.selected);
                customBtns = itemView.findViewById(R.id.custom_btn);
                edit = itemView.findViewById(R.id.edit);
                editOrPreview = (ImageView) itemView.findViewById(R.id.edit_or_preview);
                screenTitle = (TextView) itemView.findViewById(R.id.screen_save_title);
                applyV = itemView.findViewById(R.id.apply);
            }
        }
    }
}
