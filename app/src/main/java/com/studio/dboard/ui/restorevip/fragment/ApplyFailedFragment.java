package com.studio.dboard.ui.restorevip.fragment;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.studio.dboard.Constant;
import com.studio.dboard.R;
import com.studio.dboard.websocket.entity.ReviewRes;


public class ApplyFailedFragment extends BaseFragment {

    private static final String qrUrl = Constant.BASE_SERVER_URL + "img/contact_qrcode.png";

    private TextView reasonV;
    private ImageView qrV;
    private ReviewRes reviewRes;

    public void setReviewRes(ReviewRes reviewRes) {
        this.reviewRes = reviewRes;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_apply_failed;
    }

    @Override
    public void initView(View view) {
        reasonV = view.findViewById(R.id.reason);
        qrV = view.findViewById(R.id.qr);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (reviewRes != null){
            reasonV.setText(reviewRes.getDesc());
        }


        Glide.with(this)
                .load(qrUrl)
                .apply(new RequestOptions()
                        .skipMemoryCache(true)
                        .diskCacheStrategy(DiskCacheStrategy.NONE))
                .into(qrV);
    }
}
