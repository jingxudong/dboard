package com.studio.dboard.ui.boardsubview;

import android.content.Context;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.studio.dboard.R;
import com.studio.dboard.view.ArcProgressView;

public class SubView9 extends BaseSubView {

    private static final String fontPath = "fonts/helveticaneueltstd_th.otf";


    private View speedRootV;
    private ArcProgressView speedProgressV;
    private TextView speedV;
    private TextView speedUnitV;

    public SubView9(Context mContext) {
        super(mContext);
    }

    @Override
    public View getSubView() {
        return subView;
    }

    @Override
    public void inflateSubView() {
        subView = LayoutInflater.from(mContext).inflate(R.layout.board_sublayout_9, null);
    }

    @Override
    public void findViews() {
        super.findViews();
        speedRootV = subView.findViewById(R.id.speed_root);
        speedProgressV = subView.findViewById(R.id.speed_progress);
        speedV = subView.findViewById(R.id.speed);
        speedUnitV = subView.findViewById(R.id.speed_unit);
    }

    @Override
    public void updateSpeedStyle(float scale) {
        int paddingTop = (int) (speedRootV.getPaddingTop() * scale);
        speedRootV.setPadding(0, paddingTop, 0, 0);

        Typeface typeface = Typeface.createFromAsset(mContext.getAssets(), fontPath);
        speedV.setTypeface(typeface);
        speedV.setTextSize(TypedValue.COMPLEX_UNIT_PX, speedV.getTextSize() * scale);

        speedUnitV.setTextSize(TypedValue.COMPLEX_UNIT_PX, speedUnitV.getTextSize() * scale);

        speedProgressV.setRadiu(speedProgressV.getRadiu() * scale);
        speedProgressV.setBarWidth(speedProgressV.getBarWidth() * scale);
    }

    @Override
    public void updateNaviStyle(float scale) {

    }

    @Override
    public void updateSpeed(int speed) {
        super.updateSpeed(speed);
        if (speed >= 0) {
            speedV.setText(speed + "");
            speedProgressV.setProgress(speed);
        }else{
            speedV.setText("--");
            speedProgressV.setProgress(0);
        }
    }
}
