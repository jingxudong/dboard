package com.studio.dboard.ui.boardsubview;

import android.content.Context;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.studio.dboard.R;

public class SubView2 extends BaseSubView {

    private static final String fontPath = "fonts/Panton.otf";

    private static final int SPEED_PADDING_TOP = 46;
    private static final int SPEED_SIZE = 95;

    private TextView speedV;

    public SubView2(Context mContext) {
        super(mContext);
    }

    @Override
    public View getSubView() {
        return subView;
    }

    @Override
    public void inflateSubView() {
        subView = LayoutInflater.from(mContext).inflate(R.layout.board_sublayout_2, null);
    }

    @Override
    public void findViews() {
        super.findViews();
        speedV = subView.findViewById(R.id.speed);
    }

    @Override
    public void updateSpeedStyle(float scale) {
        Typeface typeface = Typeface.createFromAsset(mContext.getAssets(), fontPath);
        speedV.setTypeface(typeface);
        speedV.setPadding(0, (int) (SPEED_PADDING_TOP * scale), 0, 0);
        speedV.setTextSize(TypedValue.COMPLEX_UNIT_PX, SPEED_SIZE * scale);
    }

    @Override
    public void updateNaviStyle(float scale) {

    }

    @Override
    public void updateSpeed(int speed) {
        super.updateSpeed(speed);
        if (speed >= 0) {
            speedV.setText(speed + "");
        }else{
            speedV.setText("--");
        }
    }
}
