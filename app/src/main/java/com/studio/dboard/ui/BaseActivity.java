package com.studio.dboard.ui;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.List;

public class BaseActivity extends FragmentActivity {

    private static List<Activity> activityList = new ArrayList<>();
    private HomeWatchReceiver watchReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        registerReceiver();
        addActivity(this);
    }

    private void registerReceiver() {
        watchReceiver = new HomeWatchReceiver();
        IntentFilter watchFilter = new IntentFilter(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
        registerReceiver(watchReceiver, watchFilter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        removeActivity(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(watchReceiver);
    }

    public static void addActivity(Activity activity){
        if (!activityList.contains(activity)){
            activityList.add(activity);
        }
    }

    public static void removeActivity(Activity activity){
        if (activityList.contains(activity)){
            activityList.remove(activity);
        }
    }

    public static void destroyAllActivities(){
        for (int i = 0; i < activityList.size(); i++){
            Activity activity = activityList.get(i);
            activity.finish();
        }
        activityList.clear();
    }

    class HomeWatchReceiver extends BroadcastReceiver {
        final String SYSTEM_DIALOG_REASON_KEY = "reason";
        final String SYSTEM_DIALOG_REASON_RECENT_APPS = "recentapps";
        final String SYSTEM_DIALOG_REASON_HOME_KEY = "homekey";

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (Intent.ACTION_CLOSE_SYSTEM_DIALOGS.equals(action)) {
                String reason = intent.getStringExtra(SYSTEM_DIALOG_REASON_KEY);
                if (reason != null) {
                    if (reason.equals(SYSTEM_DIALOG_REASON_HOME_KEY)
                            || reason.equals(SYSTEM_DIALOG_REASON_RECENT_APPS)) {
                        destroyAllActivities();
                    }
                }
            }
        }
    }
}
