package com.studio.dboard.ui.setting.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.studio.dboard.Constant;
import com.studio.dboard.R;
import com.studio.dboard.ui.active.FactoryActivationActivity;
import com.studio.dboard.ui.setting.ShareActivity;
import com.studio.dboard.ui.restorevip.RestoreVipActivity;
import com.studio.dboard.ui.setting.HelpCenterActivity;
import com.studio.dboard.utils.ActivationUtils;

/**
 * Created by peak on 2018/3/12.
 */

public class NeedHelpFragment extends Fragment implements View.OnClickListener {

    private View helpCenter;
    private View shareXuiV;
    private View factoryActiV;
    private View restoreVipV;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.fragment_need_help, null);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        helpCenter = view.findViewById(R.id.help_center);
        shareXuiV = view.findViewById(R.id.share);
        factoryActiV = view.findViewById(R.id.factory_active);
        restoreVipV = view.findViewById(R.id.restore_vip);
        helpCenter.setOnClickListener(this);
        shareXuiV.setOnClickListener(this);
        factoryActiV.setOnClickListener(this);
        restoreVipV.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        String content = "";
        boolean isActive = ActivationUtils.checkActive(getContext(), content, Constant.ACTIVE_FILE_PATH);
        if (isActive){
            factoryActiV.setVisibility(View.INVISIBLE);
            restoreVipV.setVisibility(View.INVISIBLE);
        }else {
            factoryActiV.setVisibility(View.VISIBLE);
            restoreVipV.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.help_center:
                Intent intent = new Intent(getActivity(), HelpCenterActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.anim_right_in, R.anim.anim_left_out);
                break;
            case R.id.share:
                intent = new Intent(getActivity(), ShareActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.anim_right_in, R.anim.anim_left_out);
                break;
            case R.id.factory_active:
                intent = new Intent(getActivity(), FactoryActivationActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.anim_right_in, R.anim.anim_left_out);
                break;
            case R.id.restore_vip:
                intent = new Intent(getActivity(), RestoreVipActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.anim_right_in, R.anim.anim_left_out);
                break;
        }
    }
}
