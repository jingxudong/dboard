package com.studio.dboard.ui.updatelog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ListView;
import com.studio.dboard.R;
import com.studio.dboard.ui.BaseActivity;

import java.util.Arrays;
import java.util.List;

/**
 * Created by peak on 2017/8/16.
 */

public class LoglistActivity extends BaseActivity implements View.OnClickListener {

    private View iKnwon;
    private ListView listView;
    private LoglistAdapter adapter;
    private List<String> datas;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loglist);
        initData();
        initView();
    }

    private void initData() {
        datas = Arrays.asList(getResources().getStringArray(R.array.log_list));
    }

    private void initView() {
        iKnwon = findViewById(R.id.i_known);
        iKnwon.setOnClickListener(this);
        listView = (ListView) findViewById(R.id.listView);
        adapter = new LoglistAdapter(getApplicationContext(), datas);
        listView.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.i_known) {
            onBackPressed();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
        overridePendingTransition(R.anim.anim_bottom_in, R.anim.anim_top_out);
    }
}
