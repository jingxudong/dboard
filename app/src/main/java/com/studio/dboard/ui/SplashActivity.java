package com.studio.dboard.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import com.studio.dboard.R;
import com.studio.dboard.dao.GreenDaoManager;
import com.studio.dboard.entity.Config;
import com.studio.dboard.ui.mainui.BoardActivity;

public class SplashActivity extends BaseFSActivity {

    public static final int ACTIVITY_BOARD_MAIN = 0;
    public static final int ACTIVITY_BOARD_LIST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        jump();
    }

    private void jump(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Config config = GreenDaoManager.getInstance(getApplicationContext())
                        .getSession()
                        .getConfigDao()
                        .load(0L);
                config.setIsProcessStartBefore(true);
                GreenDaoManager.getInstance(getApplicationContext())
                        .getSession()
                        .getConfigDao()
                        .update(config);
                Intent intent = null;
                int whereTogo = getIntent().getIntExtra("activity", ACTIVITY_BOARD_MAIN);
                if (whereTogo == ACTIVITY_BOARD_MAIN) {
                    intent = new Intent(SplashActivity.this, BoardActivity.class);
                }else {
                    intent = new Intent(SplashActivity.this, BoardlistActivity.class);
                }
                startActivity(intent);
                overridePendingTransition(R.anim.anim_right_in, R.anim.anim_left_out);
                finish();

            }
        }, 2000);
    }


}
