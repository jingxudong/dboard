package com.studio.dboard.ui.mainui.adapter;

import android.app.Activity;
import android.graphics.Color;
import android.support.v4.view.PagerAdapter;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import com.studio.dboard.R;
import com.studio.dboard.dialog.ServiceTermsDialog;

/**
 * Created by peak on 2017/12/4.
 */

public class GuidePagerAdapter extends PagerAdapter {


    private Activity mContext;
    private GuideListener listener;

    public GuidePagerAdapter(Activity context) {
        this.mContext = context;
    }

    public void setListener(GuideListener listener) {
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        if (position == 2){
            View view = LayoutInflater.from(mContext).inflate(R.layout.guide, null);
            View setTheme = view.findViewById(R.id.btn_set);
            setTheme.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null){
                        listener.onGuideDismiss();
                    }
                }
            });
            TextView serviceTerm = (TextView) view.findViewById(R.id.service_terms_btn);
            SpannableString spannableString = new SpannableString(mContext.getString(R.string.service_terms_btn));
            ForegroundColorSpan colorSpan = new ForegroundColorSpan(Color.parseColor("#536ca4"));
            spannableString.setSpan(colorSpan, 0, 10, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            colorSpan = new ForegroundColorSpan(Color.parseColor("#8db0ff"));
            spannableString.setSpan(colorSpan, 10, spannableString.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            serviceTerm.setText(spannableString);
            serviceTerm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ServiceTermsDialog serviceTermsDialog = new ServiceTermsDialog(mContext);
                    serviceTermsDialog.show();
                }
            });
            container.addView(view);
            return view;
        }else {
            ImageView imageView = new ImageView(mContext);
            ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            imageView.setLayoutParams(params);
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            int resId = -1;
            if (position == 0){
                resId = R.drawable.flash_screen_01;
            } else if (position == 1) {
                resId = R.drawable.flash_screen_02;
            }
            Picasso.get().load(resId)
                    .into(imageView);
            container.addView(imageView);
            return imageView;
        }

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View)object);
    }

    public interface GuideListener{
        void onGuideDismiss();
    }
}
