package com.studio.dboard.ui.restorevip;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.View;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.studio.dboard.Constant;
import com.studio.dboard.R;
import com.studio.dboard.allog.LogUtils;
import com.studio.dboard.dao.GreenDaoManager;
import com.studio.dboard.entity.Config;
import com.studio.dboard.retrofit.ServiceGenerator;
import com.studio.dboard.retrofit.bean.AppActivation;
import com.studio.dboard.retrofit.webapi.IActivationService;
import com.studio.dboard.ui.BaseActivity;
import com.studio.dboard.ui.active.fragment.ActivationErrorFragment;
import com.studio.dboard.ui.active.fragment.ActivationSuccessFragment;
import com.studio.dboard.ui.active.fragment.ActivationTimeOutFragment;
import com.studio.dboard.ui.restorevip.fragment.ApplyClosedFragment;
import com.studio.dboard.ui.restorevip.fragment.ApplyErrorFragment;
import com.studio.dboard.ui.restorevip.fragment.ApplyFailedFragment;
import com.studio.dboard.ui.restorevip.fragment.ApplyLoadingFragment;
import com.studio.dboard.ui.restorevip.fragment.BaseFragment;
import com.studio.dboard.ui.restorevip.fragment.ThroughApplyFragment;
import com.studio.dboard.ui.restorevip.fragment.WaitApplyFragment;
import com.studio.dboard.ui.restorevip.fragment.WaitScanQrFragment;
import com.studio.dboard.ui.setting.SettingActivity;
import com.studio.dboard.utils.ActivationUtils;
import com.studio.dboard.utils.FileUtils;
import com.studio.dboard.utils.NetworkUtils;
import com.studio.dboard.utils.SystemUtils;
import com.studio.dboard.websocket.entity.ApplyRes;
import com.studio.dboard.websocket.entity.BaseWsModel;
import com.studio.dboard.websocket.entity.ReviewRes;
import com.umeng.analytics.MobclickAgent;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import java.net.URI;
import java.util.HashMap;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class RestoreVipActivity extends BaseActivity {

    private String url = Constant.WEB_SERVER_URL + "ws/launcher/reapply/";


    private BaseFragment currentFragment;
    private WaitScanQrFragment waitScanQrFragment;
    private WaitApplyFragment waitApplyFragment;
    private ThroughApplyFragment throughApplyFragment;
    private ApplyFailedFragment applyFailedFragment;
    private ApplyLoadingFragment applyLoadingFragment;
    private ApplyClosedFragment applyClosedFragment;
    private ApplyErrorFragment applyErrorFragment;
    private ActivationSuccessFragment activeSuccessFragment;
    private ActivationErrorFragment activationErrorFragment;
    private ActivationTimeOutFragment activationTimeOutFragment;


    private WebSocketClient client;

    private Config config;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restore_vip);

        config = GreenDaoManager.getInstance(getApplicationContext()).getSession()
                .getConfigDao()
                .load(0L);

        initView();

        if (NetworkUtils.isNetworkConnected(getApplicationContext())){
            connect();
        }else {
            showFragment(activationTimeOutFragment, ApplyErrorFragment.class.getName());
        }
    }



    private void connect() {
        String uu = config.getUu();
        URI uri = URI.create(url + SystemUtils.getFactoryCode(getApplicationContext()) + uu);
        client = new WebSocketClient(uri) {
            @Override
            public void onOpen(ServerHandshake handshakedata) {
                LogUtils.i("peak", "onOpen="+handshakedata.getHttpStatusMessage());
                try {
                    String applySign = config.getApplySign();
                    if (TextUtils.isEmpty(applySign)){
                        sendApply();
                    }else {
                        sendReview(applySign);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (RemoteException e){
                    e.printStackTrace();
                } catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onMessage(String text) {
                try {
                    JSONObject jsonObject = JSONObject.parseObject(text);
                    String msgType = jsonObject.getString("msgtype");
                    if (msgType.equals(BaseWsModel.MSG_TYPE_APPLY)){
                        ApplyRes applyRes = JSONObject.parseObject(text, ApplyRes.class);
                        LogUtils.i("peak", "applyRes="+applyRes.toString());
                        actionApply(applyRes);
                    }else if (msgType.equals(BaseWsModel.MSG_TYPE_REWIEW)){
                        ReviewRes reviewRes = JSONObject.parseObject(text, ReviewRes.class);
                        LogUtils.i("peak", "reviewRes="+reviewRes.toString());
                        actionReview(reviewRes);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onClose(int code, String reason, boolean remote) {
                if (code > 1000){
                    applyErrorFragment.setErrorCode(code);
                    applyErrorFragment.setErrorMsg(reason);
                    showFragment(applyErrorFragment, ActivationErrorFragment.class.getName());
                }
            }

            @Override
            public void onError(Exception ex) {
                showFragment(activationTimeOutFragment, ActivationTimeOutFragment.class.getName());
            }
        };

        new Thread(){
            @Override
            public void run() {
                try {
                    client.connectBlocking();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }.start();

        showFragment(applyLoadingFragment, ApplyLoadingFragment.class.getName());
    }

    private void initView() {
        waitScanQrFragment = new WaitScanQrFragment();
        waitApplyFragment = new WaitApplyFragment();
        throughApplyFragment = new ThroughApplyFragment();
        applyFailedFragment = new ApplyFailedFragment();
        activeSuccessFragment = new ActivationSuccessFragment();
        applyLoadingFragment = new ApplyLoadingFragment();
        applyClosedFragment = new ApplyClosedFragment();
        applyErrorFragment = new ApplyErrorFragment();
        activationErrorFragment = new ActivationErrorFragment();
        activationTimeOutFragment = new ActivationTimeOutFragment();
    }

    /**
     * 显示fragment
     * @param targetFragment
     */
    private void showFragment(BaseFragment targetFragment, String tagName) {
        FragmentTransaction transaction = getSupportFragmentManager()
                .beginTransaction();
        transaction.addToBackStack(null);
        BaseFragment addedFragment = (BaseFragment) getSupportFragmentManager().findFragmentByTag(tagName);
        if (!targetFragment.isAdded() && addedFragment == null) {
            if (currentFragment == null){
                transaction
                        .add(R.id.content, targetFragment, tagName)
                        .commit();
            }else {
                transaction
                        .hide(currentFragment)
                        .add(R.id.content, targetFragment, tagName)
                        .commit();
            }
        } else {
            transaction
                    .hide(currentFragment)
                    .show(targetFragment)
                    .commit();
        }
        currentFragment = targetFragment;
    }

    private void sendApply() throws Exception {
        LogUtils.i("peak", "sendApply");
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("msgtype", "apply");
        jsonObject.put("sign", SystemUtils.getSystemUUID(getApplicationContext()));
        jsonObject.put("vername", SystemUtils.getVersion(getApplicationContext()));
        jsonObject.put("osver", SystemUtils.getSystemVersion());
        jsonObject.put("mac", SystemUtils.getMac(getApplicationContext()));
        jsonObject.put("sim", SystemUtils.getImei(getApplicationContext()));
        jsonObject.put("osid", SystemUtils.getAndroidId(getApplicationContext()));
        jsonObject.put("androidver", Build.VERSION.SDK_INT + "");
        jsonObject.put("iccid", SystemUtils.getIccid(getApplicationContext()));
        jsonObject.put("model", Build.MODEL);

        String uu = config.getUu();
        jsonObject.put("uu", SystemUtils.getFactoryCode(getApplicationContext())
                + uu);
        client.send(jsonObject.toString());
    }

    private void sendReview(String applySign){
        LogUtils.i("peak", "sendReview");
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("msgtype", "wait");
        jsonObject.put("applysign", applySign);
        client.send(jsonObject.toString());
    }

    private void actionApply(ApplyRes applyRes) throws Exception {
        if (applyRes.getResult() == 1){
            config.setApplySign(applyRes.getApplysign());
            config.setQrCode(applyRes.getQrcode());
            GreenDaoManager.getInstance(getApplicationContext()).getSession()
                    .getConfigDao()
                    .update(config);
            showFragment(waitScanQrFragment, WaitScanQrFragment.class.getName());
        }else{
            applyErrorFragment.setErrorCode(applyRes.getErr().getCode());
            applyErrorFragment.setErrorMsg(applyRes.getErr().getInfo());
            showFragment(applyErrorFragment, ApplyErrorFragment.class.getName());
        }
    }

    private void actionReview(ReviewRes reviewRes) throws Exception {
        if (reviewRes.getResult() == 1){
            LogUtils.i("peak", "status="+reviewRes.getStatus());
            switch (reviewRes.getStatus()){
                case ReviewRes.STATUS_WAIT_SCAN:
                    showFragment(waitScanQrFragment, WaitScanQrFragment.class.getName());
                    break;
                case ReviewRes.STATUS_APPLY_WAIT:
                    showFragment(waitApplyFragment, WaitApplyFragment.class.getName());
                    break;
                case ReviewRes.STATUS_APPLY_THROUGHED:
                    showFragment(throughApplyFragment, ThroughApplyFragment.class.getName());
                    active(true);
                    break;
                case ReviewRes.STATUS_APPLY_REFUSED:
                    applyFailedFragment.setReviewRes(reviewRes);
                    showFragment(applyFailedFragment, ApplyFailedFragment.class.getName());
                    break;
                case ReviewRes.STATUS_APPLY_CLOSED:
                    showFragment(applyClosedFragment, ApplyClosedFragment.class.getName());
                    break;
            }
        }else {
            applyErrorFragment.setErrorCode(reviewRes.getErr().getCode());
            applyErrorFragment.setErrorMsg(reviewRes.getErr().getInfo());
            showFragment(applyErrorFragment, ApplyErrorFragment.class.getName());
        }
    }

    public void onActive(View view){
        LogUtils.i("peak", "onActive");
        active(false);
    }

    private void active(final boolean isAutoActive){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("sign", SystemUtils.getSystemUUID(getApplicationContext()));
            jsonObject.put("vername", SystemUtils.getVersion(getApplicationContext()));
            jsonObject.put("osvername", SystemUtils.getSystemVersion());
            jsonObject.put("mac", SystemUtils.getMac(getApplicationContext()));
            jsonObject.put("sim", SystemUtils.getImei(getApplicationContext()));
            jsonObject.put("osid", SystemUtils.getAndroidId(getApplicationContext()));
            jsonObject.put("androidver", Build.VERSION.SDK_INT);
            jsonObject.put("iccid", SystemUtils.getIccid(getApplicationContext()));
            jsonObject.put("model", Build.MODEL);

            String uu = config.getUu();
            jsonObject.put("uu", SystemUtils.getFactoryCode(getApplicationContext())
                    + uu);

            LogUtils.i("peak", "jsonObject="+jsonObject.toString());

            ServiceGenerator.createService(IActivationService.class)
                    .activation(jsonObject.toString(), SystemUtils.getVersionCode(getApplicationContext()) + "",
                            SystemUtils.getVersion(getApplicationContext()))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<AppActivation>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {
                            LogUtils.i("peak", "onError="+e.getMessage());
                            if (isAutoActive){
                                showFragment(throughApplyFragment, ThroughApplyFragment.class.getName());
                            }else {
                                showFragment(activationTimeOutFragment, ActivationTimeOutFragment.class.getName());
                            }
                        }

                        @Override
                        public void onNext(AppActivation activation) {
                            LogUtils.i("peak", "activation="+activation.toString());
                            if (activation.getResult() == 1) {
                                config.setActiveKey(activation.getKey());
                                GreenDaoManager.getInstance(getApplicationContext())
                                        .getSession()
                                        .getConfigDao()
                                        .update(config);

                                FileUtils.writeToFile(Constant.ACTIVE_FILE_PATH, activation.getKey());
                                if (ActivationUtils.checkActive(getApplicationContext(), activation.getKey(),
                                        Constant.ACTIVE_FILE_PATH)) {
                                    closeWs();
                                    showFragment(activeSuccessFragment, ActivationSuccessFragment.class.getName());

                                    //统计
                                    HashMap<String,String> map = new HashMap<>();
                                    map.put("active_type", "终端用户激活");
                                    map.put("active_channel", SystemUtils.getChannelFromVersion(getApplicationContext()));
                                    MobclickAgent.onEvent(getApplicationContext(), "reactive", map);
                                } else {
                                    if (isAutoActive){
                                        showFragment(throughApplyFragment, ThroughApplyFragment.class.getName());
                                    }else {
                                        showFragment(activationErrorFragment, ActivationErrorFragment.class.getName());
                                    }
                                }
                            } else {
                                if (isAutoActive){
                                    showFragment(throughApplyFragment, ThroughApplyFragment.class.getName());
                                }else {
                                    activationErrorFragment.setCode(activation.getErr().getCode());
                                    showFragment(activationErrorFragment, ActivationErrorFragment.class.getName());
                                }
                            }
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onReturn(View view){
        onBackPressed();
    }

    public void onBack(View view){
        if (view.getId() == R.id.error_back) {
            showFragment(throughApplyFragment, ThroughApplyFragment.class.getName());
        }else if (view.getId() == R.id.timeout_back){
            Intent intent = new Intent(this, SettingActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.anim_left_in, R.anim.anim_right_out);
            finish();
        }
    }

    public void onSetNetwork(View view){
        NetworkUtils.setNetwork(getApplicationContext());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        closeWs();
        Intent intent = new Intent(this, SettingActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.anim_left_in, R.anim.anim_right_out);
        finish();
    }

    @Override
    protected void onStop() {
        super.onStop();
        closeWs();
    }

    private void closeWs(){
        if (client != null){
            client.close();
            client = null;
        }
    }
}
