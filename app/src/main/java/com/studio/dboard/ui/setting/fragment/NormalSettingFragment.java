package com.studio.dboard.ui.setting.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.studio.dboard.R;
import com.studio.dboard.dao.GreenDaoManager;
import com.studio.dboard.dialog.MoreTipDialog;
import com.studio.dboard.dialog.PermissionDialog;
import com.studio.dboard.entity.Config;
import com.studio.dboard.ui.setting.adapter.TimeSettingAdapter;
import com.studio.dboard.utils.PermissionUtils;
import com.studio.dboard.utils.SPUtils;

public class NormalSettingFragment extends Fragment implements View.OnClickListener,
        PermissionDialog.OnSetListener {

    private static final int MY_PERMISSIONS_REQUEST_PACKAGE_USAGE_STATS = 0;

    public static final int[] SCREEN_SAVE_TIMES_INT = new int[]{30, 60, 90, 120, -1};

    private View screenSaveTip;

    private View descInfoRoot;
    private TextView descTitle;
    private TextView descContent;

    private Config config;
    private SPUtils spUtils;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        config = GreenDaoManager.getInstance(getContext()).getSession()
                .getConfigDao()
                .load(0L);
        spUtils = new SPUtils(getContext());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_normal_setting, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        descInfoRoot = view.findViewById(R.id.desc_info);
        descInfoRoot.setOnClickListener(this);
        descTitle = (TextView) view.findViewById(R.id.desc_title);
        descContent = (TextView) view.findViewById(R.id.desc_content);

        initStartTime(view);
        initSpeedOffset(view);
        initFlowBubble(view);
        initFullscreen(view);
        initGpsThrough(view);
    }

    /**
     * 仪表界面弹出时间设置
     */
    private void initStartTime(View view){
        View screenSave = view.findViewById(R.id.root_0);
        final TextView screenSaveTime = (TextView) view.findViewById(R.id.screen_save_time);
        screenSaveTip = view.findViewById(R.id.screen_save_time_list);
        screenSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!PermissionUtils.hasUsagePermission(getContext())) {
                    showUsagePermissionDialog();
                }else{
                    screenSaveTip.setVisibility(View.VISIBLE);
                }
            }
        });

        ListView screenSaveListView = (ListView) view.findViewById(R.id.screen_save_list_view);
        int screenSaveTimePosition = spUtils.getInt("screen_save_time_position", 4);
        final TimeSettingAdapter screenSaveTimeAdapter = new TimeSettingAdapter(getContext(), SCREEN_SAVE_TIMES_INT);
        screenSaveTimeAdapter.setCheckPosition(screenSaveTimePosition);
        screenSaveListView.setAdapter(screenSaveTimeAdapter);
        screenSaveListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                View itemLayout = view.findViewById(R.id.item_layout);
                itemLayout.setSelected(true);
                screenSaveTimeAdapter.setCheckPosition(position);
                screenSaveTimeAdapter.notifyDataSetChanged();
                spUtils.putInt("screen_save_time_position", position);
                config.setDashStartTime(SCREEN_SAVE_TIMES_INT[position]);
                GreenDaoManager.getInstance(getContext()).getSession()
                        .getConfigDao()
                        .update(config);
                screenSaveTip.setVisibility(View.GONE);
                if (SCREEN_SAVE_TIMES_INT[position] != -1) {
                    screenSaveTime.setText(SCREEN_SAVE_TIMES_INT[position] + getString(R.string.second));
                }else {
                    screenSaveTime.setText(getString(R.string.close));
                }
            }
        });

        screenSaveTip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                screenSaveTip.setVisibility(View.GONE);
            }
        });
        if (SCREEN_SAVE_TIMES_INT[screenSaveTimePosition] != -1) {
            screenSaveTime.setText(SCREEN_SAVE_TIMES_INT[screenSaveTimePosition] + getString(R.string.second));
        }else {
            screenSaveTime.setText(getString(R.string.close));
        }

        View help = view.findViewById(R.id.btn_help_0);
        help.setOnClickListener(this);
    }

    /**
     * 速度矫正
     */
    private void initSpeedOffset(View view){
        final SeekBar seekBar = view.findViewById(R.id.seek_bar);
        final View speedOffsetRoot = view.findViewById(R.id.root_4);
        final TextView sppedOffsetV = view.findViewById(R.id.result);
        View subBtn = view.findViewById(R.id.sub);
        View sumBtn = view.findViewById(R.id.sum);
        subBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int progress = config.getSpeedOffset();
                if (progress > 0){
                    progress--;
                }
                seekBar.setProgress(progress);
                int value = progress - Config.DEFAULT_SPEED_OFFSET;
                sppedOffsetV.setText(value + "%");

                config.setSpeedOffset(progress);
                GreenDaoManager.getInstance(getContext()).getSession()
                        .getConfigDao()
                        .update(config);
            }
        });
        sumBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int progress = config.getSpeedOffset();
                if (progress < Config.DEFAULT_SPEED_MAX_DELT){
                    progress++;
                }
                seekBar.setProgress(progress);
                int value = progress - Config.DEFAULT_SPEED_OFFSET;
                sppedOffsetV.setText(value + "%");

                config.setSpeedOffset(progress);
                GreenDaoManager.getInstance(getContext()).getSession()
                        .getConfigDao()
                        .update(config);
            }
        });

        int progress = config.getSpeedOffset();
        int value = progress - Config.DEFAULT_SPEED_OFFSET;
        sppedOffsetV.setText(value + "%");
        seekBar.setProgress(progress);
        seekBar.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus){
                    speedOffsetRoot.setSelected(true);
                }else {
                    speedOffsetRoot.setSelected(false);
                }
            }
        });
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                int value = progress - Config.DEFAULT_SPEED_OFFSET;
                sppedOffsetV.setText(value + "%");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int progress = seekBar.getProgress();
                config.setSpeedOffset(progress);
                GreenDaoManager.getInstance(getContext()).getSession()
                        .getConfigDao()
                        .update(config);
            }
        });

        View help = view.findViewById(R.id.btn_help_1);
        help.setOnClickListener(this);
    }


    /**
     * 悬浮速度球
     * @param view
     */
    private void initFlowBubble(View view){
        final CheckBox checkBoxV = view.findViewById(R.id.enable_floatview);
        View rootLayout = view.findViewById(R.id.root_2);

        boolean isChecked = config.getEnableFlowBubble();
        checkBoxV.setChecked(isChecked);

        rootLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isChecked = checkBoxV.isChecked();
                isChecked = !isChecked;
                checkBoxV.setChecked(isChecked);
                config.setEnableFlowBubble(isChecked);
                GreenDaoManager.getInstance(getContext()).getSession()
                        .getConfigDao()
                        .update(config);
            }
        });

        checkBoxV.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                config.setEnableFlowBubble(isChecked);
                GreenDaoManager.getInstance(getContext()).getSession()
                        .getConfigDao()
                        .update(config);
            }
        });

        View help = view.findViewById(R.id.btn_help_2);
        help.setOnClickListener(this);
    }


    /**
     * 全屏设置
     * @param view
     */
    private void initFullscreen(View view){
        final CheckBox checkBoxV = view.findViewById(R.id.enable_fullscreen);
        View rootLayout = view.findViewById(R.id.root_3);

        boolean isChecked = config.getIsFullScreen();
        checkBoxV.setChecked(isChecked);

        rootLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isChecked = checkBoxV.isChecked();
                isChecked = !isChecked;
                checkBoxV.setChecked(isChecked);
                config.setIsFullScreen(isChecked);
                GreenDaoManager.getInstance(getContext()).getSession()
                        .getConfigDao()
                        .update(config);
            }
        });

        checkBoxV.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                config.setIsFullScreen(isChecked);
                GreenDaoManager.getInstance(getContext()).getSession()
                        .getConfigDao()
                        .update(config);
            }
        });

        View help = view.findViewById(R.id.btn_help_3);
        help.setOnClickListener(this);
    }

    /**
     * 导航信息透传
     * @param view
     */
    private void initGpsThrough(View view){
        final CheckBox checkBoxV = view.findViewById(R.id.enable_gps_through);
        View rootLayout = view.findViewById(R.id.root_4);

        boolean isChecked = config.getEnableGpsThrough();
        checkBoxV.setChecked(isChecked);

        rootLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isChecked = checkBoxV.isChecked();
                isChecked = !isChecked;
                checkBoxV.setChecked(isChecked);
                config.setEnableGpsThrough(isChecked);
                GreenDaoManager.getInstance(getContext()).getSession()
                        .getConfigDao()
                        .update(config);
            }
        });

        checkBoxV.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                config.setEnableGpsThrough(isChecked);
                GreenDaoManager.getInstance(getContext()).getSession()
                        .getConfigDao()
                        .update(config);
            }
        });

        View help = view.findViewById(R.id.btn_help_4);
        help.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_help_0:
                descInfoRoot.setVisibility(View.VISIBLE);
                descTitle.setText(getString(R.string.screen_save_time));
                descContent.setText(getString(R.string.dash_start_time_desc));
                break;
            case R.id.btn_help_1:
                descInfoRoot.setVisibility(View.VISIBLE);
                descTitle.setText(getString(R.string.dashboard_speed_offset));
                descContent.setText(getString(R.string.speed_offset_desc));
                break;
            case R.id.btn_help_2:
                descInfoRoot.setVisibility(View.VISIBLE);
                descTitle.setText(getString(R.string.float_view));
                descContent.setText(getString(R.string.virtual_button_desc));
                break;
            case R.id.btn_help_3:
                descInfoRoot.setVisibility(View.VISIBLE);
                descTitle.setText(getString(R.string.full_screen));
                descContent.setText(getString(R.string.enable_fullscreen_desc));
                break;
            case R.id.btn_help_4:
                descInfoRoot.setVisibility(View.VISIBLE);
                descTitle.setText(getString(R.string.gps_info_through));
                descContent.setText(getString(R.string.gps_info_through_desc));
                break;
            case R.id.desc_info:
                descInfoRoot.setVisibility(View.GONE);
                break;
        }
    }

    /**
     * 显示申请usage permission 权限提示
     */
    private void showUsagePermissionDialog(){
        PermissionDialog permissionDialog = new PermissionDialog();
        permissionDialog.setSetListener(this);
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.add(permissionDialog, "permissionDialog");
        transaction.commitAllowingStateLoss();
    }

    @Override
    public void onSetUsagePerm() {
        startActivityForResult(new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS),
                MY_PERMISSIONS_REQUEST_PACKAGE_USAGE_STATS);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MY_PERMISSIONS_REQUEST_PACKAGE_USAGE_STATS) {
            if (!PermissionUtils.hasUsagePermission(getContext())) {
                showUsagePermissionDialog();
            }else{
                screenSaveTip.setVisibility(View.VISIBLE);
            }
        }
    }
}
