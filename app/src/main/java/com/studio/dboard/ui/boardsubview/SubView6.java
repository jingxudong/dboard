package com.studio.dboard.ui.boardsubview;

import android.content.Context;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.studio.dboard.R;

public class SubView6 extends BaseSubView {


    public SubView6(Context mContext) {
        super(mContext);
    }

    @Override
    public View getSubView() {
        return subView;
    }

    @Override
    public void inflateSubView() {
        subView = LayoutInflater.from(mContext).inflate(R.layout.board_sublayout_6, null);
    }

    @Override
    public void updateSpeedStyle(float scale) {

    }

    @Override
    public void updateNaviStyle(float scale) {

    }
}
