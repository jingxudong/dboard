package com.studio.dboard.ui.setting;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.view.View;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.studio.dboard.Constant;
import com.studio.dboard.R;
import com.studio.dboard.ui.BaseActivity;


/**
 * Created by peak on 2017/9/8.
 */

public class ShareActivity extends BaseActivity {

    private View qrRootV;
    private ImageView qrImageV;
    private View networkErrorV;
    private ContentLoadingProgressBar loadingV;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share);
        initView();
    }

    private void initView() {
        qrRootV = findViewById(R.id.qr_root);
        loadingV = findViewById(R.id.loading);
        loadingV.getIndeterminateDrawable().setColorFilter(getResources()
                .getColor(android.R.color.holo_blue_light), PorterDuff.Mode.MULTIPLY);
        qrImageV = findViewById(R.id.qr_img);
        networkErrorV = findViewById(R.id.network_error);

        loadImage();
    }

    public void onRetry(View view){
        loadImage();
    }

    private void loadImage(){
        qrRootV.setVisibility(View.INVISIBLE);
        loadingV.show();
        networkErrorV.setVisibility(View.GONE);
        Glide.with(this)
                .load(Constant.BASE_SERVER_URL + "img/share_qrcode_db.png")
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        qrRootV.setVisibility(View.INVISIBLE);
                        loadingV.hide();
                        networkErrorV.setVisibility(View.VISIBLE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        qrRootV.setVisibility(View.VISIBLE);
                        loadingV.hide();
                        return false;
                    }
                })
                .apply(new RequestOptions()
                        .skipMemoryCache(true)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .centerInside())
                .into(qrImageV);
    }

    public void onReturn(View view){
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, SettingActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.anim_left_in, R.anim.anim_right_out);
        finish();
    }
}
