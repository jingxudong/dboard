package com.studio.dboard.ui.active.fragment;

import android.os.Bundle;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.studio.dboard.R;
import com.studio.dboard.allog.LogUtils;
import com.studio.dboard.dao.GreenDaoManager;
import com.studio.dboard.entity.Config;
import com.studio.dboard.utils.ImageUtils;
import com.studio.dboard.utils.SystemUtils;


/**
 * Created by mateng on 2017/3/29.
 */

public class FatoryActivationFragment extends Fragment implements View.OnClickListener {

    private ImageView twocode;
    private View tempNotActive;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_factory_active, container, false);
        twocode = (ImageView) view.findViewById(R.id.two_code);
        tempNotActive = view.findViewById(R.id.temp_not_active);
        tempNotActive.setOnClickListener(this);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setTwoCode();
    }

    private void setTwoCode(){
        Config config = GreenDaoManager.getInstance(getContext()).getSession()
                .getConfigDao()
                .load(0L);
        String uu = config.getUu();
        try {
            String codeContent = "http://weixin.qq.com/r/_Ujp8ezECLF3raiS9x3r?uu="
                    + SystemUtils.getFactoryCode(getContext()) + uu;
            int size = getResources().getDimensionPixelSize(R.dimen.dp_240);
            twocode.setImageBitmap(ImageUtils.encodeAsBitmap(codeContent, size));
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.temp_not_active){
            getActivity().finish();
            getActivity().overridePendingTransition(R.anim.anim_left_in, R.anim.anim_right_out);
        }
    }
}
