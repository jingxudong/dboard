package com.studio.dboard.ui.active.fragment;

import android.view.View;
import android.widget.TextView;

import com.studio.dboard.R;
import com.studio.dboard.ui.restorevip.fragment.BaseFragment;

/**
 * Created by mateng on 2017/3/29.
 */

public class ActivationErrorFragment extends BaseFragment {

    private static final int ERROR_OVER_FACTORY_MAX_ACTIVE_COUNT = -115;
    private static final int ERROR_OVER_DEVICE_MAX_ACTIVE_COUNT = -118;
    private static final int ERROR_UNAUTHORIZE = -119;
    private static final int ERROR_AUTHORIZE_USED = -121;

    private TextView error;
    private int code = 0;

    public void setCode(int code){
        this.code = code;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_activation_err;
    }

    @Override
    public void initView(View view) {
        error = (TextView) view.findViewById(R.id.text_error);
    }

    @Override
    public void onResume() {
        super.onResume();
        refresh();
    }

    private void refresh(){
        switch (code){
            case ERROR_OVER_FACTORY_MAX_ACTIVE_COUNT:
                error.setText("激活数已用完，请在[XUI百变主题]公众账号中购买:" + code);
                break;
            case ERROR_OVER_DEVICE_MAX_ACTIVE_COUNT:
                error.setText("该设备重复激活次数已到达上限:" + code);
                break;
            case ERROR_UNAUTHORIZE:
                error.setText("该设备未通过微信扫码授权，无法激活:" + code);
                break;
            case ERROR_AUTHORIZE_USED:
                error.setText("该设备授权资格已被占用，无法激活:" + code);
                break;
            default:
                error.setText("您的内部存储已满，请尝试删除一些内容后再次激活:" + code);
        }
    }
}
