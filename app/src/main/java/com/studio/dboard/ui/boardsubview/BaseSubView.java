package com.studio.dboard.ui.boardsubview;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.studio.dboard.R;
import com.studio.dboard.allog.LogUtils;
import com.studio.dboard.utils.GpsUtils;

import java.text.DecimalFormat;

public class BaseSubView implements ISubView {

    protected Context mContext;
    protected View subView;
    protected ImageView naviIcon;
    protected TextView distanceV;
    protected TextView unitV;
    protected TextView roadNameV;
    protected View naviLayout;
    protected View naviContent;


    private float pointerProgress = 0;
    private int oldSpeed = 0;
    private int newSpeed = 0;
    private float progressSpeed = 0;
    private ValueAnimator speedAnimator;


    public BaseSubView(Context mContext) {
        this.mContext = mContext;
    }

    public View getSubView() {
        return subView;
    }

    @Override
    public void inflateSubView() {

    }

    @Override
    public void findViews() {
        naviIcon = (ImageView) subView.findViewById(R.id.navi_icon);
        distanceV = (TextView) subView.findViewById(R.id.distance);
        unitV = (TextView) subView.findViewById(R.id.unit);
        roadNameV = (TextView) subView.findViewById(R.id.roadName);
        naviLayout = subView.findViewById(R.id.navi_layout);
        naviContent = subView.findViewById(R.id.navi_content);
        naviContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = mContext.getPackageManager()
                        .getLaunchIntentForPackage("com.autonavi.amapauto");
                if (intent != null) {
                    mContext.startActivity(intent);
                }
            }
        });
    }

    @Override
    public void updateSpeedStyle(float scale) {

    }

    @Override
    public void updateNaviStyle(float scale) {

    }

    public void startSpeedAnim(int speed){
        if (speed >= 0 ){
            if (speedAnimator != null){
                speedAnimator.cancel();
            }
            oldSpeed = newSpeed;
            newSpeed = speed;
            if (oldSpeed != newSpeed) {
                speedAnimator = ValueAnimator.ofFloat(0f, 1.0f);
                speedAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        pointerProgress = animation.getAnimatedFraction();
                        progressSpeed = oldSpeed + ((newSpeed - oldSpeed) * pointerProgress);
                        updateSpeed((int)progressSpeed);
                    }
                });
                speedAnimator.setDuration(1000);
                speedAnimator.setInterpolator(new LinearInterpolator());
                speedAnimator.start();
            }else if(oldSpeed == 0 && newSpeed == 0){
                updateSpeed(0);
            }
        } else {
            updateSpeed(-1);
        }
    }

    @Override
    public void updateSpeed(int speed) {

    }

    @Override
    public void updateNaviState(int iconIndex, String roadName, int distance) {
        if (naviLayout != null && !TextUtils.isEmpty(roadName)) {
            show();
            if (naviIcon != null) {
                naviIcon.setImageResource(GpsUtils.getNaviIconResId(iconIndex));
            }
            if (distance >= 1000){
                DecimalFormat format=new java.text.DecimalFormat("0.0");
                String content = format.format(distance / 1000f);
                if (distanceV != null) {
                    distanceV.setText(content);
                }
                if (unitV != null) {
                    unitV.setText("公里后");
                }
            }else if(distance > 0){
                if (distanceV != null) {
                    distanceV.setText(distance + "");
                }
                if (unitV != null) {
                    unitV.setText("米后");
                }
            }else {
                if (distanceV != null) {
                    distanceV.setText("现在");
                }
                if (unitV != null) {
                    unitV.setText("");
                }
            }
            if (roadNameV != null) {
                roadNameV.setText(roadName);
            }
        }else {
            if (naviLayout != null) {
                naviLayout.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void updateExtraState(int extraState) {
        //8:开始导航; 10:开始模拟导航; 24:进入巡航播报状态
        if (extraState == 8 || extraState == 10 || extraState == 24){
            show();
        }

        //39:到达目的地通知; 9:结束导航; 12:停止模拟导航; 25:退出巡航播报状态
        if (extraState == 39 || extraState == 9 || extraState == 12 || extraState == 25){ //到达目的地
            dismiss();
        }
    }

    private void show(){
        if (naviLayout != null) {
            naviLayout.setVisibility(View.VISIBLE);
        }
    }

    private void dismiss(){
        distanceV.setText("");
        unitV.setText("");
        roadNameV.setText("");
        naviIcon.setImageBitmap(null);
        naviLayout.setVisibility(View.GONE);
    }
}
