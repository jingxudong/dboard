package com.studio.dboard.ui.boardsubview;

import android.content.Context;
import android.view.ViewGroup;

import com.studio.dboard.Constant;
import com.studio.dboard.dao.GreenDaoManager;
import com.studio.dboard.entity.Board;
import com.studio.dboard.entity.Config;

public class SubViewFactory {

    public static BaseSubView buildSubView(Context mContext, ViewGroup contentV){
        Config config = GreenDaoManager.getInstance(mContext).getSession()
                .getConfigDao()
                .load(0L);
        int boardId = config.getBoardId();
        BaseSubView subView = instanceSubView(mContext, boardId);
        subView.inflateSubView();
        subView.findViews();
        contentV.removeAllViews();
        contentV.addView(subView.getSubView());
        return subView;
    }

    public static BaseSubView buildPreSubView(Context mContext, ViewGroup contentV){
        Config config = GreenDaoManager.getInstance(mContext).getSession()
                .getConfigDao()
                .load(0L);
        int boardId = config.getPreviewBoardId();
        BaseSubView subView = instanceSubView(mContext, boardId);
        subView.inflateSubView();
        subView.findViews();
        contentV.removeAllViews();
        contentV.addView(subView.getSubView());
        return subView;
    }

    private static BaseSubView instanceSubView(Context context, int boardId){
        BaseSubView subView = null;
        switch (boardId){
            case Board.ID_BOARD_1:
                subView = new SubView1(context);
                break;
            case Board.ID_BOARD_2:
                subView = new SubView2(context);
                break;
            case Board.ID_BOARD_3:
                subView = new SubView3(context);
                break;
            case Board.ID_BOARD_4:
                subView = new SubView4(context);
                break;
            case Board.ID_BOARD_5:
                subView = new SubView5(context);
                break;
            case Board.ID_BOARD_6:
                subView = new SubView6(context);
                break;
            case Board.ID_BOARD_7:
                subView = new SubView7(context);
                break;
            case Board.ID_BOARD_8:
                subView = new SubView8(context);
                break;
            case Board.ID_BOARD_9:
                subView = new SubView9(context);
                break;
            case Board.ID_BOARD_10:
                subView = new SubView10(context);
                break;
        }
        return subView;
    }

}
