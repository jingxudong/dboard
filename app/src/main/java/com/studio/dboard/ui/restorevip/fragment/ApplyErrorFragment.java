package com.studio.dboard.ui.restorevip.fragment;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.studio.dboard.Constant;
import com.studio.dboard.R;

public class ApplyErrorFragment extends BaseFragment {

    private static final String qrUrl = Constant.BASE_SERVER_URL + "img/contact_qrcode.png";

    private TextView reasonV;
    private ImageView qrV;
    private String errorMsg = "";
    private int errorCode = 0;

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_apply_error;
    }

    @Override
    public void initView(View view) {
        reasonV = view.findViewById(R.id.reason);
        qrV = view.findViewById(R.id.qr);
    }

    @Override
    public void onResume() {
        super.onResume();
        String error = String.format(getString(R.string.error_reason), errorMsg);
        reasonV.setText(error + ":" + errorCode);

        Glide.with(this)
                .load(qrUrl)
                .apply(new RequestOptions()
                        .skipMemoryCache(true)
                        .diskCacheStrategy(DiskCacheStrategy.NONE))
                .into(qrV);
    }
}
