package com.studio.dboard.ui.boardsubview;

import android.content.Context;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.studio.dboard.R;

public class SubView7 extends BaseSubView {

    private static final String fontPath = "fonts/Helvetica_LT_33_Thin_Extended.ttf";
    private static final String unitFontPath = "fonts/Helvetica_LT_43_Light_Extended.ttf";

    private static final int SPEED_PADDING_TOP = 50;
    private static final int SPEED_SIZE = 92;

    private TextView speedV;
    private TextView speedUnitV;

    public SubView7(Context mContext) {
        super(mContext);
    }

    @Override
    public View getSubView() {
        return subView;
    }

    @Override
    public void inflateSubView() {
        subView = LayoutInflater.from(mContext).inflate(R.layout.board_sublayout_7, null);
    }

    @Override
    public void findViews() {
        super.findViews();
        speedV = subView.findViewById(R.id.speed);
        speedUnitV = subView.findViewById(R.id.speed_unit);
    }

    @Override
    public void updateSpeedStyle(float scale) {
        Typeface typeface = Typeface.createFromAsset(mContext.getAssets(), fontPath);
        speedV.setTypeface(typeface);
        speedV.setPadding(0, (int) (SPEED_PADDING_TOP * scale), 0, 0);
        speedV.setTextSize(TypedValue.COMPLEX_UNIT_PX, SPEED_SIZE * scale);

        Typeface unitTypeface = Typeface.createFromAsset(mContext.getAssets(), unitFontPath);
        speedUnitV.setTypeface(unitTypeface);
    }

    @Override
    public void updateNaviStyle(float scale) {

    }

    @Override
    public void updateSpeed(int speed) {
        super.updateSpeed(speed);
        if (speed >= 0) {
            speedV.setText(speed + "");
        }else{
            speedV.setText("--");
        }
    }
}
