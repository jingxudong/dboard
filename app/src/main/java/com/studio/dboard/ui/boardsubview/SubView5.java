package com.studio.dboard.ui.boardsubview;

import android.content.Context;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.studio.dboard.R;

public class SubView5 extends BaseSubView {

    private static final String fontPath = "fonts/Fairview_Regular.otf";

    private static final int SPEED_MARGIN_TOP = 201;
    private static final int SPEED_SIZE = 40;

    private View speedRootV;
    private TextView speedBitV;
    private TextView speedTenV;
    private TextView speedHundredV;

    public SubView5(Context mContext) {
        super(mContext);
    }

    @Override
    public View getSubView() {
        return subView;
    }

    @Override
    public void inflateSubView() {
        subView = LayoutInflater.from(mContext).inflate(R.layout.board_sublayout_5, null);
    }

    @Override
    public void findViews() {
        super.findViews();
        speedRootV = subView.findViewById(R.id.speed_root);
        speedBitV = subView.findViewById(R.id.speed_bit);
        speedTenV = subView.findViewById(R.id.speed_ten);
        speedHundredV = subView.findViewById(R.id.speed_hundred);
    }

    @Override
    public void updateSpeedStyle(float scale) {
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) speedRootV.getLayoutParams();
        params.topMargin = (int) (SPEED_MARGIN_TOP * scale);
        speedRootV.requestLayout();

        Typeface typeface = Typeface.createFromAsset(mContext.getAssets(), fontPath);

        speedBitV.setTypeface(typeface);
        speedBitV.setTextSize(TypedValue.COMPLEX_UNIT_PX, SPEED_SIZE * scale);

        speedTenV.setTypeface(typeface);
        speedTenV.setTextSize(TypedValue.COMPLEX_UNIT_PX, SPEED_SIZE * scale);

        speedHundredV.setTypeface(typeface);
        speedHundredV.setTextSize(TypedValue.COMPLEX_UNIT_PX, SPEED_SIZE * scale);

    }

    @Override
    public void updateNaviStyle(float scale) {

    }

    @Override
    public void updateSpeed(int speed) {
        super.updateSpeed(speed);
        if (speed >= 0) {
            int bitValue = speed % 10;
            int tenValue = speed % 100 / 10;
            int hundredValue = speed / 100;

            speedBitV.setText(bitValue + "");
            speedTenV.setText(tenValue + "");
            speedHundredV.setText(hundredValue + "");
        }else {
            speedBitV.setText("-");
            speedTenV.setText("-");
            speedHundredV.setText("-");
        }
    }
}
