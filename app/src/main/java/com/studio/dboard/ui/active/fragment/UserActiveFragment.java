package com.studio.dboard.ui.active.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.studio.dboard.Constant;
import com.studio.dboard.R;
import com.studio.dboard.dao.GreenDaoManager;
import com.studio.dboard.entity.Config;
import com.studio.dboard.retrofit.ServiceGenerator;
import com.studio.dboard.retrofit.bean.TwoCodeInfo;
import com.studio.dboard.retrofit.webapi.ITwoCodeService;
import com.studio.dboard.ui.BoardlistActivity;
import com.studio.dboard.utils.ImageUtils;
import com.studio.dboard.utils.SystemUtils;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/**
 * Created by peak on 2017/10/23.
 */

public class UserActiveFragment extends Fragment {

    private static final String IMAGE_URL = Constant.BASE_SERVER_URL + "img/pic_vip_title_db.png";

    private ImageView operateImage;
    private ImageView twoCode;
    private ProgressBar loading;
    private View networkError;
    private View laterActiveV;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_active_user, container, false);
        initView(view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        loading.setVisibility(View.VISIBLE);
        Glide.with(getActivity())
                .load(IMAGE_URL)
                .apply(new RequestOptions()
                        .error(R.drawable.pic_vip_banner_error)
                        .skipMemoryCache(true)
                        .diskCacheStrategy(DiskCacheStrategy.NONE))
                .into(operateImage);
        getTwocodeInfo();
    }

    private void initView(View view) {
        operateImage = view.findViewById(R.id.operate_img);
        twoCode = view.findViewById(R.id.two_code);
        loading = (ProgressBar) view.findViewById(R.id.loading);
        networkError = view.findViewById(R.id.network_error);
        networkError.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getTwocodeInfo();
                loading.setVisibility(View.VISIBLE);
                networkError.setVisibility(View.GONE);
                Glide.with(getActivity())
                        .load(IMAGE_URL)
                        .apply(new RequestOptions()
                                .error(R.drawable.pic_vip_banner_error)
                                .skipMemoryCache(true)
                                .diskCacheStrategy(DiskCacheStrategy.NONE))
                        .into(operateImage);
            }
        });
        laterActiveV = view.findViewById(R.id.later_active);
        laterActiveV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), BoardlistActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.anim_left_in, R.anim.anim_right_out);
                getActivity().finish();
            }
        });
    }

    private void getTwocodeInfo(){
        Config config = GreenDaoManager.getInstance(getContext()).getSession()
                .getConfigDao()
                .load(0L);
        String uu = config.getUu();

        ServiceGenerator.createService(ITwoCodeService.class)
                .getTwoCode(SystemUtils.getFactoryCode(getContext())
                        + uu, SystemUtils.getVersionCode(getContext()) + "",
                        SystemUtils.getVersion(getContext()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<TwoCodeInfo>() {
                    @Override
                    public void onCompleted() {
                        loading.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(Throwable e) {
                        loading.setVisibility(View.GONE);
                        networkError.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onNext(TwoCodeInfo twoCodeInfo) {
                        if (twoCodeInfo.getResult() == 1) {
                            int size = getResources().getDimensionPixelSize(R.dimen.dp_220);
                            twoCode.setImageBitmap(ImageUtils.encodeAsBitmap(twoCodeInfo.getScurl(), size));
                        } else {
                            networkError.setVisibility(View.VISIBLE);
                        }
                        loading.setVisibility(View.GONE);
                    }
                });
    }

}
