package com.studio.dboard.ui.setting.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.studio.dboard.R;


/**
 * Created by peak on 2017/10/6.
 */

public class TimeSettingAdapter extends BaseAdapter {

    private Context mContext;
    private int[] list;
    private int checkPosition = -1;

    public TimeSettingAdapter(Context context, int[] list) {
        this.mContext = context;
        this.list = list;
    }

    public void setCheckPosition(int checkPosition) {
        this.checkPosition = checkPosition;
    }

    @Override
    public int getCount() {
        if (list != null){
            return list.length;
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        if (list != null){
            return list[position];
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null){
            holder = new ViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_time, null);
            holder.time = (TextView) convertView.findViewById(R.id.time);
            holder.itemLayout = convertView.findViewById(R.id.item_layout);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (list[position] != -1) {
            holder.time.setText(list[position] + mContext.getString(R.string.second));
        }else {
            holder.time.setText(mContext.getString(R.string.close));
        }
        if (position != checkPosition) {
            holder.itemLayout.setSelected(false);
        } else {
            holder.itemLayout.setSelected(true);
        }

        return convertView;
    }

    class ViewHolder {
        TextView time;
        View itemLayout;
    }
}
