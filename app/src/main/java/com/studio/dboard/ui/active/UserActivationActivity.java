package com.studio.dboard.ui.active;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.studio.dboard.Constant;
import com.studio.dboard.R;
import com.studio.dboard.allog.LogUtils;
import com.studio.dboard.dao.GreenDaoManager;
import com.studio.dboard.entity.Config;
import com.studio.dboard.retrofit.ServiceGenerator;
import com.studio.dboard.retrofit.bean.AppActivation;
import com.studio.dboard.retrofit.webapi.IActivationService;
import com.studio.dboard.ui.BaseActivity;
import com.studio.dboard.ui.BoardlistActivity;
import com.studio.dboard.ui.active.fragment.ActivationErrorFragment;
import com.studio.dboard.ui.active.fragment.ActivationLoadingFragment;
import com.studio.dboard.ui.active.fragment.ActivationSuccessFragment;
import com.studio.dboard.ui.active.fragment.ActivationTimeOutFragment;
import com.studio.dboard.ui.active.fragment.UserActiveFragment;
import com.studio.dboard.utils.ActivationUtils;
import com.studio.dboard.utils.FileUtils;
import com.studio.dboard.utils.NetworkUtils;
import com.studio.dboard.utils.SystemUtils;
import com.umeng.analytics.MobclickAgent;
import java.util.HashMap;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by peak on 2017/11/28.
 */

public class UserActivationActivity extends BaseActivity {

    private static final String tag = "UserActive";

    private UserActiveFragment userActiveFragment;
    private ActivationTimeOutFragment timeoutFragment;
    private ActivationErrorFragment errorFragment;

    private Config config;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_active_rootlayout);
        config = GreenDaoManager.getInstance(getApplicationContext()).getSession()
                .getConfigDao()
                .load(0L);
        userActiveFragment = new UserActiveFragment();
        timeoutFragment = new ActivationTimeOutFragment();
        errorFragment = new ActivationErrorFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.active_rootlayout,
                userActiveFragment).commitAllowingStateLoss();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, BoardlistActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.anim_left_in, R.anim.anim_right_out);
        finish();
    }

    public void onSetNetwork(View view){
        NetworkUtils.setNetwork(getApplicationContext());
    }

    public void onActive(View view){
        String content = config.getActiveKey();

        if (ActivationUtils.checkActive(getApplicationContext(), content, Constant.ACTIVE_FILE_PATH)) {
            getSupportFragmentManager().beginTransaction().replace(R.id.active_rootlayout,
                    new ActivationSuccessFragment()).commitAllowingStateLoss();
        } else {
            getActivationInfo();
        }
    }

    public void onBack(View view){
        getSupportFragmentManager().beginTransaction().replace(R.id.active_rootlayout,
                userActiveFragment).commitAllowingStateLoss();
    }

    public void onReturn(View view){
        onBackPressed();
    }

    private void getActivationInfo() {
        if (!ActivationLoadingFragment.getInstance().isAdded())
            ActivationLoadingFragment.getInstance().show(getSupportFragmentManager(), "");

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("sign", SystemUtils.getSystemUUID(getApplicationContext()));
            jsonObject.put("vername", SystemUtils.getVersion(getApplicationContext()));
            jsonObject.put("osvername", SystemUtils.getSystemVersion());
            jsonObject.put("mac", SystemUtils.getMac(getApplicationContext()));
            jsonObject.put("sim", SystemUtils.getImei(getApplicationContext()));
            jsonObject.put("osid", SystemUtils.getAndroidId(getApplicationContext()));
            jsonObject.put("androidver", Build.VERSION.SDK_INT);
            jsonObject.put("iccid", SystemUtils.getIccid(getApplicationContext()));
            jsonObject.put("model", Build.MODEL);

            String uu = config.getUu();
            jsonObject.put("uu", SystemUtils.getFactoryCode(getApplicationContext())
                    + uu);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        LogUtils.i(tag,"普通用户激活="+jsonObject.toString());

        ServiceGenerator.createService(IActivationService.class)
                .activation(jsonObject.toString(), SystemUtils.getVersionCode(getApplicationContext()) + "",
                        SystemUtils.getVersion(getApplicationContext()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<AppActivation>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        getSupportFragmentManager().beginTransaction().replace(R.id.active_rootlayout,
                                timeoutFragment).commitAllowingStateLoss();
                        ActivationLoadingFragment.getInstance().dismissAllowingStateLoss();
                    }

                    @Override
                    public void onNext(AppActivation activation) {
                        LogUtils.i(tag,"普通用户激活反馈activation="+activation.toString());
                        if (activation.getResult() == 1) {

                            config.setActiveKey(activation.getKey());
                            GreenDaoManager.getInstance(getApplicationContext()).getSession()
                                    .getConfigDao()
                                    .update(config);

                            FileUtils.writeToFile(Constant.ACTIVE_FILE_PATH, activation.getKey());
                            if (ActivationUtils.checkActive(getApplicationContext(), activation.getKey(),
                                    Constant.ACTIVE_FILE_PATH)) {
                                getSupportFragmentManager().beginTransaction().replace(R.id.active_rootlayout,
                                        new ActivationSuccessFragment()).commitAllowingStateLoss();
                                ActivationLoadingFragment.getInstance().dismissAllowingStateLoss();

                                //统计
                                HashMap<String,String> map = new HashMap<>();
                                map.put("active_type", "终端用户激活");
                                map.put("active_channel", SystemUtils.getChannelFromVersion(getApplicationContext()));
                                MobclickAgent.onEvent(getApplicationContext(), "active", map);
                            } else {
                                errorFragment.setCode(0);
                                getSupportFragmentManager().beginTransaction().replace(R.id.active_rootlayout,
                                        errorFragment).commitAllowingStateLoss();
                                ActivationLoadingFragment.getInstance().dismissAllowingStateLoss();
                            }
                        } else {
                            errorFragment.setCode(activation.getErr().getCode());
                            getSupportFragmentManager().beginTransaction().replace(R.id.active_rootlayout,
                                    errorFragment).commitAllowingStateLoss();
                            ActivationLoadingFragment.getInstance().dismissAllowingStateLoss();
                        }


                    }
                });
    }
}
