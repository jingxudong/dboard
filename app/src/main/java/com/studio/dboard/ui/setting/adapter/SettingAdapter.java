package com.studio.dboard.ui.setting.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.studio.dboard.R;

import java.util.List;

/**
 * Created by mateng on 2017/6/7.
 */

public class SettingAdapter extends BaseAdapter {

    private int checkPosition = -1;

    private List<String> titles;
    private OnItemClickListener listener;

    public SettingAdapter(List<String> titles) {
        this.titles = titles;
    }

    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public void setCheckPosition(int checkPosition) {
        this.checkPosition = checkPosition;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return titles.size();
    }

    @Override
    public Object getItem(int position) {
        return titles.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null){
            holder = new ViewHolder();
            convertView = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.item_setting, parent, false);
            holder.title = (TextView) convertView.findViewById(R.id.title);
            holder.frame_item = convertView.findViewById(R.id.frame_item);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        if (position != checkPosition) {
            holder.frame_item.setSelected(false);
        } else {
            holder.frame_item.setSelected(true);
        }

        String title = titles.get(position);
        holder.title.setText(title);
        holder.frame_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkPosition == position)
                    return;
                v.setSelected(true);
                checkPosition = position;
                notifyDataSetChanged();
                if (listener != null){
                    listener.onItemClick(null, position);
                }
            }
        });

        return convertView;
    }

    class ViewHolder {
        private TextView title;
        private View frame_item;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }
}
