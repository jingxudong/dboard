package com.studio.dboard.ui.boardsubview;

import android.content.Context;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.studio.dboard.R;
import com.studio.dboard.allog.LogUtils;
import com.studio.dboard.view.ArcProgressView;

public class SubView10 extends BaseSubView {

    private static final int MAX_VALUE = 260;

    private ArcProgressView speedProgressV;
    private TextView speedV;
    private View speedLayoutV;


    public SubView10(Context mContext) {
        super(mContext);
    }

    @Override
    public View getSubView() {
        return subView;
    }

    @Override
    public void inflateSubView() {
        subView = LayoutInflater.from(mContext).inflate(R.layout.board_sublayout_10, null);
    }

    @Override
    public void findViews() {
        super.findViews();
        speedProgressV = subView.findViewById(R.id.speed_progress);
        speedProgressV.setMaxValue(MAX_VALUE);
        speedV = subView.findViewById(R.id.speed);
        speedLayoutV = subView.findViewById(R.id.speed_layout);
    }

    @Override
    public void updateSpeedStyle(float scale) {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.CENTER_HORIZONTAL);
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        int bottomMargin = mContext.getResources().getDimensionPixelSize(R.dimen.dp_15);
        params.bottomMargin = bottomMargin;
        speedLayoutV.setLayoutParams(params);
        speedLayoutV.requestLayout();
    }

    @Override
    public void updateNaviStyle(float scale) {

    }

    @Override
    public void updateSpeed(int speed) {
        super.updateSpeed(speed);
        if (speed >= 0) {
            speedV.setText(speed + "");
            speedProgressV.setProgress(speed);
        }else{
            speedV.setText("--");
            speedProgressV.setProgress(0);
        }
    }

    @Override
    public void updateNaviState(int iconIndex, String roadName, int distance) {
        super.updateNaviState(iconIndex, roadName, distance);
        if (!TextUtils.isEmpty(roadName)){
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            int leftMargin = mContext.getResources().getDimensionPixelSize(R.dimen.dp_60);
            int bottomMargin = mContext.getResources().getDimensionPixelSize(R.dimen.dp_15);
            params.leftMargin = leftMargin;
            params.bottomMargin = bottomMargin;
            speedLayoutV.setLayoutParams(params);
            speedLayoutV.requestLayout();
        }
    }

    @Override
    public void updateExtraState(int extraState) {
        super.updateExtraState(extraState);
        //8:开始导航; 10:开始模拟导航; 24:进入巡航播报状态
        if (extraState == 8 || extraState == 10 || extraState == 24){
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            int leftMargin = mContext.getResources().getDimensionPixelSize(R.dimen.dp_60);
            int bottomMargin = mContext.getResources().getDimensionPixelSize(R.dimen.dp_25);
            params.leftMargin = leftMargin;
            params.bottomMargin = bottomMargin;
            speedLayoutV.setLayoutParams(params);
            speedLayoutV.requestLayout();
        }

        //39:到达目的地通知; 9:结束导航; 12:停止模拟导航; 25:退出巡航播报状态
        if (extraState == 39 || extraState == 9 || extraState == 12 || extraState == 25){ //到达目的地
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.addRule(RelativeLayout.CENTER_HORIZONTAL);
            params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            int bottomMargin = mContext.getResources().getDimensionPixelSize(R.dimen.dp_25);
            params.bottomMargin = bottomMargin;
            speedLayoutV.setLayoutParams(params);
            speedLayoutV.requestLayout();
        }
    }
}
