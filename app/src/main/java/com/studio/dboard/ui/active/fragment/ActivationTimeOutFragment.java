package com.studio.dboard.ui.active.fragment;

import android.view.View;

import com.studio.dboard.R;
import com.studio.dboard.ui.restorevip.fragment.BaseFragment;


/**
 * Created by mateng on 2017/3/29.
 */

public class ActivationTimeOutFragment extends BaseFragment {


    @Override
    public int getLayoutRes() {
        return R.layout.fragment_activation_timeout;
    }

    @Override
    public void initView(View view) {

    }
}
