package com.studio.dboard.ui.active;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.studio.dboard.Constant;
import com.studio.dboard.R;
import com.studio.dboard.allog.LogUtils;
import com.studio.dboard.dao.GreenDaoManager;
import com.studio.dboard.entity.Config;
import com.studio.dboard.retrofit.ServiceGenerator;
import com.studio.dboard.retrofit.bean.AppActivation;
import com.studio.dboard.retrofit.webapi.IActivationService;
import com.studio.dboard.ui.BaseActivity;
import com.studio.dboard.ui.active.fragment.ActivationErrorFragment;
import com.studio.dboard.ui.active.fragment.ActivationLoadingFragment;
import com.studio.dboard.ui.active.fragment.ActivationSuccessFragment;
import com.studio.dboard.ui.active.fragment.ActivationTimeOutFragment;
import com.studio.dboard.ui.active.fragment.FatoryActivationFragment;
import com.studio.dboard.utils.ActivationUtils;
import com.studio.dboard.utils.FileUtils;
import com.studio.dboard.utils.NetworkUtils;
import com.studio.dboard.utils.SystemUtils;
import com.umeng.analytics.MobclickAgent;

import java.util.HashMap;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by peak on 2017/9/8.
 */

public class FactoryActivationActivity extends BaseActivity {

    private static final String tag = "FactoryActive";

    private FatoryActivationFragment activationFragment;
    private ActivationTimeOutFragment timeoutFragment;
    private ActivationErrorFragment errorFragment;

    private Config config;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_active_rootlayout);

        config = GreenDaoManager.getInstance(getApplicationContext()).getSession()
                .getConfigDao().load(0L);

        activationFragment = new FatoryActivationFragment();

        getSupportFragmentManager().beginTransaction().replace(R.id.active_rootlayout,
                activationFragment).commitAllowingStateLoss();


    }

    public void onSetNetwork(View view) {
        NetworkUtils.setNetwork(getApplicationContext());
    }

    public void onActivation(View view) {
        String content = config.getActiveKey();

        if (ActivationUtils.checkActive(getApplicationContext(), content, Constant.ACTIVE_FILE_PATH)) {
            getSupportFragmentManager().beginTransaction().replace(R.id.active_rootlayout,
                    new ActivationSuccessFragment()).commitAllowingStateLoss();
        } else {
            getActivationInfo();
        }
    }

    public void onBack(View view) {
        getSupportFragmentManager().beginTransaction().replace(R.id.active_rootlayout, activationFragment)
                .commitAllowingStateLoss();
    }

    private void getActivationInfo() {
        if (!ActivationLoadingFragment.getInstance().isAdded())
            ActivationLoadingFragment.getInstance().show(getSupportFragmentManager(), "");
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("sign", SystemUtils.getSystemUUID(getApplicationContext()));
            jsonObject.put("vername", SystemUtils.getVersion(getApplicationContext()));
            jsonObject.put("osvername", SystemUtils.getSystemVersion());
            jsonObject.put("mac", SystemUtils.getMac(getApplicationContext()));
            jsonObject.put("sim", SystemUtils.getImei(getApplicationContext()));
            jsonObject.put("osid", SystemUtils.getAndroidId(getApplicationContext()));
            jsonObject.put("androidver", Build.VERSION.SDK_INT);
            jsonObject.put("iccid", SystemUtils.getIccid(getApplicationContext()));
            jsonObject.put("model", Build.MODEL);

            String uu = config.getUu();
            jsonObject.put("uu", SystemUtils.getFactoryCode(getApplicationContext())
                    + uu);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        LogUtils.i(tag,"企业激活="+jsonObject.toString());

        ServiceGenerator.createService(IActivationService.class)
                .activation(jsonObject.toString(), SystemUtils.getVersionCode(getApplicationContext()) + "",
                        SystemUtils.getVersion(getApplicationContext()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<AppActivation>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (timeoutFragment == null)
                            timeoutFragment = new ActivationTimeOutFragment();
                        getSupportFragmentManager().beginTransaction().replace(R.id.active_rootlayout,
                                timeoutFragment).commitAllowingStateLoss();
                        ActivationLoadingFragment.getInstance().dismissAllowingStateLoss();
                    }

                    @Override
                    public void onNext(AppActivation activation) {
                        Log.i(tag, "activation="+activation);
                        if (activation.getResult() == 1) {

                            config.setActiveKey(activation.getKey());
                            GreenDaoManager.getInstance(getApplicationContext()).getSession()
                                    .getConfigDao()
                                    .update(config);

                            FileUtils.writeToFile(Constant.ACTIVE_FILE_PATH, activation.getKey());
                            if (ActivationUtils.checkActive(getApplicationContext(), activation.getKey(),
                                    Constant.ACTIVE_FILE_PATH)) {
                                getSupportFragmentManager().beginTransaction().replace(R.id.active_rootlayout,
                                        new ActivationSuccessFragment()).commitAllowingStateLoss();
                                ActivationLoadingFragment.getInstance().dismissAllowingStateLoss();

                                //统计
                                HashMap<String,String> map = new HashMap<>();
                                map.put("active_type", "厂家激活");
                                map.put("active_channel", SystemUtils.getChannelFromVersion(getApplicationContext()));
                                MobclickAgent.onEvent(getApplicationContext(), "active", map);
                            } else {
                                if (errorFragment == null)
                                    errorFragment = new ActivationErrorFragment();
                                errorFragment.setCode(0);
                                getSupportFragmentManager().beginTransaction().replace(R.id.active_rootlayout,
                                        errorFragment).commitAllowingStateLoss();
                                ActivationLoadingFragment.getInstance().dismissAllowingStateLoss();
                            }
                        } else {
                            if (errorFragment == null)
                                errorFragment = new ActivationErrorFragment();
                            errorFragment.setCode(activation.getErr().getCode());
                            getSupportFragmentManager().beginTransaction().replace(R.id.active_rootlayout,
                                    errorFragment).commitAllowingStateLoss();
                            ActivationLoadingFragment.getInstance().dismissAllowingStateLoss();
                        }


                    }
                });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}
