package com.studio.dboard.ui.active.fragment;


import android.animation.ValueAnimator;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;

import com.studio.dboard.R;


public class ActivationLoadingFragment extends DialogFragment {


    private View icon_activated;

    public ActivationLoadingFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(android.support.v4.app.DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        setCancelable(false);
    }

    private static ActivationLoadingFragment instance;

    public static ActivationLoadingFragment getInstance() {
        if (instance == null)
            instance = new ActivationLoadingFragment();
        return instance;
    }

    @Override
    public void onStart() {
        super.onStart();
        WindowManager.LayoutParams lp = getDialog().getWindow().getAttributes();
        lp.dimAmount = 1f;
        getDialog().getWindow().setAttributes(lp);
        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.bg_default);
    }

    ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 360);

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_activation_loading, container, false);

        icon_activated = view.findViewById(R.id.icon_activated);

        valueAnimator.setRepeatCount(-1);
        valueAnimator.setDuration(1000);
        valueAnimator.setInterpolator(new LinearInterpolator());
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                icon_activated.setRotation((Float) valueAnimator.getAnimatedValue());
            }
        });

        valueAnimator.start();

        // Inflate the layout for this fragment
        return view;
    }


    @Override
    public void dismiss() {
        valueAnimator.cancel();
        ActivationLoadingFragment.super.dismiss();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().getAttributes().windowAnimations = R.style.dialogAnim;
        return dialog;
    }


}
