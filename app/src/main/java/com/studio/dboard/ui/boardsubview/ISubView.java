package com.studio.dboard.ui.boardsubview;

import android.view.View;

public interface ISubView {

    void inflateSubView();
    void findViews();
    void updateSpeedStyle(float scale);
    void updateNaviStyle(float scale);
    void updateSpeed(int speed);
    void updateNaviState(int iconIndex, String roadName, int distance);
    void updateExtraState(int extraState);
}
