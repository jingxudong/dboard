package com.studio.dboard.ui.setting.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.studio.dboard.Constant;
import com.studio.dboard.R;
import com.studio.dboard.dao.GreenDaoManager;
import com.studio.dboard.entity.Config;
import com.studio.dboard.ui.updatelog.LoglistActivity;
import com.studio.dboard.utils.ActivationUtils;
import com.studio.dboard.utils.SystemUtils;


/**
 * Created by peak on 2017/8/15.
 */

public class AboutFragment extends Fragment implements View.OnClickListener {

    private TextView versionName;
    private View logList;
    private TextView isActiveView;
    private View logo;
    private long[] mHits = new long[5];



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_about, container, false);
        versionName = (TextView) view.findViewById(R.id.version_name);
        String format = getString(R.string.version_name);
        String result = String.format(format, SystemUtils.getVersion(getContext()));
        versionName.setText(result);
        logList = view.findViewById(R.id.log_list);
        logList.setOnClickListener(this);
        isActiveView = (TextView) view.findViewById(R.id.is_active);

        Config config = GreenDaoManager.getInstance(getContext()).getSession()
                .getConfigDao()
                .load(0L);
        String content = config.getActiveKey();
        if (ActivationUtils.checkActive(getContext(), content, Constant.ACTIVE_FILE_PATH)){
            isActiveView.setText(getString(R.string.actived));
            isActiveView.setTextColor(getResources().getColor(R.color.actived));
        }else {
            isActiveView.setText(getString(R.string.unactive));
            isActiveView.setTextColor(getResources().getColor(R.color.unactive));
        }
        logo = view.findViewById(R.id.logo);
        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //每点击一次 实现左移一格数据
                System.arraycopy(mHits, 1, mHits, 0, mHits.length - 1);
                //给数组的最后赋当前时钟值
                mHits[mHits.length - 1] = SystemClock.uptimeMillis();
                //当0出的值大于当前时间-500时  证明在500秒内点击了数组长度的次数
                if(mHits[0] > SystemClock.uptimeMillis() - 5000){
//                    Intent intent = new Intent(getContext(), ToolsActivity.class);
//                    startActivity(intent);
                }
            }
        });
        return view;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.log_list){
            Intent intent = new Intent(getActivity(), LoglistActivity.class);
            startActivity(intent);
        }
    }
}
