package com.studio.dboard.ui.mainui;


import android.Manifest;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.studio.dboard.BoardService;
import com.studio.dboard.Constant;
import com.studio.dboard.INaviListener;
import com.studio.dboard.ISpeedListener;
import com.studio.dboard.ITimeListener;
import com.studio.dboard.IWeatherListener;
import com.studio.dboard.R;
import com.studio.dboard.allog.LogUtils;
import com.studio.dboard.dao.GreenDaoManager;
import com.studio.dboard.entity.Config;
import com.studio.dboard.ui.BoardlistActivity;
import com.studio.dboard.ui.SplashActivity;
import com.studio.dboard.ui.active.UserActivationActivity;
import com.studio.dboard.ui.boardsubview.BaseSubView;
import com.studio.dboard.ui.boardsubview.SubViewFactory;
import com.studio.dboard.ui.mainui.adapter.GuidePagerAdapter;
import com.studio.dboard.ui.BaseFSActivity;
import com.studio.dboard.utils.ActivationUtils;
import com.studio.dboard.utils.FileUtils;
import com.studio.dboard.utils.WeatherUtils;
import com.studio.dboard.view.IndicatorView;
import com.studio.dboard.view.dashboard.BaseBoardView;
import com.studio.dboard.view.dashboard.ContinueBoardView;
import com.watermark.androidwm_light.WatermarkBuilder;
import com.watermark.androidwm_light.bean.WatermarkImage;
import com.watermark.androidwm_light.bean.WatermarkText;
import java.io.IOException;
import java.io.InputStream;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;

@RuntimePermissions
public class BoardActivity extends BaseFSActivity implements GuidePagerAdapter.GuideListener,
        View.OnClickListener {

    private static final String tag = "BoardUI";

    private static final String tmpFontPath = "fonts/Helvetica_LT_35_Thin.ttf";

    private ImageView waterMark;
    private ViewGroup boardParent;
    private BaseBoardView boardView;
    private TextView tempuratureV;
    private ImageView weatherImgV;
    private TextView weatherStateV;
    private TextView cityV;
    private ViewGroup contentV;
    private BaseSubView subView;
    private View quickStartV;
    private View guideView;
    private View tip1;
    private View tip2;

    private ServiceConnection connection;
    private BoardService.BoardBinder mBinder;


    ISpeedListener speedListener = new ISpeedListener.Stub() {
        @Override
        public void onSpeed(final int speed) throws RemoteException {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (speed >= 0) {
                        boardView.setSpeed(speed);
                    }else{
                        boardView.setSpeed(0);
                    }
                    subView.startSpeedAnim(speed);
                }
            });
        }
    };

    IWeatherListener weatherListener = new IWeatherListener.Stub() {
        @Override
        public void onWeather(final String local, final String weatherState, final String temperature) throws RemoteException {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    setWeather(local, weatherState, temperature);
                }
            });

        }
    };

    INaviListener naviListener = new INaviListener.Stub() {
        @Override
        public void onNaviInfo(final int iconIndex, final int distance, final String roadName) throws RemoteException {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    setNaviInfo(iconIndex, distance, roadName);
                }
            });
        }

        @Override
        public void onNaviStateChanged(final int state) throws RemoteException {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    controlNaviVisible(state);
                }
            });
        }
    };

    ITimeListener timeListener = new ITimeListener.Stub() {
        @Override
        public void onTime(final int hour, final int min, final int second) throws RemoteException {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    setTime(hour, min, second);
                }
            });
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_board);
        config.setIsSpeedMock(false);
        GreenDaoManager.getInstance(getApplicationContext()).getSession()
                .getConfigDao().update(config);

        if (!config.getIsProcessStartBefore()) {
            startSplash();
        }else{
            BoardActivityPermissionsDispatcher.initViewWithPermissionCheck(this);
        }
    }

    private void startSplash(){
        Intent intent = new Intent(this, SplashActivity.class);
        intent.putExtra("activity", SplashActivity.ACTIVITY_BOARD_MAIN);
        startActivity(intent);
        finish();
    }

    private void init(){
        displayActiveButton();

        //关闭速度模拟，防止异常情况导致的无法关闭
        config.setIsSpeedMock(false);
        GreenDaoManager.getInstance(getApplicationContext())
                .getSession()
                .getConfigDao()
                .update(config);

        displayGuide();
    }

    /**
     * 是否显示激活按钮
     */
    private void displayActiveButton() {
        View floatView = findViewById(R.id.float_active_btn);
        boolean isActive = ActivationUtils.checkActive(getApplicationContext(), config.getActiveKey(),
                Constant.ACTIVE_FILE_PATH);
        if (isActive) {
            floatView.setVisibility(View.GONE);
        } else {
            floatView.setVisibility(View.VISIBLE);
        }
        floatView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BoardActivity.this, UserActivationActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    private boolean displayGuide(){
        boolean isFirstStart = config.getIsFirstStart();
        String activeKey = config.getActiveKey();

        //未激活状态下，第一次启动，显示引导页
        guideView = findViewById(R.id.guid_layout);
        boolean isActive = ActivationUtils.checkActive(getApplicationContext(), activeKey,
                Constant.ACTIVE_FILE_PATH);
        if (isFirstStart && !isActive) {
            guideView.setVisibility(View.VISIBLE);
            ViewPager guidePager = (ViewPager) findViewById(R.id.guidPager);
            final IndicatorView guidIndicator = (IndicatorView) findViewById(R.id.guid_indicator);
            final GuidePagerAdapter guidAdapter = new GuidePagerAdapter(this);
            guidAdapter.setListener(this);
            guidePager.setAdapter(guidAdapter);
            guidePager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    float offset = position * 1.0f / (guidAdapter.getCount() - 1);
                    guidIndicator.setPosition(offset);
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
            return true;
        }else {
            guideView.setVisibility(View.GONE);
            return false;
        }
    }

    @Override
    public void onGuideDismiss() {
        config.setIsFirstStart(false);
        GreenDaoManager.getInstance(getApplicationContext()).getSession()
                .getConfigDao().update(config);

        jump2BoardList();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        BoardActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }


    @NeedsPermission({Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.READ_PHONE_STATE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE})
    void initView() {
        waterMark = findViewById(R.id.water_mark);
        boardParent = findViewById(R.id.board_parent);
        contentV = findViewById(R.id.content);
        subView = SubViewFactory.buildSubView(getApplicationContext(), contentV);
        weatherImgV = findViewById(R.id.weather_img);
        weatherStateV = findViewById(R.id.weather_state);
        cityV = findViewById(R.id.weather_location);
        tempuratureV = findViewById(R.id.temperature);
        Typeface typeface = Typeface.createFromAsset(getAssets(), tmpFontPath);
        tempuratureV.setTypeface(typeface);
        quickStartV = findViewById(R.id.quick_start);
        quickStartV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                quickStartV.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.gradually_out));
                quickStartV.setVisibility(View.GONE);
            }
        });
        View dboardSet = findViewById(R.id.dboard_setting);
        dboardSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jump2BoardList();
            }
        });


        tip1 = findViewById(R.id.tip_1);
        if(!config.getIsDisTips()){
            tip1.postDelayed(new Runnable() {
                @Override
                public void run() {
                    tip1.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.gradually_in));
                    tip1.setVisibility(View.VISIBLE);
                }
            }, 1000);
        }else{
            tip1.setVisibility(View.GONE);
        }
        TextView tipTitle1V = findViewById(R.id.tip_title_1);
        String title1 = getString(R.string.click_anywhere_into_set);
        SpannableStringBuilder spannable1 = new SpannableStringBuilder(title1);
        spannable1.setSpan(new ForegroundColorSpan(Color.parseColor("#f6ff00")), 0, 2,
                Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        spannable1.setSpan(new ForegroundColorSpan(Color.parseColor("#ffffff")), 2,
                title1.length(),
                Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        tipTitle1V.setText(spannable1);

        tip2 = findViewById(R.id.tip_2);
        TextView tipTitle2V = findViewById(R.id.tip_title_2);
        String title2 = getString(R.string.long_click_exit);
        SpannableStringBuilder spannable2 = new SpannableStringBuilder(title2);
        spannable2.setSpan(new ForegroundColorSpan(Color.parseColor("#f6ff00")), 0, 2,
                Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        spannable2.setSpan(new ForegroundColorSpan(Color.parseColor("#ffffff")), 2,
                title2.length(),
                Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        tipTitle2V.setText(spannable2);

        tip1.setOnClickListener(this);
        tip2.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        init();
        buildBoardView();
        bindBoardService();
    }

    private void buildBoardView(){
        boardView = new ContinueBoardView(getApplicationContext());
        boardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                quickStartV.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.gradually_in));
                quickStartV.setVisibility(View.VISIBLE);
            }
        });

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        boardView.setLayoutParams(params);
        boardParent.removeAllViews();
        boardParent.addView(boardView);
        boardView.onResume();
    }

    /**
     * 跳转到仪表列表界面
     */
    private void jump2BoardList(){
        Intent intent = new Intent(BoardActivity.this, BoardlistActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.anim_right_in, R.anim.anim_left_out);
        finish();
    }

    private void bindBoardService(){
        connection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                LogUtils.i(tag, "onServiceConnected");
                mBinder = (BoardService.BoardBinder) service;
                if (mBinder != null) {
                    mBinder.registerSpeedListener(speedListener);
                    mBinder.registerWeatherListener(weatherListener);
                    mBinder.registerNaviListener(naviListener);
                    mBinder.registerTimeListener(timeListener);
                    mBinder.queryWeather();
                }
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {

            }
        };
        Intent intent = new Intent(this, BoardService.class);
        bindService(intent, connection, BIND_AUTO_CREATE);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        LogUtils.i(tag, "onWindowFocusChanged="+hasFocus);
        if (hasFocus){
            checkBottomBar();
            updateSubViewParams();
            buildWaterMark();
        }

        boardView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                onBackPressed();
                return true;
            }
        });
    }

    private void checkBottomBar(){
        if (config.getIsFullScreen()) {
            int threshold = 900 * contentV.getWidth() / 768;
            if (contentV.getHeight() > threshold){
                View placeHolderV = findViewById(R.id.placeholder);
                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) placeHolderV.getLayoutParams();
                params.height = contentV.getHeight() - threshold + 10;
                placeHolderV.setLayoutParams(params);
                placeHolderV.requestLayout();
                placeHolderV.setVisibility(View.VISIBLE);

                View bottomMask = findViewById(R.id.bottom_mask);
                bottomMask.setVisibility(View.VISIBLE);
            }
        }
    }

    private void updateSubViewParams(){
        int width = contentV.getWidth();
        int height = contentV.getHeight();
        float scale = Math.min(width * 1.0f / Constant.STANDARD_WIDTH,
                height * 1.0f / Constant.STANDARD_HEIGHT);
        subView.updateSpeedStyle(scale);
    }

    private void buildWaterMark(){

        boolean isActive = ActivationUtils.checkActive(getApplicationContext(), config.getActiveKey(),
                Constant.ACTIVE_FILE_PATH);
        if (!isActive){
            waterMark.setVisibility(View.VISIBLE);
            InputStream inputStream = null;
            try {
                inputStream = getAssets().open("perm/encrypt.mark");
            } catch (IOException e) {
                e.printStackTrace();
            }
            boolean isNeedMarkText = false;
            if (inputStream != null){
                Bitmap bitmap = FileUtils.aesDecryptBitmap(inputStream);

                if (bitmap != null){
                    try {
                        WatermarkImage watermarkImage = new WatermarkImage(bitmap)
                                .setSize(0.333)
                                .setImageAlpha(255);
                        WatermarkBuilder
                                .create(getApplicationContext(), waterMark)
                                .loadWatermarkImage(watermarkImage)
                                .setTileMode(true)
                                .getWatermark()
                                .setToImageView(waterMark);
                        bitmap.recycle();
                    }catch (Exception e){
                        e.printStackTrace();
                        isNeedMarkText = true;
                    }
                }else {
                    isNeedMarkText = true;
                }
            }else {
                isNeedMarkText = true;
            }

            if (isNeedMarkText){
                WatermarkText watermarkText = new WatermarkText("试用版请激活")
                        .setPositionX(0.5)
                        .setPositionY(0.5)
                        .setTextColor(Color.WHITE)
                        .setTextShadow(0.1f,
                                3,
                                3,
                                Color.parseColor("#80000000"))
                        .setTextAlpha(150)
                        .setRotation(-45)
                        .setTextSize(35);
                WatermarkBuilder
                        .create(getApplicationContext(), waterMark)
                        .loadWatermarkText(watermarkText) // use .loadWatermarkImage(watermarkImage) to load an image.
                        .setTileMode(true)
                        .getWatermark()
                        .setToImageView(waterMark);
            }
        }else{
            waterMark.setVisibility(View.GONE);
        }
    }

    private void setWeather(String local, String weatherState, String temperature){
        if (!TextUtils.isEmpty(local) && !TextUtils.isEmpty(weatherState)) {
            Bitmap bitmap = WeatherUtils.getWeatherImage(getApplicationContext(), weatherState);
            weatherImgV.setImageBitmap(bitmap);
            tempuratureV.setText(temperature);
            weatherStateV.setText(weatherState);
            cityV.setText(local);
            boardView.setWeather(weatherState);
        }
    }

    private void setNaviInfo(int iconIndex, int distance, String roadName){
        subView.updateNaviState(iconIndex, roadName, distance);
    }

    private void controlNaviVisible(int state){
        subView.updateExtraState(state);
    }

    private void setTime(int hour, int min, int second){
        boardView.setTime(hour, min, second);
    }


    @Override
    protected void onPause() {
        super.onPause();
        boardView.onPause();
        if (mBinder != null){
            mBinder.unregisterSpeedListener(speedListener);
            mBinder.unregisterWeatherListener(weatherListener);
            mBinder.unregisterNaviListener(naviListener);
            mBinder.unregisterTimeListener(timeListener);
        }
        unbindService(connection);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tip_1){
            tip1.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.anim_left_out));
            tip1.setVisibility(View.GONE);
            tip2.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.anim_right_in));
            tip2.setVisibility(View.VISIBLE);
        }else if(v.getId() == R.id.tip_2){
            tip2.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.gradually_out));
            tip2.setVisibility(View.GONE);
            config.setIsDisTips(true);
            GreenDaoManager.getInstance(getApplicationContext()).getSession()
                    .getConfigDao()
                    .update(config);
        }
    }


}
