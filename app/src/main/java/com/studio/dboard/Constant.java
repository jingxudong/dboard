package com.studio.dboard;

import android.os.Environment;

import java.io.File;

public class Constant {

    public static final int STANDARD_WIDTH = 768;
    public static final int STANDARD_HEIGHT = 890;

    //自身文件夹根目录
    public static final String ROOT_DIR = "DBoard/";

    //广播等action 根字符串，例如：com.studio.dboard.ACTION_TEST
    public static final String ROOT_ACTION = "com.studio.dboard.";

    public static final String UPDATE_DIR = Environment.getExternalStorageDirectory().getPath()
            + File.separator + ROOT_DIR + "update/";
    public static final String ACTIVE_FILE_PATH = Environment.getExternalStorageDirectory().getPath()
            + File.separator + ROOT_DIR + "perm/key";
    public static final String IMG_DIR = Environment.getExternalStorageDirectory().getPath()
            + File.separator + ROOT_DIR + "image/";
    public static final String WEATHER_ASSETS_DIR = "weather";
    public static final String WEATHER_ASSETS_IMG_DIR = "weather/img";

    //阿里LOG系统应用服务地址
    public static final String LOG_SERVER = "http://auth1.xzhushou.cn/";

    //内网(局域网)测试
    private static final String DEBUG_SERVER_LAN_IP = "192.168.230.200/";

    //正式服务
    private static final String RELEASE_SERVER_IP = "ws2.xzhushou.cn/";

    //外网测试
    private static final String DEBUG_SERVER_OUT_IP = "123.57.25.103/";
//    private static final String DEBUG_SERVER_OUT_IP = "140.143.247.126/";


    //    public static final String BASE_SERVER_IP = BuildConfig.DEBUG ? DEBUG_SERVER_LAN_IP : RELEASE_SERVER_IP;
    public static final String BASE_SERVER_IP = RELEASE_SERVER_IP;
//    public static final String BASE_SERVER_IP = DEBUG_SERVER_LAN_IP;
//    public static final String BASE_SERVER_IP = DEBUG_SERVER_OUT_IP;


    public static final String BASE_SERVER_URL = "http://" + BASE_SERVER_IP;
    public static final String SERVER_URL = BASE_SERVER_URL + "XStewardWeb/";
    public static final String WEB_SERVER_URL = "ws://" + BASE_SERVER_IP + "XStewardWeb/";
}
