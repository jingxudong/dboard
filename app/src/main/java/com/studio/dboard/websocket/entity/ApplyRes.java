package com.studio.dboard.websocket.entity;


public class ApplyRes extends BaseWsModel {

    private String applysign;
    private String qrcode;

    public String getApplysign() {
        return applysign;
    }

    public void setApplysign(String applysign) {
        this.applysign = applysign;
    }

    public String getQrcode() {
        return qrcode;
    }

    public void setQrcode(String qrcode) {
        this.qrcode = qrcode;
    }
}
