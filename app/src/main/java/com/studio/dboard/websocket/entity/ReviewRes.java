package com.studio.dboard.websocket.entity;

public class ReviewRes extends BaseWsModel {

    public static final int STATUS_WAIT_SCAN = 1;
    public static final int STATUS_APPLY_WAIT = 2;
    public static final int STATUS_APPLY_THROUGHED = 3;
    public static final int STATUS_APPLY_REFUSED = 4;
    public static final int STATUS_APPLY_CLOSED = 5;

    private int status;
    private String desc;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public String toString() {
        return "ReviewRes{" +
                "status=" + status +
                ", desc='" + desc + '\'' +
                '}';
    }
}
