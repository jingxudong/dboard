package com.studio.dboard.websocket.entity;


public class BaseWsModel {

    public static final String MSG_TYPE_APPLY = "apply_result";
    public static final String MSG_TYPE_REWIEW = "review_result";

    private String msgtype;
    private int result;
    private ErrBean err;

    public String getMsgtype() {
        return msgtype;
    }

    public void setMsgtype(String msgtype) {
        this.msgtype = msgtype;
    }

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public ErrBean getErr() {
        return err;
    }

    public void setErr(ErrBean err) {
        this.err = err;
    }

    public static class ErrBean {
        /**
         * code : 错误code
         * info : 错误信息
         */

        private int code;
        private String info;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getInfo() {
            return info;
        }

        public void setInfo(String info) {
            this.info = info;
        }

        @Override
        public String toString() {
            return "ErrBean{" +
                    "code=" + code +
                    ", info='" + info + '\'' +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "BaseModel{" +
                "result=" + result +
                ", err=" + err +
                '}';
    }
}
