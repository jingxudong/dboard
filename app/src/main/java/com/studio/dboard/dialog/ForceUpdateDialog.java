package com.studio.dboard.dialog;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import com.studio.dboard.R;


/**
 * Created by peak on 2018/3/8.
 */

public class ForceUpdateDialog extends BaseUpdateDialog {

    private TextView titleV;
    private TextView updateLogV;
    private View nowUpdate;
    private View exitV;

    private String title = "";
    private String content = "";

    private View.OnClickListener positiveListener;
    private View.OnClickListener negtiveListener;

    public ForceUpdateDialog(@NonNull Context context) {
        super(context);
    }

    public ForceUpdateDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    protected ForceUpdateDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCanceledOnTouchOutside(false);
    }

    @Override
    protected int getResLayoutId() {
        return R.layout.update_force;
    }

    @Override
    protected void initView() {
        titleV = (TextView) findViewById(R.id.update_title);
        updateLogV = (TextView) findViewById(R.id.update_log);
        nowUpdate = findViewById(R.id.update_now);
        exitV = findViewById(R.id.exit);

        titleV.setText(title);
        updateLogV.setText(content);
        if (positiveListener != null) {
            nowUpdate.setOnClickListener(positiveListener);
        }
        if (negtiveListener != null) {
            exitV.setOnClickListener(negtiveListener);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, @NonNull KeyEvent event) {
        return super.onKeyDown(keyCode, event);
    }

    public void setPositiveListener(View.OnClickListener listener){
        this.positiveListener = listener;
    }

    public void setNegtiveListener(View.OnClickListener listener){
        this.negtiveListener = listener;
    }

    public void setContent(String content){
        this.content = content;
    }

    public void setTitle(String title){
        this.title = title;
    }
}
