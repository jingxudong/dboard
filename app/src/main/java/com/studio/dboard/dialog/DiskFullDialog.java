package com.studio.dboard.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.studio.dboard.R;

public class DiskFullDialog extends BaseUpdateDialog {

    private View.OnClickListener confirmListener;

    public DiskFullDialog(@NonNull Context context) {
        super(context);
    }

    public DiskFullDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    protected DiskFullDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCanceledOnTouchOutside(false);
    }

    @Override
    protected int getResLayoutId() {
        return R.layout.dialog_disk_full;
    }

    @Override
    protected void initView() {
        View confirmV = findViewById(R.id.confirm);
        confirmV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (confirmListener != null){
                    confirmListener.onClick(v);
                }
                dismiss();
            }
        });
    }

    public void setConfirmListener(View.OnClickListener confirmListener) {
        this.confirmListener = confirmListener;
    }
}
