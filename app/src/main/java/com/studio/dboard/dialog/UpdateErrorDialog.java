package com.studio.dboard.dialog;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import com.studio.dboard.R;


/**
 * Created by peak on 2018/3/9.
 */

public class UpdateErrorDialog extends BaseUpdateDialog {

    private View retryDownload;
    private View btnClose;

    private View.OnClickListener positiveListener;
    private View.OnClickListener negtiveListener;

    public UpdateErrorDialog(@NonNull Context context) {
        super(context);
    }

    public UpdateErrorDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    protected UpdateErrorDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCanceledOnTouchOutside(false);
    }

    @Override
    protected int getResLayoutId() {
        return R.layout.update_error;
    }

    @Override
    protected void initView() {
        retryDownload = findViewById(R.id.update_retry);
        btnClose = findViewById(R.id.btn_close);

        if (positiveListener != null){
            retryDownload.setOnClickListener(positiveListener);
        }

        if (negtiveListener != null){
            btnClose.setOnClickListener(negtiveListener);
        }
    }

    public void setPositiveListener(View.OnClickListener positiveListener) {
        this.positiveListener = positiveListener;
    }

    public void setNegtiveListener(View.OnClickListener negtiveListener) {
        this.negtiveListener = negtiveListener;
    }
}
