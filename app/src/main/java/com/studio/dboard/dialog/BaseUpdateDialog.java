package com.studio.dboard.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.Window;
import android.view.WindowManager;

public abstract class BaseUpdateDialog extends Dialog {


    public BaseUpdateDialog(@NonNull Context context) {
        super(context);
    }

    public BaseUpdateDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    protected BaseUpdateDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow().setDimAmount(0.9f);
        getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        setContentView(getResLayoutId());
        initView();
    }

    protected abstract int getResLayoutId();

    protected abstract void initView();


}
