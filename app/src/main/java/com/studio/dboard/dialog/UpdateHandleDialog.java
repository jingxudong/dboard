package com.studio.dboard.dialog;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.studio.dboard.R;


/**
 * Created by peak on 2018/3/8.
 */

public class UpdateHandleDialog extends BaseUpdateDialog {

    private TextView titleV;
    private TextView updateLogV;
    private View nowUpdate;
    private View btnClose;

    private String title = "";
    private String content = "";

    private View.OnClickListener positiveListener;
    private View.OnClickListener negtiveListener;
    private View.OnClickListener skipListener;

    public UpdateHandleDialog(@NonNull Context context) {
        super(context);
    }

    public UpdateHandleDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    protected UpdateHandleDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCanceledOnTouchOutside(false);
    }

    @Override
    protected int getResLayoutId() {
        return R.layout.update_dialog_2;
    }

    @Override
    protected void initView() {
        titleV = (TextView) findViewById(R.id.update_title);
        updateLogV = (TextView) findViewById(R.id.update_log);
        nowUpdate = findViewById(R.id.update_now);
        btnClose = findViewById(R.id.btn_close);

        titleV.setText(title);
        updateLogV.setText(content);
        if (positiveListener != null) {
            nowUpdate.setOnClickListener(positiveListener);
        }
        if (negtiveListener != null) {
            btnClose.setOnClickListener(negtiveListener);
        }
    }

    public void setPositiveListener(View.OnClickListener listener){
        this.positiveListener = listener;
    }

    public void setNegtiveListener(View.OnClickListener listener){
        this.negtiveListener = listener;
    }

    public void setContent(String content){
        this.content = content;
    }

    public void setTitle(String title){
        this.title = title;
    }
}
