package com.studio.dboard.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.Window;
import com.studio.dboard.R;

public class MoreTipDialog extends Dialog implements View.OnClickListener {


    private OnKnowListener listener;

    public MoreTipDialog setListener(OnKnowListener listener) {
        this.listener = listener;
        return this;
    }

    public MoreTipDialog(@NonNull Context context) {
        super(context);
    }

    public MoreTipDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    protected MoreTipDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow().setDimAmount(0.9f);
        setContentView(R.layout.dialog_more_tip);
        initView();
    }

    private void initView() {
        View iKnow = findViewById(R.id.i_know);
        iKnow.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        dismiss();
        if (listener != null){
            listener.onKnow();
        }
    }

    public interface OnKnowListener{
        void onKnow();
    }
}
