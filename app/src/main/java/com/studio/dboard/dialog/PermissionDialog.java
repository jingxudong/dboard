package com.studio.dboard.dialog;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.studio.dboard.R;

public class PermissionDialog extends DialogFragment {

    private OnSetListener setListener;

    public void setSetListener(OnSetListener setListener) {
        this.setListener = setListener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCanceledOnTouchOutside(false);
        getDialog().getWindow().setDimAmount(0.9f);
        View view = LayoutInflater.from(getContext()).inflate(R.layout.dialog_permission_require,
                container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        View confirmV = view.findViewById(R.id.confirm);
        confirmV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissAllowingStateLoss();

                if (setListener != null){
                    setListener.onSetUsagePerm();
                }
            }
        });
    }

    public interface OnSetListener{
        void onSetUsagePerm();
    }
}
