package com.studio.dboard.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.Window;

import com.studio.dboard.R;


/**
 * Created by peak on 2018/3/8.
 */

public class ServiceTermsDialog extends Dialog implements View.OnClickListener {

    private View iKnow;

    public ServiceTermsDialog(@NonNull Context context) {
        super(context);
    }

    public ServiceTermsDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    protected ServiceTermsDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow().setDimAmount(0.9f);
        setContentView(R.layout.dialog_service_terms);
        initView();
    }

    private void initView(){
        iKnow = findViewById(R.id.i_known);
        iKnow.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.i_known){
            this.dismiss();
        }
    }
}
