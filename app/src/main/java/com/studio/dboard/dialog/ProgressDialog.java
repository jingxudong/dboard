package com.studio.dboard.dialog;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ProgressBar;

import com.studio.dboard.R;


/**
 * Created by peak on 2018/3/9.
 */

public class ProgressDialog extends BaseUpdateDialog {

    private ProgressBar progressBar;
    private View btnClose;

    private View.OnClickListener negtiveListener;

    public ProgressDialog(@NonNull Context context) {
        super(context);
    }

    public ProgressDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    protected ProgressDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCanceledOnTouchOutside(false);
    }

    @Override
    protected int getResLayoutId() {
        return R.layout.update_progress;
    }

    @Override
    protected void initView() {
        progressBar = (ProgressBar) findViewById(R.id.progress);
        btnClose = findViewById(R.id.btn_close);
        if (negtiveListener != null){
            btnClose.setOnClickListener(negtiveListener);
        }
    }

    public void setProgress(int progress){
        if (progressBar != null){
            progressBar.setProgress(progress);
        }
    }

    public void setNegtiveListener(View.OnClickListener negtiveListener) {
        this.negtiveListener = negtiveListener;
    }
}
