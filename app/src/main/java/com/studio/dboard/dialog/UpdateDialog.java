package com.studio.dboard.dialog;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.studio.dboard.R;


/**
 * Created by peak on 2018/3/8.
 */

public class UpdateDialog extends BaseUpdateDialog {

    private TextView titleV;
    private TextView updateLogV;
    private View nowUpdate;
    private View btnClose;
    private View skipUpdate;

    private String title = "";
    private String content = "";

    private View.OnClickListener positiveListener;
    private View.OnClickListener negtiveListener;
    private View.OnClickListener skipListener;

    public UpdateDialog(@NonNull Context context) {
        super(context);
    }

    public UpdateDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    protected UpdateDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCanceledOnTouchOutside(false);
    }

    @Override
    protected int getResLayoutId() {
        return R.layout.update_dialog;
    }

    @Override
    protected void initView() {
        titleV = (TextView) findViewById(R.id.update_title);
        updateLogV = (TextView) findViewById(R.id.update_log);
        nowUpdate = findViewById(R.id.update_now);
        btnClose = findViewById(R.id.btn_close);
        skipUpdate = findViewById(R.id.exit);

        titleV.setText(title);
        updateLogV.setText(content);
        if (positiveListener != null) {
            nowUpdate.setOnClickListener(positiveListener);
        }
        if (negtiveListener != null) {
            btnClose.setOnClickListener(negtiveListener);
        }
        if (skipListener != null){
            skipUpdate.setOnClickListener(skipListener);
        }
    }

    public void setPositiveListener(View.OnClickListener listener){
        this.positiveListener = listener;
    }

    public void setNegtiveListener(View.OnClickListener listener){
        this.negtiveListener = listener;
    }

    public void setSkipListener(View.OnClickListener skipListener) {
        this.skipListener = skipListener;
    }

    public void setContent(String content){
        this.content = content;
    }

    public void setTitle(String title){
        this.title = title;
    }
}
