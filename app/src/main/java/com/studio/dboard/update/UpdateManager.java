package com.studio.dboard.update;

/**
 * Created by peak on 2017/11/22.
 */

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Toast;
import com.studio.dboard.Constant;
import com.studio.dboard.R;
import com.studio.dboard.dao.GreenDaoManager;
import com.studio.dboard.dialog.DiskFullDialog;
import com.studio.dboard.dialog.ForceUpdateDialog;
import com.studio.dboard.dialog.ProgressDialog;
import com.studio.dboard.dialog.UpdateDialog;
import com.studio.dboard.dialog.UpdateErrorDialog;
import com.studio.dboard.dialog.UpdateHandleDialog;
import com.studio.dboard.entity.Config;
import com.studio.dboard.retrofit.ServiceGenerator;
import com.studio.dboard.retrofit.bean.Update;
import com.studio.dboard.retrofit.webapi.ICheckUpdateService;
import com.studio.dboard.retrofit.webapi.IUpdateService;
import com.studio.dboard.utils.StorageUtils;
import com.studio.dboard.utils.SystemUtils;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class UpdateManager {

    private static final String tag = "UpdateManager";

    private static final int DOWN_START = 1;
    private static final int DOWN_UPDATE = 2;
    private static final int DOWN_OVER = 3;
    private static final int DOWN_ERROR = 4;
    private static final int DISK_FULL = 5;

    private static final String saveFileName = Constant.UPDATE_DIR + "xui_update.apk";

    private Context mContext;
    private Update curUpdate;
    private DownloadTask mdownApkRunnable;
    private ProgressDialog progressDialog;
    private UpdateDialog updateDialog;
    private UpdateHandleDialog updateHandleDialog;
    private UpdateErrorDialog updateErrorDialog;
    private DiskFullDialog diskFullDialog;
    private ForceUpdateDialog forceUpdateDialog;


    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case DOWN_START:
                    progressDialog.setProgress(0);
                    break;
                case DOWN_UPDATE:
                    int progress = msg.arg1;
                    progressDialog.setProgress(progress);
                    break;
                case DOWN_OVER:
                    progressDialog.dismiss();
                    installApk();
                    break;
                case DOWN_ERROR:
                    progressDialog.dismiss();
                    showErrorDialog();
                    break;
                case DISK_FULL:
                    progressDialog.dismiss();
                    showDiskFullDialog();
                    break;
                default:
                    break;
            }
        };
    };

    public UpdateManager(Context context) {
        this.mContext = context;
    }

    private void showDiskFullDialog(){
        if (diskFullDialog == null){
            diskFullDialog = new DiskFullDialog(mContext);
            diskFullDialog.setConfirmListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (curUpdate.getUpdtype() == 2){
                        System.exit(0);
                    }
                }
            });
            if (!diskFullDialog.isShowing()) {
                diskFullDialog.show();
            }
        }
    }

    private void showNoticeDialog() {
        if (updateDialog == null) {
            updateDialog = new UpdateDialog(mContext);
            updateDialog.setPositiveListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showDownloadDialog();
                    updateDialog.dismiss();
                }
            });
            updateDialog.setNegtiveListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    updateDialog.dismiss();
                }
            });
            updateDialog.setSkipListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Config config = GreenDaoManager.getInstance(mContext).getSession()
                            .getConfigDao().load(0L);
                    config.setSkipVerCode(curUpdate.getNvc());
                    updateDialog.dismiss();
                }
            });
        }

        String title = String.format(mContext.getString(R.string.found_new_version), curUpdate.getNv());
        updateDialog.setTitle(title);
        String desc = curUpdate.getDesc();
        String content = "";
        if (!TextUtils.isEmpty(desc)) {
            content = desc.replace("\\n", "\n");
        }
        updateDialog.setContent(content);

        if (!updateDialog.isShowing()) {
            updateDialog.show();
        }
    }

    private void showForceNoticeDialog(){
        if (forceUpdateDialog == null){
            forceUpdateDialog = new ForceUpdateDialog(mContext);
            forceUpdateDialog.setPositiveListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showDownloadDialog();
                    forceUpdateDialog.dismiss();
                }
            });
            forceUpdateDialog.setNegtiveListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    forceUpdateDialog.dismiss();
                    System.exit(0);
                }
            });
            forceUpdateDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                    return true;
                }
            });
        }
        String title = String.format(mContext.getString(R.string.found_new_version), curUpdate.getNv());
        forceUpdateDialog.setTitle(title);
        String desc = curUpdate.getDesc();
        String content = "";
        if (!TextUtils.isEmpty(desc)) {
            content = desc.replace("\\n", "\n");
        }
        forceUpdateDialog.setContent(content);

        if (!forceUpdateDialog.isShowing()) {
            forceUpdateDialog.show();
        }
    }

    private void showHandleNoticeDialog() {
        if (updateHandleDialog == null) {
            updateHandleDialog = new UpdateHandleDialog(mContext);
            updateHandleDialog.setPositiveListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showDownloadDialog();
                    updateHandleDialog.dismiss();
                }
            });
            updateHandleDialog.setNegtiveListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    updateHandleDialog.dismiss();
                }
            });
        }

        String title = String.format(mContext.getString(R.string.found_new_version), curUpdate.getNv());
        updateHandleDialog.setTitle(title);
        String desc = curUpdate.getDesc();
        String content = "";
        if (!TextUtils.isEmpty(desc)) {
            content = desc.replace("\\n", "\n");
        }
        updateHandleDialog.setContent(content);

        if (!updateHandleDialog.isShowing()) {
            updateHandleDialog.show();
        }
    }

    private void showDownloadDialog() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(mContext);
            progressDialog.setNegtiveListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mdownApkRunnable.setInterceptFlag(true);
                    progressDialog.dismiss();
                    if (curUpdate.getUpdtype() == 2){
                        System.exit(0);
                    }
                }
            });
            progressDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                    mdownApkRunnable.setInterceptFlag(true);
                    progressDialog.dismiss();
                    if (curUpdate.getUpdtype() == 2){
                        System.exit(0);
                    }
                    return true;
                }
            });
        }

        if (!progressDialog.isShowing()) {
            progressDialog.setProgress(0);
            progressDialog.show();
        }

        downloadApk();
    }

    private void showErrorDialog(){
        if (updateErrorDialog == null) {
            updateErrorDialog = new UpdateErrorDialog(mContext);
            updateErrorDialog.setPositiveListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    updateErrorDialog.dismiss();
                    showDownloadDialog();
                }
            });
            updateErrorDialog.setNegtiveListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    updateErrorDialog.dismiss();
                    if (curUpdate.getUpdtype() == 2){
                        System.exit(0);
                    }
                }
            });
            updateErrorDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                    updateErrorDialog.dismiss();
                    if (curUpdate.getUpdtype() == 2){
                        System.exit(0);
                    }
                    return true;
                }
            });
        }

        if (!updateErrorDialog.isShowing()) {
            updateErrorDialog.show();
        }
    }

    class DownloadTask implements Runnable {

        private boolean interceptFlag = false;

        public void setInterceptFlag(boolean interceptFlag){
            this.interceptFlag = interceptFlag;
        }

        @Override
        public void run() {
            try {
                URL url = new URL(curUpdate.getFurl());

                HttpURLConnection conn = (HttpURLConnection) url
                        .openConnection();
                conn.connect();
                int totalSize = conn.getContentLength();
                if(!StorageUtils.isSdSizeAvailable(mContext, totalSize / 1024 / 1024)){
                    mHandler.sendEmptyMessage(DISK_FULL);
                    return;
                }

                mHandler.sendEmptyMessage(DOWN_START);
                InputStream is = conn.getInputStream();

                File file = new File(Constant.UPDATE_DIR);
                if (!file.exists()) {
                    file.mkdirs();
                }

                File downFile = new File(saveFileName);
                if (downFile.exists()){
                    if (curUpdate.getFsize() == downFile.length()
                            && curUpdate.getNvc() == SystemUtils.getApkVersionCode(mContext, saveFileName)){
                        mHandler.sendEmptyMessage(DOWN_OVER);
                        return;
                    }else {
                        downFile.delete();
                    }
                }

                String tempPath = saveFileName + ".temp";
                File tempFile = new File(tempPath);
                FileOutputStream fos = new FileOutputStream(tempFile);

                int count = 0;
                int progress;
                float percent;
                byte buf[] = new byte[4 * 1024];

                do {
                    int numread = is.read(buf);
                    if (numread == -1) {
                        tempFile.renameTo(downFile);
                        // 下载完成通知安装
                        mHandler.sendEmptyMessage(DOWN_OVER);
                        break;
                    }
                    fos.write(buf, 0, numread);

                    count += numread;
                    percent = (float) count / totalSize;
                    progress = (int) Math.ceil(percent * 100);
                    // 更新进度
                    Message msg = mHandler.obtainMessage();
                    msg.what = DOWN_UPDATE;
                    msg.arg1 = progress;
                    mHandler.sendMessage(msg);

                } while (!interceptFlag);// 点击取消就停止下载.

                fos.close();
                is.close();
            } catch (Exception e) {
                e.printStackTrace();
                mHandler.sendEmptyMessage(DOWN_ERROR);
            }
        }
    }

    /**
     * 下载apk
     *
     */

    private void downloadApk() {
        if (mdownApkRunnable != null){
            mdownApkRunnable.setInterceptFlag(true);
        }
        mdownApkRunnable = new DownloadTask();
        Thread downLoadThread = new Thread(mdownApkRunnable);
        downLoadThread.start();
    }

    /**
     * 安装apk
     *
     */
    private void installApk() {
        File apkfile = new File(saveFileName);
        if (!apkfile.exists()) {
            return;
        }
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setDataAndType(Uri.fromFile(apkfile),
                "application/vnd.android.package-archive");
        mContext.startActivity(intent);

        if (curUpdate.getUpdtype() == 2){
            System.exit(0);
        }
    }

    private String formatSize(int size){
        DecimalFormat df = new DecimalFormat(".00");
        if (size < 1024) {
            return size + "B";
        }else {
            float kb = size / 1024f;
            if (kb < 1024) {
                return df.format(kb) + "KB";
            } else {
                float mb = kb / 1024;
                if (mb < 1024) {
                    return df.format(mb) + "MB";
                } else {
                    float gb = mb / 1024;
                    return df.format(gb) + "GB";
                }
            }
        }
    }

    public void checkUpdate(){
        ServiceGenerator.createService(IUpdateService.class)
                .update(SystemUtils.getVersionCode(mContext) + "", SystemUtils.getVersion(mContext))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Update>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(Update update) {
                        Log.i(tag, "update="+update);
                        curUpdate = update;
                        if (update.getResult() == 1) {
                            if (update.getUpdtype() == 2){
                                showForceNoticeDialog();
                            }else if (update.getUpdtype() == 1){
                                try {
                                    Config config = GreenDaoManager.getInstance(mContext).getSession()
                                            .getConfigDao().load(0L);
                                    if (update.getNvc() > config.getSkipVerCode()) {
                                        showNoticeDialog();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                });

    }

    public void checkHandleUpdate(){
        ServiceGenerator.createService(ICheckUpdateService.class)
                .update(SystemUtils.getVersionCode(mContext) + "", SystemUtils.getVersion(mContext))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Update>() {
                    @Override
                    public void onCompleted() {
                        Log.i(tag, "onCompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        Toast.makeText(mContext, mContext.getText(R.string.check_update_error)
                                        .toString(),
                                Toast.LENGTH_LONG).show();

                    }

                    @Override
                    public void onNext(Update update) {
                        curUpdate = update;
                        if (update.getResult() == 1) {
                            if (update.getUpdtype() == 2){
                                showForceNoticeDialog();
                            }else if (update.getUpdtype() == 1){
                                showHandleNoticeDialog();
                            }else if (update.getUpdtype() == 0){
                                Toast.makeText(mContext, mContext.getText(R.string.already_newest_version),
                                        Toast.LENGTH_LONG).show();
                            }
                        }else {
                            Toast.makeText(mContext, mContext.getText(R.string.already_newest_version)
                                            .toString() + ":"
                                    + update.getErr().getCode(),
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    public void onDestroy(){
        if (updateDialog != null){
            updateDialog.dismiss();
        }
        if (progressDialog != null){
            progressDialog.dismiss();
        }
        if (updateErrorDialog != null){
            updateErrorDialog.dismiss();
        }
        mContext = null;
    }
}

