package com.studio.dboard;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.multidex.MultiDex;

import com.aliyun.sls.android.sdk.SLSDatabaseManager;
import com.studio.dboard.allog.LogUtils;
import com.studio.dboard.dao.GreenDaoManager;
import com.studio.dboard.entity.Config;
import com.studio.dboard.utils.SystemUtils;
import com.umeng.commonsdk.UMConfigure;


public class BoardApp extends Application {

    private static final String tag = "BoardApp";

    public static int activityCount = 0;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        LogUtils.i(tag, "BoardApp-start");
        //阿里收集日志数据库
        SLSDatabaseManager.getInstance().setupDB(this);
        initConfig();
        initUMeng();
        startBoardService();
        initBackgroundCallback();
    }

    /**
     * 监听自身应用是否在前台
     */
    private void initBackgroundCallback() {
        registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

            }

            @Override
            public void onActivityStarted(Activity activity) {
                activityCount++;
                if (activityCount > 0){
                    sendBroadcast(new Intent(BoardService.FlowBubbleReceiver.ACTION_BUBBLE_DISMISS));
                }
            }

            @Override
            public void onActivityResumed(Activity activity) {

            }

            @Override
            public void onActivityPaused(Activity activity) {

            }

            @Override
            public void onActivityStopped(Activity activity) {
                activityCount--;
                if (activityCount == 0){
                    sendBroadcast(new Intent(BoardService.FlowBubbleReceiver.ACTION_BUBBLE_DISPLAY));
                }
            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

            }

            @Override
            public void onActivityDestroyed(Activity activity) {

            }
        });
    }

    private void initConfig(){
        //初始化配置参数
        long count = GreenDaoManager.getInstance(getApplicationContext())
                .getSession()
                .getConfigDao()
                .count();
        if (count == 0){
            Config config = new Config();
            config.initValue(getApplicationContext());
            GreenDaoManager.getInstance(getApplicationContext())
                    .getSession()
                    .getConfigDao()
                    .insert(config);
        }else {
            Config config = GreenDaoManager.getInstance(getApplicationContext())
                    .getSession()
                    .getConfigDao()
                    .load(0L);
            config.initValue(getApplicationContext());
            config.setIsProcessStartBefore(false);
            if (!config.getProductId().equals(SystemUtils.getProductId(getApplicationContext()))){
                config.setProductId(SystemUtils.getProductId(getApplicationContext()));
                config.setUu(SystemUtils.getUu());
            }
            GreenDaoManager.getInstance(getApplicationContext())
                    .getSession()
                    .getConfigDao()
                    .update(config);
        }
    }

    private void initUMeng(){
        //友盟初始化
        UMConfigure.setLogEnabled(false);
        UMConfigure.init(this, UMConfigure.DEVICE_TYPE_PHONE,"");
    }

    private void startBoardService(){
        Intent intent = new Intent(getApplicationContext(), BoardService.class);
        startService(intent);
    }
}
