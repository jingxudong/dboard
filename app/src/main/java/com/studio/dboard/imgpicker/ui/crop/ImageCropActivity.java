package com.studio.dboard.imgpicker.ui.crop;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.studio.dboard.Constant;
import com.studio.dboard.R;
import com.studio.dboard.allog.LogUtils;
import com.studio.dboard.imgpicker.ImagePicker;
import com.studio.dboard.imgpicker.base.activity.ImagePickerBaseActivity;
import com.studio.dboard.imgpicker.data.ImageBean;
import com.studio.dboard.imgpicker.data.ImageContants;
import com.studio.dboard.imgpicker.data.ImagePickerCropParams;
import com.studio.dboard.imgpicker.data.ImagePickerOptions;
import com.studio.dboard.imgpicker.ui.grid.view.ImageDataActivity;
import com.studio.dboard.imgpicker.widget.crop.CropUtil;
import com.studio.dboard.imgpicker.widget.crop.CropView;
import com.studio.dboard.utils.DateUtils;
import com.studio.dboard.utils.LauarUtils;
import com.studio.dboard.view.CustomDigitalClock;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Calendar;

/**
 * 裁剪界面
 */
public class ImageCropActivity extends ImagePickerBaseActivity {

    private static final String fontPath = "fonts/Helvetica_LT_33_Thin_Extended.ttf";

    private CustomDigitalClock clock;
    private TextView monthV;
    private TextView weekV;
    private TextView lunarV;

    private ImagePickerOptions mOptions;
    private String mOriginPath;
    private CropView mCropView;
    private Handler mHandler;
    private Dialog mDialog;
    private ImagePickerCropParams mCropParams;

    private Typeface tf;
    private TimeReceiver timeReceiver;
    private DataReceiver dataReceiver;

    private View loadingV;


    @Override
    protected int getContentViewResId() {
        mHandler = new Handler(getMainLooper());
        return R.layout.activity_image_crop;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerReceiver();
    }

    @Override
    protected void initUI(View contentView) {
        Intent intent = getIntent();
        mOriginPath = intent.getStringExtra(ImageContants.INTENT_KEY_ORIGIN_PATH);
        mOptions = intent.getParcelableExtra(ImageContants.INTENT_KEY_OPTIONS);
        tf = Typeface.createFromAsset(getAssets(), fontPath);

        loadingV = findView(R.id.loading);
        clock = (CustomDigitalClock) findViewById(R.id.clock);
        clock.setTypeface(tf);
        monthV = (TextView) findViewById(R.id.month);
        weekV = (TextView) findViewById(R.id.week);
        lunarV = (TextView) findViewById(R.id.lauar);
        mCropView = findView(R.id.cv_crop);
        addClick(R.id.btn_crop_cancel);
        addClick(R.id.btn_crop_confirm);
        addClick(R.id.btn_select_img);
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshTime();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver();
    }

    private void unregisterReceiver(){
        unregisterReceiver(timeReceiver);
        unregisterReceiver(dataReceiver);
    }

    private void registerReceiver(){
        timeReceiver = new TimeReceiver();
        IntentFilter filter = new IntentFilter(Intent.ACTION_TIME_TICK);
        registerReceiver(timeReceiver, filter);

        dataReceiver = new DataReceiver();
        IntentFilter dataIntentFilter = new IntentFilter(DataReceiver.ACTION_IMAGE_DATA);
        registerReceiver(dataReceiver, dataIntentFilter);
    }

    private void refreshTime(){
        LauarUtils.Lunar1();
        String ganZhiYear = LauarUtils.getGanZhiYear();
        String lunarMonth = LauarUtils.getLunarMonth();
        String greMonthDay = DateUtils.getGregorianMonth();
        String weekDay = DateUtils.getWeek();
        if (monthV != null){
            monthV.setText(greMonthDay);
        }

        if (weekV != null){
            weekV.setText(weekDay);
        }

        if (lunarV != null){
            lunarV.setText(ganZhiYear + " " + lunarMonth);
        }
    }

    @Override
    protected void initData() {
        startCrop();
    }

    @Override
    protected void onClick(View v, int id) {
        if (id == R.id.btn_crop_cancel) {
            setResult(RESULT_CANCELED);
            finish();
        } else if (id == R.id.btn_crop_confirm) {
            if (mOptions.isActive()){
                returnCropedImage();
            }else {
                showActiveDialog();
            }
        } else if (id == R.id.btn_select_img){
            Intent intent = new Intent(this, ImageDataActivity.class);
            startActivity(intent);
        }
    }

    private void showActiveDialog(){
        final Dialog dialog = new Dialog(this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.dialog_unactive);
        View cancel = dialog.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        View toActive = dialog.findViewById(R.id.active);
        toActive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                ImagePicker.ToActiveListener listener = ImagePicker.getInstance().getToActivelistener();
                if (listener != null){
                    listener.toActive();
                }
                finish();
            }
        });
        dialog.show();
    }

    @Override
    protected void onTouch(View view, int id, MotionEvent motionEven) {
        if (motionEven.getAction() == MotionEvent.ACTION_DOWN){
            view.setAlpha(0.5f);
        }else if(motionEven.getAction() == MotionEvent.ACTION_UP){
            view.setAlpha(1.0f);
        }
    }

    //保存并返回数据
    private void returnCropedImage() {
        if (TextUtils.isEmpty(mOriginPath)){
//            Intent intent = new Intent();
//            setResult(RESULT_OK, intent);
            finish();
            return;
        }

        File file = new File(mOriginPath);
        if (!file.exists()){
//            Intent intent = new Intent();
//            setResult(RESULT_OK, intent);
            finish();
            return;
        }

        //拷贝并保存截图
        new SaveTask().execute("");
    }

    private void showDialog() {
        loadingV.setVisibility(View.VISIBLE);
    }

    private void closeDialog() {
        loadingV.setVisibility(View.GONE);
    }

    private void startCrop(){
        if (mOptions == null) {
            showShortToast(R.string.error_imagepicker_lack_params);
//            setResult(RESULT_CANCELED);
            finish();
            return;
        }

        if (!TextUtils.isEmpty(mOriginPath)) {
            File file = new File(mOriginPath);
            long size = file.length();
            if (size > 15 * 1024 * 1024){
                showToobigDialog();
                return;
            }
        }

        mCropParams = mOptions.getCropParams();
        mCropView.load(mOriginPath)
                .setNeedGuideLine(mOptions.isNeedGuideLine())
                .setDefault(R.drawable.custom_default_pic)
                .setAspect(mCropParams.getAspectX(), mCropParams.getAspectY())
                .setOutputSize(mCropParams.getOutputX(), mCropParams.getOutputY())
                .start(this);
    }

    private void showToobigDialog(){
        final Dialog dialog = new Dialog(this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.layout_size_too_big);
        View btn = dialog.findViewById(R.id.i_know);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    class TimeReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Intent.ACTION_TIME_TICK)){
                Calendar cal = Calendar.getInstance();
                int min = cal.get(Calendar.MINUTE);
                if (min == 0) { //整点变化
                    refreshTime();
                }
            }
        }
    }

    public class DataReceiver extends BroadcastReceiver {

        public static final String ACTION_IMAGE_DATA = Constant.ROOT_ACTION + "ACTION_IMAGE_DATA";

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(ACTION_IMAGE_DATA)){
                mOriginPath = intent.getStringExtra(ImageContants.INTENT_KEY_ORIGIN_PATH);
                startCrop();
            }
        }
    }

    class SaveTask extends AsyncTask<String, Integer, ImageBean>{

        Bitmap bitmap;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog();
            bitmap = mCropView.getOutput();
        }

        @Override
        protected ImageBean doInBackground(String... strings) {
            String cachePath = mOptions.getCachePath();
            String name;
            if (TextUtils.isEmpty(mOptions.getSaveName())){
                name = CropUtil.createCropName();
            }else {
                name = mOptions.getSaveName();
            }

            File originFile = new File(mOriginPath);
            String fileName = originFile.getName();
            int index = fileName.lastIndexOf(".");
            String lastSufix = fileName.substring(index);
            String targetPath = Environment.getExternalStorageDirectory()
                    .getAbsolutePath() + File.separator + Constant.ROOT_DIR + "temp/origin" + lastSufix;

            if (!mOriginPath.equals(targetPath)) {
                copy(mOriginPath, targetPath);
            }

            String resultPath = CropUtil.saveBmp(bitmap, cachePath, name);

            if (TextUtils.isEmpty(resultPath)) {
                return null;
            } else {
                //删除没用的图片
                File cacheFile = new File(cachePath);
                File[] files = cacheFile.listFiles();
                for (int i = 0; i < files.length; i++){
                    File file = files[i];
                    if (!file.getAbsolutePath().equals(resultPath)){
                        file.delete();
                    }
                }

                //保存成功后返回给上级界面
                ImageBean imageBean = new ImageBean();
                imageBean.setImagePath(resultPath);
                imageBean.setOriginPath(targetPath);
                imageBean.setWidth(bitmap.getWidth());
                imageBean.setHeight(bitmap.getHeight());
                return imageBean;
            }
        }

        @Override
        protected void onPostExecute(ImageBean imageBean) {
            super.onPostExecute(imageBean);
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    closeDialog();
                }
            });

            if (imageBean == null) {
                showShortToast(R.string.imagepicker_crop_save_fail);
//                setResult(RESULT_CANCELED);
            }else {
//                Intent intent = new Intent();
//                intent.putExtra(ImageContants.INTENT_RESULT_SINGLE__DATA, imageBean);
//                setResult(RESULT_OK, intent);
                ImagePicker.OnResultListener onResultListener = ImagePicker.getInstance()
                        .getOnResultListener();
                if (onResultListener != null){
                    onResultListener.onResult(imageBean);
                }
            }
            finish();
            System.gc();
        }

        private void copy(String sourcePath, String targetPath){
            try {
                File sourceFile = new File(sourcePath);
                File targetFile = new File(targetPath);
                if (!targetFile.getParentFile().exists()){
                    targetFile.getParentFile().mkdirs();
                }
                if (!targetFile.exists()){
                    targetFile.createNewFile();
                }
                FileOutputStream fos = new FileOutputStream(targetFile);
                FileInputStream fis = new FileInputStream(sourceFile);
                byte[] buffer = new byte[8 * 1024];
                int length = 0;
                while ((length = fis.read(buffer)) != -1){
                    fos.write(buffer, 0, length);
                }
                fos.flush();
                fos.close();
                fis.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}
