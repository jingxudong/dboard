package com.studio.dboard.imgpicker;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;

import com.studio.dboard.imgpicker.data.ImageBean;
import com.studio.dboard.imgpicker.data.ImageContants;
import com.studio.dboard.imgpicker.data.ImageDataModel;
import com.studio.dboard.imgpicker.data.ImagePickType;
import com.studio.dboard.imgpicker.data.ImagePickerCropParams;
import com.studio.dboard.imgpicker.data.ImagePickerOptions;
import com.studio.dboard.imgpicker.ui.crop.ImageCropActivity;
import com.studio.dboard.imgpicker.ui.grid.view.ImageDataActivity;
import com.studio.dboard.imgpicker.utils.IImagePickerDisplayer;

/**
 * Created by LWK
 * TODO 调用方法的入口
 */

public class ImagePicker
{
    /**
     * 返回结果中包含图片数据的Intent的键值
     */
    public static final String INTENT_RESULT_DATA = "ImageBeans";

    private ImagePickerOptions mOptions;
    private ToActiveListener toActivelistener;
    private OnResultListener onResultListener;

    public ToActiveListener getToActivelistener() {
        return toActivelistener;
    }

    public ImagePicker setToActivelistener(ToActiveListener toActivelistener) {
        this.toActivelistener = toActivelistener;
        return this;
    }

    public OnResultListener getOnResultListener() {
        return onResultListener;
    }

    public ImagePicker setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
        return this;
    }

    private static ImagePicker instance = null;

    public static ImagePicker getInstance(){
        if (instance == null){
            instance = new ImagePicker();
        }
        return instance;
    }

    private ImagePicker() {
        mOptions = new ImagePickerOptions();
    }

    public ImagePicker setSavename(String name){
        mOptions.setSaveName(name);
        return this;
    }

    public ImagePicker pickType(ImagePickType mode) {
        mOptions.setType(mode);
        return this;
    }

    public ImagePicker maxNum(int maxNum) {
        mOptions.setMaxNum(maxNum);
        return this;
    }

    public ImagePicker needCamera(boolean b) {
        mOptions.setNeedCamera(b);
        return this;
    }

    public ImagePicker cachePath(String path) {
        mOptions.setCachePath(path);
        return this;
    }

    public ImagePicker doCrop(ImagePickerCropParams cropParams) {
        mOptions.setNeedCrop(cropParams != null);
        mOptions.setCropParams(cropParams);
        return this;
    }

    public ImagePicker doCrop(int aspectX, int aspectY, int outputX, int outputY) {
        mOptions.setNeedCrop(true);
        mOptions.setCropParams(new ImagePickerCropParams(aspectX, aspectY, outputX, outputY));
        return this;
    }

    public ImagePicker displayer(IImagePickerDisplayer displayer) {
        ImageDataModel.getInstance().setDisplayer(displayer);
        return this;
    }

    public ImagePicker needGuideLine(boolean b){
        mOptions.setNeedGuideLine(b);
        return this;
    }

    public ImagePicker isActive(boolean isActive){
        mOptions.setActive(isActive);
        return this;
    }

    /**
     * 发起选择图片
     *
     * @param activity    发起的Activity
     * @param requestCode 请求码
     */
    public void start(Activity activity, int requestCode) {
        Intent intent = new Intent(activity, ImageDataActivity.class);
        intent.putExtra(ImageContants.INTENT_KEY_OPTIONS, mOptions);
        activity.startActivityForResult(intent, requestCode);
    }

    /**
     * 发起选择图片
     * @param activity
     * @param requestCode
     * @param mOptions
     */
    public void start(Activity activity, int requestCode, ImagePickerOptions mOptions){
        Intent intent = new Intent(activity, ImageDataActivity.class);
        intent.putExtra(ImageContants.INTENT_KEY_OPTIONS, mOptions);
        activity.startActivityForResult(intent, requestCode);
    }

    /**
     * 发起选择图片
     *
     * @param fragment    发起的Fragment
     * @param requestCode 请求码
     */
    public void start(Fragment fragment, int requestCode) {
        Intent intent = new Intent(fragment.getActivity(), ImageDataActivity.class);
        intent.putExtra(ImageContants.INTENT_KEY_OPTIONS, mOptions);
        fragment.startActivityForResult(intent, requestCode);
    }

    /**
     * 发起选择图片
     *
     * @param fragment    发起的Fragment(V4包)
     * @param requestCode 请求码
     */
    public void start(android.support.v4.app.Fragment fragment, int requestCode) {
        Intent intent = new Intent(fragment.getActivity(), ImageDataActivity.class);
        intent.putExtra(ImageContants.INTENT_KEY_OPTIONS, mOptions);
        fragment.startActivityForResult(intent, requestCode);
    }

    /**
     * 跳转到该界面的公共方法
     *
     * @param activity   发起跳转的Activity
     * @param originPath 待裁剪图片路径
     */
    public void startCrop(Activity activity, String originPath) {
        Intent intent = new Intent(activity, ImageCropActivity.class);
        intent.putExtra(ImageContants.INTENT_KEY_ORIGIN_PATH, originPath);
        intent.putExtra(ImageContants.INTENT_KEY_OPTIONS, mOptions);
        activity.startActivity(intent);
    }


    public interface ToActiveListener{
        void toActive();
    }

    public interface OnResultListener{
        void onResult(ImageBean imageBean);
    }
}
