package com.studio.dboard.imgpicker.data;

import android.hardware.usb.UsbDevice;
import android.os.Parcel;
import android.os.Parcelable;

public class StorageInfo implements Parcelable{

    public static final int TYPE_SD_STORAGE = 0;
    public static final int TYPE_OTG_STORAGE = 1;

    private String name;
    private int type;
    private String rootPath;
    private UsbDevice usbDevice;
    private boolean isRemoved;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getRootPath() {
        return rootPath;
    }

    public void setRootPath(String rootPath) {
        this.rootPath = rootPath;
    }

    public UsbDevice getUsbDevice() {
        return usbDevice;
    }

    public void setUsbDevice(UsbDevice usbDevice) {
        this.usbDevice = usbDevice;
    }

    public boolean isRemoved() {
        return isRemoved;
    }

    public void setRemoved(boolean removed) {
        isRemoved = removed;
    }

    @Override
    public String toString() {
        return "StorageInfo{" +
                "name=" + name +
                ", type=" + type +
                ", rootPath='" + rootPath + '\'' +
                ", usbDevice=" + usbDevice +
                ", isRemoved=" + isRemoved +
                '}';
    }

    public StorageInfo(){}

    public StorageInfo(Parcel in){
        this.name = in.readString();
        this.type = in.readInt();
        this.rootPath = in.readString();
        this.usbDevice = in.readParcelable(UsbDevice.class.getClassLoader());
        this.isRemoved = in.readByte() == 1 ? true : false;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeInt(type);
        dest.writeString(rootPath);
        dest.writeParcelable(usbDevice, flags);
        dest.writeByte(isRemoved ? (byte)1 : (byte)0);
    }

    public static final Creator<StorageInfo> CREATOR = new Creator<StorageInfo>() {
        @Override
        public StorageInfo createFromParcel(Parcel in) {
            return new StorageInfo(in);
        }

        @Override
        public StorageInfo[] newArray(int size) {
            return new StorageInfo[size];
        }
    };

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StorageInfo that = (StorageInfo) o;

        return rootPath.equals(that.rootPath);
    }

    @Override
    public int hashCode() {
        return rootPath.hashCode();
    }
}
