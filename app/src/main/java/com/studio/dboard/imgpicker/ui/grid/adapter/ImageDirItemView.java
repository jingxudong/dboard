package com.studio.dboard.imgpicker.ui.grid.adapter;

import android.view.View;
import android.view.ViewGroup;

import com.studio.dboard.R;
import com.studio.dboard.imgpicker.base.adapter.IImagePickerItemView;
import com.studio.dboard.imgpicker.base.adapter.ImagePickerViewHolder;
import com.studio.dboard.imgpicker.ui.grid.view.IImageDataView;

public class ImageDirItemView implements IImagePickerItemView {

    private IImageDataView mViewImpl;

    public ImageDirItemView(IImageDataView viewImpl) {
        this.mViewImpl = viewImpl;
    }

    @Override
    public int getItemViewLayoutId() {
        return R.layout.layout_imagepicker_dir;
    }

    @Override
    public boolean isForViewType(Object item, int position) {
        return position == 0;
    }

    @Override
    public void setData(ImagePickerViewHolder holder, Object o, int position, ViewGroup parent) {

        holder.setClickListener(R.id.open_dir_select, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewImpl.startDirSelect();
            }
        });
    }
}
