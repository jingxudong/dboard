package com.studio.dboard.imgpicker.ui.grid.view;

import android.Manifest;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewStub;
import android.widget.AbsListView;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.studio.dboard.R;
import com.studio.dboard.allog.LogUtils;
import com.studio.dboard.imgpicker.base.activity.ImagePickerBaseActivity;
import com.studio.dboard.imgpicker.data.ImageBean;
import com.studio.dboard.imgpicker.data.ImageContants;
import com.studio.dboard.imgpicker.data.ImageFloderBean;
import com.studio.dboard.imgpicker.data.ImagePickerOptions;
import com.studio.dboard.imgpicker.ui.crop.ImageCropActivity;
import com.studio.dboard.imgpicker.ui.grid.adapter.ImageDataAdapter;
import com.studio.dboard.imgpicker.ui.grid.presenter.ImageDataPresenter;
import com.studio.dboard.imgpicker.utils.ImagePickerComUtils;
import com.studio.dboard.imgpicker.utils.PermissionChecker;
import com.studio.dboard.imgpicker.widget.ImagePickerActionBar;

import java.util.List;

import static com.studio.dboard.imgpicker.data.ImageContants.REQUEST_CODE_PERMISSION_SDCARD;

/**
 * 展示图片数据的Activity
 * [入口]
 */
public class ImageDataActivity extends ImagePickerBaseActivity implements IImageDataView
        , ImageFloderPop.onFloderItemClickListener, AbsListView.OnScrollListener {

    private ImageDataPresenter mPresenter;
    private ImagePickerActionBar mActionBar;
    private GridView mGridView;
    private ProgressBar mPgbLoading;
    private View mViewFloder;
    private TextView mTvFloderName;
    private ImageDataAdapter mAdapter;
    private ImageFloderBean mCurFloder;
    private int mColumnWidth;
    private int mColumnNum;
    private Parcelable mState;

    @Override
    protected int getContentViewResId() {
        mPresenter = new ImageDataPresenter(this);
        return R.layout.activity_image_data;
    }

    @Override
    protected void initUI(View contentView) {

        mActionBar = findView(R.id.acb_image_data);
        mActionBar.setTitle(R.string.imagepicker_title_select_image);

        ViewStub viewStub = findView(R.id.vs_image_data);
        viewStub.inflate();
        mGridView = findView(R.id.gv_image_data);
        mGridView.setOnScrollListener(this);
        mPgbLoading = findView(R.id.pgb_image_data);
        mViewFloder = findView(R.id.ll_image_data_bottom_floder);
        mTvFloderName = findView(R.id.tv_image_data_bottom_flodername);

        mViewFloder.setOnClickListener(this);
        mActionBar.hidePreview();


    }

    @Override
    protected void initData() {
        calColumn();
        mAdapter = new ImageDataAdapter(this, mColumnWidth, this);
        mGridView.setAdapter(mAdapter);
        doScanData();
    }

    @Override
    public ImagePickerOptions getOptions() {
        return null;
    }

    @Override
    public void startTakePhoto() {
    }

    @Override
    public void startDirSelect() {
        doDirSelect();
    }

    /**
     * 从目录里面选择图片
     */
    private void doDirSelect() {
        Log.i("peak", "doDirSelect");
        Intent intent = new Intent(this, DiskActivity.class);
        startActivity(intent);
        finish();
    }

    //执行扫描sd卡的方法
    private void doScanData() {
        if (!ImagePickerComUtils.isSdExist()) {
            showShortToast(R.string.error_no_sdcard);
            return;
        }

        boolean hasPermission = PermissionChecker.checkPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                REQUEST_CODE_PERMISSION_SDCARD, R.string.dialog_imagepicker_permission_sdcard_message);
        //有权限直接扫描
        if (hasPermission)
            mPresenter.scanData(this);
    }

    @Override
    public void showLoading() {
        if (mPgbLoading != null) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mPgbLoading.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    @Override
    public void hideLoading() {
        if (mPgbLoading != null) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mPgbLoading.setVisibility(View.GONE);
                }
            });
        }
    }

    @Override
    public void onDataChanged(final List<ImageBean> dataList) {
        if (mGridView != null && mAdapter != null) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mGridView.setVisibility(View.VISIBLE);
                    mAdapter.refreshDatas(dataList);
                    mGridView.setSelection(0);
                }
            });
        }
    }

    @Override
    public void onFloderChanged(ImageFloderBean floderBean) {
        if (mCurFloder != null && floderBean != null && mCurFloder.equals(floderBean))
            return;

        mCurFloder = floderBean;
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (mTvFloderName != null)
                    mTvFloderName.setText(mCurFloder.getFloderName());
            }
        });
        mPresenter.checkDataByFloder(floderBean);
    }

    @Override
    public void onImageClicked(ImageBean imageBean, int position) {
        //返回选择的图片进行裁剪
        Intent intent = new Intent(ImageCropActivity.DataReceiver.ACTION_IMAGE_DATA);
        intent.putExtra(ImageContants.INTENT_KEY_ORIGIN_PATH, imageBean.getImagePath());
        sendBroadcast(intent);
        finish();

    }

    @Override
    public void onSelectNumChanged(int curNum) {
    }

    @Override
    public void warningMaxNum() {
    }

    @Override
    protected void onClick(View v, int id) {
        if (id == R.id.ll_image_data_bottom_floder) {
            //弹出文件夹切换菜单
            new ImageFloderPop().showAtBottom(this, mContentView, mCurFloder, this);
        }
    }

    @Override
    protected void onTouch(View view, int id, MotionEvent motionEven) {

    }

    @Override
    public void onFloderItemClicked(ImageFloderBean floderBean) {
        onFloderChanged(floderBean);
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        boolean[] result;
        switch (requestCode) {
            case REQUEST_CODE_PERMISSION_SDCARD:
                result = PermissionChecker.onRequestPermissionsResult(this, permissions, grantResults, false
                        , R.string.dialog_imagepicker_permission_sdcard_nerver_ask_message);
                //                if (result[0])
                //                    mPresenter.scanData(this);
                //无论成功失败都去扫描，以便更新视图
                mPresenter.scanData(this);
                break;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }



    //计算列数和每列宽度
    private void calColumn() {
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = metrics.widthPixels;
        int densityDpi = metrics.densityDpi;
        int orientation = getResources().getConfiguration().orientation;
        int minColumn = orientation == Configuration.ORIENTATION_LANDSCAPE ? 4 : 3;

        //计算列数
        mColumnNum = screenWidth / densityDpi;
        mColumnNum = mColumnNum < minColumn ? minColumn : mColumnNum;
        //计算每列宽度
        int columnSpace = (int) (2 * metrics.density);
        mColumnWidth = (screenWidth - columnSpace * (mColumnNum - 1)) / mColumnNum;

        if (mGridView != null) {
            mGridView.setColumnWidth(mColumnWidth);
            mGridView.setNumColumns(mColumnNum);
        }
        if (mAdapter != null)
            mAdapter.adjustLayoutSize(mColumnWidth);
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDestory();
        super.onDestroy();
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        //记录下滚动位置
        mState = view.onSaveInstanceState();
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount,
                         int totalItemCount) {
        //nothing to do
    }
}
