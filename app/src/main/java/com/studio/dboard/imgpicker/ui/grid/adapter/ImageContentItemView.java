package com.studio.dboard.imgpicker.ui.grid.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.studio.dboard.R;
import com.studio.dboard.imgpicker.base.adapter.IImagePickerItemView;
import com.studio.dboard.imgpicker.base.adapter.ImagePickerViewHolder;
import com.studio.dboard.imgpicker.data.ImageBean;
import com.studio.dboard.imgpicker.data.ImageContants;
import com.studio.dboard.imgpicker.data.ImageDataModel;
import com.studio.dboard.imgpicker.ui.grid.view.IImageDataView;


/**
 * Created by LWK
 * TODO 显示图片的GridItem
 */
public class ImageContentItemView implements IImagePickerItemView<ImageBean> {
    private IImageDataView mViewImpl;

    public ImageContentItemView(IImageDataView viewImpl) {
        this.mViewImpl = viewImpl;
    }

    @Override
    public int getItemViewLayoutId() {
        return R.layout.layout_image_data_content_listitem;
    }

    @Override
    public boolean isForViewType(ImageBean item, int position) {
        return position != 0;
    }

    @Override
    public void setData(ImagePickerViewHolder holder, final ImageBean imageBean,
                        final int position, ViewGroup parent) {
        ImageView imgContent = holder.findView(R.id.img_imagepicker_grid_content);

        //显示UI
        if (imageBean != null)
            ImageDataModel.getInstance().getDisplayer()
                    .display(holder.getContext(), imageBean.getImagePath(), imgContent
                            , R.drawable.glide_default_picture, R.drawable.glide_default_picture
                            , ImageContants.DISPLAY_THUMB_SIZE, ImageContants.DISPLAY_THUMB_SIZE);

        imgContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mViewImpl != null)
                    mViewImpl.onImageClicked(imageBean, position);
            }
        });
    }
}
