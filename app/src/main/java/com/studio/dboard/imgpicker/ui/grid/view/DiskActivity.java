package com.studio.dboard.imgpicker.ui.grid.view;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.studio.dboard.R;
import com.studio.dboard.imgpicker.base.activity.ImagePickerBaseActivity;
import com.studio.dboard.imgpicker.data.StorageInfo;
import com.studio.dboard.utils.StorageUtils;

import java.util.ArrayList;
import java.util.List;

public class DiskActivity extends ImagePickerBaseActivity implements AdapterView.OnItemClickListener {

    private GridView disks;
    private DiskAdapter adapter;
    private UsbReceiver usbReceiver;


    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver();
    }

    private void registerReceiver(){
        usbReceiver = new UsbReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
        intentFilter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
        intentFilter.addAction(Intent.ACTION_MEDIA_CHECKING);
        intentFilter.addAction(Intent.ACTION_MEDIA_EJECT);
        intentFilter.addAction(Intent.ACTION_MEDIA_MOUNTED);
        intentFilter.addAction(Intent.ACTION_MEDIA_UNMOUNTED);
        intentFilter.addDataScheme("file");
        registerReceiver(usbReceiver, intentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(usbReceiver);
    }

    @Override
    protected int getContentViewResId() {
        return R.layout.activity_image_disk;
    }

    @Override
    protected void initUI(View contentView) {
        disks = contentView.findViewById(R.id.disks);
        adapter = new DiskAdapter();
        disks.setAdapter(adapter);
        disks.setOnItemClickListener(this);

        addClick(R.id.back);
    }

    @Override
    protected void initData() {
        List<StorageInfo> storages = StorageUtils.getAllStorages(getApplicationContext());
        adapter.setData(storages);
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onClick(View v, int id) {
        if (id == R.id.back){
            Intent intent = new Intent(this, ImageDataActivity.class);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, ImageDataActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onTouch(View view, int id, MotionEvent motionEven) {

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        StorageInfo storageInfo = adapter.getItem(position);
        Intent intent = new Intent(this, ImageDirActivity.class);
        intent.putExtra("storageinfo", storageInfo);
        startActivity(intent);
        finish();
    }


    class DiskAdapter extends BaseAdapter {

        private List<StorageInfo> disks = new ArrayList<>();

        public void setData(List<StorageInfo> datas) {
            this.disks.clear();
            this.disks.addAll(datas);
        }

        @Override
        public int getCount() {
            return disks.size();
        }

        @Override
        public StorageInfo getItem(int position) {
            return disks.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Holder holder = null;
            if (convertView == null){
                convertView = LayoutInflater.from(getApplicationContext()).inflate(
                        R.layout.layout_image_disk_item, null);
                holder = new Holder();
                holder.diskImg = convertView.findViewById(R.id.disk_img);
                holder.name = convertView.findViewById(R.id.name);
                holder.path = convertView.findViewById(R.id.path);
                convertView.setTag(holder);
            }else {
                holder = (Holder) convertView.getTag();
            }

            StorageInfo storageInfo = getItem(position);
            holder.name.setText(storageInfo.getName());
            if (storageInfo.getType() == StorageInfo.TYPE_SD_STORAGE){
                holder.path.setText(storageInfo.getRootPath());
                holder.diskImg.setImageResource(R.drawable.btn_local_disk_bg_selector);
            } else if(storageInfo.getType() == StorageInfo.TYPE_OTG_STORAGE){
                holder.path.setText(storageInfo.getUsbDevice().getDeviceName());
                holder.diskImg.setImageResource(R.drawable.btn_usb_disk_bg_selector);
            }
            return convertView;
        }

        class Holder {
            ImageView diskImg;
            TextView name;
            TextView path;
        }
    }


    class UsbReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.i("peak", "action="+action);
            if (action.equals(UsbManager.ACTION_USB_DEVICE_DETACHED)){
                UsbDevice usbDevice = (UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                if (usbDevice != null) {
                    initData();
                }
            } else if (action.equals(UsbManager.ACTION_USB_DEVICE_ATTACHED)) {
                UsbDevice usbDevice = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                if (usbDevice != null) {
                    initData();
                }
            } else if (action.equals(Intent.ACTION_MEDIA_MOUNTED)) {
                String path = intent.getDataString();
                initData();
            } else if (action.equals(Intent.ACTION_MEDIA_UNMOUNTED)) {
                String path = intent.getDataString();
                initData();
            } else if (action.equals(Intent.ACTION_MEDIA_EJECT)) {
                String path = intent.getDataString();
                initData();
            } else if (action.equals(Intent.ACTION_MEDIA_CHECKING)) {

            }
        }
    }
}
