package com.studio.dboard.imgpicker.data;


import com.github.mjdev.libaums.fs.UsbFile;

import java.io.File;

public class FileInfo {
    public static final int TYPE_SD_FILE = 0;
    public static final int TYPE_OTG_FILE = 1;

    private int type;
    private File file;
    private UsbFile usbFile;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public UsbFile getUsbFile() {
        return usbFile;
    }

    public void setUsbFile(UsbFile usbFile) {
        this.usbFile = usbFile;
    }
}
