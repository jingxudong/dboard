package com.studio.dboard.imgpicker.data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by LWK
 * TODO 图片选择各参数
 */

public class ImagePickerOptions implements Parcelable
{
    private ImagePickType type = ImagePickType.SINGLE;
    private int maxNum = 1;
    private boolean needCamera = true;
    private boolean needCrop;
    private ImagePickerCropParams cropParams;
    private String cachePath = ImageContants.DEF_CACHE_PATH;
    private String saveName;
    private boolean needGuideLine = true;
    private boolean needDirSelect = true;
    private boolean isActive = false;

    public ImagePickType getType()
    {
        return type;
    }

    public void setType(ImagePickType type)
    {
        this.type = type;
    }

    public int getMaxNum()
    {
        return maxNum;
    }

    public void setMaxNum(int maxNum)
    {
        if (maxNum > 0)
            this.maxNum = maxNum;
    }

    public boolean isNeedCamera()
    {
        return needCamera;
    }

    public void setNeedCamera(boolean needCamera)
    {
        this.needCamera = needCamera;
    }

    public boolean isNeedCrop()
    {
        return needCrop;
    }

    public void setNeedCrop(boolean needCrop)
    {
        this.needCrop = needCrop;
    }

    public String getCachePath()
    {
        return cachePath;
    }

    public void setCachePath(String cachePath)
    {
        this.cachePath = cachePath;
    }

    public ImagePickerCropParams getCropParams()
    {
        return cropParams;
    }

    public void setCropParams(ImagePickerCropParams cropParams)
    {
        this.cropParams = cropParams;
    }

    public boolean isNeedGuideLine() {
        return needGuideLine;
    }

    public void setNeedGuideLine(boolean needGuideLine) {
        this.needGuideLine = needGuideLine;
    }

    public String getSaveName() {
        return saveName;
    }

    public void setSaveName(String saveName) {
        this.saveName = saveName;
    }

    public boolean isNeedDirSelect() {
        return needDirSelect;
    }

    public void setNeedDirSelect(boolean needDirSelect) {
        this.needDirSelect = needDirSelect;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    @Override
    public String toString()
    {
        return "ImagePickerOptions{" +
                "type=" + type +
                ", maxNum=" + maxNum +
                ", needCamera=" + needCamera +
                ", needCrop=" + needCrop +
                ", cropParams=" + cropParams +
                ", cachePath='" + cachePath + '\'' +
                ", needGuideLine='" + needGuideLine + '\'' +
                ", saveName='" + saveName + '\'' +
                ", needDirSelect='" + needDirSelect + '\'' +
                ", isActive='" + isActive + '\'' +
                '}';
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.type == null ? -1 : this.type.ordinal());
        dest.writeInt(this.maxNum);
        dest.writeByte(this.needCamera ? (byte) 1 : (byte) 0);
        dest.writeByte(this.needCrop ? (byte) 1 : (byte) 0);
        dest.writeParcelable(this.cropParams, flags);
        dest.writeString(this.cachePath);
        dest.writeByte(this.needGuideLine ? (byte) 1 : (byte) 0);
        dest.writeString(this.saveName);
        dest.writeByte(this.needDirSelect ? (byte) 1 : (byte) 0);
        dest.writeByte(isActive ? (byte) 1 : (byte) 0);
    }

    public ImagePickerOptions() {
    }

    protected ImagePickerOptions(Parcel in) {
        int tmpMode = in.readInt();
        this.type = tmpMode == -1 ? null : ImagePickType.values()[tmpMode];
        this.maxNum = in.readInt();
        this.needCamera = in.readByte() != 0;
        this.needCrop = in.readByte() != 0;
        this.cropParams = in.readParcelable(ImagePickerCropParams.class.getClassLoader());
        this.cachePath = in.readString();
        this.needGuideLine = in.readByte() != 0;
        this.saveName = in.readString();
        this.needDirSelect = in.readByte() != 0;
        this.isActive = in.readByte() != 0;
    }

    public static final Creator<ImagePickerOptions> CREATOR = new Creator<ImagePickerOptions>() {
        @Override
        public ImagePickerOptions createFromParcel(Parcel source) {
            return new ImagePickerOptions(source);
        }

        @Override
        public ImagePickerOptions[] newArray(int size)
        {
            return new ImagePickerOptions[size];
        }
    };
}
