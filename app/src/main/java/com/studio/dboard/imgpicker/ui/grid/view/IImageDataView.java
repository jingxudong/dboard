package com.studio.dboard.imgpicker.ui.grid.view;

import com.studio.dboard.imgpicker.base.activity.IImageBaseView;
import com.studio.dboard.imgpicker.data.ImageBean;
import com.studio.dboard.imgpicker.data.ImageFloderBean;
import com.studio.dboard.imgpicker.data.ImagePickerOptions;

import java.util.List;

/**
 * Created by LWK
 * TODO ImageDataActivity的View层接口
 */

public interface IImageDataView extends IImageBaseView {
    ImagePickerOptions getOptions();

    void startTakePhoto();

    void startDirSelect();

    void showLoading();

    void hideLoading();

    void onDataChanged(List<ImageBean> dataList);

    void onFloderChanged(ImageFloderBean floderBean);

    void onImageClicked(ImageBean imageBean, int position);

    void onSelectNumChanged(int curNum);

    void warningMaxNum();
}
