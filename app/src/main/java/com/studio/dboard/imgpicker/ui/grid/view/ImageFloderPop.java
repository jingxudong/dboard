package com.studio.dboard.imgpicker.ui.grid.view;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.graphics.drawable.BitmapDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.PopupWindow;

import com.studio.dboard.R;
import com.studio.dboard.imgpicker.data.ImageDataModel;
import com.studio.dboard.imgpicker.data.ImageFloderBean;
import com.studio.dboard.imgpicker.ui.grid.adapter.ImageFloderAdapter;

/**
 * Created by LWK
 * TODO 展示文件夹的PopupWindow
 */

public class ImageFloderPop implements PopupWindow.OnDismissListener,
        AdapterView.OnItemClickListener {
    private PopupWindow mPopupWindow;
    private ListView mListView;
    private ImageFloderAdapter mAdapter;
    private onFloderItemClickListener mListener;
    private ValueAnimator mAnimator;
    private View rootPop;


    /**
     * 从Activity底部弹出来
     */
    public void showAtBottom(Activity activity, View parent, ImageFloderBean curFloder,
                             onFloderItemClickListener listener) {
        this.mListener = listener;

        View contentView = LayoutInflater.from(activity).inflate(R.layout.layout_image_floder_pop, null);
        mPopupWindow = new PopupWindow(contentView, ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT, true);
        mPopupWindow.setBackgroundDrawable(new BitmapDrawable());// 响应返回键必须的语句。
        mPopupWindow.setFocusable(true);//设置pop可获取焦点
        mPopupWindow.setAnimationStyle(R.style.FloderPopAnimStyle);//设置显示、消失动画
        mPopupWindow.setOutsideTouchable(true);//设置点击外部可关闭pop
        mPopupWindow.setOnDismissListener(this);

        mListView = (ListView) contentView.findViewById(R.id.lv_image_floder_list);
        final int position = ImageDataModel.getInstance().getAllFloderList().indexOf(curFloder);
        mAdapter = new ImageFloderAdapter(activity, position);
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(this);

        rootPop = contentView.findViewById(R.id.root_pop);
        rootPop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPopupWindow.dismiss();
            }
        });

        mPopupWindow.showAtLocation(parent, Gravity.BOTTOM, 0, 0);
        toggleWindowAlpha();

        // 增加绘制监听
        ViewTreeObserver vto = mListView.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                // 移除监听
                mListView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                mListView.setSelection(position);
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (mListener != null)
            mListener.onFloderItemClicked(mAdapter.getItem(position));
        if (mPopupWindow != null)
            mPopupWindow.dismiss();
    }

    @Override
    public void onDismiss() {
        toggleWindowAlpha();
    }

    /**
     * 切换窗口透明度
     */
    private void toggleWindowAlpha() {
        if (mAnimator != null) {
            mAnimator.cancel();
            mAnimator = null;
        }

        mAnimator = ValueAnimator.ofFloat(0.5f, 1.0f);
        mAnimator.setDuration(200);//动画时间要和PopupWindow弹出动画的时间一致
        mAnimator.setInterpolator(new LinearInterpolator());
        mAnimator.start();
    }

    public interface onFloderItemClickListener {
        void onFloderItemClicked(ImageFloderBean floderBean);
    }
}
