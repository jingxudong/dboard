package com.studio.dboard.imgpicker.ui.grid.view;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import com.github.mjdev.libaums.UsbMassStorageDevice;
import com.github.mjdev.libaums.fs.FileSystem;
import com.github.mjdev.libaums.fs.UsbFile;
import com.github.mjdev.libaums.fs.UsbFileInputStream;
import com.studio.dboard.R;
import com.studio.dboard.imgpicker.base.activity.ImagePickerBaseActivity;
import com.studio.dboard.imgpicker.data.FileInfo;
import com.studio.dboard.imgpicker.data.ImageContants;
import com.studio.dboard.imgpicker.data.StorageInfo;
import com.studio.dboard.imgpicker.ui.crop.ImageCropActivity;
import com.studio.dboard.imgpicker.ui.grid.adapter.DirsAdapter;
import com.studio.dboard.utils.ImageUtils;

import java.io.File;
import java.io.FileFilter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class ImageDirActivity extends ImagePickerBaseActivity implements AdapterView.OnItemClickListener {

    public static final String IMG_DIR = Environment.getExternalStorageDirectory().getPath()
            + "/XCloudC/image/";

    private static String rootDir;
    private View loadingV;
    private TextView titleV;
    private StorageInfo storageInfo;
    private FileInfo curFileInfo;
    private TextView dirsName;
    private GridView dirsList;
    private DirsAdapter adapter;
    private UsbReceiver usbReceiver;


    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver();
    }

    private void registerReceiver(){
        usbReceiver = new UsbReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(UsbReceiver.ACTION_USB_PERMISSION);
        intentFilter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
        intentFilter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
        intentFilter.addAction(Intent.ACTION_MEDIA_EJECT);
        intentFilter.addAction(Intent.ACTION_MEDIA_MOUNTED);
        intentFilter.addAction(Intent.ACTION_MEDIA_UNMOUNTED);
        intentFilter.addDataScheme("file");
        registerReceiver(usbReceiver, intentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(usbReceiver);
    }

    @Override
    protected int getContentViewResId() {
        return R.layout.activity_image_dir;
    }

    @Override
    protected void initUI(View contentView) {
        loadingV = contentView.findViewById(R.id.loading);
        titleV = contentView.findViewById(R.id.disk_title);
        dirsName = contentView.findViewById(R.id.dirs_name);
        dirsList = contentView.findViewById(R.id.dirs_list);
        dirsList.setOnItemClickListener(this);
        addClick(R.id.back);

        adapter = new DirsAdapter(getApplicationContext());
        dirsList.setAdapter(adapter);
    }

    @Override
    protected void initData() {
        storageInfo = getIntent().getParcelableExtra("storageinfo");
        titleV.setText(storageInfo.getName());
        if (storageInfo.getType() == StorageInfo.TYPE_SD_STORAGE){
            initSd(storageInfo);
        }else if (storageInfo.getType() == StorageInfo.TYPE_OTG_STORAGE){
            UsbDevice usbDevice = storageInfo.getUsbDevice();
            try {
                initOtg(usbDevice);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void initSd(StorageInfo storageInfo){
        rootDir = storageInfo.getRootPath();
        File file = new File(rootDir);
        curFileInfo = new FileInfo();
        curFileInfo.setType(FileInfo.TYPE_SD_FILE);
        curFileInfo.setFile(file);
        if (file.exists() && file.isDirectory()){
            refreshData(curFileInfo);
        }
    }

    private void initOtg(UsbDevice usbDevice) throws Exception{
        UsbMassStorageDevice[] devices = UsbMassStorageDevice.getMassStorageDevices(getApplicationContext());
        UsbMassStorageDevice curUsbDevice = null;
        for (UsbMassStorageDevice device : devices){
            UsbDevice tempUsbDevice = device.getUsbDevice();
            if (!usbDevice.equals(tempUsbDevice)){
                continue;
            }
            curUsbDevice = device;
        }

        UsbManager usbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
        if (curUsbDevice != null){
            Log.i("peak", "hasPermission="+usbManager.hasPermission(usbDevice));
            if (usbManager.hasPermission(usbDevice)){
                curUsbDevice.init();
                FileSystem currentFs = curUsbDevice.getPartitions().get(0).getFileSystem();
                UsbFile usbFile = currentFs.getRootDirectory();

                curFileInfo = new FileInfo();
                curFileInfo.setType(FileInfo.TYPE_OTG_FILE);
                curFileInfo.setUsbFile(usbFile);
                refreshData(curFileInfo);

            }else {
                Intent intent = new Intent(UsbReceiver.ACTION_USB_PERMISSION);
                intent.setPackage(getPackageName());
                PendingIntent permissionIntent = PendingIntent.getBroadcast(this, 0,
                        intent, 0);
                usbManager.requestPermission(usbDevice, permissionIntent);


//                PackageManager pm = getPackageManager();
//                ApplicationInfo aInfo = pm.getApplicationInfo(getPackageName(),
//                        0);
//                IBinder b = ServiceManager.getService(USB_SERVICE);
//                IUsbManager service = IUsbManager.Stub.asInterface(b);
//                service.grantDevicePermission(usbDevice, aInfo.uid);
//
//                Intent intent = new Intent(UsbReceiver.ACTION_USB_PERMISSION);
//                intent.putExtra(UsbManager.EXTRA_DEVICE, usbDevice);
//                intent.putExtra(UsbManager.EXTRA_PERMISSION_GRANTED, true);
//                sendBroadcast(intent);
            }
        }
    }

    private void refreshData(FileInfo fileInfo){
        if (fileInfo.getType() == FileInfo.TYPE_SD_FILE) {
            File file = fileInfo.getFile();
            dirsName.setText(file.getAbsolutePath());
        } else if (fileInfo.getType() == FileInfo.TYPE_OTG_FILE) {
            UsbFile usbFile = fileInfo.getUsbFile();
            String path = getUsbFilePath(usbFile);
            dirsName.setText(path);
        }
        new LoadTask().execute(fileInfo);
    }

    private String getUsbFilePath(UsbFile usbFile){
        UsbFile parentUsbFile = usbFile.getParent();
        if (parentUsbFile != null){
            return getUsbFilePath(parentUsbFile) + File.separator + usbFile.getName();
        }else {
            return "UsbStorage";
        }
    }

    @Override
    protected void onClick(View v, int id) {
        if (id == R.id.back){
            if (curFileInfo == null){
                Intent intent = new Intent(this, DiskActivity.class);
                startActivity(intent);
                finish();
                return;
            }

            if (curFileInfo.getType() == FileInfo.TYPE_SD_FILE) {
                File file = curFileInfo.getFile();
                if (file.getAbsolutePath().equals(rootDir)) {
                    Intent intent = new Intent(this, DiskActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    File parentFile = file.getParentFile();
                    curFileInfo = new FileInfo();
                    curFileInfo.setType(FileInfo.TYPE_SD_FILE);
                    curFileInfo.setFile(parentFile);
                    refreshData(curFileInfo);
                }
            } else if (curFileInfo.getType() == FileInfo.TYPE_OTG_FILE) {
                UsbFile usbFile = curFileInfo.getUsbFile();
                UsbFile parentUsbFile = usbFile.getParent();
                if (parentUsbFile == null){
                    Intent intent = new Intent(this, DiskActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    curFileInfo = new FileInfo();
                    curFileInfo.setType(FileInfo.TYPE_OTG_FILE);
                    curFileInfo.setUsbFile(parentUsbFile);
                    refreshData(curFileInfo);
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (curFileInfo.getType() == FileInfo.TYPE_SD_FILE) {
            File file = curFileInfo.getFile();
            if (file.getAbsolutePath().equals(rootDir)) {
                Intent intent = new Intent(this, DiskActivity.class);
                startActivity(intent);
                finish();
            } else {
                File parentFile = file.getParentFile();
                curFileInfo = new FileInfo();
                curFileInfo.setType(FileInfo.TYPE_SD_FILE);
                curFileInfo.setFile(parentFile);
                refreshData(curFileInfo);
            }
        } else if (curFileInfo.getType() == FileInfo.TYPE_OTG_FILE) {
            UsbFile usbFile = curFileInfo.getUsbFile();
            UsbFile parentUsbFile = usbFile.getParent();
            if (parentUsbFile == null){
                Intent intent = new Intent(this, DiskActivity.class);
                startActivity(intent);
                finish();
            } else {
                curFileInfo = new FileInfo();
                curFileInfo.setType(FileInfo.TYPE_OTG_FILE);
                curFileInfo.setUsbFile(parentUsbFile);
                refreshData(curFileInfo);
            }
        }
    }


    @Override
    protected void onTouch(View view, int id, MotionEvent motionEven) {

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        FileInfo fileInfo = (FileInfo) adapterView.getItemAtPosition(position);
        if (fileInfo.getType() == FileInfo.TYPE_SD_FILE) {
            File file = fileInfo.getFile();
            if (file.isDirectory()) {
                refreshData(fileInfo);
                curFileInfo = fileInfo;
            } else if (file.isFile()) {
                Intent intent = new Intent(ImageCropActivity.DataReceiver.ACTION_IMAGE_DATA);
                intent.putExtra(ImageContants.INTENT_KEY_ORIGIN_PATH, file.getAbsolutePath());
                sendBroadcast(intent);
                finish();
            }
        }else if (fileInfo.getType() == FileInfo.TYPE_OTG_FILE) {
            UsbFile usbFile = fileInfo.getUsbFile();
            if (usbFile.isDirectory()) {
                refreshData(fileInfo);
                curFileInfo = fileInfo;
            } else {
                new CopyTask().execute(usbFile);
            }
        }
    }


    class UsbReceiver extends BroadcastReceiver {

        private static final String ACTION_USB_PERMISSION = "com.studio.xlauncher.USB_PERMISSION";

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(ACTION_USB_PERMISSION)){
                UsbDevice usbDevice = (UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                    if (usbDevice != null) {
                        try {
                            initOtg(usbDevice);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            } else if (action.equals(UsbManager.ACTION_USB_DEVICE_DETACHED)){
                UsbDevice usbDevice = (UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                if (usbDevice != null){
                    if (storageInfo.getType() == StorageInfo.TYPE_OTG_STORAGE) {
                        UsbDevice curUsbDevice = storageInfo.getUsbDevice();
                        if (curUsbDevice.getDeviceId() == usbDevice.getDeviceId()) {
                            Intent intent1 = new Intent(ImageDirActivity.this, DiskActivity.class);
                            startActivity(intent1);
                            finish();
                        }
                    }else {
                        Intent intent1 = new Intent(ImageDirActivity.this, DiskActivity.class);
                        startActivity(intent1);
                        finish();
                    }
                }
            } else if (action.equals(UsbManager.ACTION_USB_DEVICE_ATTACHED)) {
                UsbDevice usbDevice = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);

            } else if (action.equals(Intent.ACTION_MEDIA_MOUNTED)) {
                String path = intent.getDataString();

            } else if (action.equals(Intent.ACTION_MEDIA_UNMOUNTED)) {
                String path = intent.getDataString();
                if (!TextUtils.isEmpty(path)){
                    if (storageInfo.getType() == StorageInfo.TYPE_SD_STORAGE) {
                        String rootPath = storageInfo.getRootPath();
                        if (path.contains(rootPath)) {
                            Intent intent1 = new Intent(ImageDirActivity.this, DiskActivity.class);
                            startActivity(intent1);
                            finish();
                        }
                    }
                }
            } else if (action.equals(Intent.ACTION_MEDIA_EJECT)) {
                String path = intent.getDataString();
                if (!TextUtils.isEmpty(path)){
                    if (storageInfo.getType() == StorageInfo.TYPE_SD_STORAGE) {
                        String rootPath = storageInfo.getRootPath();
                        if (path.contains(rootPath)) {
                            Intent intent1 = new Intent(ImageDirActivity.this, DiskActivity.class);
                            startActivity(intent1);
                            finish();
                        }
                    }
                }
            }
        }
    }


    class LoadTask extends AsyncTask<FileInfo, Integer, List<FileInfo>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            loadingV.setVisibility(View.VISIBLE);
        }

        @Override
        protected List<FileInfo> doInBackground(FileInfo... infos) {
            FileInfo fileInfo = infos[0];
            if (fileInfo != null){
                List<FileInfo> fileInfos = new ArrayList<>();
                if (fileInfo.getType() == FileInfo.TYPE_SD_FILE) {
                    File file = fileInfo.getFile();
                    File[] files = file.listFiles(new FileFilter() {
                        @Override
                        public boolean accept(File file) {
                            if (file.isHidden()) {
                                return false;
                            }

                            if (file.isFile() && !ImageUtils.isValideImage(file)) {
                                return false;
                            }
                            return true;
                        }
                    });
                    List<File> datas = Arrays.asList(files);
                    Collections.sort(datas, new Comparator<File>() {
                        @Override
                        public int compare(File file1, File file2) {
                            if (file1.isDirectory() && file2.isFile())
                                return -1;
                            if (file1.isFile() && file2.isDirectory())
                                return 1;
                            return file1.getName().toLowerCase().compareTo(file2.getName().toLowerCase());
                        }
                    });
                    for (int i = 0; i < datas.size(); i++) {
                        File tempFile = datas.get(i);
                        FileInfo fileInfo1 = new FileInfo();
                        fileInfo1.setType(FileInfo.TYPE_SD_FILE);
                        fileInfo1.setFile(tempFile);
                        fileInfos.add(fileInfo1);
                    }
                }else if (fileInfo.getType() == FileInfo.TYPE_OTG_FILE) {
                    UsbFile usbFile = fileInfo.getUsbFile();
                    UsbFile[] usbFiles = null;
                    try {
                        usbFiles = usbFile.listFiles();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    List<UsbFile> datas = Arrays.asList(usbFiles);
                    Collections.sort(datas, new Comparator<UsbFile>() {
                        @Override
                        public int compare(UsbFile file1, UsbFile file2) {
                            if (file1.isDirectory() && !file2.isDirectory())
                                return -1;
                            if (!file1.isDirectory() && file2.isDirectory())
                                return 1;
                            return file1.getName().toLowerCase().compareTo(file2.getName().toLowerCase());
                        }
                    });
                    for (int i = 0; i < datas.size(); i++) {
                        UsbFile tempFile = datas.get(i);
                        Log.i("peak", "usbFile.name="+tempFile.getName());
                        if (tempFile.getName().startsWith(".")){
                            continue;
                        }
                        if (!tempFile.isDirectory() && !ImageUtils.isValideImage(tempFile)){
                            continue;
                        }
                        FileInfo fileInfo1 = new FileInfo();
                        fileInfo1.setType(FileInfo.TYPE_OTG_FILE);
                        fileInfo1.setUsbFile(tempFile);
                        fileInfos.add(fileInfo1);
                    }
                }
                return fileInfos;
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<FileInfo> fileInfos) {
            super.onPostExecute(fileInfos);
//            loadingV.setVisibility(View.GONE);
            if (fileInfos != null) {
                adapter.setData(fileInfos);
                adapter.notifyDataSetChanged();
            }
        }
    }


    class CopyTask extends AsyncTask<UsbFile, Integer, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            loadingV.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(UsbFile... usbFiles) {
            UsbFile usbFile = usbFiles[0];
            if (usbFile != null){
                File file = new File(IMG_DIR + File.separator + usbFile.getName());
                try{
                    FileOutputStream fos = new FileOutputStream(file);
                    InputStream is = new UsbFileInputStream(usbFile);
                    int bytesRead = 0;
                    byte[] buffer = new byte[1024 * 8];//作者的推荐写法是currentFs.getChunkSize()为buffer长度
                    while ((bytesRead = is.read(buffer)) != -1) {
                        fos.write(buffer, 0, bytesRead);
                    }
                    fos.flush();
                    fos.close();
                    is.close();
                }catch (Exception e){
                    e.printStackTrace();
                }
                return file.getAbsolutePath();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String path) {
            super.onPostExecute(path);
            if (!TextUtils.isEmpty(path)){
                Intent intent = new Intent(ImageCropActivity.DataReceiver.ACTION_IMAGE_DATA);
                intent.putExtra(ImageContants.INTENT_KEY_ORIGIN_PATH, path);
                sendBroadcast(intent);
                finish();
            }

        }
    }
}
