package com.studio.dboard.imgpicker.ui.grid.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.github.mjdev.libaums.fs.UsbFile;
import com.studio.dboard.R;
import com.studio.dboard.imgpicker.data.FileInfo;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class DirsAdapter extends BaseAdapter {


    private Context mContext;
    private List<FileInfo> datas = new ArrayList<>();

    public DirsAdapter(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return datas.size();
    }

    @Override
    public FileInfo getItem(int i) {
        return datas.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public void setData(List files){
        this.datas.clear();
        this.datas.addAll(files);
    }

    public void clear(){
        this.datas.clear();
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        DirHolder holder;
        if (convertView == null){
            convertView = LayoutInflater.from(mContext).inflate(R.layout.layout_image_dir_listitem, null);
            holder = new DirHolder();
            holder.img = (ImageView) convertView.findViewById(R.id.img);
            holder.name = convertView.findViewById(R.id.name);
            convertView.setTag(holder);
        }else {
            holder = (DirHolder) convertView.getTag();
        }

        FileInfo fileInfo = getItem(position);

        if (fileInfo.getType() == FileInfo.TYPE_SD_FILE) {
            File file = fileInfo.getFile();
            if (file.isDirectory()) {
                holder.img.setImageResource(R.drawable.folder_select);
            } else {
                Glide.with(mContext)
                        .load(file)
                        .apply(new RequestOptions()
                                .placeholder(R.drawable.picture_icon)
                                .error(R.drawable.picture_icon)
                        .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC))
                        .thumbnail( 0.1f )
                        .into(holder.img);
            }
            holder.name.setText(file.getName());
        } else if (fileInfo.getType() == FileInfo.TYPE_OTG_FILE) {
            UsbFile usbFile = fileInfo.getUsbFile();
            if (usbFile.isDirectory()) {
                holder.img.setImageResource(R.drawable.folder_select);
            } else {
                holder.img.setImageResource(R.drawable.picture_icon);
            }
            holder.name.setText(usbFile.getName());
        }

        return convertView;
    }


    class DirHolder {
        ImageView img;
        TextView name;
    }
}
