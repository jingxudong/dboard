package com.studio.dboard;

import android.animation.ValueAnimator;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.PixelFormat;
import android.net.ConnectivityManager;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteCallbackList;
import android.os.RemoteException;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.services.weather.LocalWeatherForecastResult;
import com.amap.api.services.weather.LocalWeatherLive;
import com.amap.api.services.weather.LocalWeatherLiveResult;
import com.amap.api.services.weather.WeatherSearch;
import com.amap.api.services.weather.WeatherSearchQuery;
import com.studio.dboard.allog.LogcatHandler;
import com.studio.dboard.ui.mainui.BoardActivity;
import com.studio.dboard.allog.LogUtils;
import com.studio.dboard.dao.GreenDaoManager;
import com.studio.dboard.entity.Config;
import com.studio.dboard.update.UpdateManager;
import com.studio.dboard.utils.AppUtils;
import com.studio.dboard.utils.NetworkUtils;
import com.studio.dboard.utils.PermissionUtils;
import com.studio.dboard.view.FlowBubble;

import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

public class BoardService extends Service implements AMapLocationListener {

    private static final String tag = "BoardService";

    //脉速表偏移量
    private static final float SPEED_OFFSET = 0.04f;

    private RemoteCallbackList<ISpeedListener> speedCallbackList = new RemoteCallbackList<>();
    private RemoteCallbackList<IWeatherListener> weatherCallbackList = new RemoteCallbackList<>();
    private RemoteCallbackList<INaviListener> naviCallbackList = new RemoteCallbackList<>();
    private RemoteCallbackList<ITimeListener> timeCallbackList = new RemoteCallbackList<>();

    private AMapLocationClient mLocationClient;
    private AMapLocationClientOption mLocationOption;

    private static String newCity = "";
    private static String oldCity = "";
    private static String newDistrict = "";
    private static String oldDistrict = "";


    private Config config;

    private FlowBubble flowBubble;

    private Timer timeTimer;
    private TimerTask timeTimerTask;

    private NetworkReceiver networkReceiver;
    private TimeReceiver timeReceiver;
    private AutoNaviReceiver naviReceiver;
    private FlowBubbleReceiver flowBubbleReceiver;

    private WindowManager windowManager;
    private WindowManager.LayoutParams windowManagerParams;

    private boolean isFlowBubbleVisible = false;

    private Handler handler = new Handler();

    public class BoardBinder extends Binder {

        public void registerSpeedListener(ISpeedListener speedListener){
            speedCallbackList.register(speedListener);
        }

        public void unregisterSpeedListener(ISpeedListener speedListener){
            speedCallbackList.unregister(speedListener);
        }

        public void registerWeatherListener(IWeatherListener weatherListener){
            weatherCallbackList.register(weatherListener);
        }

        public void unregisterWeatherListener(IWeatherListener weatherListener){
            weatherCallbackList.unregister(weatherListener);
        }

        public void registerNaviListener(INaviListener naviListener){
            naviCallbackList.register(naviListener);
        }

        public void unregisterNaviListener(INaviListener naviListener){
            naviCallbackList.unregister(naviListener);
        }

        public void registerTimeListener(ITimeListener timeListener){
            timeCallbackList.register(timeListener);
        }

        public void unregisterTimeListener(ITimeListener timeListener){
            timeCallbackList.unregister(timeListener);
        }

        @Deprecated
        public void displayFlowBubble(){
            if (config.getEnableFlowBubble()) {
                displayFlowBubbleIml();
            }
        }

        @Deprecated
        public void dismissFlowBubble(){
            dismissFlowBubbleIml();
        }

        public void queryWeather(){
            queryWeatherWithCity();
        }
    }


    @Override
    public IBinder onBind(Intent intent) {
        return new BoardBinder();
    }


    @Override
    public void onCreate() {
        super.onCreate();
        init();
    }

    private void init() {
        initConfig();
        initLogCat();
        startGps();
        startTimeTimer();
        initFlowBubble();
        displayFlowBubbleIml();
        registerReceiver();
        checkUpdate();
    }

    private void checkUpdate(){
        new UpdateManager(getApplicationContext()).checkUpdate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        LogUtils.i(tag, "onStartCommand");
        return START_NOT_STICKY;
    }

    /**
     * 初始化悬浮球
     */
    private void initFlowBubble(){
        windowManager = (WindowManager)getSystemService(Context.WINDOW_SERVICE);
        windowManagerParams = new WindowManager.LayoutParams();
        windowManagerParams.type = WindowManager.LayoutParams.TYPE_SYSTEM_ALERT;
        windowManagerParams.format = PixelFormat.RGBA_8888;
        windowManagerParams.flags = WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                | WindowManager.LayoutParams.FLAG_FULLSCREEN
                | WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN;
        int size = getResources().getDimensionPixelSize(R.dimen.dp_166);
        int x = getResources().getDimensionPixelSize(R.dimen.dp_562);
        int y = getResources().getDimensionPixelSize(R.dimen.dp_165);
        windowManagerParams.x = x;
        windowManagerParams.y = y;
        windowManagerParams.width = size;
        windowManagerParams.height = size;
        windowManagerParams.gravity = Gravity.LEFT | Gravity.TOP; //设置坐标原点位置
        flowBubble = (FlowBubble) LayoutInflater.from(getApplicationContext()).inflate(R.layout.flow_bubble, null);
        flowBubble.setWindowManager(windowManager);
        flowBubble.setWindowManagerParams(windowManagerParams);
        flowBubble.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), BoardActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
    }

    /**
     * 显示悬浮球
     */
    private void displayFlowBubbleIml(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if(!Settings.canDrawOverlays(getApplicationContext())) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                return;
            }
        }

        if (config.getEnableFlowBubble()) {
            if (!isFlowBubbleVisible) {
                windowManager.addView(flowBubble, windowManagerParams);
                isFlowBubbleVisible = true;
            }
        }
    }

    /**
     * 隐藏悬浮球
     */
    private void dismissFlowBubbleIml(){
        if (isFlowBubbleVisible){
            windowManager.removeViewImmediate(flowBubble);
            isFlowBubbleVisible = false;
        }
    }

    /**
     * 日志查询
     */
    private void initLogCat(){
        LogcatHandler.getInstance(getApplicationContext()).start();
    }

    private void registerReceiver(){
        networkReceiver = new NetworkReceiver();
        IntentFilter networkFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(networkReceiver, networkFilter);

        timeReceiver = new TimeReceiver();
        IntentFilter timeFilter = new IntentFilter(Intent.ACTION_TIME_TICK);
        registerReceiver(timeReceiver, timeFilter);

        naviReceiver = new AutoNaviReceiver();
        IntentFilter naviFilter = new IntentFilter(AutoNaviReceiver.AUTONAVI_STANDARD_BROADCAST_SEND);
        registerReceiver(naviReceiver, naviFilter);

        flowBubbleReceiver = new FlowBubbleReceiver();
        IntentFilter bubbleFilter = new IntentFilter();
        bubbleFilter.addAction(FlowBubbleReceiver.ACTION_BUBBLE_DISPLAY);
        bubbleFilter.addAction(FlowBubbleReceiver.ACTION_BUBBLE_DISMISS);
        registerReceiver(flowBubbleReceiver, bubbleFilter);

    }

    private void unregisterReceiver(){
        unregisterReceiver(networkReceiver);
        unregisterReceiver(timeReceiver);
        unregisterReceiver(naviReceiver);
    }

    private void initConfig(){
        config = GreenDaoManager.getInstance(getApplicationContext())
                .getSession()
                .getConfigDao()
                .load(0L);
    }

    private void startTimeTimer(){
        stopTimeTimer();
        timeTimer = new Timer();
        timeTimerTask = new TimeTask();
        timeTimer.schedule(timeTimerTask, 0, 1000);
    }

    private void stopTimeTimer(){
        if (timeTimer != null){
            timeTimer.cancel();
            timeTimer = null;
        }
    }

    private void startGps(){
        mLocationClient = new AMapLocationClient(getApplicationContext());
        //初始化定位参数
        mLocationOption = new AMapLocationClientOption();
        //设置定位模式为高精度模式，Battery_Saving为低功耗模式，Device_Sensors是仅设备模式
        mLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Hight_Accuracy);
        //设置定位间隔,单位毫秒,默认为1000ms
        mLocationOption.setInterval(1000);
        //设置是否只定位一次,默认为false
        mLocationOption.setOnceLocation(false);
        mLocationClient.setLocationOption(mLocationOption);
        mLocationClient.setLocationListener(this);
        mLocationClient.startLocation();
    }

    private void destroyGps(){
        if (mLocationClient != null){
            mLocationClient.stopLocation();
            mLocationClient.unRegisterLocationListener(this);
            mLocationClient.onDestroy();
        }
    }

    @Override
    public void onLocationChanged(AMapLocation aMapLocation) {
        if (aMapLocation != null) {
            if (aMapLocation.getLocationType() != 0 && aMapLocation.getErrorCode() == 0) {//定位成功
                newCity = aMapLocation.getCity();
                newDistrict = aMapLocation.getDistrict();

//                LogUtils.i(tag, "newCity="+newCity + "  oldCity="+oldCity);
                if (!TextUtils.isEmpty(newCity) && !newCity.equals(oldCity)){
                    oldCity = newCity;
                    oldDistrict = newDistrict;
                    queryWeatherWithCity();
                }

                if (config.getIsSpeedMock()){
                    return;
                }

                double speedDouble = aMapLocation.getSpeed();
                //汽车脉速表普遍比实际速度高2%-5%
                int progress = config.getSpeedOffset();
                int value = progress - Config.DEFAULT_SPEED_OFFSET;
                float offset = value * 1.0f / 100;
                int speed = 0;
                //由于定位漂移，所以过滤掉速度小于1时的情况
                if (speedDouble > 1) {
                    //增加偏移量
                    speed = (int) Math.floor(((float)(speedDouble)) * 3.6f * (1 + SPEED_OFFSET) * (1 + offset));
                }
                sendSpeed(speed);
            }else {
                if (config.getIsSpeedMock()){
                    return;
                }
                sendSpeed(-1);
            }
        }else {
            if (config.getIsSpeedMock()){
                return;
            }
            sendSpeed(-1);
        }

    }

    private synchronized void sendSpeed(int speed){
        if (flowBubble != null && !config.getIsSpeedMock()) {
            flowBubble.setSpeed(speed);
        }

        try {
            int count = speedCallbackList.beginBroadcast();
            for (int i = 0; i < count; i++){
                ISpeedListener speedCallback = speedCallbackList.getBroadcastItem(i);
                speedCallback.onSpeed(speed);
            }
            speedCallbackList.finishBroadcast();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private synchronized void sendMockSpeed(int speed){
        try {
            int count = speedCallbackList.beginBroadcast();
            for (int i = 0; i < count; i++){
                ISpeedListener speedCallback = speedCallbackList.getBroadcastItem(i);
                speedCallback.onSpeed(speed);
            }
            speedCallbackList.finishBroadcast();
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    private void queryWeatherWithCity(){
        LogUtils.i(tag, "queryWeatherWithCity-oldCity="+oldCity);
        if (!TextUtils.isEmpty(oldCity)) {
            WeatherSearchQuery query = new WeatherSearchQuery(oldCity, WeatherSearchQuery.WEATHER_TYPE_LIVE);
            WeatherSearch weatherSearch = new WeatherSearch(getApplicationContext());
            weatherSearch.setOnWeatherSearchListener(new WeatherSearch.OnWeatherSearchListener() {
                @Override
                public void onWeatherLiveSearched(LocalWeatherLiveResult localWeatherLiveResult, int code) {
                    String weatherState = "";
                    String temperature = "";
                    LogUtils.i(tag, "weather code="+code);
                    if (code == 1000){
                        if (localWeatherLiveResult != null && localWeatherLiveResult.getLiveResult() != null) {
                            LocalWeatherLive weatherLive = localWeatherLiveResult.getLiveResult();
                            weatherState = weatherLive.getWeather();
                            temperature = weatherLive.getTemperature();
                        }else {
                            weatherState = "";
                            temperature = "";
                        }
                    }else {
                        weatherState = "";
                        temperature = "";
                    }

                    //市域获取不到天气数据的话，从区级获取天气数据
                    if (TextUtils.isEmpty(weatherState) || TextUtils.isEmpty(temperature)){
                        queryWeatherWithDistrict();
                    }
                    LogUtils.i(tag, "weatherState="+weatherState + "  temperature="+temperature);
                    sendWeather(oldCity, weatherState, temperature);
                }

                @Override
                public void onWeatherForecastSearched(LocalWeatherForecastResult localWeatherForecastResult, int i) {

                }
            });
            weatherSearch.setQuery(query);
            weatherSearch.searchWeatherAsyn();
        }else {
            sendWeather("", "", "");
        }
    }

    private void queryWeatherWithDistrict(){
        if (!TextUtils.isEmpty(oldDistrict)) {
            WeatherSearchQuery query = new WeatherSearchQuery(oldDistrict, WeatherSearchQuery.WEATHER_TYPE_LIVE);
            WeatherSearch weatherSearch = new WeatherSearch(getApplicationContext());
            weatherSearch.setOnWeatherSearchListener(new WeatherSearch.OnWeatherSearchListener() {
                @Override
                public void onWeatherLiveSearched(LocalWeatherLiveResult localWeatherLiveResult, int code) {
                    String weatherState = "";
                    String temperature = "";
                    if (code == 1000){
                        if (localWeatherLiveResult != null && localWeatherLiveResult.getLiveResult() != null) {
                            LocalWeatherLive weatherLive = localWeatherLiveResult.getLiveResult();
                            weatherState = weatherLive.getWeather();
                            temperature = weatherLive.getTemperature();
                        }else {
                            weatherState = "";
                            temperature = "";
                        }
                    }else {
                        weatherState = "";
                        temperature = "";
                    }

                    sendWeather(oldDistrict, weatherState, temperature);
                }

                @Override
                public void onWeatherForecastSearched(LocalWeatherForecastResult localWeatherForecastResult, int i) {

                }
            });
            weatherSearch.setQuery(query);
            weatherSearch.searchWeatherAsyn();
        }
    }

    private synchronized void sendWeather(String local, String weatherState, String temperature){
        try {
            int count = weatherCallbackList.beginBroadcast();
            for (int i = 0; i < count; i++){
                IWeatherListener weatherListener = weatherCallbackList.getBroadcastItem(i);
                weatherListener.onWeather(local, weatherState, temperature);
            }
            weatherCallbackList.finishBroadcast();
        } catch (Exception ex){
            ex.printStackTrace();
        } catch (Error e){
            e.printStackTrace();
        } finally {
            try {
                weatherCallbackList.finishBroadcast();
            }catch (Exception ex){
                ex.printStackTrace();
            } catch (Error e){
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        destroyGps();
        unregisterReceiver();
    }

    /**
     * 计时，每秒钟刷新一下时间
     */
    class TimeTask extends TimerTask {

        private int num = 0;
        private double angle = 0;
        private int speed = 0;

        private int countTime = 0;

        @Override
        public void run() {
            timeBlock();
            boardBlock();
            speedMockBlock();
        }

        public void reset(){
            angle = 0;
            num = 0;
            speed = 0;
        }

        /**
         * 读取时间并通知给界面
         */
        private void timeBlock(){
            Calendar cal = Calendar.getInstance();
            int hour = cal.get(Calendar.HOUR_OF_DAY);
            int min = cal.get(Calendar.MINUTE);
            int second = cal.get(Calendar.SECOND);
            try {
                int count = timeCallbackList.beginBroadcast();
                for (int i = 0; i < count; i++){
                    ITimeListener timeListener = timeCallbackList.getBroadcastItem(i);
                    timeListener.onTime(hour, min, second);
                }
                timeCallbackList.finishBroadcast();
            }catch (RemoteException e){
                e.printStackTrace();
            }
        }

        /**
         * 计时弹出仪表界面
         */
        private void boardBlock(){
            boolean enable = true;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
                if (PermissionUtils.hasUsagePermission(getApplicationContext())){
                    enable = true;
                }else {
                    enable = false;
                }
            }
            if (enable) {
                int time = config.getDashStartTime();
                if (time != -1) {
                    if (AppUtils.isHomeToTopAct(getApplicationContext())) {
                        countTime++;
                        if (countTime % time == 0) {
                            countTime = 0;
                            Intent intent = new Intent(getApplicationContext(), BoardActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                    } else {
                        countTime = 0;
                    }
                } else {
                    countTime = 0;
                }
            }
        }

        /**
         * 速度模拟并通知界面
         */
        private void speedMockBlock(){
            //速度模拟
            if (config.getIsSpeedMock()) {
                angle = num * Math.PI / 360.0;
                num += 5;
                if (num % 180 == 0) {
                    num = 0;
                }
                speed = Math.max((int) (Math.sin(angle) * 200) - 10, 0);
                LogUtils.i(tag, "speed="+speed);
                sendMockSpeed(speed);
            }else {
                reset();
            }
        }
    }


    /**
     * 监听网络变化广播
     */
    public class NetworkReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
                if (NetworkUtils.isNetworkConnected(context)){
                    queryWeatherWithCity();
                }
            }
        }
    }

    /**
     * 监听时间变化
     */
    class TimeReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Intent.ACTION_TIME_TICK)){
                Calendar cal = Calendar.getInstance();
                int hour = cal.get(Calendar.HOUR_OF_DAY);
                int min = cal.get(Calendar.MINUTE);
                if (min % 30 == 0) { //每半小时获取一次天气
                    queryWeatherWithCity();
                }
            }
        }
    }

    /**
     * 高德导航信息透传
     */
    class AutoNaviReceiver extends BroadcastReceiver {

        /**
         * 发送请求广播时用
         */
        public static final String AUTONAVI_STANDARD_BROADCAST_RECV = "AUTONAVI_STANDARD_BROADCAST_RECV";
        /**
         * 接收信息
         */
        public static final String AUTONAVI_STANDARD_BROADCAST_SEND = "AUTONAVI_STANDARD_BROADCAST_SEND";

        /**
         * 协议的唯一ID
         */
        public static final String KEY_TYPE = "KEY_TYPE";

        /**
         * 导航转向图标，对应的值为int类型
         */
        public static final String ICON = "ICON";

        //导航类型，对应的值为int类型
        //0：GPS导航
        //1：模拟导航
        public static final String TYPE = "TYPE";

        //下一道路名，对应的值为String类型
        public static final String NEXT_ROAD_NAME = "NEXT_ROAD_NAME";

        //当前导航段剩余距离，对应的值为int类型，单位：米
        public static final String SEG_REMAIN_DIS = "SEG_REMAIN_DIS";

        /**
         * 高德地图运行状态
         */
        public static final String EXTRA_STATE = "EXTRA_STATE";

        //电子眼限速度，对应的值为int类型，无限速则为0，单位：公里/小时
        public static final String CAMERA_SPEED = "CAMERA_SPEED";
        //下一个将要路过的电子眼编号，若为-1则对应的道路上没有电子眼，对应的值为int类型
        public static final String CAMERA_INDEX = "CAMERA_INDEX";
        //距离最近的电子眼距离，对应的值为int类型，单位：米
        public static final String CAMERA_DIST = "CAMERA_DIST";

        @Override
        public void onReceive(Context context, Intent intent) {
            if (!config.getEnableGpsThrough()){
                return;
            }
            String action = intent.getAction();
            if (action.equals(AUTONAVI_STANDARD_BROADCAST_SEND)){
                int keyType = intent.getIntExtra(KEY_TYPE, -1);
                switch (keyType){
                    case 10041: //导航版本信息
                        String versionNum = intent.getStringExtra("VERSION_NUM");
                        String channelNum = intent.getStringExtra("CHANNEL_NUM");
                        break;
                    case 10001:
                        int type = intent.getIntExtra(TYPE, -1);
                        int iconIndex = intent.getIntExtra(ICON, -1);
                        String roadName = intent.getStringExtra(NEXT_ROAD_NAME);
                        int distance = intent.getIntExtra(SEG_REMAIN_DIS, -1);

                        try {
                            int count = naviCallbackList.beginBroadcast();
                            for (int i = 0; i < count; i++){
                                INaviListener naviListener = naviCallbackList.getBroadcastItem(i);
                                try {
                                    naviListener.onNaviInfo(iconIndex, distance, roadName);
                                } catch (RemoteException e) {
                                    e.printStackTrace();
                                }
                            }
                            naviCallbackList.finishBroadcast();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        break;
                    case 10019:
                        int extraState = intent.getIntExtra(EXTRA_STATE, -1);
                        try {
                            int count = naviCallbackList.beginBroadcast();
                            for (int i = 0; i < count; i++) {
                                INaviListener naviListener = naviCallbackList.getBroadcastItem(i);
                                try {
                                    naviListener.onNaviStateChanged(extraState);
                                } catch (RemoteException e) {
                                    e.printStackTrace();
                                }
                            }
                            naviCallbackList.finishBroadcast();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        break;
                }
            }
        }
    }

    /**
     * 控制悬浮球显示或隐藏
     */
    public class FlowBubbleReceiver extends BroadcastReceiver{

        public static final String ACTION_BUBBLE_DISPLAY = Constant.ROOT_ACTION + "ACTION_BUBBLE_DISPLAY";
        public static final String ACTION_BUBBLE_DISMISS = Constant.ROOT_ACTION +  "ACTION_BUBBLE_DISMISS";

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(ACTION_BUBBLE_DISPLAY)){
                displayFlowBubbleIml();
            }else if (action.equals(ACTION_BUBBLE_DISMISS)){
                dismissFlowBubbleIml();
            }
        }
    }
}
