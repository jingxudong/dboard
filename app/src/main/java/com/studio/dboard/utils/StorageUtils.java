package com.studio.dboard.utils;

import android.app.ActivityManager;
import android.content.Context;
import android.hardware.usb.UsbDevice;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.os.storage.StorageManager;
import android.text.format.Formatter;
import android.util.Log;

import com.github.mjdev.libaums.UsbMassStorageDevice;
import com.studio.dboard.imgpicker.data.StorageInfo;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class StorageUtils {


    /**
     * 获得SD卡总大小
     *
     * @return
     */
    public static String getSDTotalSizeFormat(Context context) {
        File path = Environment.getExternalStorageDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSize();
        long totalBlocks = stat.getBlockCount();
        return Formatter.formatFileSize(context, blockSize * totalBlocks);
    }

    /**
     * 获得SD卡总大小
     *
     * @return
     */
    public static long getSDTotalSize(Context context) {
        File path = Environment.getExternalStorageDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSize();
        long totalBlocks = stat.getBlockCount();
        return blockSize * totalBlocks;
    }

    /**
     * 获得sd卡剩余容量，即可用大小
     *
     * @return
     */
    public static String getSDAvailableSizeFormat(Context context) {
        File path = Environment.getExternalStorageDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSize();
        long availableBlocks = stat.getAvailableBlocks();
        return Formatter.formatFileSize(context, blockSize * availableBlocks);
    }

    /**
     * 获得sd卡剩余容量，即可用大小
     *
     * @return
     */
    public static long getSDAvailableSize(Context context) {
        File path = Environment.getExternalStorageDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSize();
        long availableBlocks = stat.getAvailableBlocks();
        return blockSize * availableBlocks;
    }

    /**
     * 获得机身内部存储空间总大小
     *
     * @return
     */
    public static String getRomTotalSize(Context context) {
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSize();
        long totalBlocks = stat.getBlockCount();
        return Formatter.formatFileSize(context, blockSize * totalBlocks);
    }

    /**
     * 获得机身可用内部存储大小
     *
     * @return
     */
    public static String getRomAvailableSize(Context context) {
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSize();
        long availableBlocks = stat.getAvailableBlocks();
        return Formatter.formatFileSize(context, blockSize * availableBlocks);
    }


    public static boolean isSdSizeAvailable(Context context, int needSize){
        long availableSize = getSDAvailableSize(context);
        if (availableSize >> 20 > (needSize + 50)){
            return true;
        }
        return false;
    }


    /**
     * 获取总运行内存大小
     * @param context
     * @return
     */
    public static String getTotalMemory(Context context){
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        activityManager.getMemoryInfo(memoryInfo);
        return Formatter.formatFileSize(context, memoryInfo.totalMem);
    }

    /**
     * 获取可用运行内存大小
     * @param context
     * @return
     */
    public static String getAvailMemory(Context context){
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        activityManager.getMemoryInfo(memoryInfo);
        return Formatter.formatFileSize(context, memoryInfo.availMem);
    }

    /**
     * 获取低运行内存阈值
     * @param context
     * @return
     */
    public static String getThresholdMemory(Context context){
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        activityManager.getMemoryInfo(memoryInfo);
        return Formatter.formatFileSize(context, memoryInfo.threshold);
    }

    /**
     * 获取低内存的状态
     * @param context
     * @return
     */
    public static boolean getLowMemoryState(Context context){
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        activityManager.getMemoryInfo(memoryInfo);
        return memoryInfo.lowMemory;
    }

    public static boolean isTotalMemoThan512(Context context){
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        activityManager.getMemoryInfo(memoryInfo);
        if (memoryInfo.totalMem >> 20 > 512){
            return true;
        }else {
            return false;
        }
    }


    public static List<StorageInfo> getAllStorages(Context context){
        ArrayList<StorageInfo> storages = new ArrayList<>();
        storages.addAll(listAllInternalStorage(context));
//        storages.addAll(listAllUsbStorage(context));
        return storages;
    }

    public static List<StorageInfo> listAllInternalStorage(Context context) {
        ArrayList<StorageInfo> storages = new ArrayList<>();
        StorageManager mStorageManager = (StorageManager) context
                .getSystemService(Context.STORAGE_SERVICE);
        Class<?> storageVolumeClazz = null;
        try {
            Method getVolumeList = mStorageManager.getClass().getMethod("getVolumeList");
            getVolumeList.setAccessible(true);
            Object result = getVolumeList.invoke(mStorageManager);
            final int length = Array.getLength(result);

            storageVolumeClazz = Class.forName("android.os.storage.StorageVolume");
            Method getPath = storageVolumeClazz.getMethod("getPath");
            getPath.setAccessible(true);

            Method getVolumeState = StorageManager.class.getDeclaredMethod("getVolumeState",
                    String.class);
            getVolumeState.setAccessible(true);

            Method getUserLabel = null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                getUserLabel = storageVolumeClazz.getMethod("getUserLabel");
                getUserLabel.setAccessible(true);
            }

            for (int i = 0; i < length; i++) {
                Object storageVolume = Array.get(result, i);
                String path = (String) getPath.invoke(storageVolume);
                String state = (String) getVolumeState.invoke(mStorageManager, path);
                if (Environment.MEDIA_MOUNTED.equals(state)) {
                    StorageInfo storageInfo = new StorageInfo();
                    storageInfo.setType(StorageInfo.TYPE_SD_STORAGE);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        String userLabel = (String) getUserLabel.invoke(storageVolume);
                        storageInfo.setName(userLabel);
                    }else {
                        String name = "";
                        int index = path.lastIndexOf("/");
                        if (index != -1){
                            name = path.substring(index + 1);
                        }
                        storageInfo.setName("存储-" + name);
                    }
                    storageInfo.setRootPath(path);
                    storages.add(storageInfo);
                }


            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return storages;
    }

    /**
     * 获取内部存储（SD卡）
     * @return
     */
    public static List<StorageInfo> listAllInternalStorage() {
        List<StorageInfo> storages = new ArrayList();
        String firstPath = Environment.getExternalStorageDirectory().getPath();
        StorageInfo storageInfo = new StorageInfo();
        storageInfo.setType(StorageInfo.TYPE_SD_STORAGE);
        storageInfo.setName("内部存储设备");
        storageInfo.setRootPath(firstPath);
        storages.add(storageInfo);

        try {
            // 运行mount命令，获取命令的输出，得到系统中挂载的所有目录
            Runtime runtime = Runtime.getRuntime();
            runtime.exec("su");
            Process proc = runtime.exec("mount");
            InputStream is = proc.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            String line;
            BufferedReader br = new BufferedReader(isr);
            while ((line = br.readLine()) != null) {

                // 将常见的linux分区过滤掉
                if (line.contains("proc") || line.contains("tmpfs") || line.contains("media")
                        || line.contains("asec") || line.contains("secure") || line.contains("system")
                        || line.contains("cache") || line.contains("sys") || line.contains("data")
                        || line.contains("shell") || line.contains("root") || line.contains("acct")
                        || line.contains("misc") || line.contains("obb")) {
                    continue;
                }

                // 下面这些分区是我们需要的
                if (line.contains("fat") || line.contains("fuse") || (line.contains("ntfs"))){
                    Log.i("line", "line="+line);
                    // 将mount命令获取的列表分割，items[0]为设备名，items[1]为挂载路径
                    String items[] = line.split(" ");
                    if (items != null && items.length > 1){
                        String path = items[1].toLowerCase(Locale.getDefault());
                        StorageInfo storageInfo1 = new StorageInfo();
                        storageInfo1.setType(StorageInfo.TYPE_SD_STORAGE);
                        storageInfo1.setName(items[0]);
                        storageInfo1.setRootPath(items[1]);
                        // 添加一些判断，确保是sd卡，如果是otg等挂载方式，可以具体分析并添加判断条件
                        if (path != null && !storages.contains(storageInfo1) && path.contains("sd")) {
                            storages.add(storageInfo1);
                        }
                    }
                }
            }

            if (br != null){
                br.close();
            }
            if (isr != null){
                isr.close();
            }
            if (is != null){
                is.close();
            }
        } catch (Exception e){
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return storages;
    }

    /**
     * 获取OTG存储（一般为U盘存储）
     * @param context
     * @return
     */
    public static List<StorageInfo> listAllUsbStorage(Context context){
        ArrayList<StorageInfo> storages = new ArrayList<>();
        try {
            UsbMassStorageDevice[] devices = UsbMassStorageDevice.getMassStorageDevices(context);
            for (int i = 0; i < devices.length; i++) {
                UsbMassStorageDevice device = devices[i];
                UsbDevice usbDevice = device.getUsbDevice();
                StorageInfo storageInfo = new StorageInfo();
                storageInfo.setType(StorageInfo.TYPE_OTG_STORAGE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    storageInfo.setName(usbDevice.getManufacturerName() + usbDevice.getProductName());
                } else {
                    storageInfo.setName("U盘存储" + (i + 1));
                }
                storageInfo.setUsbDevice(usbDevice);
                storages.add(storageInfo);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return storages;
    }
}
