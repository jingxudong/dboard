package com.studio.dboard.utils;


import android.util.Base64;

public class Coder {
    public static byte[] decryptBASE64(String key) {
        return Base64.decode(key.getBytes(), Base64.DEFAULT);
    }

    public static String encryptBASE64(byte[] key) {
        return new String(Base64.decode(key, Base64.DEFAULT));
    }
}
