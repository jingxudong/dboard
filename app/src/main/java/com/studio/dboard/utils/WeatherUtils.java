package com.studio.dboard.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;


import com.studio.dboard.Constant;
import com.studio.dboard.R;
import com.studio.dboard.allog.LogUtils;
import com.studio.dboard.entity.TimeImage;
import com.studio.dboard.entity.Weather;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by peak on 2017/11/8.
 */

public class WeatherUtils {

    public static Bitmap getWeatherImage(Context context, String weatherState){
        if (TextUtils.isEmpty(weatherState)){
            return null;
        }
        int position = -1;
        switch (weatherState){
            case "晴":
                position = 0;
                break;
            case "多云":
                position = 1;
                break;
            case "阴":
                position = 2;
                break;
            case "阵雨":
                position = 3;
                break;
            case "雷阵雨":
                position = 4;
                break;
            case "雷阵雨并伴有冰雹":
                position = 5;
                break;
            case "雨夹雪":
                position = 6;
                break;
            case "小雨":
                position = 7;
                break;
            case "中雨":
                position = 8;
                break;
            case "大雨":
                position = 9;
                break;
            case "暴雨":
            case "大暴雨":
            case "特大暴雨":
                position = 10;
                break;
            case "阵雪":
                position = 11;
                break;
            case "小雪":
                position = 12;
                break;
            case "中雪":
                position = 13;
                break;
            case "大雪":
                position = 14;
                break;
            case "暴雪":
                position = 15;
                break;
            case "轻雾":
            case "雾":
                position = 16;
                break;
            case "冻雨":
                position = 17;
                break;
            case "沙尘暴":
            case "扬沙":
            case "强沙尘暴":
                position = 18;
                break;
            case "小雨-中雨":
                position = 19;
                break;
            case "中雨-大雨":
                position = 20;
                break;
            case "大雨-暴雨":
            case "暴雨-大暴雨":
            case "大暴雨-特大暴雨":
                position = 21;
                break;
            case "小雪-中雪":
                position = 22;
                break;
            case "中雪-大雪":
                position = 23;
                break;
            case "大雪-暴雪":
                position = 24;
                break;
            case "浮尘":
                position = 25;
                break;
            case "飑":
                position = 26;
                break;
            case "龙卷风":
                position = 27;
                break;
            case "弱高吹雪":
                position = 28;
                break;
            case "霾":
                position = 29;
                break;
            default:
                return null;
        }

        TypedArray typedArray = context.getResources().obtainTypedArray(R.array.weather_imgs);
        int[] resIds = new int[typedArray.length()];
        for (int i = 0; i < resIds.length; i++){
            resIds[i] = typedArray.getResourceId(i, 0);
        }
        return ImageUtils.getBitmap(context, resIds[position], 1.0f, 1.0f);
    }

    public static Drawable getWeatherBgDrawable(Context context, ArrayList<Weather> weathers, String weatherState, int hour){
        try {
            for (Weather weather : weathers){
                if (weather.getName().equals(weatherState)){
                    ArrayList<TimeImage> timeImages = weather.getTimeImages();
                    for (TimeImage timeImage : timeImages){
                        if (hour >= timeImage.getBegin() && hour < timeImage.getEnd()){
                            ArrayList<String> images = timeImage.getImages();
                            Random random = new Random();
                            int index = random.nextInt(images.size());
                            String imageName = images.get(index);
                            InputStream is = context.getAssets().open(Constant.WEATHER_ASSETS_IMG_DIR +
                                    File.separator + imageName);
                            return ImageUtils.bitmap2Drawable(BitmapFactory.decodeStream(is));
                        }
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public static Bitmap getWeatherBgBitmap(Context context, ArrayList<Weather> weathers, String weatherState, int hour){
        try {
            for (Weather weather : weathers){
                if (weather.getName().equals(weatherState)){
                    ArrayList<TimeImage> timeImages = weather.getTimeImages();
                    for (TimeImage timeImage : timeImages){
                        if (hour >= timeImage.getBegin() && hour < timeImage.getEnd()){
                            ArrayList<String> images = timeImage.getImages();
                            Random random = new Random();
                            int index = random.nextInt(images.size());
                            String imageName = images.get(index);
                            LogUtils.i("peak", "imageName="+imageName);
                            InputStream is = context.getAssets().open(Constant.WEATHER_ASSETS_IMG_DIR +
                                    File.separator + imageName);
                            return BitmapFactory.decodeStream(is);
                        }
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
