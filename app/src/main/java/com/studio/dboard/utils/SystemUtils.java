package com.studio.dboard.utils;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import com.studio.dboard.allog.LogUtils;
import com.studio.dboard.dao.GreenDaoManager;
import com.studio.dboard.entity.Config;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import static android.content.Context.TELEPHONY_SERVICE;

/**
 * Created by mateng on 2017/3/29.
 */

public class SystemUtils {

    private static final String tag = "SystemUtils";

    public static String getSystemUUID(Context context) {
        Config config = GreenDaoManager.getInstance(context).getSession()
                .getConfigDao().load(0L);
        String sign = config.getSign();

//        LogUtils.log(tag,"请求服务器时从数据库中获取sign=" + sign);

        if (TextUtils.isEmpty(sign)) {
            sign = getImei(context);
            if (TextUtils.isEmpty(sign)) {
                sign = getMac(context);
            }
            if (TextUtils.isEmpty(sign)){
                sign = getAndroidId(context);
            }
            if (TextUtils.isEmpty(sign)) {
                return "";
            } else {
                config.setSign(sign);
                GreenDaoManager.getInstance(context).getSession().update(config);
            }
        }
        return MD5Util.encrypt(sign);
    }

    /**
     * 获取Mac地址
     *
     * @return
     */
    public static String getMac(Context context) {
        String macSerial = "";
        String str = "";

        try {
            Process pp = Runtime.getRuntime().exec("cat /sys/class/net/wlan0/address ");
            InputStreamReader ir = new InputStreamReader(pp.getInputStream());
            LineNumberReader input = new LineNumberReader(ir);

            for (; null != str; ) {
                str = input.readLine();
                if (str != null) {
                    macSerial = str.trim();// 去空格
                    break;
                }
            }

//            if (TextUtils.isEmpty(macSerial)){
//                macSerial = getLocalMacAddressFromBusybox();
//            }
//
//            if (TextUtils.isEmpty(macSerial)){
//                macSerial = getLocalMacAddressFromIp(context);
//            }
//
//            if (TextUtils.isEmpty(macSerial)){
//                macSerial = getLocalMacAddressFromWifiInfo(context);
//            }
        } catch (IOException ex) {
            // 赋予默认值
            ex.printStackTrace();
        }
        return macSerial;
    }

    /**
     *  * 系统版本
     *  *
     *  * @return
     *  
     */
    public static String getSystemVersion() {
        return Build.DISPLAY;
    }

    /**
     * 获取设备串号:IMEI
     * @param context
     * @return
     */
    public static String getImei(Context context) {
        TelephonyManager tm = (TelephonyManager) context.getSystemService(TELEPHONY_SERVICE);
        String imei = tm.getDeviceId();
        return imei == null ? "" : imei;
    }

    /**
     * 获取ICCID:集成电路卡识别码（固化在手机SIM卡中,就是SIM卡的序列号）很容易伪造哦
     * @return
     */
    public static String getIccid(Context context){
        TelephonyManager tm = (TelephonyManager) context.getSystemService(TELEPHONY_SERVICE);
        String iccid = tm.getSimSerialNumber();
        return iccid == null ? "" : iccid;
    }

    /**
     * 获取Android ID
     *
     * @param context
     * @return
     */
    public static String getAndroidId(Context context) {
        String androidId = Settings.System.getString(context.getContentResolver(), Settings.System.ANDROID_ID);
        if (TextUtils.isEmpty(androidId)){
            return "";
        }
        return androidId;
    }

    /**
     * 获取版本名
     *
     * @return 当前应用的版本名
     */
    public static String getVersion(Context context) {
        try {
            PackageManager manager = context.getPackageManager();
            PackageInfo info = manager.getPackageInfo(context.getPackageName(), 0);
            return info.versionName;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 从版本名中获取渠道名称
     * @param context
     * @return
     */
    public static String getChannelFromVersion(Context context){
        String versionName = getVersion(context);
        String[] splites = versionName.split("_");
        if (splites.length == 2){
            String[] strs = splites[1].split("-");
            if (strs.length >= 1){
                return strs[0];
            }
        }
        return "";
    }

    /**
     * 获取厂商码
     * @param context
     * @return
     */
    public static String getFactoryCode(Context context){
        String versionName = getVersion(context);
        String[] splites = versionName.split("-");
        return splites[splites.length - 1];
    }

    /**
     * 获取产品ID号
     * @param context
     * @return
     */
    public static String getProductId(Context context){
        String factoryId = getFactoryCode(context);
        if (!TextUtils.isEmpty(factoryId)){
            return factoryId.substring(0, 2);
        }
        return "";
    }

    /**
     * 获取版本号
     *
     * @return 当前应用的版本号
     */
    public static int getVersionCode(Context context) {
        try {
            PackageManager manager = context.getPackageManager();
            PackageInfo info = manager.getPackageInfo(context.getPackageName(), 0);
            return info.versionCode;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * 获取apk包的版本号code
     * @param context
     * @param filePath
     * @return
     */
    public static int getApkVersionCode(Context context, String filePath){
        PackageManager packageManager = context.getPackageManager();
        PackageInfo packageInfo = packageManager.getPackageArchiveInfo(filePath,
                PackageManager.GET_ACTIVITIES);
        if (packageInfo != null){
            return packageInfo.versionCode;
        }else {
            return -1;
        }
    }

    public static String getUu(){
        return getRandom(20);
    }

    private static String getRandom(int length) {
        String[] strs = new String[] {"0", "1", "2", "3", "4", "5", "6", "7",
                "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
                "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V",
                "W", "X", "Y", "Z", "a", "b", "c", "d", "e", "f", "g", "h",
                "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t",
                "u", "v", "w", "x", "y", "z" };

        final int RANDOM_LENGTH = 62;
        Random rand = new Random();

        StringBuilder uuid = new StringBuilder();
        for (int i = 0; i < length; i++) {
            int index = rand.nextInt(RANDOM_LENGTH);
            uuid.append(strs[index]);
        }
        return uuid.toString();
    }

    public static String getLocalMacAddressFromWifiInfo(Context context){
        WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        WifiInfo info = wifi.getConnectionInfo();
        return info.getMacAddress();
    }

    public static String getLocalMacAddressFromBusybox(){
        String result = "";
        String Mac = "";
        result = callCmd("busybox ifconfig","HWaddr");

        //如果返回的result == null，则说明网络不可取
        if(result==null){
            return "网络出错，请检查网络";
        }

        //对该行数据进行解析
        //例如：eth0      Link encap:Ethernet  HWaddr 00:16:E8:3E:DF:67
        if(result.length()>0 && result.contains("HWaddr")==true){
            Mac = result.substring(result.indexOf("HWaddr")+6, result.length()-1);

             /*if(Mac.length()>1){
                 Mac = Mac.replaceAll(" ", "");
                 result = "";
                 String[] tmp = Mac.split(":");
                 for(int i = 0;i<tmp.length;++i){
                     result +=tmp[i];
                 }
             }*/
            result = Mac;
        }
        return result;
    }

    private static String callCmd(String cmd, String filter) {
        String result = "";
        String line = "";
        try {
            Process proc = Runtime.getRuntime().exec(cmd);
            InputStreamReader is = new InputStreamReader(proc.getInputStream());
            BufferedReader br = new BufferedReader(is);

            //执行命令cmd，只取结果中含有filter的这一行
            while ((line = br.readLine ()) != null && line.contains(filter)== false) {
                //result += line;
            }

            result = line;
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getLocalMacAddressFromIp(Context context) {
        String mac_s= "";
        try {
            byte[] mac;
            NetworkInterface ne= NetworkInterface.getByInetAddress(InetAddress.getByName(getLocalIpAddress()));
            mac = ne.getHardwareAddress();
            mac_s = byte2hex(mac);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return mac_s;
    }

    public static String byte2hex(byte[] b) {
        StringBuffer hs = new StringBuffer(b.length);
        String stmp = "";
        int len = b.length;
        for (int n = 0; n < len; n++) {
            stmp = Integer.toHexString(b[n] & 0xFF);
            if (stmp.length() == 1)
                hs = hs.append("0").append(stmp);
            else {
                hs = hs.append(stmp);
            }
        }
        return String.valueOf(hs);
    }

    public static String getLocalIpAddress() {
        try {
            String ipv4 = "";
            List<NetworkInterface> nilist = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface ni: nilist)
            {
                List<InetAddress> ialist = Collections.list(ni.getInetAddresses());
                for (InetAddress address: ialist){
                    if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
                        if (!address.isLoopbackAddress() && address instanceof Inet4Address)
                        {
                            return ipv4;
                        }
                    }

                }

            }

        } catch (SocketException ex) {

        }
        return null;
    }


    public static final int ALLOCATION_MEMORY_32 = 32;
    public static final int MAX_ALLOCATION_MEMORY_32 = 32;

    /**
     * 获取系统给应用分配的内存大小 单位：M
     * @param context
     * @return
     */
    public static int getAllocateMemory(Context context){
        ActivityManager activityManager = (ActivityManager) context.getSystemService(
                Context.ACTIVITY_SERVICE);
        int memorySize = activityManager.getMemoryClass();
        return memorySize;
    }

    /**
     * 获取系统给应用分配的最大内存 单位：字节byte
     * @return
     */
    public static int getMaxAllocateMemory(){
        Runtime rt = Runtime.getRuntime();
        int maxMemory = (int) (rt.maxMemory() / (1024 * 1024));
        return maxMemory;
    }
}
