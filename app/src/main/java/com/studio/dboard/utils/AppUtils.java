package com.studio.dboard.utils;

import android.app.ActivityManager;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.RequiresApi;

import com.studio.dboard.allog.LogUtils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class AppUtils {


    /**
     * 获取系统桌面
     * @param context
     * @return
     */
    public static List<String> getHomes(Context context){
        List<String> names = new ArrayList<>();
        try {
            PackageManager pm = context.getPackageManager();
            Intent mainIntent = new Intent(Intent.ACTION_MAIN);
            mainIntent.addCategory(Intent.CATEGORY_HOME);
            List<ResolveInfo> resolveInfos = pm.queryIntentActivities(mainIntent,
                    PackageManager.MATCH_DEFAULT_ONLY);
            for (int i = 0; i < resolveInfos.size(); i++) {
                ResolveInfo resolveInfo = resolveInfos.get(i);
                names.add(resolveInfo.activityInfo.packageName);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return names;
    }

    public static String getTopActPkgNameLow(Context context){
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> list = am.getRunningTasks(1);
        if (list != null && list.size() > 0) {
            ComponentName cpn = list.get(0).topActivity;
            return cpn.getPackageName();
        }

        return "";
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public static String getTopActPkgNameHigh(Context context){
        UsageStatsManager m =(UsageStatsManager)context.getSystemService(Context.USAGE_STATS_SERVICE);
        long time = System.currentTimeMillis() - 24 * 60 * 60 * 1000;
        List<UsageStats> queryUsageStats = m.queryUsageStats(UsageStatsManager.INTERVAL_BEST, time, System.currentTimeMillis());
        if (queryUsageStats != null && !queryUsageStats.isEmpty()) {
            UsageStats recentStats = null;
            for (UsageStats usageStats : queryUsageStats) {
                if (recentStats == null
                        || recentStats.getLastTimeUsed() < usageStats.getLastTimeUsed()) {
                    recentStats = usageStats;
                }
            }
            return recentStats.getPackageName();
        }
        return "";
    }

    public static boolean isHomeToTopAct(Context context){
        List<String> names = getHomes(context);
        String topActPkgName = "";
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP){
            topActPkgName = getTopActPkgNameLow(context);
            return names.contains(topActPkgName);
        }else {
            topActPkgName = getTopActPkgNameHigh(context);
            return names.contains(topActPkgName);
        }
    }
}
