package com.studio.dboard.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by peak on 2017/11/9.
 */

public class DateUtils {

    public static final String FORMAT_Y_M_D = "yyyy-MM-dd HH:mm";
    public static final String FORMAT_12 = "hh:mm";
    public static final String FORMAT_24 = "HH:mm";

    public static long parseTime(String time){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        try {
            Date date = sdf.parse(time);
            return date.getTime();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static long parseTime(String format, String time){
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        try {
            Date date = sdf.parse(time);
            return date.getTime();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static String[] getValuesOfDate(){
        String[] values = new String[6];
        long time = System.currentTimeMillis();
        Date date = new Date(time);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy");
        String year = simpleDateFormat.format(date);
        values[0] = year;

        simpleDateFormat = new SimpleDateFormat("MM-dd");
        String month = simpleDateFormat.format(date);
        values[1] = month;

        simpleDateFormat = new SimpleDateFormat("EEEE");
        String week = simpleDateFormat.format(date);
        values[2] = week;

        simpleDateFormat = new SimpleDateFormat("HH");
        String hour = simpleDateFormat.format(date);
        values[3] = hour;

        simpleDateFormat = new SimpleDateFormat("mm");
        String minate = simpleDateFormat.format(date);
        values[4] = minate;

        simpleDateFormat = new SimpleDateFormat("ss");
        String second = simpleDateFormat.format(date);
        values[5] = second;

        return values;
    }

    /**
     * 小时，分钟
     * @return
     */
    public static String getTime(boolean isFormat24){
        long time = System.currentTimeMillis();
        Date date = new Date(time);
        SimpleDateFormat simpleDateFormat;
        if (isFormat24){
            simpleDateFormat = new SimpleDateFormat(FORMAT_24);
        }else {
            simpleDateFormat = new SimpleDateFormat(FORMAT_12);
        }
        String timeStr = simpleDateFormat.format(date);
        return timeStr;
    }

    public static int getHour24(){
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT+08:00"));
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        return hour;
    }

    /**
     * 获取24小时制时间（小时：分钟）
     * @return
     */
    public static String getTime24(){
        long time = System.currentTimeMillis();
        Date date = new Date(time);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(FORMAT_24);
        return simpleDateFormat.format(date);
    }

    /**
     * 获取阳历月日
     * @return
     */
    public static String getGregorianMonth(){
        long time = System.currentTimeMillis();
        Date date = new Date(time);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("M月d日");
        String month = simpleDateFormat.format(date);
        return month;
    }

    public static String getWeek(){
        long time = System.currentTimeMillis();
        Date date = new Date(time);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEEE");
        String week = simpleDateFormat.format(date);
        return week;
    }

    public static String getWeekEnglish(String week){
        switch (week){
            case "星期一":
                return "MON";
            case "星期二":
                return "TUE";
            case "星期三":
                return "WED";
            case "星期四":
                return "THU";
            case "星期五":
                return "FRI";
            case "星期六":
                return "SAT";
            case "星期日":
                return "SUN";
        }
        return "--:--";
    }

    public static String xq2zhou(String week){
        switch (week){
            case "星期一":
                return "周一";
            case "星期二":
                return "周二";
            case "星期三":
                return "周三";
            case "星期四":
                return "周四";
            case "星期五":
                return "周五";
            case "星期六":
                return "周六";
            case "星期日":
                return "周日";
        }
        return "--:--";
    }


    /**
     * GMT(格林威治标准时间)转换当前北京时间
     * 比如：1526217409 -->2018/5/13 21:16:49 与北京时间相差8个小时，调用下面的方法，是在1526217409加上8*3600秒
     * @param tzTime
     * @return
     */
    public static long stampToLocal8Long(String tzTime) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        tzTime = tzTime.replace("T", " ");
        tzTime = tzTime.replace("Z", "");
        Date date = null;
        try {
            date = df.parse(tzTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long localTime = date.getTime() + 8 * 3600 * 1000;
        return localTime;
    }

    /**
     * 获取 年月日  YYYY-MM-dd
     * @return
     */
    public static String getFormatYMD() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String date = format.format(new Date(System.currentTimeMillis()));
        return date;// 2012年10月03日 23:41:31
    }

    public static String getFormatYMDHMS() {
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date1 = format1.format(new Date(System.currentTimeMillis()));
        return date1;// 2012-10-03 23:41:31
    }
}
