package com.studio.dboard.utils;

import android.content.Context;
import android.text.TextUtils;
import com.studio.dboard.R;
import com.studio.dboard.allog.LogUtils;
import com.studio.dboard.dao.GreenDaoManager;
import com.studio.dboard.entity.Config;
import java.io.File;

/**
 * Created by mateng on 2017/5/24.
 */
public class ActivationUtils {

    private static final String tag = "ActivationUtils";

    /**
     * 检测是否激活
     * @param context
     * @param path
     * @return
     */
    public static boolean checkActive(Context context, String content, String path) {
        LogUtils.i(tag,"检查是否激活 checkActive");
        LogUtils.i(tag,"save path="+path);
        //先获取文件存储当中的激活信息
        if (checkInternalActive(context, content, path)){
            return true;
        }else if (checkFileActive(context, path)){
            return true;
        }else {
            return false;
        }
    }

    /**
     * 从文件存储拿激活数据进行检测
     * @param context
     * @param path
     * @return
     */
    private static boolean checkInternalActive(Context context, String content, String path){
        LogUtils.i(tag,"内部文件-新方式");
        try {
            LogUtils.i(tag,"content="+content);
            if (!TextUtils.isEmpty(content)) {
                String[] keys = content.split("\n");
                if (keys.length > 0) {
                    String key = "";
                    if (keys.length == 2){
                        key = keys[1];
                    }else if (keys.length == 1){
                        key = keys[0];
                    }

                    String puk = context.getString(R.string.puk);

                    boolean isActive = compare(context, puk, key);
                    if (isActive) {
                        FileUtils.writeToFile(path, content);
                        return true;
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    /**
     * 从文件存储拿激活数据进行检测(使用旧的puk)
     * @param context
     * @param path
     * @return
     */
    private static boolean checkInternalActiveOld(Context context, String content, String path){
        LogUtils.i(tag,"内部文件-旧方式");
        try {
            LogUtils.i(tag,"content="+content);

            if (!TextUtils.isEmpty(content)) {
                String[] keys = content.split("\n");
                if (keys.length > 0) {
                    String key = "";
                    String puk = "";
                    if (keys.length == 2){
                        puk = keys[0];
                        key = keys[1];
                    }else if (keys.length == 1){
                        key = keys[0];
                    }

                    boolean isActive = false;
                    if (!TextUtils.isEmpty(puk)) {
                        isActive = compare(context, puk, key);
                    }

                    if (isActive) {
                        FileUtils.writeToFile(path, content);
                        return true;
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    /**
     * 从本地文件中拿激活数据进行检测
     * @param context
     * @param path
     * @return
     */
    private static boolean checkFileActive(Context context, String path){
        LogUtils.i(tag,"外部文件-新方式");
        try {
            File file = new File(path);
            if (!file.exists()) {
                file.getParentFile().mkdirs();
                try {
                    file.createNewFile();
                    return false;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            String content = FileUtils.readFromFile(path);
            LogUtils.i(tag,"content="+content);
            if (!TextUtils.isEmpty(content)) {
                String[] keys = content.split("\n");
                if (keys.length > 0) {
                    String key = "";
                    if (keys.length == 2){//老版本
                        key = keys[1];
                    }else if (keys.length == 1){//新版本
                        key = keys[0];
                    }

                    String puk = context.getString(R.string.puk);

                    boolean isActive = compare(context, puk, key);
                    if (isActive) {
                        Config config = GreenDaoManager.getInstance(context).getSession()
                                .getConfigDao()
                                .load(0L);
                        config.setActiveKey(content);
                        GreenDaoManager.getInstance(context).getSession()
                                .getConfigDao()
                                .update(config);
                        return true;
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    /**
     * 从本地文件中拿激活数据进行检测(使用旧的puk)
     * @param context
     * @param path
     * @return
     */
    private static boolean checkFileActiveOld(Context context, String path){
        LogUtils.i(tag,"外部文件-旧方式");
        try {
            File file = new File(path);
            if (!file.exists()) {
                file.getParentFile().mkdirs();
                try {
                    file.createNewFile();
                    return false;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            String content = FileUtils.readFromFile(path);
            if (!TextUtils.isEmpty(content)) {
                String[] keys = content.split("\n");
                if (keys.length > 0) {
                    String key = "";
                    String puk = "";
                    if (keys.length == 2){//老版本
                        puk = keys[0];
                        key = keys[1];
                    }else if (keys.length == 1){//新版本
                        key = keys[0];
                    }

                    boolean isActive = false;
                    if (!TextUtils.isEmpty(puk)) {
                        isActive = compare(context, puk, key);
                    }

                    if (isActive) {
                        Config config = GreenDaoManager.getInstance(context).getSession()
                                .getConfigDao()
                                .load(0L);
                        config.setActiveKey(content);
                        GreenDaoManager.getInstance(context).getSession()
                                .getConfigDao()
                                .update(config);
                        return true;
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    /**
     * 匹配唯一标示（imei / mac / androidId）
     * @param context
     * @param puk
     * @param key
     * @return
     */
    private static boolean compare(Context context, String puk, String key){
        LogUtils.i(tag,"puk="+puk);
        LogUtils.i(tag,"key="+key);
        String sign = decrypt(puk, key);
        LogUtils.i(tag,"服务器唯一标示sign="+sign);
        boolean isActive = false;

        //先从数据库获取唯一标示进行匹配
        Config config = GreenDaoManager.getInstance(context).getSession()
                .getConfigDao()
                .load(0L);
        String internalSign = config.getSign();
        LogUtils.i(tag,"从数据库获取唯一标示sign="+internalSign);
        if (!TextUtils.isEmpty(internalSign)){
            String md5Sign = MD5Util.encrypt(internalSign);
            LogUtils.i(tag, "内部匹配="+(sign.equals(md5Sign)));
            if (sign.equals(md5Sign)){
                return true;
            }
        }


        //从外部设备参数获取唯一标示进行匹配
        if (!isActive) {
            String imei = SystemUtils.getImei(context);
            LogUtils.i(tag,"本地imei="+imei);
            if (!TextUtils.isEmpty(imei)) {
                String md5Imei = MD5Util.encrypt(imei);
                LogUtils.i(tag,"md5Imei=" + md5Imei);
                LogUtils.i(tag, "imei匹配="+(sign.equals(md5Imei)));
                if (sign.equals(md5Imei)) {
                    config.setSign(imei);
                    isActive = true;
                }
            }
        }

        if (!isActive){
            String mac = SystemUtils.getMac(context);
            LogUtils.i(tag,"本地mac="+mac);
            if (!TextUtils.isEmpty(mac)) {
                String md5Mac = MD5Util.encrypt(mac);
                LogUtils.i(tag,"md5Mac=" + md5Mac);
                LogUtils.i(tag, "mac匹配="+(sign.equals(md5Mac)));
                if (sign.equals(md5Mac)) {
                    config.setSign(mac);
                    isActive = true;
                }
            }
        }

        if (!isActive){
            String androidId = SystemUtils.getAndroidId(context);
            LogUtils.i(tag,"本地androidId="+androidId);
            if (!TextUtils.isEmpty(androidId)) {
                String md5Androidid = MD5Util.encrypt(androidId);
                LogUtils.i(tag,"md5Androidid=" + md5Androidid);
                LogUtils.i(tag, "androidId匹配="+(sign.equals(md5Androidid)));
                if (sign.equals(md5Androidid)) {
                    config.setSign(androidId);
                    isActive = true;
                }
            }
        }

        GreenDaoManager.getInstance(context).getSession()
                .getConfigDao()
                .update(config);
        return isActive;
    }

    /**
     * 是否需要重新激活
     * @return
     */
    public static String isNeedReactive(Context context, String content, String keyPath){
        if (!checkInternalActive(context, content, keyPath)
                && checkInternalActiveOld(context, content, keyPath)) {
            return content;
        }

        if (!checkFileActive(context, keyPath)
                && checkFileActiveOld(context, keyPath)){
            String contentFromFlile = FileUtils.readFromFile(keyPath);
            return contentFromFlile;
        }
        return "";
    }

    /**
     * 解密
     *
     * @param publicKey
     * @param key
     * @return
     */
    private static String decrypt(String publicKey, String key) {
        byte[] b = Coder.decryptBASE64(key);
        byte[] result = new byte[0];
        try {
            result = RSACoder.decryptByPublicKey(b, publicKey);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new String(result);
    }
}
