package com.studio.dboard.utils;

import android.view.View;
import com.studio.dboard.R;
import java.util.ArrayList;

/**
 * Created by peak on 2017/12/27.
 */

public class GpsUtils {

    private static final String tag = "GpsUtils";

    private static final int MAX_SIZE = 5;
    private static ArrayList<Double> datas = new ArrayList<>();
    private static final double[] x = new double[]{0, 1, 2, 3, 4};

    public static int getNaviIconResId(int index){
        int resId = View.NO_ID;
        switch (index){
            case 1:
                resId = R.drawable.sou01;
                break;
            case 2:
                resId = R.drawable.sou02;
                break;
            case 3:
                resId = R.drawable.sou03;
                break;
            case 4:
                resId = R.drawable.sou04;
                break;
            case 5:
                resId = R.drawable.sou05;
                break;
            case 6:
                resId = R.drawable.sou06;
                break;
            case 7:
                resId = R.drawable.sou07;
                break;
            case 8:
                resId = R.drawable.sou08;
                break;
            case 9:
                resId = R.drawable.sou09;
                break;
            case 10:
                resId = R.drawable.sou10;
                break;
            case 11:
                resId = R.drawable.sou11;
                break;
            case 12:
                resId = R.drawable.sou12;
                break;
            case 13:
                resId = R.drawable.sou13;
                break;
            case 14:
                resId = R.drawable.sou14;
                break;
            case 15:
                resId = R.drawable.sou15;
                break;
            case 16:
                resId = R.drawable.sou16;
                break;
            case 17:
                resId = R.drawable.sou17;
                break;
            case 18:
                resId = R.drawable.sou18;
                break;
            case 19:
                resId = R.drawable.sou19;
                break;
            case 20:
                resId = R.drawable.sou20;
                break;
        }
        return resId;
    }
}
