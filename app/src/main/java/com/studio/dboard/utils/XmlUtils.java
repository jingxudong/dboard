package com.studio.dboard.utils;

import android.content.Context;

import com.studio.dboard.Constant;
import com.studio.dboard.entity.TimeImage;
import com.studio.dboard.entity.Weather;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import java.io.InputStream;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

/**
 * Created by peak on 2017/10/10.
 */

public class XmlUtils {
    private static final String tag = "XmlUtils";


    public static ArrayList<Weather> parseWeathers(Context context){
        ArrayList<Weather> weathers = new ArrayList<>();
        try {
            InputStream stream = context.getAssets().open(Constant.WEATHER_ASSETS_DIR + "/weather_bg.xml");
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            // 获取XML文档结构
            Document document = builder.parse(stream);
            // 获取根节点
            Element rootElement = document.getDocumentElement();
            NodeList nodeList = rootElement.getElementsByTagName("weather");
            int length = nodeList.getLength();
            for (int i = 0; i < length; i++) {
                Weather weather = new Weather();
                Element element = (Element) nodeList.item(i);
                String idStr = element.getAttribute("id");
                int id = Integer.valueOf(idStr);
                String name = element.getAttribute("name");
                weather.setId(id);
                weather.setName(name);

                NodeList timeImageList = element.getElementsByTagName("time-image");
                ArrayList<TimeImage> timeImages = new ArrayList<>();
                for (int j = 0; j < timeImageList.getLength(); j++){
                    TimeImage timeImage = new TimeImage();
                    Element timeImageElement = (Element) timeImageList.item(j);
                    String beginStr = timeImageElement.getAttribute("begin");
                    int begin = Integer.valueOf(beginStr);
                    String endStr = timeImageElement.getAttribute("end");
                    int end = Integer.valueOf(endStr);
                    timeImage.setBegin(begin);
                    timeImage.setEnd(end);

                    ArrayList<String> images = new ArrayList<>();
                    NodeList imageList = timeImageElement.getElementsByTagName("image");
                    for (int k = 0; k < imageList.getLength(); k++){
                        String fileName = imageList.item(k).getFirstChild().getNodeValue();
                        images.add(fileName);
                    }
                    timeImage.setImages(images);
                    timeImages.add(timeImage);
                }

                weather.setTimeImages(timeImages);
                weathers.add(weather);
            }
            stream.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return weathers;
    }
}
