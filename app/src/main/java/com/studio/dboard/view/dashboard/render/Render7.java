package com.studio.dboard.view.dashboard.render;

import android.content.Context;
import android.content.res.Resources;

import com.studio.dboard.R;
import com.studio.dboard.view.dashboard.shape.common.Texture;
import com.studio.dboard.view.dashboard.shape.board7.TexturePro;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class Render7 extends BaseRender {

    private static final int START_ANGLE_LEFT = 230;
    protected static final float ANGLE_MAX = 260;
    protected static final int SPEED_MAX = 260;
    private static final int DASHBOARD_WIDTH = 514;
    private static final int OFFSET_BOARD_Y = 40;//屏幕中心向下偏移
    private static final int SHADOW_WIDTH = 570;
    private static final int SHADOW_HEIGHT = 290;
    private static final int OFFSET_SHADOW_Y = 146;//屏幕中心向下偏移

    private int dashboard_width;
    private int offset_board_y;
    private int shadowWidth;
    private int shadowHeight;
    private int offset_shadow_y;

    private Texture texBg;
    private Texture texShadow;
    private Texture texMarkBg;
    private TexturePro texMark;


    public Render7(Context mContext, Resources res) {
        super(mContext, res);
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        super.onSurfaceCreated(gl, config);
        texBg = new Texture(res, R.drawable.background_07);
        texShadow = new Texture(res, R.drawable.dashbroad_shadow);
        texMarkBg = new Texture(res, R.drawable.dashbroad_disc_bg_07);
        texMark = new TexturePro(res, R.drawable.dashbroad_disc_tick_mark_07);
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        super.onSurfaceChanged(gl, width, height);
        dashboard_width = Math.round(DASHBOARD_WIDTH * scale);
        offset_board_y = Math.round(OFFSET_BOARD_Y * scale);
        shadowWidth = Math.round(SHADOW_WIDTH * scale);
        shadowHeight = Math.round(SHADOW_HEIGHT * scale);
        offset_shadow_y = Math.round(OFFSET_SHADOW_Y * scale);

        float[] bgPosition = new float[]{
                toGLX(0),
                toGLY(0),
                toGLX(screenWidth),
                toGLY(0),
                toGLX(0),
                toGLY(screenHeight),
                toGLX(screenWidth),
                toGLY(screenHeight),
        };
        texBg.setPositions(bgPosition);

        float[] boardPosition = new float[]{
                toGLX(screenWidth / 2 - dashboard_width / 2),
                toGLY(screenHeight / 2 - dashboard_width / 2),
                toGLX(screenWidth / 2 + dashboard_width / 2),
                toGLY(screenHeight / 2 - dashboard_width / 2),
                toGLX(screenWidth / 2 - dashboard_width / 2),
                toGLY(screenHeight / 2 + dashboard_width / 2),
                toGLX(screenWidth / 2 + dashboard_width / 2),
                toGLY(screenHeight / 2 + dashboard_width / 2),
        };
        texMarkBg.setPositions(boardPosition);
        texMark.setPositions(boardPosition);

        float[] shadowPosition = new float[]{
                toGLX(screenWidth / 2 - shadowWidth / 2),
                toGLY(screenHeight / 2 - shadowHeight / 2),
                toGLX(screenWidth / 2 + shadowWidth / 2),
                toGLY(screenHeight / 2 - shadowHeight / 2),
                toGLX(screenWidth / 2 - shadowWidth / 2),
                toGLY(screenHeight / 2 + shadowHeight / 2),
                toGLX(screenWidth / 2 + shadowWidth / 2),
                toGLY(screenHeight / 2 + shadowHeight / 2),
        };
        texShadow.setPositions(shadowPosition);

        texMark.setScreenPix(screenWidth, screenHeight);
        texMark.setuCenter(new float[]{screenWidth / 2, screenHeight / 2 - offset_board_y});
        texMark.setScale(scale);
    }

    @Override
    public void onDrawFrame(GL10 gl) {
        super.onDrawFrame(gl);
        float sweepAngle = animSpeed * ANGLE_MAX / SPEED_MAX;
        float angle = (START_ANGLE_LEFT + sweepAngle) % 360;
        texMark.setAngle(angle);

        //绘制背景
        VaryTools.resetMatrix();
        texBg.draw(VaryTools.getFinalMatrix());

        //绘制阴影
        VaryTools.resetMatrix();
        float shadowTransitionY = toGLY(screenHeight / 2 + offset_shadow_y);
        VaryTools.translate(0, shadowTransitionY, 0);
        texShadow.draw(VaryTools.getFinalMatrix());

        //绘制仪表
        VaryTools.resetMatrix();
        float transitionY = toGLY(screenHeight / 2 + offset_board_y);
        VaryTools.translate(0, transitionY, 0);
        texMarkBg.draw(VaryTools.getFinalMatrix());
        texMark.draw(VaryTools.getFinalMatrix());
    }

    @Override
    public void notifyDestroy() {

    }
}
