package com.studio.dboard.view.dashboard.render;

import android.content.Context;
import android.content.res.Resources;

import com.studio.dboard.dao.GreenDaoManager;
import com.studio.dboard.entity.Board;
import com.studio.dboard.entity.Config;

/**
 * Created by peak on 2018/1/31.
 */

public class RenderFactory {

    public static BaseRender build(Context context, Resources res){
        Config config = GreenDaoManager.getInstance(context)
                .getSession()
                .getConfigDao()
                .load(0L);
        int boardId = config.getBoardId();
        BaseRender instance = null;
        switch (boardId){
            case Board.ID_BOARD_1:
                instance = new Render1(context, res);
                break;
            case Board.ID_BOARD_2:
                instance = new Render2(context, res);
                break;
            case Board.ID_BOARD_3:
                instance = new Render3(context, res);
                break;
            case Board.ID_BOARD_4:
                instance = new Render4(context, res);
                break;
            case Board.ID_BOARD_5:
                instance = new Render5(context, res);
                break;
            case Board.ID_BOARD_6:
                instance = new Render6(context, res);
                break;
            case Board.ID_BOARD_7:
                instance = new Render7(context, res);
                break;
            case Board.ID_BOARD_8:
                instance = new Render8(context, res);
                break;
            case Board.ID_BOARD_9:
                instance = new Render9(context, res);
                break;
            case Board.ID_BOARD_10:
                instance = new Render10(context, res);
                break;
            default:
                instance = new Render1(context, res);
        }
        return instance;
    }

    public static BaseRender buildPre(Context context, Resources res){
        Config config = GreenDaoManager.getInstance(context)
                .getSession()
                .getConfigDao()
                .load(0L);
        int boardId = config.getPreviewBoardId();
        BaseRender instance = null;
        switch (boardId){
            case Board.ID_BOARD_1:
                instance = new Render1(context, res);
                break;
            case Board.ID_BOARD_2:
                instance = new Render2(context, res);
                break;
            case Board.ID_BOARD_3:
                instance = new Render3(context, res);
                break;
            case Board.ID_BOARD_4:
                instance = new Render4(context, res);
                break;
            case Board.ID_BOARD_5:
                instance = new Render5(context, res);
                break;
            case Board.ID_BOARD_6:
                instance = new Render6(context, res);
                break;
            case Board.ID_BOARD_7:
                instance = new Render7(context, res);
                break;
            case Board.ID_BOARD_8:
                instance = new Render8(context, res);
                break;
            case Board.ID_BOARD_9:
                instance = new Render9(context, res);
                break;
            case Board.ID_BOARD_10:
                instance = new Render10(context, res);
                break;
            default:
                instance = new Render1(context, res);
        }
        return instance;
    }
}
