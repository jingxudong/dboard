package com.studio.dboard.view.dashboard;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.os.Build;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.View;

import com.studio.dboard.allog.LogUtils;
import com.studio.dboard.dao.GreenDaoManager;
import com.studio.dboard.entity.Config;
import com.studio.dboard.view.dashboard.render.BaseRender;
import com.studio.dboard.view.dashboard.render.RenderFactory;

/**
 * Created by peak on 2018/2/1.
 */

public abstract class BaseBoardView extends GLSurfaceView {

    private static final String tag = "BaseDashboardView";

    protected BaseRender render;

    public BaseBoardView(Context context) {
        super(context);
        init();
    }

    public BaseBoardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public void init(){
        setEGLContextClientVersion(2);
        setRender();
        setRenderMode();
    }

    abstract void setRender();

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();

    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        render.notifyDestroy();
    }

    /**
     * 设置渲染模式
     */
    protected abstract void setRenderMode();

    public void initProgress(float progress){
        render.initProgress(progress);

    }

    public void setTime(int hour, int minate, int second){
        render.notifyTimeChanged(hour, minate, second);

    }

    public void setSpeed(float speed){
        render.notifySpeedChanged(speed);

    }

    public void setWeather(String weatherState){
        render.notifyWeather(weatherState);

    }
}
