package com.studio.dboard.view.dashboard.render;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.studio.dboard.R;
import com.studio.dboard.dao.GreenDaoManager;
import com.studio.dboard.entity.Config;
import com.studio.dboard.view.dashboard.shape.common.Texture;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class Render10 extends BaseRender {


    private Texture texBg;

    private Config appConfig;


    public Render10(Context mContext, Resources res) {
        super(mContext, res);
        appConfig = GreenDaoManager.getInstance(mContext).getSession().getConfigDao().load(0L);
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        super.onSurfaceCreated(gl, config);
        Bitmap bg = BitmapFactory.decodeFile(appConfig.getScreenSaveImgPath());
        if (bg != null){
            texBg = new Texture(res, bg);
        }else {
            texBg = new Texture(res, R.drawable.pic_default_10);
        }
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        super.onSurfaceChanged(gl, width, height);

        float[] bgPosition = new float[]{
                toGLX(0),
                toGLY(0),
                toGLX(screenWidth),
                toGLY(0),
                toGLX(0),
                toGLY(screenHeight),
                toGLX(screenWidth),
                toGLY(screenHeight),
        };
        texBg.setPositions(bgPosition);
    }

    @Override
    public void onDrawFrame(GL10 gl) {
        super.onDrawFrame(gl);

        VaryTools.resetMatrix();
        texBg.draw(VaryTools.getFinalMatrix());
    }

    @Override
    public void notifyDestroy() {

    }
}
