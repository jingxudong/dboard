package com.studio.dboard.view.dashboard.shape.board4;

import android.content.res.Resources;
import android.opengl.GLES20;
import com.studio.dboard.view.dashboard.shape.BaseShape;
import com.studio.dboard.view.dashboard.shape.ShaderUtils;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.ArrayList;

import javax.microedition.khronos.opengles.GL10;

/**
 * Created by peak on 2018/2/5.
 * 虚线绘制
 */

public class DotLine extends BaseShape {

    private static final String tag = "DotLine";

    private int mProgram;

    //顶点维数
    private static final int COORDS_PER_VERTEX = 3;
    /**
     * 颜色维数
     */
    private static final int COLOR_PER_VERTEX = 4;

    private float vertexs[];
    private FloatBuffer vertexBuffer;
    private ArrayList< Float > positions = new ArrayList<>();
    private FloatBuffer colorBuffer;

    //设置颜色，依次为红绿蓝和透明通道
    private float colors[];

    private float dotLength;
    private float dotSpace;
    private float lineWidth;

    private float[] startPosition;
    private float[] endPosition;

    private int mPositionHandle;
    private int mColorHandle;
    private int mMVPMatrixHandle;

    public DotLine(Resources res) {
        super(res);
    }

    @Override
    protected void initProgram(){
        mProgram = ShaderUtils.createProgram(res,
                "shader/canvas/canvasvert.glsl",
                "shader/canvas/canvasfrag.glsl");

        // get handle to shape's transformation matrix
        mMVPMatrixHandle = GLES20.glGetUniformLocation(mProgram, "uMatrix");
        // get handle to vertex shader's vPosition member
        mPositionHandle = GLES20.glGetAttribLocation(mProgram, "aPosition");
        // get handle to shape's vColor member
        mColorHandle = GLES20.glGetAttribLocation(mProgram, "aColor");
    }

    public void setParameter(float[] startPosition, float[] endPosition, float dotLength, float dotSpace, float lineWidth){
        this.startPosition = startPosition;
        this.endPosition = endPosition;
        this.dotLength = dotLength;
        this.dotSpace = dotSpace;
        this.lineWidth = lineWidth;

        createPosition();
        createColors();
    }

    private void createPosition(){
        int count = (int)(Math.hypot(endPosition[0] - startPosition[0], endPosition[1] - startPosition[1])
                / (dotLength + dotSpace));
        float remain = (float) (Math.hypot(endPosition[0] - startPosition[0], endPosition[1] - startPosition[1])
                        % (dotLength + dotSpace));


        //x1 != x2 有斜率
        if (startPosition[0] != endPosition[0]) {
            float sin = (float) (Math.abs(endPosition[0] - startPosition[0]) / Math.hypot(endPosition[0] - startPosition[0], endPosition[1] - startPosition[1]));

            float k = (endPosition[1] - startPosition[1]) / (endPosition[0] - startPosition[0]);
            float c = (endPosition[0] * startPosition[1] - startPosition[0] * endPosition[1]) / (endPosition[0] - startPosition[0]);
            float flag = (endPosition[0] - startPosition[0]) / Math.abs(endPosition[0] - startPosition[0]);

            float everageX1 = (dotLength + dotSpace) * sin * flag;
            float everageX2 = dotLength * sin * flag;


            for (int i = 0; i < count; i++) {
                float x = startPosition[0] + everageX1 * i;
                positions.add(x);
                positions.add(k * x + c);
                positions.add(0.0f);

                x = startPosition[0] + everageX1 * i + everageX2;
                positions.add(x);
                positions.add(k * x + c);
                positions.add(0.0f);
            }

            if (remain != 0) {
                float x = startPosition[0] + everageX1 * count;
                positions.add(x);
                positions.add(k * x + c);
                positions.add(0.0f);

                if (remain >= dotLength) {
                    x = startPosition[0] + everageX1 * count + everageX2;
                    positions.add(x);
                    positions.add(k * x + c);
                    positions.add(0.0f);
                }else {
                    positions.add(endPosition[0]);
                    positions.add(endPosition[1]);
                    positions.add(0.0f);
                }
            }
        }else {
            float flag = (endPosition[1] - startPosition[1]) / Math.abs(endPosition[1] - startPosition[1]);
            for (int i = 0; i < count; i++){
                positions.add(startPosition[0]);
                positions.add(startPosition[1] + (dotLength + dotSpace) * i * flag);
                positions.add(0.0f);

                positions.add(startPosition[0]);
                positions.add(startPosition[1] + ((dotLength + dotSpace) * i + dotLength) * flag);
                positions.add(0.0f);
            }

            if (remain != 0) {

                positions.add(startPosition[0]);
                positions.add(startPosition[1] + (dotLength + dotSpace) * count * flag);
                positions.add(0.0f);

                if (remain >= dotLength) {
                    positions.add(startPosition[0]);
                    positions.add(startPosition[1] + ((dotLength + dotSpace) * count + dotLength) * flag);
                    positions.add(0.0f);
                }else {
                    positions.add(endPosition[0]);
                    positions.add(endPosition[1]);
                    positions.add(0.0f);
                }
            }
        }

        vertexs = new float[positions.size()];
        for (int i = 0; i < positions.size(); i++){
            vertexs[i] = positions.get(i);
        }

        ByteBuffer bb = ByteBuffer.allocateDirect(
                vertexs.length * 4);
        bb.order(ByteOrder.nativeOrder());
        vertexBuffer = bb.asFloatBuffer();
        vertexBuffer.put(vertexs);
        vertexBuffer.position(0);

        positions.clear();
    }

    private void createColors(){
        int count = vertexs.length / COORDS_PER_VERTEX;
        colors = new float[count * COLOR_PER_VERTEX];
        for (int i = 0; i < count; i++){
            colors[i * COLOR_PER_VERTEX] = 1.0f;
            colors[i * COLOR_PER_VERTEX + 1] = 1.0f;
            colors[i * COLOR_PER_VERTEX + 2] = 1.0f;
            colors[i * COLOR_PER_VERTEX + 3] = 0.4f - i * 0.4f / (count - 1);
        }

        ByteBuffer bb = ByteBuffer.allocateDirect(
                colors.length * 4);
        bb.order(ByteOrder.nativeOrder());
        colorBuffer = bb.asFloatBuffer();
        colorBuffer.put(colors);
        colorBuffer.position(0);
    }

    @Override
    public void draw(float[] mMVPMatrix) {
        // Add program to OpenGL ES environment
        GLES20.glUseProgram(mProgram);

        // Pass the projection and view transformation to the shader
        GLES20.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, mMVPMatrix, 0);

        // Enable a handle to the triangle vertices
        GLES20.glEnableVertexAttribArray(mPositionHandle);
        // Prepare the arc coordinate data
        GLES20.glVertexAttribPointer(mPositionHandle, COORDS_PER_VERTEX, GLES20.GL_FLOAT, false,
                0, vertexBuffer);

        // Set colors for drawing the arc
        GLES20.glEnableVertexAttribArray(mColorHandle);
        GLES20.glVertexAttribPointer(mColorHandle, COLOR_PER_VERTEX, GLES20.GL_FLOAT,false,
                0, colorBuffer);

        GLES20.glLineWidth(lineWidth);
        // Draw the arc
        GLES20.glDrawArrays(GLES20.GL_LINES, 0, vertexs.length / COORDS_PER_VERTEX);
        //索引法绘制
//        GLES20.glDrawElements(GLES20.GL_TRIANGLES, indexs.length, GLES20.GL_UNSIGNED_SHORT, indexBuffer);

        // Disable vertex array
        GLES20.glDisableVertexAttribArray(mPositionHandle);
        // Disable colors array
        GLES20.glDisableVertexAttribArray(mColorHandle);
    }

    @Override
    public void notifyDestroy() {

    }
}
