package com.studio.dboard.view.dashboard.shape.common;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.opengl.GLES20;
import android.opengl.GLUtils;

import com.studio.dboard.view.dashboard.shape.BaseShape;
import com.studio.dboard.view.dashboard.shape.ShaderUtils;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;

/**
 * Created by peak on 2018/1/13.
 * 仪表盘7辐射背景
 */

public class TextureChangeColor extends BaseShape {

    protected int mProgram;
    protected int mTexSamplerHandle;
    protected int mTexCoordHandle;
    protected int mPosCoordHandle;
    protected int mMVPMatrixHandle;
    private int mColorHandle;


    protected FloatBuffer mTexVertices;
    protected FloatBuffer mPosVertices;


    private float[] colors = new float[]{
            0.0f, 0.0f, 1.0f, 1.0f //rgba
    };


    public TextureChangeColor(Resources res, int... resIds) {
        super(res, resIds);
    }

    public void setColor(float[] colors){
        this.colors = colors;
    }

    @Override
    protected void initProgram(){
        mProgram = ShaderUtils.createProgram(res,
                "shader/texturechangecolor/TextureChangeColorVert.glsl",
                "shader/texturechangecolor/TextureChangeColorFrag.glsl");

        mMVPMatrixHandle = GLES20.glGetUniformLocation(mProgram,"uMatrix");
        mTexSamplerHandle = GLES20.glGetUniformLocation(mProgram,"uTexture");
        mTexCoordHandle = GLES20.glGetAttribLocation(mProgram, "aCoordinate");
        mPosCoordHandle = GLES20.glGetAttribLocation(mProgram, "aPosition");
        mColorHandle = GLES20.glGetUniformLocation(mProgram,"uColor");
    }

    public void initPositions(float[] positions) {
        //纹理坐标屏幕左上角为原点(左下，右下，左上，右上)
        float[] TEX_VERTICES = { 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f };
        mTexVertices = ByteBuffer.allocateDirect(TEX_VERTICES.length * 4)
                .order(ByteOrder.nativeOrder()).asFloatBuffer();
        mTexVertices.put(TEX_VERTICES).position(0);
        //顶点坐标(左下，右下，左上，右上)
        mPosVertices = ByteBuffer.allocateDirect(positions.length * 4)
                .order(ByteOrder.nativeOrder()).asFloatBuffer();
        mPosVertices.put(positions).position(0);
    }

    @Override
    public void draw(float[] mMVPMatrix) {
        GLES20.glUseProgram(mProgram);

        GLES20.glUniformMatrix4fv(mMVPMatrixHandle,1,false, mMVPMatrix,0);

        //激活绑定纹理单元
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureIds[0]);
        GLES20.glUniform1i(mTexSamplerHandle, 0);

        GLES20.glEnableVertexAttribArray(mPosCoordHandle);
        //传入顶点坐标
        GLES20.glVertexAttribPointer(mPosCoordHandle,2, GLES20.GL_FLOAT,false,0, mPosVertices);

        GLES20.glEnableVertexAttribArray(mTexCoordHandle);
        //传入纹理坐标
        GLES20.glVertexAttribPointer(mTexCoordHandle,2, GLES20.GL_FLOAT,false,0, mTexVertices);

        GLES20.glUniform4fv(mColorHandle, 1, colors, 0);

        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP,0,4);

        // Disable vertex array
        GLES20.glDisableVertexAttribArray(mPosCoordHandle);
        GLES20.glDisableVertexAttribArray(mTexCoordHandle);
    }

    @Override
    public void notifyDestroy() {

    }
}
