package com.studio.dboard.view.dashboard.render;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;

import com.studio.dboard.R;
import com.studio.dboard.view.dashboard.shape.common.Texture;
import com.studio.dboard.view.dashboard.shape.common.TextureChangeColor;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class Render6 extends BaseRender {


    protected static final float ANGLE_MAX = 260;
    protected static final int SPEED_MAX = 260;
    private static final int THRESHOLD = 120;
    private static final int DASHBOARD_WIDTH = 446;
    private static final int OFFSET_BOARD_Y = 40;//屏幕中心向下偏移
    private static final int SHADOW_WIDTH = 570;
    private static final int SHADOW_HEIGHT = 290;
    private static final int OFFSET_SHADOW_Y = 146;//屏幕中心向下偏移

    private int dashboard_width;
    private int offset_board_y;
    private int shadowWidth;
    private int shadowHeight;
    private int offset_shadow_y;

    private Texture texBg;
    private Texture texShadow;
    private Texture texMarkBg;
    private Texture texLight;
    private TextureChangeColor texGlow;
    private Texture texMark;
    private Texture texPointer;


    public Render6(Context mContext, Resources res) {
        super(mContext, res);
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        super.onSurfaceCreated(gl, config);
        texBg = new Texture(res, R.drawable.background_06);
        texShadow = new Texture(res, R.drawable.dashbroad_shadow);
        texMarkBg = new Texture(res, R.drawable.dashbroad_disc_bg_06);
        texLight = new Texture(res, R.drawable.dashbroad_disc_high_light_06);
        texGlow = new TextureChangeColor(res, R.drawable.dashbroad_disc_speed_glow_06);
        texMark = new Texture(res, R.drawable.dashbroad_disc_tick_mark_06);
        texPointer = new Texture(res, R.drawable.dashbroad_disc_pointer_06);
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        super.onSurfaceChanged(gl, width, height);
        dashboard_width = Math.round(DASHBOARD_WIDTH * scale);
        offset_board_y = Math.round(OFFSET_BOARD_Y * scale);
        shadowWidth = Math.round(SHADOW_WIDTH * scale);
        shadowHeight = Math.round(SHADOW_HEIGHT * scale);
        offset_shadow_y = Math.round(OFFSET_SHADOW_Y * scale);

        float[] bgPosition = new float[]{
                toGLX(0),
                toGLY(0),
                toGLX(screenWidth),
                toGLY(0),
                toGLX(0),
                toGLY(screenHeight),
                toGLX(screenWidth),
                toGLY(screenHeight),
        };
        texBg.setPositions(bgPosition);

        float[] boardPosition = new float[]{
                toGLX(screenWidth / 2 - dashboard_width / 2),
                toGLY(screenHeight / 2 - dashboard_width / 2),
                toGLX(screenWidth / 2 + dashboard_width / 2),
                toGLY(screenHeight / 2 - dashboard_width / 2),
                toGLX(screenWidth / 2 - dashboard_width / 2),
                toGLY(screenHeight / 2 + dashboard_width / 2),
                toGLX(screenWidth / 2 + dashboard_width / 2),
                toGLY(screenHeight / 2 + dashboard_width / 2),
        };
        texMarkBg.setPositions(boardPosition);
        texLight.setPositions(boardPosition);
        texGlow.initPositions(boardPosition);
        texMark.setPositions(boardPosition);
        texPointer.setPositions(boardPosition);

        float[] shadowPosition = new float[]{
                toGLX(screenWidth / 2 - shadowWidth / 2),
                toGLY(screenHeight / 2 - shadowHeight / 2),
                toGLX(screenWidth / 2 + shadowWidth / 2),
                toGLY(screenHeight / 2 - shadowHeight / 2),
                toGLX(screenWidth / 2 - shadowWidth / 2),
                toGLY(screenHeight / 2 + shadowHeight / 2),
                toGLX(screenWidth / 2 + shadowWidth / 2),
                toGLY(screenHeight / 2 + shadowHeight / 2),
        };
        texShadow.setPositions(shadowPosition);
    }

    @Override
    public void onDrawFrame(GL10 gl) {
        super.onDrawFrame(gl);
        float[] colors = calculateColor(animSpeed);
        texGlow.setColor(colors);

        //绘制背景
        VaryTools.resetMatrix();
        texBg.draw(VaryTools.getFinalMatrix());

        //绘制阴影
        VaryTools.resetMatrix();
        float shadowTransitionY = toGLY(screenHeight / 2 + offset_shadow_y);
        VaryTools.translate(0, shadowTransitionY, 0);
        texShadow.draw(VaryTools.getFinalMatrix());

        //绘制仪表
        VaryTools.resetMatrix();
        float transitionY = toGLY(screenHeight / 2 + offset_board_y);
        VaryTools.translate(0, transitionY, 0);
        texMarkBg.draw(VaryTools.getFinalMatrix());
        texLight.draw(VaryTools.getFinalMatrix());
        texGlow.draw(VaryTools.getFinalMatrix());
        texMark.draw(VaryTools.getFinalMatrix());
        float sweepAngle = animSpeed * ANGLE_MAX / SPEED_MAX;
        float rotateAngle = -50 - sweepAngle;
        VaryTools.rotate(rotateAngle, 0,0,1);
        texPointer.draw(VaryTools.getFinalMatrix());

    }

    @Override
    public void notifyDestroy() {

    }

    private float[] calculateColor(float speed){
        int startColor = Color.parseColor("#224c97");
        int endColor = Color.parseColor("#c50000");

        int startColorRed = Color.red(startColor);
        int startColorGreen = Color.green(startColor);
        int startColorBlue = Color.blue(startColor);
        int endColorRed = Color.red(endColor);
        int endColorGreen = Color.green(endColor);
        int endColorBlue = Color.blue(endColor);

        float progress = speed / THRESHOLD < 1.0 ? speed / THRESHOLD : 1.0f;

        int colorRed = (int) (startColorRed + (endColorRed - startColorRed) * progress);
        int colorGreen = (int) (startColorGreen + (endColorGreen - startColorGreen) * progress);
        int colorBlue = (int) (startColorBlue + (endColorBlue - startColorBlue) * progress);

        float[] colors = new float[4];
        colors[0] = colorRed * 1.0f / 255;
        colors[1] = colorGreen * 1.0f / 255;
        colors[2] = colorBlue * 1.0f / 255;
        colors[3] = 1.0f;
        return colors;
    }
}
