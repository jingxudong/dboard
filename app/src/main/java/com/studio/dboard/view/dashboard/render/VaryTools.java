package com.studio.dboard.view.dashboard.render;

import android.opengl.Matrix;

/**
 * Created by wuwang on 2016/10/30
 */

public class VaryTools {

    private static float[] mProjMatrix = new float[16];//投影矩阵
    private static float[] mVMatrix = new float[16];//摄像机位置朝向矩阵
    private static float[] mModuleMatrix = new float[16];//模型变换矩阵（位移、缩放、旋转）
    private static float[] mMVPMatrix = new float[16];//最后起作用的总变换矩阵
    private static float[] tempMatrix = new float[16];   //临时变换矩阵
    static float[][] mStack = new float[10][16];   //用于保存变换矩阵的类
    static int stackTop = -1; //标识栈顶的索引


    /**
     * 初始化变换矩阵
     */
    public static void resetMatrix() {
        Matrix.setIdentityM(mModuleMatrix, 0);
    }

    /**
     * 把变换矩阵保存到栈中
     */
    public static void pushMatrix() {
        stackTop++;
        for (int i = 0; i < 16; i++) {
            mStack[stackTop][i] = mModuleMatrix[i];
        }
    }

    /**
     * 从栈中读取变换矩阵
     */
    public static void popMatrix() {
        for (int i = 0; i < 16; i++) {
            mModuleMatrix[i] = mStack[stackTop][i];
        }
        stackTop--;
    }

    /**
     * 平移变换
     */
    public static void translate(float x, float y, float z) {
        Matrix.translateM(mModuleMatrix, 0, x, y, z);
    }

    /**
     * 旋转变换
     *
     * @param angle
     * @param x
     * @param y
     */
    public static void rotate(float angle, float x, float y, float z) {
        Matrix.rotateM(mModuleMatrix, 0, angle, x, y, z);
    }

    /**
     * 缩放变换
     */
    public static void scale(float x, float y, float z) {
        Matrix.scaleM(mModuleMatrix, 0, x, y, z);
    }


    /**
     * 设置摄像机
     *
     * @param cx  摄像机位置x
     * @param cy  摄像机位置y
     * @param cz  摄像机位置z
     * @param tx  摄像机目标点x
     * @param ty  摄像机目标点y
     * @param tz  摄像机目标点z
     * @param upx 摄像机UP向量X分量
     * @param upy 摄像机UP向量Y分量
     * @param upz 摄像机UP向量Z分量
     */
    public static void setCameraLookAtM(float cx, float cy, float cz, float tx, float ty, float tz, float upx, float upy, float upz) {
        Matrix.setLookAtM(mVMatrix, 0, cx, cy, cz, tx, ty, tz, upx, upy, upz);
    }

    /**
     * 设置透视投影
     * @param fovy
     * @param ratio
     * @param zNear
     * @param zFar
     */
    public static void setPerspectiveM(int fovy, float ratio, float zNear, float zFar){
        Matrix.perspectiveM(mProjMatrix, 0, fovy, ratio, zNear, zFar);
    }

    /**
     * 设置透视投影
     *
     * @param left   near面的left
     * @param right  near面的right
     * @param bottom near面的bottom
     * @param top    near面的top
     * @param near   near面距离
     * @param far    far面距离
     */
    public static void setFrustum(float left, float right, float bottom, float top, float near, float far) {
        Matrix.frustumM(mProjMatrix, 0, left, right, bottom, top, near, far);
    }

    /**
     * 设置正交投影参数
     *
     * @param left   near面的left
     * @param right  near面的right
     * @param bottom near面的bottom
     * @param top    near面的top
     * @param near   near面距离
     * @param far    far面距离
     */
    public static void setProjectOrtho(float left, float right, float bottom, float top, float near, float far) {
        Matrix.orthoM(mProjMatrix, 0, left, right, bottom, top, near, far);
    }

    /**
     * 获取具体物体的总变换矩阵
     *
     * @param spec 变换矩阵
     * @return
     */
    public static float[] getFinalMatrix(float[] spec) {
        Matrix.multiplyMM(tempMatrix, 0, mVMatrix, 0, spec, 0);
        Matrix.multiplyMM(mMVPMatrix, 0, mProjMatrix, 0, tempMatrix, 0);
        return mMVPMatrix;
    }

    /**
     * 获取具体物体的变换之后的矩阵
     *
     * @return
     */
    public static float[] getFinalMatrix() {
        Matrix.multiplyMM(tempMatrix, 0, mProjMatrix, 0, mVMatrix, 0);
        Matrix.multiplyMM(mMVPMatrix, 0, tempMatrix, 0, mModuleMatrix, 0);
        return mMVPMatrix;
    }

    /**
     * 获取具体物体的变换之后的逆转置矩阵
     * @return
     */
    public static float[] getReverseMatrix(){
        return mModuleMatrix;
    }

}
