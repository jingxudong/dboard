package com.studio.dboard.view.dashboard.shape.board4;

import android.content.res.Resources;
import android.opengl.GLES20;

import com.studio.dboard.view.dashboard.shape.BaseShape;
import com.studio.dboard.view.dashboard.shape.ShaderUtils;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.ArrayList;

import javax.microedition.khronos.opengles.GL10;

/**
 * 空心圆
 * Created by peak on 2018/1/31.
 */

public class Circle extends BaseShape {


    private static final int NUM = 360;//份数
    //顶点维数
    private static final int COORDS_PER_VERTEX = 3;

    private int mProgram;

    private int mPositionHandle;
    private int mColorHandle;
    private int mMVPMatrixHandle;

    //设置颜色，依次为红绿蓝和透明通道
    private float[] color = new float[]{ 1.0f, 0.0f, 0.0f, 1.0f };
    //顶点数组
    private float[] vertexs;

    private float centerX;
    private float centerY;
    private float radiu;
    private float z;

    private FloatBuffer vertexBuffer;


    public Circle(Resources res) {
        super(res);
    }

    public void setParameter(float centerX, float centerY, float radiu, float z){
        this.centerX = centerX;
        this.centerY = centerY;
        this.radiu = radiu;
        this.z = z;

        createVertex();
    }

    public void setColor(float[] color){
        this.color = color;
    }

    @Override
    protected void initProgram(){
        mProgram = ShaderUtils.createProgram(res,
                "shader/canvas/canvas_uniform_vert.glsl",
                "shader/canvas/canvas_uniform_frag.glsl");

        // get handle to shape's transformation matrix
        mMVPMatrixHandle = GLES20.glGetUniformLocation(mProgram, "uMatrix");
        // get handle to vertex shader's vPosition member
        mPositionHandle = GLES20.glGetAttribLocation(mProgram, "aPosition");
        // get handle to shape's vColor member
        mColorHandle = GLES20.glGetUniformLocation(mProgram, "uColor");
    }

    private void createVertex(){
        ArrayList<Float> positions = new ArrayList<>();
        float everageAngel = 360 / NUM;//每一份的角度
        for (int i = 0; i < 360; i += everageAngel){
            positions.add(centerX + (float) (radiu * Math.cos(i * Math.PI / 180f)));
            positions.add(centerY + (float) (radiu * Math.sin(i * Math.PI / 180f)));
            positions.add(z);
        }

        vertexs = new float[positions.size()];
        for (int i = 0; i < positions.size(); i++){
            vertexs[i] = positions.get(i);
        }

        ByteBuffer bb = ByteBuffer.allocateDirect(
                vertexs.length * 4);
        bb.order(ByteOrder.nativeOrder());
        vertexBuffer = bb.asFloatBuffer();
        vertexBuffer.put(vertexs);
        vertexBuffer.position(0);
    }

    @Override
    public void draw(float[] mMVPMatrix) {
        // Add program to OpenGL ES environment
        GLES20.glUseProgram(mProgram);

        // Pass the projection and view transformation to the shader
        GLES20.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, mMVPMatrix, 0);

        // Enable a handle to the triangle vertices
        GLES20.glEnableVertexAttribArray(mPositionHandle);
        // Prepare the arc coordinate data
        GLES20.glVertexAttribPointer(mPositionHandle, COORDS_PER_VERTEX, GLES20.GL_FLOAT, false,
                0, vertexBuffer);

        // Set color for drawing the triangle
        GLES20.glUniform4fv(mColorHandle, 1, color, 0);

        GLES20.glLineWidth(1);
        // Draw the arc
        GLES20.glDrawArrays(GLES20.GL_LINE_LOOP, 0, vertexs.length / COORDS_PER_VERTEX);
        //索引法绘制
//        GLES20.glDrawElements(GLES20.GL_TRIANGLES, indexs.length, GLES20.GL_UNSIGNED_SHORT, indexBuffer);

        // Disable vertex array
        GLES20.glDisableVertexAttribArray(mPositionHandle);
    }

    @Override
    public void notifyDestroy() {

    }
}
