package com.studio.dboard.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import com.studio.dboard.R;

public class ArcProgressView extends View {

    private int maxValue;
    private int progress = 0;

    private int progressColor;//进度条圆弧颜色
    private int bgColor;//背景圆弧颜色
    private float startAngle;//背景圆弧的起始角度
    private float sweepAngle;//背景圆弧扫过的角度
    private float barWidth;//圆弧进度条宽度
    private float radiu;//圆弧半径
    private float progressAngle = 0;

    private Paint bgPaint;
    private Paint progressPaint;

    private RectF mRectF;

    public float getBarWidth() {
        return barWidth;
    }

    public void setBarWidth(float barWidth) {
        this.barWidth = barWidth;
        invalidate();
    }

    public float getRadiu() {
        return radiu;
    }

    public void setRadiu(float radiu) {
        this.radiu = radiu;
        invalidate();
    }

    public int getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(int maxValue) {
        this.maxValue = maxValue;
        invalidate();
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
        progressAngle = progress * sweepAngle / maxValue;
        invalidate();
    }

    public float getProgressAngle() {
        return progressAngle;
    }

    public void setProgressAngle(float progressAngle) {
        this.progressAngle = progressAngle;
    }

    public ArcProgressView(Context context) {
        super(context);
    }

    public ArcProgressView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public ArcProgressView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context,AttributeSet attrs){
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ArcProgressView);

        progressColor = typedArray.getColor(R.styleable.ArcProgressView_progress_color, Color.GREEN);//默认为绿色
        bgColor = typedArray.getColor(R.styleable.ArcProgressView_bg_color,Color.GRAY);//默认为灰色
        startAngle = typedArray.getFloat(R.styleable.ArcProgressView_startAngle,0);//默认为0
        sweepAngle = typedArray.getFloat(R.styleable.ArcProgressView_sweepAngle,360);//默认为360
        barWidth = typedArray.getDimension(R.styleable.ArcProgressView_barWidth,10);//默认为10px
        radiu = typedArray.getDimension(R.styleable.ArcProgressView_arc_r,50);//默认为50px
        maxValue = typedArray.getInt(R.styleable.ArcProgressView_max_value,260);//默认为260
        progress = typedArray.getInt(R.styleable.ArcProgressView_progress,0);//默认为0
        typedArray.recycle();//typedArray用完之后需要回收，防止内存泄漏

        progressPaint = new Paint();
        bgPaint = new Paint();

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        progressPaint.setAntiAlias(true);
        progressPaint.setStyle(Paint.Style.STROKE);
        progressPaint.setStrokeCap(Paint.Cap.ROUND);
        progressPaint.setColor(progressColor);
        progressPaint.setStrokeWidth(barWidth);

        bgPaint.setAntiAlias(true);
        bgPaint.setStyle(Paint.Style.STROKE);
        bgPaint.setStrokeCap(Paint.Cap.ROUND);
        bgPaint.setColor(bgColor);
        bgPaint.setStrokeWidth(barWidth);

        mRectF = new RectF(
                barWidth / 2 + getPaddingLeft() + 1,
                barWidth / 2 + getPaddingTop() + 1,
                (radiu + barWidth) * 2 + getPaddingLeft() - barWidth / 2 - 1,
                (radiu + barWidth) * 2 + getPaddingTop() - barWidth / 2 - 1
        );
        canvas.drawArc(mRectF,startAngle,sweepAngle,false,bgPaint);
        canvas.drawArc(mRectF,startAngle,progressAngle,false, progressPaint);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        widthMeasureSpec = (int) ((radiu + barWidth) * 2) + getPaddingLeft() + getPaddingRight();
        heightMeasureSpec = (int) ((radiu + barWidth) * 2) + getPaddingTop() + getPaddingBottom();
        setMeasuredDimension(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);

    }
}
