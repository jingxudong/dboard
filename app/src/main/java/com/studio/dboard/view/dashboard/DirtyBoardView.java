package com.studio.dboard.view.dashboard;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;

import com.studio.dboard.view.dashboard.render.RenderFactory;

/**
 * Created by peak on 2018/1/10.
 */

public class DirtyBoardView extends BaseBoardView {

    private static final String tag = "DirtyDashboardView";

    public DirtyBoardView(Context context) {
        super(context);
    }

    public DirtyBoardView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    @Override
    void setRender() {
        render = RenderFactory.build(getContext(), getResources());
        setRenderer(render);
    }

    @Override
    protected void setRenderMode() {
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
    }

    @Override
    public void initProgress(float progress){
        super.initProgress(progress);
        requestRender();
    }

    @Override
    public void setTime(int hour, int minate, int second){
        super.setTime(hour, minate, second);
        requestRender();
    }

    @Override
    public void setSpeed(float speed){
        super.setSpeed(speed);
        requestRender();
    }

    @Override
    public void setWeather(String weatherState) {
        super.setWeather(weatherState);
        requestRender();
    }
}
