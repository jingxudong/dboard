package com.studio.dboard.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.studio.dboard.R;


/**
 * Created by peak on 2017/9/7.
 */

public class IndicatorView extends View {

    private static final String tag = "IndicatorView";

    private static final int TYPE_LINE = 0;
    private static final int TYPE_CYCLE = 1;
    private int pages = 1;
    private float position = 0f;
    private int bgColor;
    private int selectColor;
    private int strokeColor;
    private int type = TYPE_LINE;
    private float space;
    private float cellWidth;
    private float cellHeight;
    private Paint paint;

    public IndicatorView(Context context) {
        super(context);
    }

    public IndicatorView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.IndicatorView);
        bgColor = a.getColor(R.styleable.IndicatorView_circle_bg_color, getResources()
                .getColor(android.R.color.darker_gray));
        selectColor = a.getColor(R.styleable.IndicatorView_select_color, getResources()
                .getColor(android.R.color.white));
        strokeColor = a.getColor(R.styleable.IndicatorView_stroke_color, getResources()
                .getColor(android.R.color.white));
        type = a.getInt(R.styleable.IndicatorView_type, TYPE_LINE);
        space = a.getDimensionPixelOffset(R.styleable.IndicatorView_space, 1);
        pages = a.getInt(R.styleable.IndicatorView_pages, 1);
        cellWidth = a.getDimensionPixelOffset(R.styleable.IndicatorView_cell_width, 1);
        cellHeight = a.getDimensionPixelOffset(R.styleable.IndicatorView_cell_height, 1);
        a.recycle();

        paint = new Paint();
    }

    public IndicatorView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.IndicatorView, defStyleAttr, 0);
        bgColor = a.getColor(R.styleable.IndicatorView_circle_bg_color, getResources()
                .getColor(android.R.color.darker_gray));
        selectColor = a.getColor(R.styleable.IndicatorView_select_color, getResources()
                .getColor(android.R.color.white));
        type = a.getInt(R.styleable.IndicatorView_type, TYPE_LINE);
        space = a.getDimensionPixelSize(R.styleable.IndicatorView_space, 1);
        pages = a.getInt(R.styleable.IndicatorView_pages, 1);
        cellWidth = a.getDimensionPixelSize(R.styleable.IndicatorView_cell_width, 1);
        cellHeight = a.getDimensionPixelSize(R.styleable.IndicatorView_cell_height, 1);
        a.recycle();

        paint = new Paint();
    }

    public void setPages(int pages) {
        this.pages = pages;
        requestLayout();
        invalidate();
    }

    public void setPosition(float position) {
        this.position = position;
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int width = getWidth();
        int height = getHeight();
        if (pages == 0){
            return;
        }

        paint.setColor(bgColor);
        paint.setStrokeWidth(1.0f);
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.FILL);//设置实心

        float everageWidth = width / pages;
        float cellWidth = width / pages - space;
        float cellHeight = height;
        if (type == TYPE_LINE){
            float radius = cellHeight / 2 + 1;
            for (int i = 0; i < pages; i++){
                paint.setColor(bgColor);
                paint.setStyle(Paint.Style.FILL);//设置空心
                RectF rectF = new RectF(everageWidth * i + space / 2, (height - cellHeight) / 2,
                        everageWidth * i + space / 2 + cellWidth, (height - cellHeight) / 2 + cellHeight);
                canvas.drawRoundRect(rectF, radius, radius, paint);
//                canvas.drawRect(rectF, paint);
                paint.setColor(strokeColor);
                paint.setStyle(Paint.Style.STROKE);//设置空心
                canvas.drawRoundRect(rectF, radius, radius, paint);
//                canvas.drawRect(rectF, paint);
            }
        }else if (type == TYPE_CYCLE){
            float radius = (Math.min(cellHeight, cellWidth) - 2) / 2;
            for (int i = 0; i < pages; i++){
                paint.setColor(bgColor);
                paint.setStyle(Paint.Style.FILL);//设置空心
                canvas.drawCircle(everageWidth * (2 * i + 1) / 2, height / 2, radius, paint);
                paint.setColor(strokeColor);
                paint.setStyle(Paint.Style.STROKE);//设置空心
                canvas.drawCircle(everageWidth * (2 * i + 1) / 2, height / 2, radius, paint);
            }

        }

        paint.setColor(selectColor);
        paint.setStrokeWidth(0.5f);
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.FILL);//设置实心
        float maxWidth = width - everageWidth;
        float progressDistance = maxWidth * position;
        if (type == TYPE_LINE){
            float radius = cellHeight / 2 + 1;
            RectF rectF = new RectF(space / 2 + progressDistance, (height - cellHeight) / 2 + 0.5f,
                    space / 2 + cellWidth + progressDistance, (height - cellHeight) / 2 + cellHeight - 0.5f);
            canvas.drawRoundRect(rectF, radius, radius, paint);
//            canvas.drawRect(rectF, paint);
        }else if (type == TYPE_CYCLE){
            float radius = (Math.min(cellHeight, cellWidth) - 2) / 2;
            canvas.drawCircle(everageWidth / 2 + progressDistance, height / 2, radius, paint);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int specMode = MeasureSpec.getMode(widthMeasureSpec);

        if (specMode == MeasureSpec.AT_MOST){
            Log.i(tag, "AT_MOST");
            int measureWidth = (int) ((cellWidth + space) * pages);
            int measureHeight = (int) cellHeight;
            setMeasuredDimension(measureWidth, measureHeight);
        }else {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }

}
