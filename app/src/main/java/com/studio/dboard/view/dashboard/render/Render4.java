package com.studio.dboard.view.dashboard.render;

import android.content.Context;
import android.content.res.Resources;
import android.opengl.GLES20;

import com.studio.dboard.R;
import com.studio.dboard.view.dashboard.shape.common.Texture;
import com.studio.dboard.view.dashboard.shape.board4.Circle;
import com.studio.dboard.view.dashboard.shape.board4.DotLine;
import com.studio.dboard.view.dashboard.shape.board4.Light;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class Render4 extends BaseRender {


    private static final int SPEED_MAX = 260;
    private static final int DASHBOARD_WIDTH = 656;
    private static final int OFFSET_BOARD_Y = 36;//屏幕中心向下偏移
    private static final int HOLLOW_CIRCLE_RADIU = 175;
    private static final int ROAD_WIDTH = 494;
    private static final int ROAD_HEIGHT = 789;
    private static final int CAR_WIDTH = 526;
    private static final int CAR_HEIGHT = 831;
    private static final int CAR_INIT_HEIGHT = 415;
    private static final int CAR_ANIM_MAX_DISTANCE = 30;
    private static final float CAR_SCALE_MIX = 0.35f;
    private static final float CAR_SCALE_MAX = 1.0f;
    private static final int ROAD_LINE_DISTANCE = 50;
    private static final int ROAD_LINE_TOP = -2500;
    private static final int ROAD_LINE_DOT_LENGTH = 220;
    private static final int ROAD_LINE_DOT_SPACE = 80;


    private int dashboard_width;
    private int offset_board_y;
    private int hollowCircleRadiu;
    private int roadLineDistance;
    private int roadWidth;
    private int roadHeight;
    private int carWidth;
    private int carHeight;
    private int carInitHeight;
    private int carAnimMaxDist;
    private int roadLineLeft;
    private int roadLineRight;
    private int roadLineTop;
    private int roadLineDotLength;
    private int roadLineDotSpace;

    private Texture texBg;
    private Texture texRoad;
    private Texture texCar;
    private Light texLight;
    private Circle hollowCircle;//空心圆
    private DotLine dotLine1;
    private DotLine dotLine2;

    private static float dist = 0;
    private static int roadLineDist = 0;

    private static long time = 0;
    private static long maxTime = 400;//车灯渐隐渐显时间400毫秒



    public Render4(Context mContext, Resources res) {
        super(mContext, res);
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        super.onSurfaceCreated(gl, config);
        texBg = new Texture(res, R.drawable.dashbroad_disc_bg_04);
        texRoad = new Texture(res, R.drawable.bg_road_04);
        texCar = new Texture(res, R.drawable.car_normal_04);
        texLight = new Light(res, R.drawable.car_light_04);
        hollowCircle = new Circle(res);
        dotLine1 = new DotLine(res);
        dotLine2 = new DotLine(res);
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        super.onSurfaceChanged(gl, width, height);
        dashboard_width = Math.round(DASHBOARD_WIDTH * scale);
        offset_board_y = Math.round(OFFSET_BOARD_Y * scale);
        hollowCircleRadiu = Math.round(HOLLOW_CIRCLE_RADIU * scale);
        roadWidth = Math.round(ROAD_WIDTH * scale);
        roadHeight = Math.round(ROAD_HEIGHT * scale);
        carWidth = Math.round(CAR_WIDTH * scale);
        carHeight = Math.round(CAR_HEIGHT * scale);
        carInitHeight = Math.round(CAR_INIT_HEIGHT * scale);
        carAnimMaxDist = Math.round(CAR_ANIM_MAX_DISTANCE * scale);
        roadLineDistance = Math.round(ROAD_LINE_DISTANCE * scale);
        roadLineTop = Math.round(ROAD_LINE_TOP * scale);
        roadLineDotLength = Math.round(ROAD_LINE_DOT_LENGTH * scale);
        roadLineDotSpace = Math.round(ROAD_LINE_DOT_SPACE * scale);

        roadLineLeft = (screenWidth - roadWidth) / 2 + roadLineDistance;
        roadLineRight = (screenWidth + roadWidth) / 2 - roadLineDistance;

        float[] circlePosition = new float[]{
                toGLX(screenWidth / 2 + dashboard_width / 2),
                toGLY(screenHeight / 2 - dashboard_width / 2),
                toGLX(screenWidth / 2 - dashboard_width / 2),
                toGLY(screenHeight / 2 - dashboard_width / 2),
                toGLX(screenWidth / 2 + dashboard_width / 2),
                toGLY(screenHeight / 2 + dashboard_width / 2),
                toGLX(screenWidth / 2 - dashboard_width / 2),
                toGLY(screenHeight / 2 + dashboard_width / 2)
        };
        texBg.setPositions(circlePosition);

        float[] roadPosition = new float[]{
                toGLX(screenWidth / 2 + roadWidth / 2),
                toGLY(screenHeight - roadHeight),
                toGLX(screenWidth / 2 - roadWidth / 2),
                toGLY(screenHeight - roadHeight),
                toGLX(screenWidth / 2 + roadWidth / 2),
                toGLY(screenHeight),
                toGLX(screenWidth / 2 - roadWidth / 2),
                toGLY(screenHeight)
        };
        texRoad.setPositions(roadPosition);

        float[] carPosition = new float[]{
                toGLX(screenWidth / 2 + carWidth / 2),
                toGLY(screenHeight - carInitHeight),
                toGLX(screenWidth / 2 - carWidth / 2),
                toGLY(screenHeight - carInitHeight),
                toGLX(screenWidth / 2 + carWidth / 2),
                toGLY(screenHeight + carHeight - carInitHeight),
                toGLX(screenWidth / 2 - carWidth / 2),
                toGLY(screenHeight + carHeight - carInitHeight)
        };
        texCar.setPositions(carPosition);
        texLight.initPositions(carPosition);

        hollowCircle.setParameter(0, 0, toGLWidth(hollowCircleRadiu), 0);
    }

    @Override
    public void onDrawFrame(GL10 gl) {
        super.onDrawFrame(gl);
        GLES20.glClearColor(0.086f,0.102f,0.125f,1.0f);
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);

        float transitionY = toGLY(screenHeight / 2 + offset_board_y);

        //绘制背景
        GLES20.glBlendFunc(GLES20.GL_DST_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
        VaryTools.resetMatrix();
        VaryTools.translate(0, transitionY, 0);
        texBg.draw(VaryTools.getFinalMatrix());

        //绘制隧道中的空心圆
        GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
        for (int i = 0; i < 8; i++){
            dist += Math.min(speed, 120) * 1.0f / SPEED_MAX * 0.015;
            if (dist >= 1.0f){
                dist = dist % 1.0f;
            }
            hollowCircle.setColor(new float[]{0.512f, 0.96f, 0.81f, 0.25f * Math.abs((8 - i) * (-1.0f) + dist) / 8});
            VaryTools.resetMatrix();
            VaryTools.translate(0, transitionY, i * (-1.0f) + dist);
            hollowCircle.draw(VaryTools.getFinalMatrix());
        }

        //绘制马路
        GLES20.glBlendFunc(GLES20.GL_DST_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
        VaryTools.resetMatrix();
        texRoad.draw(VaryTools.getFinalMatrix());


        //绘制车灯
        if (!(hour >= 6 && hour < 19)){ //24小时制，判断是否是夜晚
            float alpha;
            if (speed == 0){
                time = System.currentTimeMillis();

                maxTime -= 15;
                if (maxTime < 0){
                    maxTime = 0;
                }
                alpha = maxTime * 1.0f / 400;
            }else {
                maxTime = 400;//恢复动画最大执行时间

                long duration = System.currentTimeMillis() - time;
                if (duration < 400){
                    alpha = duration * 1.0f / 400;
                }else {
                    alpha = 1.0f;
                }
            }
            texLight.setAlpha(alpha);
        }else {
            texLight.setAlpha(0);
        }
        GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
        VaryTools.resetMatrix();
        float tranY = toGLHeight(Math.min(animSpeed, 80) * carAnimMaxDist / 80  * scale);
        VaryTools.translate(0, tranY, 0);
        float carScale = (CAR_SCALE_MAX - Math.min(animSpeed, 80) * (CAR_SCALE_MAX - CAR_SCALE_MIX) / 80) * scale;
        VaryTools.scale(carScale, carScale, 1.0f);
        //绘制汽车
        texCar.draw(VaryTools.getFinalMatrix());
        texLight.draw(VaryTools.getFinalMatrix());


        //绘制路两边的虚线
        roadLineDist += Math.min(speed, 80) * 1.0f / 80 * 25;
        if (roadLineDist >= (roadLineDotLength + roadLineDotSpace)){
            roadLineDist %= (roadLineDotLength + roadLineDotSpace);
        }

        dotLine1.setParameter(
                new float[]{toGLX(roadLineLeft), toGLY(screenHeight / 2 + roadLineDist)},
                new float[]{toGLX(roadLineLeft), toGLY(roadLineTop + roadLineDist)},
                toGLHeight(roadLineDotLength),
                toGLHeight(roadLineDotSpace),
                3 * scale
        );

        dotLine2.setParameter(
                new float[]{toGLX(roadLineRight), toGLY(screenHeight / 2 + roadLineDist)},
                new float[]{toGLX(roadLineRight), toGLY(roadLineTop + roadLineDist)},
                toGLHeight(roadLineDotLength),
                toGLHeight(roadLineDotSpace),
                3 * scale
        );

        VaryTools.resetMatrix();
        VaryTools.translate(0, toGLHeight(-screenHeight / 2), 0);
        VaryTools.rotate(-86, 1, 0, 0);
        dotLine1.draw(VaryTools.getFinalMatrix());

        VaryTools.resetMatrix();
        VaryTools.translate(0, toGLHeight(-screenHeight / 2), 0);
        VaryTools.rotate(-86, 1, 0, 0);
        dotLine2.draw(VaryTools.getFinalMatrix());
    }

    @Override
    public void notifyDestroy() {

    }
}
