/*
 *
 * BaseShape.java
 * 
 * Created by Wuwang on 2016/9/30
 */
package com.studio.dboard.view.dashboard.shape;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLES20;
import android.opengl.GLUtils;

import javax.microedition.khronos.opengles.GL10;

/**
 * Description:
 */
public abstract class BaseShape {

    protected int screenWidth;
    protected int screenHeight;
    protected Resources res;
    protected int[] textureIds;

    public BaseShape(Resources res){
        this.res = res;
        initProgram();
    }

    public BaseShape(Resources res, int... resIds) {
        this(res);
        initTexture(resIds);
    }

    public BaseShape(Resources res, Bitmap... bitmaps) {
        this(res);
        initTexture(bitmaps);
    }

    public void setScreenPix(int screenWidth, int screenHeight) {
        this.screenWidth = screenWidth;
        this.screenHeight = screenHeight;
    }


    protected void initTexture(int... resIds){
        textureIds = new int[resIds.length];
        GLES20.glGenTextures(textureIds.length, textureIds, 0);
        for (int i = 0; i < resIds.length; i++) {
            int textureId = textureIds[i];
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureId);
            //设置缩小过滤为使用纹理中坐标最接近的一个像素的颜色作为需要绘制的像素颜色
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
            //设置放大过滤为使用纹理中坐标最接近的若干个颜色，通过加权平均算法得到需要绘制的像素颜色
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
            //设置环绕方向S，截取纹理坐标到[1/2n,1-1/2n]。将导致永远不会与border融合
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
            //设置环绕方向T，截取纹理坐标到[1/2n,1-1/2n]。将导致永远不会与border融合
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);

            //根据以上指定的参数，生成一个2D纹理
            int resId = resIds[i];
            Bitmap bitmap = BitmapFactory.decodeResource(res, resId);
            GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0);
            bitmap.recycle();
        }
    }

    public void initTexture(Bitmap... bitmaps){
        textureIds = new int[bitmaps.length];
        GLES20.glGenTextures(textureIds.length, textureIds, 0);
        for (int i = 0; i < bitmaps.length; i++) {
            int textureId = textureIds[i];
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureId);
            //设置缩小过滤为使用纹理中坐标最接近的一个像素的颜色作为需要绘制的像素颜色
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
            //设置放大过滤为使用纹理中坐标最接近的若干个颜色，通过加权平均算法得到需要绘制的像素颜色
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
            //设置环绕方向S，截取纹理坐标到[1/2n,1-1/2n]。将导致永远不会与border融合
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
            //设置环绕方向T，截取纹理坐标到[1/2n,1-1/2n]。将导致永远不会与border融合
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);

            //根据以上指定的参数，生成一个2D纹理
            Bitmap bitmap = bitmaps[i];
            GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0);
            bitmap.recycle();
        }
    }

    protected abstract void initProgram();

    public abstract void draw(float[] mMVPMatrix);

    public abstract void notifyDestroy();
}
