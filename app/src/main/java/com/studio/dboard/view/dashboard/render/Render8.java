package com.studio.dboard.view.dashboard.render;

import android.content.Context;
import android.content.res.Resources;

import com.studio.dboard.R;
import com.studio.dboard.view.dashboard.shape.common.Texture;
import com.studio.dboard.view.dashboard.shape.board8.TextureColorPro;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class Render8 extends BaseRender {

    protected static final int SPEED_MAX = 220;
    private static final int START_ANGLE = 180;
    protected static final float ANGLE_MAX = 330;
    private static final int DASHBOARD_WIDTH = 534;
    private static final int OFFSET_BOARD_Y = 60;//屏幕中心向下偏移

    private int dashboard_width;
    private int offset_board_y;

    private Texture texBg;
    private Texture texMarkBg;
    private TextureColorPro texColorBg;
    private Texture texMark;
    private Texture texHourPointer;
    private Texture texMinatePointer;
    private Texture texSecondPointer;


    public Render8(Context mContext, Resources res) {
        super(mContext, res);
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        super.onSurfaceCreated(gl, config);
        texBg = new Texture(res, R.drawable.background_08);
        texMarkBg = new Texture(res, R.drawable.dashbroad_disc_bg_08);
        texColorBg = new TextureColorPro(res, R.drawable.clock_speed_color_08);
        texMark = new Texture(res, R.drawable.clock_inside_shadow_08);
        texHourPointer = new Texture(res, R.drawable.clock_hour_08);
        texMinatePointer = new Texture(res, R.drawable.clock_minute_08);
        texSecondPointer = new Texture(res, R.drawable.clock_second_08);
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        super.onSurfaceChanged(gl, width, height);
        dashboard_width = Math.round(DASHBOARD_WIDTH * scale);
        offset_board_y = Math.round(OFFSET_BOARD_Y * scale);

        float[] bgPosition = new float[]{
                toGLX(0),
                toGLY(0),
                toGLX(screenWidth),
                toGLY(0),
                toGLX(0),
                toGLY(screenHeight),
                toGLX(screenWidth),
                toGLY(screenHeight),
        };
        texBg.setPositions(bgPosition);

        float[] boardPosition = new float[]{
                toGLX(screenWidth / 2 - dashboard_width / 2),
                toGLY(screenHeight / 2 - dashboard_width / 2),
                toGLX(screenWidth / 2 + dashboard_width / 2),
                toGLY(screenHeight / 2 - dashboard_width / 2),
                toGLX(screenWidth / 2 - dashboard_width / 2),
                toGLY(screenHeight / 2 + dashboard_width / 2),
                toGLX(screenWidth / 2 + dashboard_width / 2),
                toGLY(screenHeight / 2 + dashboard_width / 2),
        };
        texMarkBg.setPositions(boardPosition);
        texColorBg.setPositions(boardPosition);
        texMark.setPositions(boardPosition);
        texHourPointer.setPositions(boardPosition);
        texMinatePointer.setPositions(boardPosition);
        texSecondPointer.setPositions(boardPosition);


        texColorBg.setScreenPix(screenWidth, screenHeight);
        texColorBg.setuCenter(new float[]{screenWidth / 2, screenHeight / 2 - offset_board_y});
        texColorBg.setScale(scale);
    }

    @Override
    public void onDrawFrame(GL10 gl) {
        super.onDrawFrame(gl);

        float sweepAngle = animSpeed * ANGLE_MAX / SPEED_MAX;
        float angle = (START_ANGLE + sweepAngle) % 360;
        texColorBg.setAngle(angle);
        texColorBg.setSpeed(animSpeed);

        //绘制背景
        VaryTools.resetMatrix();
        texBg.draw(VaryTools.getFinalMatrix());

        //绘制clock表盘
        VaryTools.resetMatrix();
        float transitionY = toGLY(screenHeight / 2 + offset_board_y);
        VaryTools.translate(0, transitionY, 0);
        texMarkBg.draw(VaryTools.getFinalMatrix());

        //绘制变色色环
        VaryTools.resetMatrix();
        VaryTools.translate(0, transitionY, 0);
        texColorBg.draw(VaryTools.getFinalMatrix());

        //绘制阴影
        VaryTools.resetMatrix();
        VaryTools.translate(0, transitionY, 0);
        texMark.draw(VaryTools.getFinalMatrix());

        //时针
        float hourAngle = 360 - (hour % 12 * 1.0f / 12 * 360 + minate % 60 * 1.0f / 60 * 30);
        VaryTools.resetMatrix();
        VaryTools.translate(0, transitionY, 0);
        VaryTools.rotate(hourAngle, 0,0,1);
        texHourPointer.draw(VaryTools.getFinalMatrix());

        //分针
        float minateAngle = 360 - (minate % 60 * 1.0f / 60 * 360 + second % 60 * 1.0f / 60 * 6);
        VaryTools.resetMatrix();
        VaryTools.translate(0, transitionY, 0);
        VaryTools.rotate(minateAngle, 0,0,1);
        texMinatePointer.draw(VaryTools.getFinalMatrix());

        //秒针
        float secondAngle = 360 - second % 60 * 1.0f / 60 * 360;
        VaryTools.resetMatrix();
        VaryTools.translate(0, transitionY, 0);
        VaryTools.rotate(secondAngle, 0,0,1);
        texSecondPointer.draw(VaryTools.getFinalMatrix());
    }

    @Override
    public void notifyDestroy() {

    }
}
