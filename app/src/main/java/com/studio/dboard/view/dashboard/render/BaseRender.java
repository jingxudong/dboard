package com.studio.dboard.view.dashboard.render;

import android.content.Context;
import android.content.res.Resources;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;

import com.studio.dboard.Constant;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;


/**
 * Created by peak on 2018/1/19.
 */
public abstract class BaseRender implements GLSurfaceView.Renderer {

    protected static final String tag = "Render";


    protected Context mContext;
    protected Resources res;

    protected float scale;

//    protected float[] mViewMatrix = new float[16];
//    protected float[] mProjectMatrix = new float[16];
//    protected float[] mMVPMatrix = new float[16];

    protected static float ratio;
    protected static int screenWidth;
    protected static int screenHeight;

    protected float initProgress = 1.0f;
    protected float animSpeed = 0;
    protected float speed = 0;
    protected float oldSpeed = 0;
    protected int second = 0;
    protected int hour = 0;
    protected int minate = 0;

    protected float delt = 0;

    protected long startTime;
    protected long curTime;
    protected long dt;


    public BaseRender(Context mContext, Resources res) {
        this.res = res;
        this.mContext = mContext;
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {

        GLES20.glEnable(GLES20.GL_TEXTURE_2D);

        GLES20.glEnable(GLES20.GL_BLEND);
        GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);

        //以下参数设置比较耗内存
        // 点平滑处理
        GLES20.glEnable(GL10.GL_POINT_SMOOTH);
        GLES20.glHint(GL10.GL_POINT_SMOOTH_HINT, GL10.GL_NICEST);
        // 直线平滑处理
        GLES20.glEnable(GL10.GL_LINE_SMOOTH);
        GLES20.glHint(GL10.GL_LINE_SMOOTH_HINT, GL10.GL_NICEST);
        // 多边形平滑处理
        GLES20.glHint(GL10.GL_POLYGON_SMOOTH_HINT, GL10.GL_NICEST);
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {


        GLES20.glViewport(0,0,width,height);
        screenWidth = width;
        screenHeight = height;
        scale = Math.min(width * 1.0f / Constant.STANDARD_WIDTH, height * 1.0f / Constant.STANDARD_HEIGHT);

        //计算宽高比
        ratio = (float)width / height;

        //设置正交投影
//        VaryTools.setProjectOrtho(-ratio,ratio,-1,1,2,10);
        //设置透视投影
//        VaryTools.setPerspectiveM(45, ratio, 2, 10);
        //设置透视投影
        VaryTools.setFrustum(-ratio, ratio, -1, 1, 2, 10);
        //设置相机位置
        VaryTools.setCameraLookAtM(
                0, 0, 2.01f, //相机的x,y,z坐标
                0f, 0f, 0f,   //目标对应的x,y,z坐标
                0f, 1.0f, 0.0f //相机的视觉向量(upx,upy,upz,三个向量最终的合成向量的方向为相机的方向)
        );

    }

    @Override
    public void onDrawFrame(GL10 gl) {
        curTime = System.currentTimeMillis();
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);

        animSpeed = oldSpeed + delt * (curTime - startTime) / 1000;
    }

    public void initProgress(float progress){
        this.initProgress = progress;
    }

    /**
     * 速度变化通知
     * @param curSpeed
     */
    public void notifySpeedChanged(float curSpeed){
        oldSpeed = speed;
        speed = curSpeed;
        delt = speed - oldSpeed;
        startTime = System.currentTimeMillis();
    }

    /**
     * 时间变化通知(秒)
     * @param second
     */
    public void notifyTimeChanged(int hour, int minate, int second){
        this.hour = hour;
        this.minate = minate;
        this.second = second;
    }

    public void notifyWeather(String weatherState){

    }

    public abstract void notifyDestroy();

    /**
     * Convert x to openGL
     *
     * @param x
     *            Screen x offset top left
     * @return Screen x offset top left in OpenGL
     */
    protected float toGLX(float x) {
        return -1.0f * ratio + toGLWidth(x);
    }

    /**
     * Convert y to openGL y
     *
     * @param y
     *            Screen y offset top left
     * @return Screen y offset top left in OpenGL
     */
    protected float toGLY(float y) {
        return 1.0f - toGLHeight(y);
    }

    /**
     * Convert width to openGL width
     *
     * @param width
     * @return Width in openGL
     */
    protected float toGLWidth(float width) {
        return 2.0f * (width / screenWidth) * ratio;
    }

    /**
     * Convert height to openGL height
     *
     * @param height
     * @return Height in openGL
     */
    protected float toGLHeight(float height) {
        return 2.0f * (height / screenHeight);
    }

    /**
     * Convert x to screen x
     *
     * @param glX
     *            openGL x
     * @return screen x
     */
    protected float toScreenX(float glX) {
        return toScreenWidth(glX - (-1 * ratio));
    }

    /**
     * Convert y to screent y
     *
     * @param glY
     *            openGL y
     * @return screen y
     */
    protected float toScreenY(float glY) {
        return toScreenHeight(1.0f - glY);
    }

    /**
     * Convert glWidth to screen width
     *
     * @param glWidth
     * @return Width in screen
     */
    protected float toScreenWidth(float glWidth) {
        return (glWidth * screenWidth) / (2.0f * ratio);
    }

    /**
     * Convert height to screen height
     *
     * @param glHeight
     * @return Height in screen
     */
    protected float toScreenHeight(float glHeight) {
        return (glHeight * screenHeight) / 2.0f;
    }
}
