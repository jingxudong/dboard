package com.studio.dboard.view.dashboard.render;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;

import com.studio.dboard.R;
import com.studio.dboard.view.dashboard.shape.common.Texture;
import com.studio.dboard.view.dashboard.shape.common.TextureChangeColor;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class Render3 extends BaseRender {

    private static final int DASHBOARD_WIDTH = 552;
    private static final float ANGLE_MAX = 260;
    private static final int SPEED_MAX = 260;
    private static final int THRESHOLD = 120;
    private static final int OFFSET_BOARD_Y = 36;//屏幕中心向下偏移
    private static final int SHADOW_WIDTH = 570;
    private static final int SHADOW_HEIGHT = 290;
    private static final int OFFSET_SHADOW_Y = 155;//屏幕中心向下偏移
    private static final int RADIU_1 = 198;
    private static final int RADIU_2 = 180;
    private static final int RADIU_3 = 170;
    private static final int RADIU_4 = 151;
    private static final int RADIU_5_MAX = 136;
    private static final int RADIU_5_MIN = 68;
    private static final int RADIU_GLOW = 190;
    private static final int OUT_LIGHT_WIDTH = 574;

    private static final int colors[][] = new int[][]{
            {
                    0xff010723, //circle4
                    0xff041043, //circle3
                    0xff08185e, //circle2
                    0xff589cff,
                    0xff011333,
                    0xff042c8c, //glow01
                    0xff589cff, //glow02
                    0xff0048dc, //pointer03
                    0xff1843ff, //pointer04
                    0xff4974ff, //pointer05
                    0xff6f8eff, //pointer06
                    0xffd3d5ff, //pointer07
            },
            {
                    0xff310000, //circle4
                    0xff4f0000, //circle3
                    0xff640000, //circle2
                    0xffde4646,
                    0xff380101,
                    0xff820f0f, //glow01
                    0xffee6868, //glow02
                    0xffc61616, //pointer03
                    0xffe82e2e, //pointer04
                    0xffed5b5b, //pointer05
                    0xfff17e7e, //pointer06
                    0xfffbd8d8, //pointer07
            }};


    private int dashboard_width;
    private int offset_board_y;
    private int shadowWidth;
    private int shadowHeight;
    private int offset_shadow_y;
    private int radiu_1;
    private int radiu_2;
    private int radiu_3;
    private int radiu_4;
    private int radiu_5_max;
    private int radiu_5_min;
    private int radiu_glow;
    private int out_light_width;


    private Texture texBg;
    private Texture texShadow;
    private Texture texMarkBg;
    private TextureChangeColor texCircle1;
    private TextureChangeColor texCircle2;
    private TextureChangeColor texCircle3;
    private TextureChangeColor texCircle4;
    private TextureChangeColor texCircle5;
    private TextureChangeColor texCircleGlow1;
    private TextureChangeColor texCircleGlow2;
    private Texture texCircleGlow3;
    private Texture texPointer1;
    private Texture texPointer2;
    private TextureChangeColor texPointer3;
    private TextureChangeColor texPointer4;
    private TextureChangeColor texPointer5;
    private TextureChangeColor texPointer6;
    private TextureChangeColor texPointer7;
    private Texture texOutLight;



    public Render3(Context mContext, Resources res) {
        super(mContext, res);
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        super.onSurfaceCreated(gl, config);
        texBg = new Texture(res, R.drawable.background_03);
        texShadow = new Texture(res, R.drawable.dashbroad_shadow);
        texMarkBg = new Texture(res, R.drawable.dashbroad_disc_bg_03);
        texCircle1 = new TextureChangeColor(res, R.drawable.dashbroad_disc_circle_03_01);
        texCircle2 = new TextureChangeColor(res, R.drawable.dashbroad_disc_circle_03_02);
        texCircle3 = new TextureChangeColor(res, R.drawable.dashbroad_disc_circle_03_03);
        texCircle4 = new TextureChangeColor(res, R.drawable.dashbroad_disc_circle_03_04);
        texCircle5 = new TextureChangeColor(res, R.drawable.dashbroad_disc_circle_03_05);
        texCircleGlow1 = new TextureChangeColor(res, R.drawable.dashbroad_disc_circle_glow_03_01);
        texCircleGlow2 = new TextureChangeColor(res, R.drawable.dashbroad_disc_circle_glow_03_02);
        texCircleGlow3 = new Texture(res, R.drawable.dashbroad_disc_circle_glow_03_03);
        texPointer1 = new Texture(res, R.drawable.dashbroad_disc_pointer_light_03_01);
        texPointer2 = new Texture(res, R.drawable.dashbroad_disc_pointer_light_03_02);
        texPointer3 = new TextureChangeColor(res, R.drawable.dashbroad_disc_pointer_light_03_03);
        texPointer4 = new TextureChangeColor(res, R.drawable.dashbroad_disc_pointer_light_03_04);
        texPointer5 = new TextureChangeColor(res, R.drawable.dashbroad_disc_pointer_light_03_05);
        texPointer6 = new TextureChangeColor(res, R.drawable.dashbroad_disc_pointer_light_03_06);
        texPointer7 = new TextureChangeColor(res, R.drawable.dashbroad_disc_pointer_light_03_07);
        texOutLight = new Texture(res, R.drawable.dashbroad_disc_light_03);
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        super.onSurfaceChanged(gl, width, height);
        dashboard_width = Math.round(DASHBOARD_WIDTH * scale);
        offset_board_y = Math.round(OFFSET_BOARD_Y * scale);
        shadowWidth = Math.round(SHADOW_WIDTH * scale);
        shadowHeight = Math.round(SHADOW_HEIGHT * scale);
        offset_shadow_y = Math.round(OFFSET_SHADOW_Y * scale);
        radiu_1 = Math.round(RADIU_1 * scale);
        radiu_2 = Math.round(RADIU_2 * scale);
        radiu_3 = Math.round(RADIU_3 * scale);
        radiu_4 = Math.round(RADIU_4 * scale);
        radiu_5_max = Math.round(RADIU_5_MAX * scale);
        radiu_5_min = Math.round(RADIU_5_MIN * scale);
        radiu_glow = Math.round(RADIU_GLOW * scale);
        out_light_width = Math.round(OUT_LIGHT_WIDTH * scale);


        float[] bgPosition = new float[]{
                toGLX(0),
                toGLY(0),
                toGLX(screenWidth),
                toGLY(0),
                toGLX(0),
                toGLY(screenHeight),
                toGLX(screenWidth),
                toGLY(screenHeight),
        };
        texBg.setPositions(bgPosition);

        float[] shadowPosition = new float[]{
                toGLX(screenWidth / 2 - shadowWidth / 2),
                toGLY(screenHeight / 2 - shadowHeight / 2),
                toGLX(screenWidth / 2 + shadowWidth / 2),
                toGLY(screenHeight / 2 - shadowHeight / 2),
                toGLX(screenWidth / 2 - shadowWidth / 2),
                toGLY(screenHeight / 2 + shadowHeight / 2),
                toGLX(screenWidth / 2 + shadowWidth / 2),
                toGLY(screenHeight / 2 + shadowHeight / 2),
        };
        texShadow.setPositions(shadowPosition);

        float[] boardPosition = new float[]{
                toGLX(screenWidth / 2 - dashboard_width / 2),
                toGLY(screenHeight / 2 - dashboard_width / 2),
                toGLX(screenWidth / 2 + dashboard_width / 2),
                toGLY(screenHeight / 2 - dashboard_width / 2),
                toGLX(screenWidth / 2 - dashboard_width / 2),
                toGLY(screenHeight / 2 + dashboard_width / 2),
                toGLX(screenWidth / 2 + dashboard_width / 2),
                toGLY(screenHeight / 2 + dashboard_width / 2),
        };
        texMarkBg.setPositions(boardPosition);
        texCircleGlow1.initPositions(boardPosition);
        texCircleGlow1.setColor(new float[]{0.016f, 0.173f, 0.549f, 1.0f});
        texCircleGlow2.initPositions(boardPosition);
        texCircleGlow2.setColor(new float[]{0.345f, 0.612f, 1.0f, 1.0f});
        texCircleGlow3.setPositions(boardPosition);
        texPointer1.setPositions(boardPosition);
        texPointer2.setPositions(boardPosition);
        texPointer3.initPositions(boardPosition);
        texPointer3.setColor(new float[]{0.0f, 0.282f, 0.863f, 1.0f});
        texPointer4.initPositions(boardPosition);
        texPointer4.setColor(new float[]{0.094f, 0.263f, 1.0f, 1.0f});
        texPointer5.initPositions(boardPosition);
        texPointer5.setColor(new float[]{0.286f, 0.455f, 1.0f, 1.0f});
        texPointer6.initPositions(boardPosition);
        texPointer6.setColor(new float[]{0.435f, 0.561f, 1.0f, 1.0f});
        texPointer7.initPositions(boardPosition);
        texPointer7.setColor(new float[]{0.827f, 0.835f, 1.0f, 1.0f});
        texCircle1.initPositions(boardPosition);
        texCircle1.setColor(new float[]{0.004f, 0.075f, 0.2f, 1.0f});
        texCircle2.initPositions(boardPosition);
        texCircle2.setColor(new float[]{0.031f, 0.094f, 0.408f, 1.0f});
        texCircle3.initPositions(boardPosition);
        texCircle3.setColor(new float[]{0.02f, 0.043f, 0.2f, 1.0f});
        texCircle4.initPositions(boardPosition);
        texCircle4.setColor(new float[]{0.0f, 0.024f, 0.102f, 1.0f});
        texCircle5.initPositions(boardPosition);
        texCircle5.setColor(new float[]{0.0f, 0.0f, 0.0f, 1.0f});

        float[] outLightPosition = new float[]{
                toGLX(screenWidth / 2 - out_light_width / 2),
                toGLY(screenHeight / 2 - out_light_width / 2),
                toGLX(screenWidth / 2 + out_light_width / 2),
                toGLY(screenHeight / 2 - out_light_width / 2),
                toGLX(screenWidth / 2 - out_light_width / 2),
                toGLY(screenHeight / 2 + out_light_width / 2),
                toGLX(screenWidth / 2 + out_light_width / 2),
                toGLY(screenHeight / 2 + out_light_width / 2),
        };
        texOutLight.setPositions(outLightPosition);
    }

    @Override
    public void onDrawFrame(GL10 gl) {
        super.onDrawFrame(gl);

        float angleLeft = -40 - animSpeed * ANGLE_MAX / SPEED_MAX;
        float curRadiu5 = radiu_5_max - (radiu_5_max - radiu_5_min) * animSpeed / SPEED_MAX;
        float radiu5Scale = curRadiu5 / radiu_5_max;
        float curRadiu4 = curRadiu5 + (radiu_glow - curRadiu5) / 3;
        float radiu4Scale = curRadiu4 / radiu_4;
        float curRadiu3 = curRadiu5 + (radiu_glow - curRadiu5) * 2 / 3;
        float radiu3Scale = curRadiu3 / radiu_3;

        texCircle1.setColor(calculateColor(animSpeed, colors[0][4], colors[1][4]));
        texCircle2.setColor(calculateColor(animSpeed, colors[0][2], colors[1][2]));
        texCircle3.setColor(calculateColor(animSpeed, colors[0][1], colors[1][1]));
        texCircle4.setColor(calculateColor(animSpeed, colors[0][0], colors[1][0]));

        texCircleGlow1.setColor(calculateColor(animSpeed, colors[0][5], colors[1][5]));
        texCircleGlow2.setColor(calculateColor(animSpeed, colors[0][6], colors[1][6]));
        texPointer3.setColor(calculateColor(animSpeed, colors[0][7], colors[1][7]));
        texPointer4.setColor(calculateColor(animSpeed, colors[0][8], colors[1][8]));
        texPointer5.setColor(calculateColor(animSpeed, colors[0][9], colors[1][9]));
        texPointer6.setColor(calculateColor(animSpeed, colors[0][10], colors[1][10]));
        texPointer7.setColor(calculateColor(animSpeed, colors[0][11], colors[1][11]));

        //绘制背景
        VaryTools.resetMatrix();
        texBg.draw(VaryTools.getFinalMatrix());

        //绘制阴影
        VaryTools.resetMatrix();
        float shadowTransitionY = toGLY(screenHeight / 2 + offset_shadow_y);
        VaryTools.translate(0, shadowTransitionY, 0);
        texShadow.draw(VaryTools.getFinalMatrix());

        //绘制仪表
        VaryTools.resetMatrix();
        float transitionY = toGLY(screenHeight / 2 + offset_board_y);
        VaryTools.translate(0, transitionY, 0);
        texMarkBg.draw(VaryTools.getFinalMatrix());
        //绘制仪表盘变色圆
        texCircle1.draw(VaryTools.getFinalMatrix());
        texCircle2.draw(VaryTools.getFinalMatrix());

        VaryTools.resetMatrix();
        VaryTools.translate(0, transitionY, 0);
        VaryTools.scale(radiu3Scale, radiu3Scale, 0);
        texCircle3.draw(VaryTools.getFinalMatrix());

        VaryTools.resetMatrix();
        VaryTools.translate(0, transitionY, 0);
        VaryTools.scale(radiu4Scale, radiu4Scale, 0);
        texCircle4.draw(VaryTools.getFinalMatrix());

        VaryTools.resetMatrix();
        VaryTools.translate(0, transitionY, 0);
        VaryTools.scale(radiu5Scale, radiu5Scale, 0);
        texCircle5.draw(VaryTools.getFinalMatrix());

        VaryTools.resetMatrix();
        VaryTools.translate(0, transitionY, 0);
        texCircleGlow1.draw(VaryTools.getFinalMatrix());
        texCircleGlow2.draw(VaryTools.getFinalMatrix());
        texCircleGlow3.draw(VaryTools.getFinalMatrix());

        //仪表盘指针
        VaryTools.resetMatrix();
        VaryTools.translate(0, transitionY, 0);
        VaryTools.rotate(angleLeft, 0, 0, 1);
        texPointer1.draw(VaryTools.getFinalMatrix());
        texPointer2.draw(VaryTools.getFinalMatrix());
        texPointer3.draw(VaryTools.getFinalMatrix());
        texPointer4.draw(VaryTools.getFinalMatrix());
        texPointer5.draw(VaryTools.getFinalMatrix());
        texPointer6.draw(VaryTools.getFinalMatrix());
        texPointer7.draw(VaryTools.getFinalMatrix());

        //外部光晕
        VaryTools.resetMatrix();
        VaryTools.translate(0, transitionY, 0);
        texOutLight.draw(VaryTools.getFinalMatrix());
    }

    @Override
    public void notifyDestroy() {

    }

    private float[] calculateColor(float speed, int startColor, int endColor){
        int startColorRed = Color.red(startColor);
        int startColorGreen = Color.green(startColor);
        int startColorBlue = Color.blue(startColor);
        int endColorRed = Color.red(endColor);
        int endColorGreen = Color.green(endColor);
        int endColorBlue = Color.blue(endColor);

        float progress = speed / THRESHOLD < 1.0 ? speed / THRESHOLD : 1.0f;

        int colorRed = (int) (startColorRed + (endColorRed - startColorRed) * progress);
        int colorGreen = (int) (startColorGreen + (endColorGreen - startColorGreen) * progress);
        int colorBlue = (int) (startColorBlue + (endColorBlue - startColorBlue) * progress);

        float[] colors = new float[4];
        colors[0] = colorRed * 1.0f / 255;
        colors[1] = colorGreen * 1.0f / 255;
        colors[2] = colorBlue * 1.0f / 255;
        colors[3] = 1.0f;
        return colors;
    }
}
