package com.studio.dboard.view.dashboard.render;

import android.content.Context;
import android.content.res.Resources;

import com.studio.dboard.R;
import com.studio.dboard.view.dashboard.shape.common.Texture;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class Render5 extends BaseRender {

    private static final int DASHBOARD_WIDTH = 436;
    private static final int OFFSET_BOARD_Y = 71;//屏幕中心向下偏移


    private int dashboard_width;
    private int offset_board_y;


    private Texture texBg;
    private Texture texMarkBg;
    private Texture texHourPointer;
    private Texture texMinatePointer;
    private Texture texSecondPointer;


    public Render5(Context mContext, Resources res) {
        super(mContext, res);
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        super.onSurfaceCreated(gl, config);
        texBg = new Texture(res, R.drawable.background_05);
        texMarkBg = new Texture(res, R.drawable.dashbroad_disc_bg_05);
        texHourPointer = new Texture(res, R.drawable.clock_hour_05);
        texMinatePointer = new Texture(res, R.drawable.clock_minute_05);
        texSecondPointer = new Texture(res, R.drawable.clock_second_05);
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        super.onSurfaceChanged(gl, width, height);
        dashboard_width = Math.round(DASHBOARD_WIDTH * scale);
        offset_board_y = Math.round(OFFSET_BOARD_Y * scale);

        float[] bgPosition = new float[]{
                toGLX(0),
                toGLY(0),
                toGLX(screenWidth),
                toGLY(0),
                toGLX(0),
                toGLY(screenHeight),
                toGLX(screenWidth),
                toGLY(screenHeight),
        };
        texBg.setPositions(bgPosition);

        float[] boardPosition = new float[]{
                toGLX(screenWidth / 2 - dashboard_width / 2),
                toGLY(screenHeight / 2 - dashboard_width / 2),
                toGLX(screenWidth / 2 + dashboard_width / 2),
                toGLY(screenHeight / 2 - dashboard_width / 2),
                toGLX(screenWidth / 2 - dashboard_width / 2),
                toGLY(screenHeight / 2 + dashboard_width / 2),
                toGLX(screenWidth / 2 + dashboard_width / 2),
                toGLY(screenHeight / 2 + dashboard_width / 2),
        };
        texMarkBg.setPositions(boardPosition);
        texHourPointer.setPositions(boardPosition);
        texMinatePointer.setPositions(boardPosition);
        texSecondPointer.setPositions(boardPosition);
    }

    @Override
    public void onDrawFrame(GL10 gl) {
        super.onDrawFrame(gl);

        //绘制背景
        VaryTools.resetMatrix();
        texBg.draw(VaryTools.getFinalMatrix());

        //绘制仪表
        VaryTools.resetMatrix();
        float transitionY = toGLY(screenHeight / 2 + offset_board_y);
        VaryTools.translate(0, transitionY, 0);
        texMarkBg.draw(VaryTools.getFinalMatrix());

        //时针
        float hourAngle = 360 - (hour % 12 * 1.0f / 12 * 360 + minate % 60 * 1.0f / 60 * 30);
        VaryTools.resetMatrix();
        VaryTools.translate(0, transitionY, 0);
        VaryTools.rotate(hourAngle, 0,0,1);
        texHourPointer.draw(VaryTools.getFinalMatrix());

        //分针
        float minateAngle = 360 - (minate % 60 * 1.0f / 60 * 360 + second % 60 * 1.0f / 60 * 6);
        VaryTools.resetMatrix();
        VaryTools.translate(0, transitionY, 0);
        VaryTools.rotate(minateAngle, 0,0,1);
        texMinatePointer.draw(VaryTools.getFinalMatrix());

        //秒针
        float secondAngle = 360 - second % 60 * 1.0f / 60 * 360;
        VaryTools.resetMatrix();
        VaryTools.translate(0, transitionY, 0);
        VaryTools.rotate(secondAngle, 0,0,1);
        texSecondPointer.draw(VaryTools.getFinalMatrix());
    }

    @Override
    public void notifyDestroy() {

    }
}
