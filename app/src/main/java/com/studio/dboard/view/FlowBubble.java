package com.studio.dboard.view;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.studio.dboard.R;
import com.studio.dboard.allog.LogUtils;


/**
 * Created by peak on 2017/12/26.
 */

public class FlowBubble extends RelativeLayout {

    private static final String tag = "FloatView";

    private static final long LONG_PRESS_TIME = 500;
    private static final int MOVE_LENGH = 100;
    private long downTime = 0;
    private long upTime = 0;
    private float mTouchX;
    private float mTouchY;
    private float x;
    private float y;
    private float mStartX;
    private float mStartY;
    private int top_bar_height = 0;
    private OnClickListener mClickListener;
    private OnLongClickListener mLongClickListener;
    private WindowManager windowManager;
    // 此windowManagerParams变量为获取的全局变量，用以保存悬浮窗口的属性
    private WindowManager.LayoutParams windowManagerParams;

    //速度动画相关
    private float pointerProgress = 0;
    private int oldSpeed = 0;
    private int newSpeed = 0;
    private float progressSpeed = 0;
    private ValueAnimator speedAnimator;

    public void setWindowManager(WindowManager windowManager) {
        this.windowManager = windowManager;
    }

    public void setWindowManagerParams(WindowManager.LayoutParams windowManagerParams) {
        this.windowManagerParams = windowManagerParams;
    }

    private LongPressedThread mLongPressedThread;
    private Handler handler = new Handler();


    public FlowBubble(Context context) {
        super(context);
        init();
    }

    public FlowBubble(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public FlowBubble(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init(){
        top_bar_height = getResources().getDimensionPixelSize(R.dimen.dp_80);
        //开一个线程，延迟LONG_PRESS_TIME时间
        mLongPressedThread = new LongPressedThread();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: // 捕获手指触摸按下动作
                downTime = System.currentTimeMillis();
                // 获取相对View的坐标，即以此View左上角为原点
                mTouchX = event.getX();
                mTouchY = event.getY();
                mStartX = event.getRawX();
                mStartY = event.getRawY();
                handler.postDelayed(mLongPressedThread, LONG_PRESS_TIME);
//                setAlpha(1.0f);
                break;
            case MotionEvent.ACTION_MOVE: // 捕获手指触摸移动动作
                int tempX = (int) event.getRawX();
                int tempY = (int) event.getRawY();
                x = tempX - mTouchX;
                y = tempY - mTouchY;
                windowManagerParams.x = (int) x;
                windowManagerParams.y = (int) y;
                if (y > top_bar_height) {
                    windowManager.updateViewLayout(this, windowManagerParams); // 刷新显示
                }

                if(Math.abs(tempX - mStartX) > MOVE_LENGH
                        || Math.abs(tempY - mStartY) > MOVE_LENGH){
                    //取消注册的长按事件
                    handler.removeCallbacks(mLongPressedThread);
                }

                break;
            case MotionEvent.ACTION_UP: // 捕获手指触摸离开动作
                float endX = event.getRawX();
                float endY = event.getRawY();
                upTime = System.currentTimeMillis();
                if (upTime - downTime < 150 && Math.abs(endX - mStartX) < MOVE_LENGH
                        && Math.abs(endY - mStartY) < MOVE_LENGH){
                    if (mClickListener != null) {
                        mClickListener.onClick(this);
                    }
                }
                mStartX = mStartY = 0;

                //取消注册的长按事件
                handler.removeCallbacks(mLongPressedThread);
//                setAlpha(0.5f);
                break;
        }
        return true;
    }

    @Override
    public void setOnClickListener(OnClickListener l) {
        this.mClickListener = l;
    }

    public void setOnLongClickListener(OnLongClickListener mLongClickListener) {
        this.mLongClickListener = mLongClickListener;
    }

    public void setSpeed(final int speed){
        if (speed >= 0 ){
            handler.post(new Runnable() {
                @Override
                public void run() {
                    if (speedAnimator != null){
                        speedAnimator.cancel();
                    }
                    oldSpeed = newSpeed;
                    newSpeed = speed;
                    if (oldSpeed != newSpeed) {
                        speedAnimator = ValueAnimator.ofFloat(0f, 1.0f);
                        speedAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                            @Override
                            public void onAnimationUpdate(ValueAnimator animation) {
                                pointerProgress = animation.getAnimatedFraction();
                                progressSpeed = oldSpeed + ((newSpeed - oldSpeed) * pointerProgress);
                                updateSpeed((int)progressSpeed);
                            }
                        });
                        speedAnimator.setDuration(1000);
                        speedAnimator.setInterpolator(new LinearInterpolator());
                        speedAnimator.start();
                    }else {
                        updateSpeed(speed);
                    }
                }
            });
        } else {
            updateSpeed(-1);
        }
    }

    public void updateSpeed(int speed){
        ArcProgressView progressView = findViewById(R.id.progress);
        TextView speedV = findViewById(R.id.speed);
        if (speed >= 0) {
            speedV.setText(speed + "");
            progressView.setProgress(speed);
        }else{
            speedV.setText("--");
            progressView.setProgress(0);
        }
    }

    public class LongPressedThread implements Runnable {

        @Override
        public void run() {
            //这里处理长按事件
            if (mLongClickListener != null){
                mLongClickListener.onLongClick(FlowBubble.this);
            }
        }
    }
}
