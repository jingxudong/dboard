package com.studio.dboard.view.dashboard.render;

import android.content.Context;
import android.content.res.Resources;
import android.opengl.GLES20;

import com.studio.dboard.R;
import com.studio.dboard.view.dashboard.shape.common.Texture;
import com.studio.dboard.view.dashboard.shape.board2.TextureHlight;
import com.studio.dboard.view.dashboard.shape.board2.TexturePointer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class Render2 extends BaseRender {

    private static final int START_ANGLE = 180;
    private static final int DASHBOARD_WIDTH = 510;
    private static final float ANGLE_MAX = 288;
    private static final int SPEED_MAX = 240;
    private static final int OFFSET_BOARD_Y = 26;//屏幕中心向下偏移
    private static final int SHADOW_WIDTH = 570;
    private static final int SHADOW_HEIGHT = 290;
    private static final int OFFSET_SHADOW_Y = 141;//屏幕中心向下偏移


    private Texture texBg;
    private Texture texShadow;
    private Texture texMarkBg;
    private TextureHlight texHlight;
    private Texture texMark;
    private TexturePointer texPointer;

    private int dashboard_width;
    private int offset_board_y;
    private int shadowWidth;
    private int shadowHeight;
    private int offset_shadow_y;


    public Render2(Context mContext, Resources res) {
        super(mContext, res);

    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        super.onSurfaceCreated(gl, config);
        texBg = new Texture(res, R.drawable.background_02);
        texShadow = new Texture(res, R.drawable.dashbroad_shadow_01);
        texMarkBg = new Texture(res, R.drawable.dashbroad_disc_bg_02);
        texHlight = new TextureHlight(res, R.drawable.dashbroad_disc_high_light_circle_02);
        texMark = new Texture(res, R.drawable.dashbroad_disc_tick_mark_02);
        texPointer = new TexturePointer(res, R.drawable.dashbroad_disc_pointer_02);
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        super.onSurfaceChanged(gl, width, height);
        dashboard_width = Math.round(DASHBOARD_WIDTH * scale);
        offset_board_y = Math.round(OFFSET_BOARD_Y * scale);
        shadowWidth = Math.round(SHADOW_WIDTH * scale);
        shadowHeight = Math.round(SHADOW_HEIGHT * scale);
        offset_shadow_y = Math.round(OFFSET_SHADOW_Y * scale);


        float[] bgPosition = new float[]{
                toGLX(0),
                toGLY(0),
                toGLX(screenWidth),
                toGLY(0),
                toGLX(0),
                toGLY(screenHeight),
                toGLX(screenWidth),
                toGLY(screenHeight),
        };
        texBg.setPositions(bgPosition);

        float[] shadowPosition = new float[]{
                toGLX(screenWidth / 2 - shadowWidth / 2),
                toGLY(screenHeight / 2 - shadowHeight / 2),
                toGLX(screenWidth / 2 + shadowWidth / 2),
                toGLY(screenHeight / 2 - shadowHeight / 2),
                toGLX(screenWidth / 2 - shadowWidth / 2),
                toGLY(screenHeight / 2 + shadowHeight / 2),
                toGLX(screenWidth / 2 + shadowWidth / 2),
                toGLY(screenHeight / 2 + shadowHeight / 2),
        };
        texShadow.setPositions(shadowPosition);

        float[] boardPosition = new float[]{
                toGLX(screenWidth / 2 - dashboard_width / 2),
                toGLY(screenHeight / 2 - dashboard_width / 2),
                toGLX(screenWidth / 2 + dashboard_width / 2),
                toGLY(screenHeight / 2 - dashboard_width / 2),
                toGLX(screenWidth / 2 - dashboard_width / 2),
                toGLY(screenHeight / 2 + dashboard_width / 2),
                toGLX(screenWidth / 2 + dashboard_width / 2),
                toGLY(screenHeight / 2 + dashboard_width / 2),
        };
        texMarkBg.setPositions(boardPosition);
        texHlight.setPositions(boardPosition);
        texMark.setPositions(boardPosition);
        texPointer.setPositions(boardPosition);

        texPointer.setScreenPix(screenWidth, screenHeight);
        texPointer.setuCenter(new float[]{screenWidth / 2, screenHeight / 2 - offset_board_y});
        texPointer.setScale(scale);
        texHlight.setScreenPix(screenWidth, screenHeight);
        texHlight.setuCenter(new float[]{screenWidth / 2, screenHeight / 2 - offset_board_y});
    }

    @Override
    public void onDrawFrame(GL10 gl) {
        super.onDrawFrame(gl);


        float sweepAngle = animSpeed * ANGLE_MAX / SPEED_MAX;
        float angle = (START_ANGLE + sweepAngle);
        texHlight.setAngle(angle);

        GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
        //绘制背景
        VaryTools.resetMatrix();
        texBg.draw(VaryTools.getFinalMatrix());

        //绘制阴影
        VaryTools.resetMatrix();
        float shadowTransitionY = toGLY(screenHeight / 2 + offset_shadow_y);
        VaryTools.translate(0, shadowTransitionY, 0);
        texShadow.draw(VaryTools.getFinalMatrix());

        //绘制仪表
        VaryTools.resetMatrix();
        float transitionY = toGLY(screenHeight / 2 + offset_board_y);
        VaryTools.translate(0, transitionY, 0);
        texMarkBg.draw(VaryTools.getFinalMatrix());
        texHlight.draw(VaryTools.getFinalMatrix());
        //绘制刻度盘
        GLES20.glBlendFunc(GLES20.GL_DST_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
        texMark.draw(VaryTools.getFinalMatrix());

        //绘制指针
        float rotateAngle = 360 - sweepAngle;
        VaryTools.rotate(rotateAngle, 0, 0, 1);
        texPointer.draw(VaryTools.getFinalMatrix());

    }

    @Override
    public void notifyDestroy() {

    }
}
