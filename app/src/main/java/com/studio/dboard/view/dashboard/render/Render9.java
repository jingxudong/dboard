package com.studio.dboard.view.dashboard.render;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;

import com.studio.dboard.R;
import com.studio.dboard.entity.Weather;
import com.studio.dboard.utils.DateUtils;
import com.studio.dboard.utils.WeatherUtils;
import com.studio.dboard.utils.XmlUtils;
import com.studio.dboard.view.dashboard.shape.common.Texture;
import com.studio.dboard.view.dashboard.shape.board9.TextureWeather;

import java.util.ArrayList;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class Render9 extends BaseRender {

    private static final int OFFSET_BOARD_Y = 70;//屏幕中心向下偏移
    private static final int SHADOW_WIDTH = 570;

    private int offset_board_y;
    private int shadow_width;

    private ArrayList<Weather> weathers;

    private TextureWeather texWeather;
    private Texture texShadow;

    private Bitmap weatherImg;



    public Render9(Context mContext, Resources res) {
        super(mContext, res);
        weathers = XmlUtils.parseWeathers(mContext);
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        super.onSurfaceCreated(gl, config);
        Bitmap defaultImg = WeatherUtils.getWeatherBgBitmap(mContext, weathers,
                "无数据", DateUtils.getHour24());
        texWeather = new TextureWeather(res, defaultImg);
        texShadow = new Texture(res, R.drawable.dashbroad_disc_bg_09);

    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        super.onSurfaceChanged(gl, width, height);
        offset_board_y = Math.round(OFFSET_BOARD_Y * scale);
        shadow_width = Math.round(SHADOW_WIDTH * scale);

        float[] bgPosition = new float[]{
                toGLX(0),
                toGLY(0),
                toGLX(screenWidth),
                toGLY(0),
                toGLX(0),
                toGLY(screenHeight),
                toGLX(screenWidth),
                toGLY(screenHeight),
        };
        texWeather.setPositions(bgPosition);

        float[] boardPosition = new float[]{
                toGLX(screenWidth / 2 - shadow_width / 2),
                toGLY(screenHeight / 2 - shadow_width / 2),
                toGLX(screenWidth / 2 + shadow_width / 2),
                toGLY(screenHeight / 2 - shadow_width / 2),
                toGLX(screenWidth / 2 - shadow_width / 2),
                toGLY(screenHeight / 2 + shadow_width / 2),
                toGLX(screenWidth / 2 + shadow_width / 2),
                toGLY(screenHeight / 2 + shadow_width / 2),
        };
        texShadow.setPositions(boardPosition);



        texWeather.setScreenPix(screenWidth, screenHeight);
        texWeather.setuCenter(new float[]{screenWidth / 2, screenHeight / 2 - offset_board_y});
        texWeather.setScale(scale);
    }

    @Override
    public void notifyWeather(String weatherState) {
        super.notifyWeather(weatherState);
        weatherImg = WeatherUtils.getWeatherBgBitmap(mContext, weathers, weatherState, DateUtils.getHour24());
    }

    @Override
    public void onDrawFrame(GL10 gl) {
        super.onDrawFrame(gl);

        if(weatherImg != null && !weatherImg.isRecycled()){
            texWeather.initTexture(weatherImg);
        }
        VaryTools.resetMatrix();
        texWeather.draw(VaryTools.getFinalMatrix());

        VaryTools.resetMatrix();
        float transitionY = toGLY(screenHeight / 2 + offset_board_y);
        VaryTools.translate(0, transitionY, 0);
        texShadow.draw(VaryTools.getFinalMatrix());
    }

    @Override
    public void notifyDestroy() {

    }
}
