package com.studio.dboard.view.dashboard;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;

import com.studio.dboard.view.dashboard.render.RenderFactory;

public class PreBoardView extends BaseBoardView {


    public PreBoardView(Context context) {
        super(context);
    }

    public PreBoardView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    void setRender() {
        render = RenderFactory.buildPre(getContext(), getResources());
        setRenderer(render);
    }

    @Override
    protected void setRenderMode() {
        setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);
    }
}
