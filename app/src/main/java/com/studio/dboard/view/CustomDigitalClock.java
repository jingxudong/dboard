package com.studio.dboard.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.ContentObserver;
import android.graphics.Typeface;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.Settings;
import android.util.AttributeSet;
import android.widget.TextView;

import com.studio.dboard.utils.DateUtils;

/**
 * Created by peak on 2017/10/16.
 */

@SuppressLint("AppCompatCustomView")
public class CustomDigitalClock extends TextView {

    private static final String fontPath = "fonts/Helvetica_LT_35_Thin.ttf";

    private FormatChangeObserver mFormatChangeObserver;
    private Runnable mTicker;
    private static Handler mHandler = new Handler();
    private boolean mTickerStopped = false;
    private static long now;
    private static long next;
    private boolean isFormat24 = true;
    private Typeface typeface;


    public CustomDigitalClock(Context context) {
        super(context);
        initClock(context);
    }

    public CustomDigitalClock(Context context, AttributeSet attrs) {
        super(context, attrs);
        initClock(context);
    }

    private void initClock(Context context) {
        typeface = Typeface.createFromAsset(context.getAssets(), fontPath);
        mFormatChangeObserver = new FormatChangeObserver();
        getContext().getContentResolver().registerContentObserver(
                Settings.System.CONTENT_URI, true, mFormatChangeObserver);
        setFormat();


    }

    @Override
    protected void onAttachedToWindow() {
        mTickerStopped = false;
        super.onAttachedToWindow();
        /**
         * requests a tick on the next hard-second boundary
         */
        mTicker = new Runnable() {
            public void run() {
                if (mTickerStopped)
                    return;
                String time = DateUtils.getTime24();
                setText(time);
                setTypeface(typeface);
                now = SystemClock.uptimeMillis();
                next = now + (1000 - now % 1000);
                mHandler.postAtTime(mTicker, next);
            }
        };
        mTicker.run();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        mHandler.removeCallbacksAndMessages(null);
        mTickerStopped = true;
        mTicker = null;
    }

    /**
     * Pulls 12/24 mode from system settings
     */
    private boolean get24HourMode() {
        return android.text.format.DateFormat.is24HourFormat(getContext());
    }

    private void setFormat() {
        isFormat24 = get24HourMode();
    }

    private class FormatChangeObserver extends ContentObserver {
        public FormatChangeObserver() {
            super(mHandler);
        }
        @Override
        public void onChange(boolean selfChange) {
            setFormat();
        }
    }
}
