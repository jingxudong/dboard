package com.studio.dboard.view.dashboard.shape.board1;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLES20;
import android.opengl.GLUtils;

import com.studio.dboard.view.dashboard.shape.BaseShape;
import com.studio.dboard.view.dashboard.shape.ShaderUtils;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;

/**
 * Created by peak on 2018/1/13.
 * 仪表盘1
 */
public class TexProgressForg extends BaseShape {

    protected int mProgram;
    protected int mTexSamplerHandle;
    protected int mTexCoordHandle;
    protected int mPosCoordHandle;
    protected int mMVPMatrixHandle;
    private int mCenterHandle;
    private int mScreenHandle;
    private int mAngleHandle;
    private int mSpeedHandle;
    private int mScaleHandle;


    protected FloatBuffer mTexVertices;
    protected FloatBuffer mPosVertices;

    private int[] textureIds;

    private float angle;

    public void setAngle(float angle) {
        this.angle = angle;
    }

    private float speed = 0;

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    private float[] uCenter;

    public void setuCenter(float[] uCenter) {
        this.uCenter = uCenter;
    }

    private float scale = 1.0f;

    public void setScale(float scale) {
        this.scale = scale;
    }

    public TexProgressForg(Resources res, int... resIds) {
        super(res, resIds);
    }

    protected void initProgram(){
        mProgram = ShaderUtils.createProgram(res,
                "shader/render1/ForgVert.glsl",
                "shader/render1/ForgFrag.glsl");

        mMVPMatrixHandle = GLES20.glGetUniformLocation(mProgram,"uMatrix");
        mTexSamplerHandle = GLES20.glGetUniformLocation(mProgram,"uTexture");
        mTexCoordHandle = GLES20.glGetAttribLocation(mProgram, "aCoordinate");
        mPosCoordHandle = GLES20.glGetAttribLocation(mProgram, "aPosition");
        mCenterHandle = GLES20.glGetUniformLocation(mProgram,"uCenter");
        mScreenHandle = GLES20.glGetUniformLocation(mProgram,"uScreen");
        mAngleHandle = GLES20.glGetUniformLocation(mProgram,"uAngle");
        mSpeedHandle = GLES20.glGetUniformLocation(mProgram,"uSpeed");
        mScaleHandle = GLES20.glGetUniformLocation(mProgram,"uScale");
    }

    public void setPositions(float[] positions) {
        //纹理坐标屏幕左上角为原点(左下，右下，左上，右上)
        float[] TEX_VERTICES = { 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f };
        mTexVertices = ByteBuffer.allocateDirect(TEX_VERTICES.length * 4)
                .order(ByteOrder.nativeOrder()).asFloatBuffer();
        mTexVertices.put(TEX_VERTICES).position(0);
        //顶点坐标(左下，右下，左上，右上)
        mPosVertices = ByteBuffer.allocateDirect(positions.length * 4)
                .order(ByteOrder.nativeOrder()).asFloatBuffer();
        mPosVertices.put(positions).position(0);
    }

    @Override
    public void draw(float[] mMVPMatrix) {
        GLES20.glUseProgram(mProgram);

        GLES20.glUniformMatrix4fv(mMVPMatrixHandle,1,false, mMVPMatrix,0);

        //激活绑定纹理单元
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureIds[0]);
        GLES20.glUniform1i(mTexSamplerHandle, 0);

        GLES20.glEnableVertexAttribArray(mPosCoordHandle);
        //传入顶点坐标
        GLES20.glVertexAttribPointer(mPosCoordHandle,2, GLES20.GL_FLOAT,false,0, mPosVertices);

        GLES20.glEnableVertexAttribArray(mTexCoordHandle);
        //传入纹理坐标
        GLES20.glVertexAttribPointer(mTexCoordHandle,2, GLES20.GL_FLOAT,false,0, mTexVertices);

        GLES20.glUniform2fv(mCenterHandle, 1, uCenter, 0);

        GLES20.glUniform2fv(mScreenHandle, 1, new float[]{screenWidth, screenHeight}, 0);

        GLES20.glUniform1f(mAngleHandle, angle);

        GLES20.glUniform1f(mSpeedHandle, speed);

        GLES20.glUniform1f(mScaleHandle, scale);

        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP,0,4);

        // Disable vertex array
        GLES20.glDisableVertexAttribArray(mPosCoordHandle);
        GLES20.glDisableVertexAttribArray(mTexCoordHandle);
    }

    @Override
    public void notifyDestroy() {
        GLES20.glDeleteProgram(mProgram);
    }

}
