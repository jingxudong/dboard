// ITimeListener.aidl
package com.studio.dboard;

// Declare any non-default types here with import statements

interface ITimeListener {
    /**
     * Demonstrates some basic types that you can use as parameters
     * and return values in AIDL.
     */
    void onTime(int hour, int min, int second);
}
