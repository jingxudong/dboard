// IWeatherListener.aidl
package com.studio.dboard;

// Declare any non-default types here with import statements

interface IWeatherListener {
    /**
     * Demonstrates some basic types that you can use as parameters
     * and return values in AIDL.
     */
    void onWeather(String local, String weatherState, String temperature);
}
