// INaviListener.aidl
package com.studio.dboard;

// Declare any non-default types here with import statements

interface INaviListener {
    /**
     * Demonstrates some basic types that you can use as parameters
     * and return values in AIDL.
     */
    void onNaviInfo(int iconIndex, int distance, String roadName);

    void onNaviStateChanged(int state);
}
