// ISpeedListener.aidl
package com.studio.dboard;

// Declare any non-default types here with import statements

interface ISpeedListener {
    /**
     * Demonstrates some basic types that you can use as parameters
     * and return values in AIDL.
     */
    void onSpeed(int speed);
}
