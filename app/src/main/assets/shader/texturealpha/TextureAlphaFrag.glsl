
precision mediump float;
uniform sampler2D uTexture;
uniform float uAlpha;
varying vec2 vCoordinate;

void main() {
    vec4 nColor = texture2D(uTexture, vCoordinate);
    vec4 vColor = vec4(nColor.r, nColor.g, nColor.b, nColor.a * uAlpha);
    gl_FragColor = vColor;
}
