
precision mediump float;
uniform sampler2D uTexture;
uniform vec4 uColor;
varying vec2 vCoordinate;

void main() {
    vec4 nColor = texture2D(uTexture, vCoordinate);
    vec4 vColor = vec4(uColor.r, uColor.g, uColor.b, nColor.a);
    gl_FragColor = vColor;
}
