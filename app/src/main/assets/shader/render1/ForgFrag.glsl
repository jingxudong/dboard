precision mediump float;
//起始角度
const float START_ANGLE = 230.0;
//结束角度
const float END_ANGLE = 140.0;
//速度级别
const float LEVEL_0 = 0.0;
const float LEVEL_1 = 30.0;
const float LEVEL_2 = 60.0;
const float LEVEL_3 = 80.0;
//圆半径
const float RADIU_OUT = 206.0;
const float RADIU_MIDDLE = 190.0;
const float RADIU_IN = 130.0;
//高亮起始色值:0x0f1a5f, 0x063915, 0x563901, 0x2b0f05
const vec4 START_COLOR_0 = vec4(0.059, 0.102, 0.373, 1.0);
const vec4 START_COLOR_1 = vec4(0.024, 0.224, 0.082, 1.0);
const vec4 START_COLOR_2 = vec4(0.337, 0.224, 0.004, 1.0);
const vec4 START_COLOR_3 = vec4(0.169, 0.059, 0.020, 1.0);
//高亮结束色值:0x81b3f9, 0x1ae054, 0xf8db1d, 0xfecb00
const vec4 END_COLOR_0 = vec4(0.506, 0.702, 0.918, 1.0);
const vec4 END_COLOR_1 = vec4(0.102, 0.878, 0.329, 1.0);
const vec4 END_COLOR_2 = vec4(0.973, 0.859, 0.118, 1.0);
const vec4 END_COLOR_3 = vec4(0.996, 0.796, 0.000, 1.0);



uniform sampler2D uTexture;//贴图
varying vec2 vCoordinate;
uniform vec2 uCenter;
uniform vec2 uScreen;
uniform float uSpeed;
uniform float uScale;
uniform float uAngle;


vec4 getColor(float edge0, float edge1, float x, vec4 color0, vec4 color1) {
    float t = (x - edge0)/(edge1 - edge0);
    vec4 tColor = color0 + (color1 - color0) * t;
    return tColor;
}

vec4 getStartColor(float speed){
    vec4 startColor;
    if(speed >= LEVEL_0 && speed < LEVEL_1){
        startColor = getColor(LEVEL_0, LEVEL_1, speed, START_COLOR_0, START_COLOR_1);
    }else if(speed >= LEVEL_1 && speed < LEVEL_2){
        startColor = getColor(LEVEL_1, LEVEL_2, speed, START_COLOR_1, START_COLOR_2);
    }else if(speed >= LEVEL_2 && speed < LEVEL_3){
        startColor = getColor(LEVEL_2, LEVEL_3, speed, START_COLOR_2, START_COLOR_3);
    }else{
        startColor = START_COLOR_3;
    }
    return startColor;
}

vec4 getEndColor(float speed){
    vec4 endColor;
    if(speed >= LEVEL_0 && speed < LEVEL_1){
        endColor = getColor(LEVEL_0, LEVEL_1, speed, END_COLOR_0, END_COLOR_1);
    }else if(speed >= LEVEL_1 && speed < LEVEL_2){
        endColor = getColor(LEVEL_1, LEVEL_2, speed, END_COLOR_1, END_COLOR_2);
    }else if(speed >= LEVEL_2 && speed < LEVEL_3){
        endColor = getColor(LEVEL_2, LEVEL_3, speed, END_COLOR_2, END_COLOR_3);
    }else{
        endColor = END_COLOR_3;
    }
    return endColor;
}


void main() {
    vec4 nColor = texture2D(uTexture, vCoordinate);

    float outr = 2.0 * RADIU_OUT * uScale / uScreen.y;
    float middler = 2.0 * RADIU_MIDDLE * uScale / uScreen.y;
    float inr = 2.0 * RADIU_IN * uScale / uScreen.y;

    //屏幕以y轴为准归一化，并将坐标原点移到屏幕中心
    vec2 uv = 2.0 * vec2(gl_FragCoord.xy - 0.5 * uScreen.xy) / uScreen.y;
    vec2 vCenter = 2.0 * vec2(uCenter.xy - 0.5 * uScreen.xy) / uScreen.y;
    //坐标原点移到左仪表盘中心
    float fragx = uv.x - vCenter.x;
    float fragy = uv.y - vCenter.y;
    float fragr = length(vec2(fragx, fragy));

//    vec3 vColor = getColor(inr, middler, fragr, getStartColor(uSpeed), getEndColor(uSpeed));
    vec4 vColor = getEndColor(uSpeed);

    if(uAngle >= START_ANGLE && uAngle < 360.0){
        if(fragx < 0.0){
            if(uAngle + 1.0 >= 360.0){
                if (fragr <= middler){
                    gl_FragColor = vec4(vColor.rgb, nColor.a);
                }else{
                    gl_FragColor = vec4(1.0, 1.0, 1.0, nColor.a);
                }
            }else{
                float edgeSlope = 1.0 / tan(radians(uAngle));
                float result = edgeSlope * fragx;
                if(fragy - result < 0.0){
                    if (fragr <= middler){
                        gl_FragColor = vec4(vColor.rgb, nColor.a);
                    }else{
                        gl_FragColor = vec4(1.0, 1.0, 1.0, nColor.a);
                    }
                }else{
                    gl_FragColor = vec4(vColor.rgb, 0.0);
                }
            }
        }else{
            gl_FragColor = vec4(vColor.rgb, 0.0);
        }
    }else if(uAngle == 0.0 || uAngle == 360.0){
        if(fragx <= 0.0){
            if (fragr <= middler){
                gl_FragColor = vec4(vColor.rgb, nColor.a);
            }else{
                gl_FragColor = vec4(1.0, 1.0, 1.0, nColor.a);
            }
        }else{
            gl_FragColor = vec4(vColor.rgb, 0.0);
        }
    }else if(uAngle > 0.0 && uAngle < 130.0){
        if(fragx < 0.0){
            if (fragr <= middler){
                gl_FragColor = vec4(vColor.rgb, nColor.a);
            }else{
                gl_FragColor = vec4(1.0, 1.0, 1.0, nColor.a);
            }
        }else{
            float edgeSlope = 1.0 / tan(radians(uAngle));
            float result = edgeSlope * fragx;
            if(fragy - result > 0.0){
                if (fragr <= middler){
                    gl_FragColor = vec4(vColor.rgb, nColor.a);
                }else{
                    gl_FragColor = vec4(1.0, 1.0, 1.0, nColor.a);
                }
            }else{
                gl_FragColor = vec4(vColor.rgb, 0.0);
            }
        }
    }else{
        gl_FragColor = nColor;
    }
}