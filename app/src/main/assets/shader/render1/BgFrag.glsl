
precision mediump float;
//速度级别
const float LEVEL_0 = 0.0;
const float LEVEL_1 = 30.0;
const float LEVEL_2 = 60.0;
const float LEVEL_3 = 80.0;

//底色色值:0x264249d9, 0x26178c4e, 0x26a48815, 0x26df0000
const vec4 COLOR_0 = vec4(0.259, 0.286, 0.851, 1.0);
const vec4 COLOR_1 = vec4(0.090, 0.549, 0.306, 1.0);
const vec4 COLOR_2 = vec4(0.643, 0.533, 0.082, 1.0);
const vec4 COLOR_3 = vec4(0.875, 0.000, 0.000, 1.0);


uniform sampler2D uTexture;//贴图
varying vec2 vCoordinate;
uniform float uSpeed;


vec4 getColor(float edge0, float edge1, float x, vec4 color0, vec4 color1) {
    float t = (x - edge0)/(edge1 - edge0);
    vec4 tColor = color0 + (color1 - color0) * t;
    return tColor;
}


void main() {
    vec4 nColor = texture2D(uTexture, vCoordinate);

    vec4 vColor = COLOR_0;
    if(uSpeed >= LEVEL_0 && uSpeed < LEVEL_1){
        vColor = getColor(LEVEL_0, LEVEL_1, uSpeed, COLOR_0, COLOR_1);
    }else if(uSpeed >= LEVEL_1 && uSpeed < LEVEL_2){
        vColor = getColor(LEVEL_1, LEVEL_2, uSpeed, COLOR_1, COLOR_2);
    }else if(uSpeed >= LEVEL_2 && uSpeed < LEVEL_3){
        vColor = getColor(LEVEL_2, LEVEL_3, uSpeed, COLOR_2, COLOR_3);
    }else if(uSpeed >= LEVEL_3){
        vColor = COLOR_3;
    }

    gl_FragColor = vec4(vColor.rgb, nColor.a);
}
