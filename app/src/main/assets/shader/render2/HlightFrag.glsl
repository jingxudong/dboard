precision mediump float;
//起始角度
const float START_ANGLE = 180.0;
//结束角度
const float END_ANGLE = 468.0;
//速度级别
const float LEVEL_0 = 0.0;
const float LEVEL_1 = 30.0;
const float LEVEL_2 = 60.0;
const float LEVEL_3 = 80.0;
//默认纯色 色值:0xff0a0a0a 透明度：100%
const vec3 PURE_COLOR = vec3(0.039, 0.039, 0.039);
//随速度变化的颜色:0x438eff, 0x3bb165, 0xfcbe01, 0xff3535
const vec3 COLOR_0 = vec3(0.263, 0.557, 1.000);
const vec3 COLOR_1 = vec3(0.231, 0.694, 0.396);
const vec3 COLOR_2 = vec3(0.988, 0.745, 0.004);
const vec3 COLOR_3 = vec3(1.000, 0.208, 0.208);


uniform sampler2D uTexture;//贴图
varying vec2 vCoordinate;
uniform vec2 uCenter;
uniform vec2 uScreen;
uniform float uAngle;
uniform float uSpeed;

float getTotalRadian(float angle){
    float delta = angle - START_ANGLE;
    if(delta <= 180.0){
        return radians(delta);
    }else{
        return radians(180.0) + radians(delta - 180.0);
    }
}

float getFactor(float x, float y, float angle) {
    float radian = atan(y, x);
    if(x < 0.0 && y < 0.0){//第三象限
        return (abs(radian) - radians(90.0)) / getTotalRadian(angle);
    }else if(x < 0.0 && y == 0.0){//负x轴
        return radians(90.0) / getTotalRadian(angle);
    }else if(y > 0.0){//第四象限
        return ((radians(180.0) - radian) + radians(90.0)) / getTotalRadian(angle);
    }else if(x > 0.0 && y == 0.0){//正x轴
        return (radians(180.0) + radians(90.0)) / getTotalRadian(angle);
    }else if(x > 0.0 && y < 0.0){
        return (radians(180.0) + radians(90.0) + abs(radian)) / getTotalRadian(angle);
    }
    return 0.0;
}



void main() {
    vec4 nColor = texture2D(uTexture, vCoordinate);

    //屏幕以y轴为准归一化，并将坐标原点移到屏幕中心
    vec2 uv = 2.0 * vec2(gl_FragCoord.xy - 0.5 * uScreen.xy) / uScreen.y;
    vec2 vCenter = 2.0 * vec2(uCenter.xy - 0.5 * uScreen.xy) / uScreen.y;
    //坐标原点移到时钟表盘中心
    float fragx = uv.x - vCenter.x;
    float fragy = uv.y - vCenter.y;

    if(uAngle == START_ANGLE){
        gl_FragColor = vec4(0.0, 0.0, 0.0, 0.0);
    }else if(uAngle > START_ANGLE && uAngle < 359.5){
        if(fragx <= 0.0){
            float edgeSlope = 1.0 / tan(radians(uAngle));
            float result = edgeSlope * fragx;
            if(fragy - result <= 0.0){
                float factor = getFactor(fragx, fragy, uAngle);
                gl_FragColor = vec4(nColor.rgb, nColor.a * factor);
            }else{
                gl_FragColor = vec4(0.0, 0.0, 0.0, 0.0);
            }
        }else{
            gl_FragColor = vec4(0.0, 0.0, 0.0, 0.0);
        }
    }
    else if(uAngle >= 359.5 && uAngle <= 360.5){
        if (fragx <= 0.0){
            float factor = getFactor(fragx, fragy, uAngle);
            gl_FragColor = vec4(nColor.rgb, nColor.a * factor);
        }
        else {
            gl_FragColor = vec4(0.0, 0.0, 0.0, 0.0);
        }
    }
    else if(uAngle > 360.5 && uAngle <= END_ANGLE){
        if(fragx > 0.0){
            float edgeSlope = 1.0 / tan(radians(uAngle));
            float result = edgeSlope * fragx;
            if(fragy - result >= 0.0){
                float factor = getFactor(fragx, fragy, uAngle);
                gl_FragColor = vec4(nColor.rgb, nColor.a * factor);
            }else{
                gl_FragColor = vec4(0.0, 0.0, 0.0, 0.0);
            }
        }else{
            float factor = getFactor(fragx, fragy, uAngle);
            gl_FragColor = vec4(nColor.rgb, nColor.a * factor);
        }
    }else{
        gl_FragColor = vec4(0.0, 0.0, 0.0, 0.0);
    }
}