precision mediump float;
//起始角度
const float START_ANGLE = 108.0;
//结束角度
const float END_ANGLE = 180.0;

uniform sampler2D uTexture;//贴图
varying vec2 vCoordinate;
uniform vec2 uCenter;
uniform vec2 uScreen;
uniform float uScale;


void main() {
    vec4 nColor = texture2D(uTexture, vCoordinate);

    //屏幕以y轴为准归一化，并将坐标原点移到屏幕中心
    vec2 uv = 2.0 * vec2(gl_FragCoord.xy - 0.5 * uScreen.xy) / uScreen.y;
    vec2 vCenter = 2.0 * vec2(uCenter.xy - 0.5 * uScreen.xy) / uScreen.y;
    //坐标原点移到左仪表盘中心
    float fragx = uv.x - vCenter.x;
    float fragy = uv.y - vCenter.y;

    if(fragx > 0.0){
        float edgeSlope1 = 1.0 / tan(radians(END_ANGLE - 0.1));
        float result1 = edgeSlope1 * fragx;
        float edgeSlope2 = 1.0 / tan(radians(START_ANGLE + 5.0));
        float result2 = edgeSlope2 * fragx;

        if(fragy < result2 && fragy > result1){
            gl_FragColor = vec4(0.0, 0.0, 0.0, 0.0);
        }else{
            gl_FragColor = nColor;
        }
    }else{
        gl_FragColor = nColor;
    }
}