precision mediump float;
//起始角度
const float START_ANGLE = 230.0;
//结束角度
const float END_ANGLE = 130.0;
//速度级别
const float LEVEL_0 = 0.0;
const float LEVEL_1 = 30.0;
const float LEVEL_2 = 60.0;
const float LEVEL_3 = 80.0;
//半径
const float RADIU_OUT = 214.0;
const float RADIU_IN = 175.0;
//纯色 色值:0xffffff 透明度：15%
const vec4 PURE_COLOR = vec4(1.0, 1.0, 1.0, 0.15);


uniform sampler2D uTexture;//贴图
varying vec2 vCoordinate;
uniform vec2 uCenter;
uniform vec2 uScreen;
uniform float uScale;
uniform float uAngle;


void main() {
    vec4 nColor = texture2D(uTexture, vCoordinate);

    float radiu_out = 2.0 * RADIU_OUT * uScale / uScreen.y;
    float radiu_in = 2.0 * RADIU_IN * uScale / uScreen.y;

    //屏幕以y轴为准归一化，并将坐标原点移到屏幕中心
    vec2 uv = 2.0 * vec2(gl_FragCoord.xy - 0.5 * uScreen.xy) / uScreen.y;
    vec2 vCenter = 2.0 * vec2(uCenter.xy - 0.5 * uScreen.xy) / uScreen.y;
    //坐标原点移到左仪表盘中心
    float fragx = uv.x - vCenter.x;
    float fragy = uv.y - vCenter.y;
    float fragr = length(vec2(fragx, fragy));

    if(uAngle == START_ANGLE){
        float edgeSlope1 = 1.0 / tan(radians(END_ANGLE + 2.0));
        float result1 = edgeSlope1 * fragx;
        float edgeSlope2 = 1.0 / tan(radians(START_ANGLE - 2.0));
        float result2 = edgeSlope2 * fragx;
        if(fragy - result1 < 0.0 && fragy - result2 < 0.0){
            gl_FragColor = nColor;
        }else{
            if(fragr >= radiu_in && fragr < radiu_out){
                gl_FragColor = vec4(PURE_COLOR.rgb, nColor.a * PURE_COLOR.a);
            }else{
                gl_FragColor = nColor;
            }
        }
    }else if(uAngle == END_ANGLE){
        gl_FragColor = nColor;
    }else if(uAngle > START_ANGLE && uAngle < 360.0){
        if(fragx <= 0.0){
            if(uAngle + 1.0 >= 360.0){
                gl_FragColor = nColor;
            }else{
                float edgeSlope = 1.0 / tan(radians(uAngle));
                float result = edgeSlope * fragx;
                if(fragy - result <= 0.0){
                    gl_FragColor = nColor;
                }else{
                    if(fragr >= radiu_in && fragr < radiu_out){
                        gl_FragColor = vec4(PURE_COLOR.rgb, nColor.a * PURE_COLOR.a);
                    }else{
                        gl_FragColor = nColor;
                    }
                }
            }
        }else{
            float edgeSlope = 1.0 / tan(radians(END_ANGLE + 2.0));
            float result = edgeSlope * fragx;
            if(fragy - result > 0.0){
                if(fragr >= radiu_in && fragr < radiu_out){
                    gl_FragColor = vec4(PURE_COLOR.rgb, nColor.a * PURE_COLOR.a);
                }else{
                    gl_FragColor = nColor;
                }
            }else{
                gl_FragColor = nColor;
            }
        }
    }else if(uAngle == 0.0 || uAngle == 360.0){
        if(fragx <= 0.0){
            gl_FragColor = nColor;
        }else{
            float edgeSlope = 1.0 / tan(radians(END_ANGLE + 2.0));
            float result = edgeSlope * fragx;
            if(fragy - result > 0.0){
                if(fragr >= radiu_in && fragr < radiu_out){
                    gl_FragColor = vec4(PURE_COLOR.rgb, nColor.a * PURE_COLOR.a);
                }else{
                    gl_FragColor = nColor;
                }
            }else{
                gl_FragColor = nColor;
            }
        }
    }else if(uAngle > 0.0 && uAngle < END_ANGLE){
        if(fragx <= 0.0){
            gl_FragColor = nColor;
        }else{
            float edgeSlope = 1.0 / tan(radians(uAngle));
            float result = edgeSlope * fragx;
            if(fragy - result >= 0.0){
                gl_FragColor = nColor;
            }else{
                float edgeSlope = 1.0 / tan(radians(END_ANGLE + 2.0));
                float result = edgeSlope * fragx;
                if(fragy - result > 0.0){
                    if(fragr >= radiu_in && fragr < radiu_out){
                        gl_FragColor = vec4(PURE_COLOR.rgb, nColor.a * PURE_COLOR.a);
                    }else{
                        gl_FragColor = nColor;
                    }
                }else{
                    gl_FragColor = nColor;
                }
            }
        }
    }else{
        float edgeSlope1 = 1.0 / tan(radians(END_ANGLE + 2.0));
        float result1 = edgeSlope1 * fragx;
        float edgeSlope2 = 1.0 / tan(radians(START_ANGLE - 2.0));
        float result2 = edgeSlope2 * fragx;
        if(fragy - result1 < 0.0 && fragy - result2 < 0.0){
            gl_FragColor = nColor;
        }else{
            if(fragr >= radiu_in && fragr < radiu_out){
                gl_FragColor = vec4(PURE_COLOR.rgb, nColor.a * PURE_COLOR.a);
            }else{
                gl_FragColor = nColor;
            }
        }
    }
}