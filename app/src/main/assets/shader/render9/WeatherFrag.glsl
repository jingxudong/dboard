
precision mediump float;

const float RADIU = 206.0;
const float STROKE_WIDTH = 12.0;
uniform sampler2D uTexture;//贴图
varying vec2 vCoordinate;
uniform vec2 uCenter;
uniform vec2 uScreen;
uniform float uScale;


void main() {

    //屏幕以y轴为准归一化，并将坐标原点移到屏幕中心
    vec2 uv = 2.0 * vec2(gl_FragCoord.xy - 0.5 * uScreen.xy) / uScreen.y;
    vec2 vCenter = 2.0 * vec2(uCenter.xy - 0.5 * uScreen.xy) / uScreen.y;

    float radiu = 2.0 * (RADIU + STROKE_WIDTH) * uScale / uScreen.y;

    //坐标原点移到时钟表盘中心
    float fragx = uv.x - vCenter.x;
    float fragy = uv.y - vCenter.y;
    float fragr = length(vec2(fragx, fragy));


    if(fragr <= radiu){
        vec4 color = vec4(0.0);


        float kernel[25];

        //9阶高斯滤波核
        // 1  1  1  1  1  1  1
        // 1  2  2  2  2  2  1
        // 1  2  4  4  4  2  1
        // 1  2  4  8  4  2  1
        // 1  2  4  4  4  2  1
        // 1  2  2  2  2  2  1
        // 1  1  1  1  1  1  1
//        kernel[72]=1.0;kernel[73]=1.0;kernel[74]=1.0;kernel[75]=1.0;kernel[76]=1.0; kernel[77]=1.0;kernel[78]=1.0;kernel[79]=1.0;kernel[80]=1.0;
//        kernel[63]=1.0;kernel[64]=2.0;kernel[65]=2.0;kernel[66]=2.0;kernel[67]=2.0; kernel[68]=2.0;kernel[69]=2.0;kernel[70]=2.0;kernel[71]=1.0;
//        kernel[54]=1.0;kernel[55]=2.0;kernel[56]=4.0;kernel[57]=4.0;kernel[58]=4.0; kernel[59]=4.0;kernel[60]=4.0;kernel[61]=2.0;kernel[62]=1.0;
//        kernel[45]=1.0;kernel[46]=2.0;kernel[47]=4.0;kernel[48]=8.0;kernel[49]=8.0; kernel[50]=8.0;kernel[51]=4.0;kernel[52]=2.0;kernel[53]=1.0;
//        kernel[36]=1.0;kernel[37]=2.0;kernel[38]=4.0;kernel[39]=8.0;kernel[40]=16.0;kernel[41]=8.0;kernel[42]=4.0;kernel[43]=2.0;kernel[44]=1.0;
//        kernel[27]=1.0;kernel[28]=2.0;kernel[29]=4.0;kernel[30]=8.0;kernel[31]=8.0; kernel[32]=8.0;kernel[33]=4.0;kernel[34]=2.0;kernel[35]=1.0;
//        kernel[18]=1.0;kernel[19]=2.0;kernel[20]=4.0;kernel[21]=4.0;kernel[22]=4.0; kernel[23]=4.0;kernel[24]=4.0;kernel[25]=2.0;kernel[26]=1.0;
//        kernel[9]=1.0; kernel[10]=2.0;kernel[11]=2.0;kernel[12]=2.0;kernel[13]=2.0; kernel[14]=2.0;kernel[15]=2.0;kernel[16]=2.0;kernel[17]=1.0;
//        kernel[0]=1.0; kernel[1]=1.0; kernel[2]=1.0; kernel[3]=1.0; kernel[4]=1.0;  kernel[5]=1.0; kernel[6]=1.0; kernel[7]=1.0; kernel[8]=1.0;

        //7阶高斯滤波核
        // 1  1  1  1  1  1  1
        // 1  2  2  2  2  2  1
        // 1  2  4  4  4  2  1
        // 1  2  4  8  4  2  1
        // 1  2  4  4  4  2  1
        // 1  2  2  2  2  2  1
        // 1  1  1  1  1  1  1
//        kernel[42]=1.0;kernel[43]=1.0;kernel[44]=1.0;kernel[45]=1.0;kernel[46]=1.0;kernel[47]=1.0;kernel[48]=1.0;
//        kernel[35]=1.0;kernel[36]=2.0;kernel[37]=2.0;kernel[38]=2.0;kernel[39]=2.0;kernel[40]=2.0;kernel[41]=1.0;
//        kernel[28]=1.0;kernel[29]=2.0;kernel[30]=4.0;kernel[31]=4.0;kernel[32]=4.0;kernel[33]=2.0;kernel[34]=1.0;
//        kernel[21]=1.0;kernel[22]=2.0;kernel[23]=4.0;kernel[24]=8.0;kernel[25]=4.0;kernel[26]=2.0;kernel[27]=1.0;
//        kernel[14]=1.0;kernel[15]=2.0;kernel[16]=4.0;kernel[17]=4.0;kernel[18]=4.0;kernel[19]=2.0;kernel[20]=1.0;
//        kernel[7]=1.0; kernel[8]=2.0; kernel[9]=2.0; kernel[10]=2.0;kernel[11]=2.0;kernel[12]=2.0;kernel[13]=1.0;
//        kernel[0]=1.0; kernel[1]=1.0; kernel[2]=1.0; kernel[3]=1.0; kernel[4]=1.0; kernel[5]=1.0; kernel[6]=1.0;

        //5阶高斯滤波核
        // 1  1  1  1  1
        // 1  2  2  2  1
        // 1  2  4  2  1
        // 1  2  2  2  1
        // 1  1  1  1  1
        kernel[20]=1.0;kernel[21]=1.0;kernel[22]=1.0; kernel[23]=1.0;kernel[24]=1.0;
        kernel[15]=1.0;kernel[16]=2.0;kernel[17]=2.0; kernel[18]=2.0;kernel[19]=1.0;
        kernel[10]=1.0;kernel[11]=2.0;kernel[12]=4.0;kernel[13]=2.0;kernel[14]=1.0;
        kernel[5]=1.0; kernel[6]=2.0; kernel[7]=2.0;  kernel[8]=2.0; kernel[9]=1.0;
        kernel[0]=1.0; kernel[1]=1.0; kernel[2]=1.0;  kernel[3]=1.0; kernel[4]=1.0;

        //3阶高斯滤波核
        // 1 1 1
        // 1 2 1
        // 1 1 1
//        kernel[6]=1.0;kernel[7]=1.0;kernel[8]=1.0;
//        kernel[3]=1.0;kernel[4]=2.0;kernel[5]=1.0;
//        kernel[0]=1.0;kernel[1]=1.0;kernel[2]=1.0;

        //移动高斯核,对原图做卷积计算
        int coreSize = 5;

        float texelOffset = 0.0015;
        int index = 0;
        for(int y = 0; y < coreSize; y++){
            for(int x = 0; x < coreSize; x++){
                //原图像素点
                int halfOffsetSize = -(coreSize / 2);
                int vIndexX = halfOffsetSize + x;
                int vIndexY = halfOffsetSize + y;
                float u = float(vIndexX) * texelOffset;
                float v = float(vIndexY) * texelOffset;
                vec2 vCoord = vCoordinate + vec2(u, v);
                vec4 currentColor = texture2D(uTexture, vCoord);
                //卷积计算
                color += currentColor * kernel[index++];
            }
        }

        //根据邻域内像素的加权平均灰度值去替代模板中心像素点的值
//        gl_FragColor = color / 224.0;
        gl_FragColor = color / 36.0;
    }else{
        gl_FragColor = texture2D(uTexture, vCoordinate);
    }
}