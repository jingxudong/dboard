precision mediump float;
//起始角度
const float START_ANGLE = 180.0;
//结束角度
const float END_ANGLE = 150.0;
//速度级别
const float LEVEL_0 = 0.0;
const float LEVEL_1 = 30.0;
const float LEVEL_2 = 60.0;
const float LEVEL_3 = 80.0;
//默认纯色 色值:0xff0a0a0a 透明度：100%
const vec3 PURE_COLOR = vec3(0.039, 0.039, 0.039);
//随速度变化的颜色:0x438eff, 0x3bb165, 0xfcbe01, 0xff3535
const vec3 COLOR_0 = vec3(0.263, 0.557, 1.000);
const vec3 COLOR_1 = vec3(0.231, 0.694, 0.396);
const vec3 COLOR_2 = vec3(0.988, 0.745, 0.004);
const vec3 COLOR_3 = vec3(1.000, 0.208, 0.208);

const float RADIU = 190.0;

uniform sampler2D uTexture;//贴图
varying vec2 vCoordinate;
uniform vec2 uCenter;
uniform vec2 uScreen;
uniform float uAngle;
uniform float uSpeed;
uniform float uScale;


vec3 getColor(float edge0, float edge1, float x, vec3 color0, vec3 color1) {
    float t = (x - edge0)/(edge1 - edge0);
    vec3 tColor = color0 + (color1 - color0) * t;
    return tColor;
}

void main() {
    vec4 nColor = texture2D(uTexture, vCoordinate);
    vec3 vColor;
    if(uSpeed >= LEVEL_0 && uSpeed < LEVEL_1){
        vColor = getColor(LEVEL_0, LEVEL_1, uSpeed, COLOR_0, COLOR_1);
    }else if(uSpeed >= LEVEL_1 && uSpeed < LEVEL_2){
        vColor = getColor(LEVEL_1, LEVEL_2, uSpeed, COLOR_1, COLOR_2);
    }else if(uSpeed >= LEVEL_2 && uSpeed < LEVEL_3){
        vColor = getColor(LEVEL_2, LEVEL_3, uSpeed, COLOR_2, COLOR_3);
    }else if(uSpeed >= LEVEL_3){
        vColor = COLOR_3;
    }

    //屏幕以y轴为准归一化，并将坐标原点移到屏幕中心
    vec2 uv = 2.0 * vec2(gl_FragCoord.xy - 0.5 * uScreen.xy) / uScreen.y;
    vec2 vCenter = 2.0 * vec2(uCenter.xy - 0.5 * uScreen.xy) / uScreen.y;
    float radiu = 2.0 * RADIU * uScale / uScreen.y;
    //坐标原点移到时钟表盘中心
    float fragx = uv.x - vCenter.x;
    float fragy = uv.y - vCenter.y;
    float fragr = length(vec2(fragx, fragy));

    if(fragr > radiu){
        if(uAngle >= START_ANGLE && uAngle <= (START_ANGLE + 0.5)){
            gl_FragColor = nColor;
        }else if(uAngle > (START_ANGLE + 0.5) && uAngle < 360.0){
            if(fragx <= 0.0){
                float edgeSlope = 1.0 / tan(radians(uAngle));
                float result = edgeSlope * fragx;
                if(fragy - result <= 0.0){
                    gl_FragColor = vec4(vColor, nColor.a);
                }else{
                    //除边缘锯齿
                    float dis = abs((edgeSlope * fragx - fragy) / length(vec2(fragx, fragy)));
                    float a = smoothstep(0.0, 0.05, dis);
                    vec3 dColor = mix(vColor, nColor.rgb, a);
                    gl_FragColor = vec4(dColor, nColor.a);
                }
            }else{
                gl_FragColor = nColor;
            }
        }else if(uAngle == 0.0 || uAngle == 360.0){
            if(fragx <= 0.0){
                gl_FragColor = vec4(vColor, nColor.a);
            }else{
                gl_FragColor = nColor;
            }
        }else if(uAngle > 0.0 && uAngle <= END_ANGLE){
            if(fragx <= 0.0){
                gl_FragColor = vec4(vColor, nColor.a);
            }else{
                float edgeSlope = 1.0 / tan(radians(uAngle));
                float result = edgeSlope * fragx;
                if(fragy - result <= 0.0){
                    //除边缘锯齿
                    float dis = abs((edgeSlope * fragx - fragy) / length(vec2(fragx, fragy)));
                    float a = smoothstep(0.0, 0.05, dis);
                    vec3 dColor = mix(vColor, nColor.rgb, a);
                    gl_FragColor = vec4(dColor, nColor.a);
                }else{
                    gl_FragColor = vec4(vColor, nColor.a);
                }
            }
        }else{
            gl_FragColor = nColor;
        }
    }else{
        gl_FragColor = nColor;
    }
}